package fr.ird.observe.services.topia.service.data;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.configuration.ObserveDataSourceConnection;
import fr.ird.observe.services.configuration.topia.ObserveDataSourceConfigurationTopiaH2;
import fr.ird.observe.services.dto.reference.DataReferenceList;
import fr.ird.observe.services.dto.source.DataSourceCreateConfigurationDto;
import fr.ird.observe.services.dto.source.DataSourceCreateWithNoReferentialImportException;
import fr.ird.observe.services.dto.source.IncompatibleDataSourceCreateConfigurationException;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import fr.ird.observe.services.service.BabModelVersionException;
import fr.ird.observe.services.service.DataSourceService;
import fr.ird.observe.services.service.DatabaseConnexionNotAuthorizedException;
import fr.ird.observe.services.service.DatabaseNotFoundException;
import fr.ird.observe.services.service.data.DeleteTripRequest;
import fr.ird.observe.services.service.data.DeleteTripResult;
import fr.ird.observe.services.service.data.ExportTripRequest;
import fr.ird.observe.services.service.data.ExportTripResult;
import fr.ird.observe.services.service.data.ImportTripRequest;
import fr.ird.observe.services.service.data.TripManagementService;
import fr.ird.observe.services.service.data.longline.TripLonglineService;
import fr.ird.observe.services.service.data.seine.TripSeineService;
import fr.ird.observe.services.topia.ObserveDataSourceConnectionTopiaTaiste;
import fr.ird.observe.services.topia.service.AbstractServiceTopiaTest;
import fr.ird.observe.services.topia.service.ReferentialServiceTopiaTest;
import fr.ird.observe.test.DatabaseName;
import fr.ird.observe.test.ObserveFixtures;
import fr.ird.observe.test.spi.CopyDatabaseConfiguration;
import fr.ird.observe.test.spi.DatabaseNameConfiguration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

/**
 * Created on 08/09/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@DatabaseNameConfiguration(DatabaseName.dataForTestSeine)
public class TripManagementServiceTopiaTest extends AbstractServiceTopiaTest {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ReferentialServiceTopiaTest.class);

    private TripManagementService tripManagementService;
    private TripSeineService tripSeineService;

    @Before
    public void setUp() throws Exception {

        tripManagementService = topiaTestMethodResource.newService(TripManagementService.class);
        tripSeineService = topiaTestMethodResource.newService(TripSeineService.class);

    }

    @Test
    public void exportTrip() {

        ExportTripRequest request = new ExportTripRequest(false, ObserveFixtures.PROGRAM_ID, ObserveFixtures.TRIP_SEINE_ID_1);
        ExportTripResult result = tripManagementService.exportTrip(request);
        Assert.assertNotNull(result);

    }

    @CopyDatabaseConfiguration
    @Test
    public void deleteTrip() {

        DataReferenceList<TripSeineDto> allTripSeineBefore = tripSeineService.getAllTripSeine();

        DeleteTripRequest request = new DeleteTripRequest(ObserveFixtures.PROGRAM_ID, ObserveFixtures.TRIP_SEINE_ID_1);
        DeleteTripResult result = tripManagementService.deleteTrip(request);
        Assert.assertNotNull(result);

        DataReferenceList<TripSeineDto> allTripSeineAfter = tripSeineService.getAllTripSeine();
        Assert.assertTrue(allTripSeineBefore.sizeReference() == allTripSeineAfter.sizeReference() + 1);
    }

    @Test
    public void importTripSeine() throws DatabaseConnexionNotAuthorizedException, DataSourceCreateWithNoReferentialImportException, BabModelVersionException, IncompatibleDataSourceCreateConfigurationException, DatabaseNotFoundException {

        ExportTripRequest request = new ExportTripRequest(false, ObserveFixtures.PROGRAM_ID, ObserveFixtures.TRIP_SEINE_ID_1);
        importTrip(request, true);

    }

    // FIXME Le test met 6 minutes, comprendre pourquoi l'import est aussi long (environ 18000 branchlines)
    @Ignore
    @DatabaseNameConfiguration(DatabaseName.dataForTestLongline)
    @Test
    public void importTripLongline() throws DatabaseConnexionNotAuthorizedException, DataSourceCreateWithNoReferentialImportException, BabModelVersionException, IncompatibleDataSourceCreateConfigurationException, DatabaseNotFoundException {

        ExportTripRequest request = new ExportTripRequest(false, ObserveFixtures.PROGRAM_ID, ObserveFixtures.TRIP_LONGLINE_ID_1);
        importTrip(request, false);

    }

    private void importTrip(ExportTripRequest request, boolean forSeine) throws DatabaseConnexionNotAuthorizedException, DataSourceCreateWithNoReferentialImportException, BabModelVersionException, IncompatibleDataSourceCreateConfigurationException, DatabaseNotFoundException {

        ExportTripResult result = tripManagementService.exportTrip(request);

        ObserveDataSourceConfigurationTopiaH2 dataSourceConfiguration = topiaTestMethodResource.createDataSourceConfigurationH2(getClass(), "importTripTarget");

        try (DataSourceService dataSourceService = TOPIA_TEST_CLASS_RESOURCE.newService(dataSourceConfiguration, DataSourceService.class)) {

            DataSourceCreateConfigurationDto dataSourceCreateConfiguration = new DataSourceCreateConfigurationDto();
            dataSourceCreateConfiguration.setImportReferentialDataSourceConfiguration(topiaTestMethodResource.getDataSourceConfiguration());

            ObserveDataSourceConnection dataSourceConnection = dataSourceService.create(dataSourceConfiguration, dataSourceCreateConfiguration);
            dataSourceConnection = new ObserveDataSourceConnectionTopiaTaiste(dataSourceConnection.getAuthenticationToken());

            TripManagementService tripManagementService = TOPIA_TEST_CLASS_RESOURCE.newService(dataSourceConnection, TripManagementService.class);
            TripSeineService tripSeineService = TOPIA_TEST_CLASS_RESOURCE.newService(dataSourceConnection, TripSeineService.class);
            TripLonglineService tripLonglineService = TOPIA_TEST_CLASS_RESOURCE.newService(dataSourceConnection, TripLonglineService.class);

            int allTripSeineBefore = forSeine ? tripSeineService.getAllTripSeine().sizeReference() :
                    tripLonglineService.getAllTripLongline().sizeReference();

            tripManagementService.importTrip(new ImportTripRequest(result));

            int allTripSeineAfter = forSeine ? tripSeineService.getAllTripSeine().sizeReference() :
                    tripLonglineService.getAllTripLongline().sizeReference();
            Assert.assertTrue(allTripSeineAfter == allTripSeineBefore + 1);

        }

    }

}
