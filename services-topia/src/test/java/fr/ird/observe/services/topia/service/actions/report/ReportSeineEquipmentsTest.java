/*
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.services.topia.service.actions.report;


import fr.ird.observe.services.service.actions.report.DataMatrix;
import fr.ird.observe.services.service.actions.report.Report;
import fr.ird.observe.services.service.actions.report.ReportRequest;
import org.junit.Assert;

/**
 * Test du report {@code discardedAccessoire}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.9
 */
public class ReportSeineEquipmentsTest extends AbstractReportServiceTopiaTest {

    @Override
    protected void testReportSyntax(Report report) {
        Assert.assertNotNull(report);
        assertReportName(
                report,
                "Liste des équipements",
                "Afficher les équipements"
        );

        assertReportDimension(
                report,
                -1,
                4,
                new String[]{"Equipement",
                        "Nombre",
                        "Utilisé dans la marée",
                        "Mesures",
                }
        );

        assertReportNbRequests(report, 1);

        ReportRequest[] requests = report.getRequests();

        assertReportRequestDimension(
                requests[0],
                ReportRequest.RequestLayout.row,
                0,
                0
        );
    }

    @Override
    protected String getReportId() {
        return "tripSeineGearUseFeatures";
    }

    @Override
    protected void testReportResult(DataMatrix result) {

        assertResultDimension(result, 4, 2, 0, 0);

        assertResultRow(result, 0, "Aucun code - Radar de route", "1", "Oui", "(  Aucun code - Portée [ m ] = 50 )");
        assertResultRow(result, 1, "Aucun code - Radeau", "1", "Oui", "(  Aucun code - Hauteur [ m ] = 3 ), (  Aucun code - Non maillant (oui/non)  pourquoi pas : maillant oui/non ? = Oui ), (  Aucun code - Matériau écologique (oui/non) = Non )");

    }

}
