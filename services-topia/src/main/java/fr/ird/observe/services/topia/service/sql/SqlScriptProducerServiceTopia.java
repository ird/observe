package fr.ird.observe.services.topia.service.sql;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ListMultimap;
import fr.ird.observe.persistence.ObserveEntityEnum;
import fr.ird.observe.persistence.ObserveTopiaApplicationContext;
import fr.ird.observe.persistence.Entities;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.service.sql.AddSqlScriptProducerRequest;
import fr.ird.observe.services.service.sql.AddSqlScriptProducerResult;
import fr.ird.observe.services.service.sql.DeleteSqlScriptProducerRequest;
import fr.ird.observe.services.dto.ObserveBlobsContainer;
import fr.ird.observe.services.service.sql.SqlScriptProducerService;
import fr.ird.observe.services.topia.ObserveServiceTopia;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.metadata.TopiaMetadataModel;
import org.nuiton.topia.service.sql.batch.SqlRequests;
import org.nuiton.topia.service.sql.batch.SqlResult;
import org.nuiton.topia.service.sql.batch.TopiaSqlBatchService;
import org.nuiton.topia.service.sql.batch.actions.BlobsContainer;
import org.nuiton.topia.service.sql.batch.actions.TopiaSqlTableSelectArgument;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.zip.GZIPOutputStream;

import static fr.ird.observe.persistence.Entities.IS_LONGLINE_ID;
import static fr.ird.observe.persistence.Entities.IS_SEINE_ID;
import static fr.ird.observe.persistence.Entities.REFERENCE_ENTITIES_LIST;

/**
 * Created on 31/12/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class SqlScriptProducerServiceTopia extends ObserveServiceTopia implements SqlScriptProducerService {

    private static final Log log = LogFactory.getLog(SqlScriptProducerServiceTopia.class);

    @Override
    public AddSqlScriptProducerResult produceAddSqlScript(AddSqlScriptProducerRequest request) {
        if (log.isTraceEnabled()) {
            log.trace("produceAddSqlScript(" + request + ")");
        }
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {

            ImmutableSet.Builder<ObserveBlobsContainer> observeBlobContainers = ImmutableSet.builder();
            try (Writer writer = new OutputStreamWriter(new GZIPOutputStream(out))) {

                TopiaSqlBatchService sqlBatchService = serviceContext.getTopiaApplicationContext().getSqlBatchService();
                SqlRequests.Builder builder = sqlBatchService.requestBuilder().to(writer);

                SqlRequests sqlRequests = sqlRequests(request, builder);
                SqlResult sqlResult = sqlBatchService.execute(sqlRequests);
                ImmutableSet<BlobsContainer> blobsContainers = sqlResult.getBlobsContainers();
                for (BlobsContainer blobsContainer : blobsContainers) {
                    observeBlobContainers.add(new ObserveBlobsContainer(blobsContainer.getTableName(),
                                                                        blobsContainer.getColumnName(),
                                                                        blobsContainer.getBlobsById()));
                }

            }

            return new AddSqlScriptProducerResult(out.toByteArray(), observeBlobContainers.build());

        } catch (IOException e) {
            throw new RuntimeException("Could not produce sql script", e);
        }

    }

    @Override
    public byte[] produceDeleteSqlScript(DeleteSqlScriptProducerRequest request) {
        if (log.isTraceEnabled()) {
            log.trace("produceDeleteSqlScript(" + request + ")");
        }
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {

            try (Writer writer = new OutputStreamWriter(new GZIPOutputStream(out))) {

                TopiaSqlBatchService sqlBatchService = serviceContext.getTopiaApplicationContext().getSqlBatchService();
                SqlRequests.Builder builder = sqlBatchService.requestBuilder().to(writer);

                SqlRequests sqlRequests = sqlRequests(request, builder);
                sqlBatchService.execute(sqlRequests);

            }

            return out.toByteArray();

        } catch (IOException e) {
            throw new RuntimeException("Could not produce delete sql script", e);
        }
    }

    protected SqlRequests sqlRequests(AddSqlScriptProducerRequest request, SqlRequests.Builder builder) {

        if (serviceContext.getTopiaApplicationContext().getConfiguration().isShowSql()) {
            builder.showSql();
        }

        if (request.isAddSchema()) {

            SqlRequests.CreateSchemaRequestBuilder createSchemaRequestBuilder = builder
                    .createSchemaBuilder()
                    .setTemporaryPath(serviceContext.getTemporaryDirectoryRoot().toPath());
            if (request.isH2()) {
                createSchemaRequestBuilder.forH2();
            } else {
                createSchemaRequestBuilder.forPostgres();
            }
            createSchemaRequestBuilder.flush();

        }

        ObserveTopiaApplicationContext topiaApplicationContext = serviceContext.getTopiaApplicationContext();
        TopiaMetadataModel metadataModel = topiaApplicationContext.getMetadataModel();

        if (request.isAddReferential()) {
            builder.replicateTablesBuilder(metadataModel)
                   .setTables(topiaApplicationContext.getReferentialTables())
                   .flush();

        }

        if (request.isAddSomeReferential()) {

            ListMultimap<Class<? extends ReferentialDto>, String> referentialIds = request.getReferentialIds();

            for (ObserveEntityEnum entityEnum : REFERENCE_ENTITIES_LIST) {
                Class entityType = entityEnum.getContract();
                List<String> ids = referentialIds.get(BINDER_ENGINE.getReferentialDtoType(entityEnum));
                if (CollectionUtils.isNotEmpty(ids)) {

                    builder.replicateTablesBuilder(metadataModel)
                           .setTables(topiaApplicationContext.getReferentialTable(entityType))
                           .setSelectArgument(TopiaSqlTableSelectArgument.of(ids))
                           .flush();

                }
            }

        }

        if (request.isAddData()) {

            ImmutableSet<String> tripIds = request.getDataIds();
            if (tripIds == null) {

                builder.replicateTablesBuilder(metadataModel)
                       .setTables(topiaApplicationContext.getTripSeineTables())
                       .replicateTablesBuilder(metadataModel)
                       .setTables(topiaApplicationContext.getTripLonglineTables())
                       .flush();

            } else {

                {

                    Set<String> tripIds1 = tripIds.stream().filter(IS_SEINE_ID).collect(Collectors.toSet());

                    if (!tripIds1.isEmpty()) {

                        builder.replicateTablesBuilder(metadataModel)
                               .setTables(topiaApplicationContext.getTripSeineTables())
                               .setSelectArgument(TopiaSqlTableSelectArgument.of(tripIds1))
                               .flush();

                    }

                }

                {

                    Set<String> tripIds1 = tripIds.stream().filter(IS_LONGLINE_ID).collect(Collectors.toSet());

                    if (!tripIds1.isEmpty()) {

                        builder.replicateTablesBuilder(metadataModel)
                               .setTables(topiaApplicationContext.getTripLonglineTables())
                               .setSelectArgument(TopiaSqlTableSelectArgument.of(tripIds1))
                               .flush();

                    }

                }

            }

        }

        return builder.build();
    }

    protected SqlRequests sqlRequests(DeleteSqlScriptProducerRequest request, SqlRequests.Builder builder) {

        if (serviceContext.getTopiaApplicationContext().getConfiguration().isShowSql()) {
            builder.showSql();
        }

        ObserveTopiaApplicationContext topiaApplicationContext = serviceContext.getTopiaApplicationContext();

        if (request.isDeleteData()) {

            ImmutableSet<String> tripIds = request.getDataIds();
            if (tripIds == null) {

                builder.deleteTablesBuilder()
                       .setTables(topiaApplicationContext.getTripSeineTables())
                       .replicateTablesBuilder(topiaApplicationContext.getMetadataModel())
                       .setTables(topiaApplicationContext.getTripLonglineTables())
                       .flush();

            } else {

                {

                    Set<String> tripIds1 = tripIds.stream().filter(Entities.IS_SEINE_ID).collect(Collectors.toSet());

                    if (!tripIds1.isEmpty()) {

                        builder.deleteTablesBuilder()
                               .setTables(topiaApplicationContext.getTripSeineTables())
                               .setSelectArgument(TopiaSqlTableSelectArgument.of(tripIds1))
                               .flush();

                    }

                }

                {

                    Set<String> tripIds1 = tripIds.stream().filter(Entities.IS_LONGLINE_ID).collect(Collectors.toSet());

                    if (!tripIds1.isEmpty()) {

                        builder.deleteTablesBuilder()
                               .setTables(topiaApplicationContext.getTripLonglineTables())
                               .setSelectArgument(TopiaSqlTableSelectArgument.of(tripIds1))
                               .flush();

                    }

                }

            }

        }

        return builder.build();
    }

}
