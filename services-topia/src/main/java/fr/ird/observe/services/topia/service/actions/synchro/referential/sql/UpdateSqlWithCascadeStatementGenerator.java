package fr.ird.observe.services.topia.service.actions.synchro.referential.sql;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Multimap;
import fr.ird.observe.services.dto.reference.ReferentialReference;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.metadata.TopiaMetadataEntity;

/**
 * Pour générer une requète sql d'ajout à partir d'un référentiel donné et aussi tous les inserts manquants.
 *
 * Created on 29/06/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0
 */
public abstract class UpdateSqlWithCascadeStatementGenerator<R extends ReferentialDto> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(UpdateSqlWithCascadeStatementGenerator.class);

    private final UpdateSqlStatementGenerator<R> delegateGenerator;
    private final Multimap<Class<? extends ReferentialDto>, String> idsOnlyExistingOnThisSide;

    public UpdateSqlWithCascadeStatementGenerator(TopiaMetadataEntity metadataEntity,
                                                  Class<R> dtoType,
                                                  Multimap<Class<? extends ReferentialDto>, String> idsOnlyExistingOnThisSide) {
        this.idsOnlyExistingOnThisSide = idsOnlyExistingOnThisSide;
        this.delegateGenerator = new UpdateSqlStatementGenerator<R>(metadataEntity, dtoType) {

            @Override
            protected <D extends ReferentialDto> void addMnAssociation(String nmAssociationTableName, String nmAssociationDbColumnName, String referentialDtoId, Class<D> associationType, String associationId, StringBuilder builder) {
                super.addMnAssociation(nmAssociationTableName, nmAssociationDbColumnName, referentialDtoId, associationType, associationId, builder);
                addMissingReferentialIfNecessary(associationType, associationId);
            }

            @Override
            protected void addReferentialReferenceParameter(String columnName, ReferentialReference parameter, StringBuilder parameters) {
                super.addReferentialReferenceParameter(columnName, parameter, parameters);
                addMissingReferentialIfNecessary(parameter.getType(), parameter.getId());
            }

            @Override
            protected void addReferentialDtoParameter(String columnName, ReferentialDto parameter, StringBuilder parameters) {
                super.addReferentialDtoParameter(columnName, parameter, parameters);
                addMissingReferentialIfNecessary(parameter.getClass(), parameter.getId());
            }
        };
    }

    protected abstract <D extends ReferentialDto> ImmutableList<String> insertMissingReferential(Class<D> referentialType, String id);

    public ImmutableList<String> generateSql(R referentialDto) {

        return delegateGenerator.generateSql(referentialDto);

    }

    private <D extends ReferentialDto> ImmutableList<String> addMissingReferentialIfNecessary(Class<D> associationType, String associationId) {

        if (idsOnlyExistingOnThisSide.containsEntry(associationType, associationId)) {

            // il faut insérer aussi ce référentiel
            ImmutableList<String> sqls = insertMissingReferential(associationType, associationId);

            // ce référentiel est désormais présent dans les deux sources
            idsOnlyExistingOnThisSide.remove(associationType, associationId);

            return sqls;

        }
        return null;
    }

}
