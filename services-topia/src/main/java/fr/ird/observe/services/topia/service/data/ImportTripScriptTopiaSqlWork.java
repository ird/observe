package fr.ird.observe.services.topia.service.data;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.persistence.RunScriptTopiaSqlWork;
import fr.ird.observe.services.dto.ObserveBlobsContainer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.sql.rowset.serial.SerialBlob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

/**
 * Created on 24/08/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ImportTripScriptTopiaSqlWork extends RunScriptTopiaSqlWork {

    /** Logger */
    private static final Log log = LogFactory.getLog(ImportTripScriptTopiaSqlWork.class);

    private final ImmutableSet<ObserveBlobsContainer> blobsContainers;

    public ImportTripScriptTopiaSqlWork(int batchSize, boolean showSql, byte[] content, ImmutableSet<ObserveBlobsContainer> blobsContainers) {
        super(batchSize, showSql, content);
        this.blobsContainers = blobsContainers;
    }

    @Override
    public void execute(Connection connection) throws SQLException {
        super.execute(connection);

        for (ObserveBlobsContainer blobsContainer : blobsContainers) {

            String tableName = blobsContainer.getTableName();
            String columnName = blobsContainer.getColumnName();
            int batchSize = 0;

            String sql = String.format("update %s SET %s = ? WHERE topiaId= ?", tableName, columnName);
            if (showSql) {
                if (log.isInfoEnabled()) {
                    log.info(sql);
                }
            }
            try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {

                for (Map.Entry<String, byte[]> containerEntry : blobsContainer.getBlobsById().entrySet()) {

                    String topiaId = containerEntry.getKey();
                    byte[] content = containerEntry.getValue();

                    preparedStatement.clearParameters();
                    preparedStatement.setBlob(1, new SerialBlob(content));
                    preparedStatement.setString(2, topiaId);
                    preparedStatement.addBatch();

                    batchSize++;

                    if (batchSize % this.batchSize == 0) {
                        flushStatement(preparedStatement);
                    }

                }


                flushStatement(preparedStatement);

            }

        }


    }

}
