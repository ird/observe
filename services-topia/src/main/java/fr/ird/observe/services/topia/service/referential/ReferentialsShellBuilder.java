package fr.ird.observe.services.topia.service.referential;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.cache.AbstractLoadingCache;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimaps;
import com.google.common.collect.SetMultimap;
import fr.ird.observe.persistence.ObserveEntityEnum;
import fr.ird.observe.entities.referentiel.ObserveReferentialEntity;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.binder.BinderEngine;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaEntityVisitor;

import java.util.Set;
import java.util.TreeSet;

/**
 * Created on 07/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class ReferentialsShellBuilder {

    public static ReferentialsShellBuilder builder(SetMultimap<Class<? extends ReferentialDto>, String> incomingReferentialIds) {
        return new ReferentialsShellBuilder(incomingReferentialIds);
    }

    private final GetEntityReferentialsShellVisitor visitor;

    public ReferentialsShellBuilder scan(TopiaEntity entity) {
        entity.accept(visitor);
        return this;
    }

    public SetMultimap<Class<? extends ReferentialDto>, String> build() {
        return Multimaps.unmodifiableSetMultimap(visitor.missingreferentialIds);
    }

    private ReferentialsShellBuilder(SetMultimap<Class<? extends ReferentialDto>, String> incomingReferentialIds) {
        visitor = new GetEntityReferentialsShellVisitor(incomingReferentialIds);
    }

    private static class GetEntityReferentialsShellVisitor implements TopiaEntityVisitor {

        private final Set<String> hitIds;
        private final SetMultimap<Class<? extends ReferentialDto>, String> incomingReferentialIds;
        private final SetMultimap<Class<? extends ReferentialDto>, String> missingreferentialIds;
        private final LoadingCache<Class<? extends ObserveReferentialEntity>, Class<? extends ReferentialDto>> typeCache;

        public GetEntityReferentialsShellVisitor(SetMultimap<Class<? extends ReferentialDto>, String> incomingReferentialIds) {
            this.incomingReferentialIds = incomingReferentialIds;
            this.typeCache = new AbstractLoadingCache<Class<? extends ObserveReferentialEntity>, Class<? extends ReferentialDto>>() {
                @Override
                public Class<? extends ReferentialDto> get(Class<? extends ObserveReferentialEntity> key) {
                    Class<? extends TopiaEntity> entityType = ObserveEntityEnum.getContractClass(key);
                    return BinderEngine.get().getReferentialDtoType((Class) entityType);
                }

                @Override
                public Class<? extends ReferentialDto> getIfPresent(Object key) {
                    return get((Class) key);
                }
            };
            this.hitIds = new TreeSet<>();
            this.missingreferentialIds = HashMultimap.create();
        }

        @Override
        public void start(TopiaEntity entity) {
            if (entity instanceof ObserveReferentialEntity) {
                String topiaId = entity.getTopiaId();
                if (hitIds.contains(topiaId)) {
                    return;
                }
                hitIds.add(topiaId);
                Class<? extends ReferentialDto> dtoType = typeCache.getUnchecked(((ObserveReferentialEntity) entity).getClass());
                if (!incomingReferentialIds.containsEntry(dtoType, topiaId)) {
                    missingreferentialIds.put(dtoType, topiaId);
                }
            }
        }

        @Override
        public void end(TopiaEntity entity) {

        }

        @Override
        public void visit(TopiaEntity entity, String propertyName, Class<?> type, Object value) {
            if (value instanceof TopiaEntity) {
                ((TopiaEntity) value).accept(this);
            }
        }

        @Override
        public void visit(TopiaEntity entity, String propertyName, Class<?> collectionType, Class<?> type, Object value) {

        }

        @Override
        public void visit(TopiaEntity entity, String propertyName, Class<?> collectionType, Class<?> type, int index, Object value) {
            if (value instanceof TopiaEntity) {
                ((TopiaEntity) value).accept(this);
            }
        }

        @Override
        public void clear() {
            hitIds.clear();
            typeCache.invalidateAll();
        }
    }

}
