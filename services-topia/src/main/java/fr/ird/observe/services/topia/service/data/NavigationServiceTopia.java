package fr.ird.observe.services.topia.service.data;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import fr.ird.observe.services.dto.reference.DataReference;
import fr.ird.observe.services.dto.reference.DataReferenceList;
import fr.ird.observe.services.dto.longline.TripLonglineDto;
import fr.ird.observe.services.dto.longline.TripLonglineHelper;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.referential.ProgramHelper;
import fr.ird.observe.services.dto.reference.ReferentialReference;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import fr.ird.observe.services.dto.seine.TripSeineHelper;
import fr.ird.observe.services.service.data.NavigationRequest;
import fr.ird.observe.services.service.data.NavigationResult;
import fr.ird.observe.services.service.data.NavigationService;
import fr.ird.observe.services.service.data.longline.TripLonglineService;
import fr.ird.observe.services.service.data.seine.TripSeineService;
import fr.ird.observe.services.service.referential.ReferentialService;
import fr.ird.observe.services.topia.ObserveServiceTopia;

/**
 * Created on 22/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class NavigationServiceTopia extends ObserveServiceTopia implements NavigationService {

    @Override
    public NavigationResult getNavigation(NavigationRequest request) {

        ImmutableSet<ReferentialReference<ProgramDto>> allPrograms = serviceContext.newService(ReferentialService.class).getReferenceSet(ProgramDto.class, null).getReferences();
        ImmutableMap<String, ReferentialReference<ProgramDto>> programsById = Maps.uniqueIndex(allPrograms, ReferentialReference::getId);


        boolean loadLongline = request.isLoadLongline();
        boolean loadSeine = request.isLoadSeine();
        boolean loadEmptyProgram = request.isLoadEmptyProgram();

        ArrayListMultimap<ReferentialReference<ProgramDto>, DataReference<TripSeineDto>> tripsSeineByProgram = ArrayListMultimap.create();
        ArrayListMultimap<ReferentialReference<ProgramDto>, DataReference<TripLonglineDto>> tripsLonglineByProgram = ArrayListMultimap.create();

        if (loadSeine) {
            TripSeineService service = serviceContext.newService(TripSeineService.class);
            DataReferenceList<TripSeineDto> trips = service.getAllTripSeine();
            for (DataReference<TripSeineDto> reference : trips.getReferences()) {
                String programId = TripSeineHelper.getProgramId(reference);
                ReferentialReference<ProgramDto> program = programsById.get(programId);
                tripsSeineByProgram.put(program, reference);
            }

        }
        if (loadLongline) {

            TripLonglineService service = serviceContext.newService(TripLonglineService.class);
            DataReferenceList<TripLonglineDto> trips = service.getAllTripLongline();
            for (DataReference<TripLonglineDto> reference : trips.getReferences()) {
                String programId = TripLonglineHelper.getProgramId(reference);
                ReferentialReference<ProgramDto> program = programsById.get(programId);
                tripsLonglineByProgram.put(program, reference);
            }

        }

        ImmutableSet.Builder<ReferentialReference<ProgramDto>> programsBuilder = ImmutableSet.builder();
        if (loadEmptyProgram) {

            // on renvoie tous les programmes
            for (ReferentialReference<ProgramDto> program : allPrograms) {
                if (loadLongline && ProgramHelper.isProgramLongline(program)) {
                    programsBuilder.add(program);
                } else if (loadSeine && ProgramHelper.isProgramSeine(program)) {
                    programsBuilder.add(program);
                }
            }

        } else {

            // on renvoie uniquement les programmes utilisés
            if (loadLongline) {
                programsBuilder.addAll(tripsLonglineByProgram.keySet());
            }
            if (loadSeine) {
                programsBuilder.addAll(tripsSeineByProgram.keySet());
            }
        }

        return new NavigationResult(programsBuilder.build(), tripsSeineByProgram, tripsLonglineByProgram);
    }
}
