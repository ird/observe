package fr.ird.observe.services.topia.service.data;

/*-
 * #%L
 * ObServe :: Services ToPIA Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.common.TripMapPoint;
import fr.ird.observe.services.dto.TripMapDto;

import java.util.LinkedHashSet;

/**
 * Created on 09/01/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class TripMapDtoFactory {

    public static TripMapDto of(String tripId, LinkedHashSet<TripMapPoint> points) {
        TripMapDto tripMapDto = new TripMapDto();
        tripMapDto.setId(tripId);
        tripMapDto.setPoints(points);
        return tripMapDto;
    }

}
