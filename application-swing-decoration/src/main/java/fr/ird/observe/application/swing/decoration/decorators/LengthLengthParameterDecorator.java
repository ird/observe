/*
 * #%L
 * ObServe :: Application Swing Decoration
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.decoration.decorators;

import fr.ird.observe.services.dto.referential.LengthLengthParameterDto;

import static org.nuiton.i18n.I18n.t;

/**
 * Decorator of {@link LengthLengthParameterDto}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.4
 */
public class LengthLengthParameterDecorator extends ObserveDecorator<LengthLengthParameterDto> {

    private static final long serialVersionUID = 1L;

    public LengthLengthParameterDecorator() throws IllegalArgumentException, NullPointerException {
        super(LengthLengthParameterDto.class, "${inputSizeMeasureType}$s##${outputSizeMeasureType}$s##" +
                t("observe.common.inputOutputFormula") + " ${inputOutputFormula}$s"
        );
    }

    @Override
    protected Comparable<?> getDefaultUndefinedValue(String token) {
        if (token.startsWith(LengthLengthParameterDto.PROPERTY_INPUT_SIZE_MEASURE_TYPE)
                || token.startsWith(LengthLengthParameterDto.PROPERTY_OUTPUT_SIZE_MEASURE_TYPE)
                ) {
            return t("observe.common.inconnu");
        }
        return super.getDefaultUndefinedValue(token);
    }

}
