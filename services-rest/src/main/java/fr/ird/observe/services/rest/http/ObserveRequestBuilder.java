package fr.ird.observe.services.rest.http;

/*
 * #%L
 * ObServe :: Services REST Implementation
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import fr.ird.observe.services.gson.ObserveDtoGsonSupplier;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.io.File;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;

/**
 * @author Arnaud Thimel - thimel@codelutin.com
 */
public class ObserveRequestBuilder {

    protected final ImmutableList.Builder<NameValuePair> parameters;

    protected final ImmutableMap.Builder<String, String> headers;

    protected final ImmutableMap.Builder<String, File> files;

    protected final Gson gson;

    protected String requestBody;

    protected String contentType;

    protected ObserveRequestMethod requestMethod;
    protected int timeout = 30 * 1000; // en millisecondes

    public static ObserveRequestBuilder create(Supplier<Gson> gsonSupplier) {
        return new ObserveRequestBuilder(gsonSupplier);
    }

    public ObserveRequest build(String baseUrl) {

        Objects.requireNonNull(baseUrl, "'baseUrl' can't be null");
        Preconditions.checkState(requestMethod != null, "'requestMethod' was not setted");

        return new ObserveRequest(
                requestMethod,
                baseUrl,
                contentType,
                requestBody,
                headers.build(),
                parameters.build(),
                files.build(),
                timeout
        );
    }

    public ObserveRequestBuilder setTimeout(int timeout) {
        this.timeout = timeout;
        return this;
    }

    public ObserveRequestBuilder setRequestMethod(ObserveRequestMethod requestMethod) {
        this.requestMethod = requestMethod;
        return this;
    }

    public ObserveRequestBuilder setRequestBody(String requestBody) {
        this.requestBody = requestBody;
        return this;
    }

    public ObserveRequestBuilder setContentType(String contentType) {
        this.contentType = contentType;
        return this;
    }

    public ObserveRequestBuilder addHeader(String key, String value) {
        checkRequestNotNull(key, value);
        headers.put(key, value);
        return this;
    }

    public ObserveRequestBuilder addParameter(String parameterName, String value) {
        checkParameterNotNull(parameterName, value);
        parameters.add(new BasicNameValuePair(parameterName, value));
        return this;
    }

    public ObserveRequestBuilder addParameter(String parameterName, Object toJson) {
        String json = gson.toJson(toJson);
        return addParameter(parameterName, json);
    }

    public ObserveRequestBuilder addParameter(String parameterName, Collection<?> toJsonList) {

        // we must add values with same paramName to get List<?> in service
        for (Object toJson : toJsonList) {
            if (toJson instanceof String) {

                // don't serialize String objects
                addParameter(parameterName, (String) toJson);
            } else {
                addParameter(parameterName, toJson);
            }
        }
        return this;
    }

    protected ObserveRequestBuilder(Supplier<Gson> gsonSupplier) {
        this.headers = new ImmutableMap.Builder<>();
        this.files = new ImmutableMap.Builder<>();
        this.parameters = new ImmutableList.Builder<>();
        this.gson = Optional.ofNullable(gsonSupplier).orElse(ObserveDtoGsonSupplier.DEFAULT_GSON_SUPPLIER).get();
    }

    protected void checkParameterNotNull(String parparameterNamemName, Object value) {
        checkKeyValueNotNull(parparameterNamemName, value, "Parameter key must be not null", "Parameter value must be not null for paramName : " + parparameterNamemName);
    }

    protected void checkRequestNotNull(String key, Object value) {
        checkKeyValueNotNull(key, value, "Request key must be not null", "Request value must be not null for key : " + key);
    }

    protected void checkKeyValueNotNull(String key, Object value, String keyErrorMessage, String valueErrorMessage) {
        Objects.requireNonNull(key, keyErrorMessage);
        Objects.requireNonNull(value, valueErrorMessage);
    }

}
