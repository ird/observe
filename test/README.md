To deploy new version of pom: mvn deploy
To install localy: mvn install
To run: mvn -Prun

Release
-------

To do a release of this project (which now uses git, refer to this page
https://forge.nuiton.org/projects/pom/repository/revisions/master/entry/codelutinpom/README.txt).