package fr.ird.observe.services.binder;

/*-
 * #%L
 * ObServe :: Services Binder
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Iterables;
import fr.ird.observe.entities.ObserveEntity;
import fr.ird.observe.entities.referentiel.I18nReferenceEntities;
import fr.ird.observe.entities.referentiel.I18nReferentialEntity;
import fr.ird.observe.entities.referentiel.ObserveReferentialEntity;
import fr.ird.observe.persistence.ObserveEntityEnum;
import fr.ird.observe.services.dto.reference.AbstractReference;
import fr.ird.observe.services.dto.IdDto;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.referential.I18nReferentialDto;
import fr.ird.observe.services.dto.referential.I18nReferentialHelper;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.reference.ReferentialReference;
import fr.ird.observe.services.binder.referential.ReferentialBinderSupport;
import org.apache.commons.collections4.CollectionUtils;
import org.nuiton.topia.persistence.TopiaEntity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class BinderSupport<E extends TopiaEntity, D extends IdDto> {

    protected final Class<E> entityType;

    protected final Class<D> dtoType;

    protected BinderSupport(Class<E> entityType, Class<D> dtoType) {
        this.entityType = entityType;
        this.dtoType = dtoType;
    }

    public abstract void copyToEntity(ReferentialLocale referentialLocale, D dto, E entity);

    public abstract void copyToDto(ReferentialLocale referentialLocale, E entity, D dto);

    // -------------------------------------------------------------------------------------------------------------- //
    // -- REFERENTIAL REFERENCE → ENTITY ---------------------------------------------------------------------------- //
    // -------------------------------------------------------------------------------------------------------------- //

    protected <DD extends ReferentialDto, EE extends ObserveReferentialEntity> EE toEntity(ReferentialReference<DD> reference, Class<EE> entityType) {

        EE entity = null;

        if (reference != null) {

            ReferentialBinderSupport<EE, DD> binder = BinderEngine.get().getReferentialBinder(reference.getType());

            entity = binder.toEntity(reference);

        }

        return entity;

    }

    // -------------------------------------------------------------------------------------------------------------- //
    // -- REFERENTIAL → ENTITY -------------------------------------------------------------------------------------- //
    // -------------------------------------------------------------------------------------------------------------- //

    protected <DD extends ReferentialDto, EE extends ObserveReferentialEntity> LinkedHashSet<EE> toEntitySet(Collection<ReferentialReference<DD>> references, Class<EE> entityType) {

        LinkedHashSet<EE> entityList = null;
        if (CollectionUtils.isNotEmpty(references)) {

            entityList = new LinkedHashSet<>(references.size());

            ReferentialReference<DD> firstReference = Iterables.get(references, 0, null);
            Objects.requireNonNull(firstReference);
            Class<DD> type = firstReference.getType();
            ReferentialBinderSupport<EE, DD> binder = BinderEngine.get().getReferentialBinder(type);

            for (ReferentialReference<DD> reference : references) {

                EE entity = binder.toEntity(reference);
                entityList.add(entity);

            }

        }
        return entityList;

    }

    // -------------------------------------------------------------------------------------------------------------- //
    // -- ENTITY → REFERENTIAL REFERENCE ---------------------------------------------------------------------------- //
    // -------------------------------------------------------------------------------------------------------------- //

    protected <EE extends ObserveReferentialEntity, DD extends ReferentialDto> ReferentialReference<DD> toReferentialReference(ReferentialLocale referentialLocale, EE entity, Class<DD> dtoType) {

        ReferentialReference<DD> reference = null;
        if (entity != null) {

            ReferentialBinderSupport<EE, DD> binder = BinderEngine.get().getReferentialBinder(dtoType);
            reference = binder.toReferentialReference(referentialLocale, entity);

        }
        return reference;

    }

    protected <EE extends ObserveReferentialEntity, DD extends ReferentialDto> List<ReferentialReference<DD>> toReferentialReferenceList(ReferentialLocale referentialLocale, Collection<EE> entities, Class<DD> dtoType) {

        List<ReferentialReference<DD>> references = null;
        if (CollectionUtils.isNotEmpty(entities)) {

            references = new ArrayList<>(entities.size());

            ReferentialBinderSupport<EE, DD> binder = BinderEngine.get().getReferentialBinder(dtoType);

            for (EE entity : entities) {

                ReferentialReference<DD> reference = binder.toReferentialReference(referentialLocale, entity);
                references.add(reference);

            }

        }
        return references;

    }

    // -------------------------------------------------------------------------------------------------------------- //
    // -- LABELS ---------------------------------------------------------------------------------------------------- //
    // -------------------------------------------------------------------------------------------------------------- //

    protected String getLabel(ReferentialLocale locale, I18nReferentialEntity entity) {
        return entity == null ? "Non trouvé" : I18nReferenceEntities.getLabel(locale.ordinal(), entity);
    }

    protected String getLabel(ReferentialLocale locale, I18nReferentialDto dto) {
        return I18nReferentialHelper.decorate(locale.ordinal(), dto);
    }

    protected String getLabel(ReferentialLocale locale, ReferentialReference dto) {
        return (String) dto.getPropertyValue(ReferentialReference.PROPERTY_LABEL);
    }

    // -------------------------------------------------------------------------------------------------------------- //
    // -- CREER DES ENTITY OU DTO ----------------------------------------------------------------------------------- //
    // -------------------------------------------------------------------------------------------------------------- //

    protected E newEntity() {
        try {
            return (E) ObserveEntityEnum.valueOf(entityType).getImplementation().newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException("What ever");
        }
    }

    protected D newDto() {
        try {
            return dtoType.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException("What ever");
        }
    }


    protected <D extends IdDto> String toDtoId(AbstractReference<D> dto) {
        return dto == null ? null : dto.getId();
    }

    protected <E extends ObserveEntity> String toEntityId(E entity) {
        return entity == null ? null : entity.getTopiaId();
    }
}
