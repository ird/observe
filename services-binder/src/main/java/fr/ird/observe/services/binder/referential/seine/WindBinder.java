package fr.ird.observe.services.binder.referential.seine;

/*-
 * #%L
 * ObServe :: Services Binder
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.referentiel.seine.Wind;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.reference.ReferentialReference;
import fr.ird.observe.services.dto.referential.seine.WindDto;
import fr.ird.observe.services.binder.referential.ReferentialBinderSupport;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class WindBinder extends ReferentialBinderSupport<Wind, WindDto> {

    public WindBinder() {
        super(Wind.class, WindDto.class);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, WindDto dto, Wind entity) {

        copyDtoReferentialFieldsToEntity(dto, entity);
        copyDtoI18nFieldsToEntity(dto, entity);

        entity.setSpeedRange(dto.getSpeedRange());
        entity.setWaveHeight(dto.getWaveHeight());

    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, Wind entity, WindDto dto) {

        copyEntityReferentialFieldsToDto(entity, dto);
        copyEntityI18nFieldsToDto(entity, dto);

        dto.setSpeedRange(entity.getSpeedRange());
        dto.setWaveHeight(entity.getWaveHeight());

    }

    @Override
    public ReferentialReference<WindDto> toReferentialReference(ReferentialLocale referentialLocale, Wind entity) {

        return toReferentialReference(entity,
                                      getLabel(referentialLocale, entity),
                                      entity.getSpeedRange());

    }

    @Override
    public ReferentialReference<WindDto> toReferentialReference(ReferentialLocale referentialLocale, WindDto dto) {

        return toReferentialReference(dto,
                                      getLabel(referentialLocale, dto),
                                      dto.getSpeedRange());
    }
}
