package fr.ird.observe.services.binder.data.seine;

/*-
 * #%L
 * ObServe :: Services Binder
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.referentiel.Species;
import fr.ird.observe.entities.seine.ObjectSchoolEstimate;
import fr.ird.observe.services.dto.reference.DataReference;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.seine.ObjectSchoolEstimateDto;
import fr.ird.observe.services.binder.data.DataBinderSupport;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ObjectSchoolEstimateBinder extends DataBinderSupport<ObjectSchoolEstimate, ObjectSchoolEstimateDto> {

    public ObjectSchoolEstimateBinder() {
        super(ObjectSchoolEstimate.class, ObjectSchoolEstimateDto.class);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, ObjectSchoolEstimateDto dto, ObjectSchoolEstimate entity) {

        copyDtoDataFieldsToEntity(dto, entity);

        entity.setTotalWeight(dto.getTotalWeight());
        entity.setSpecies(toEntity(dto.getSpecies(), Species.class));

    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, ObjectSchoolEstimate entity, ObjectSchoolEstimateDto dto) {

        copyEntityDataFieldsToDto(entity, dto);

        dto.setTotalWeight(entity.getTotalWeight());
        dto.setSpecies(toReferentialReference(referentialLocale, entity.getSpecies(), SpeciesDto.class));

    }

    @Override
    public DataReference<ObjectSchoolEstimateDto> toDataReference(ReferentialLocale referentialLocale, ObjectSchoolEstimate entity) {
        return toDataReference(entity, getLabel(referentialLocale, entity.getSpecies()), entity.getTotalWeight());
    }

    @Override
    public DataReference<ObjectSchoolEstimateDto> toDataReference(ReferentialLocale referentialLocale, ObjectSchoolEstimateDto dto) {
        return toDataReference(dto, getLabel(referentialLocale, dto.getSpecies()), dto.getTotalWeight());
    }
}
