package fr.ird.observe.services.binder.data;

/*-
 * #%L
 * ObServe :: Services Binder
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import fr.ird.observe.entities.CommentableEntity;
import fr.ird.observe.entities.ObserveDataEntity;
import fr.ird.observe.services.dto.CommentableDto;
import fr.ird.observe.services.dto.DataDto;
import fr.ird.observe.services.dto.reference.DataReference;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.ObserveDataReferenceSets;
import fr.ird.observe.services.dto.reference.ReferenceSetDefinition;
import fr.ird.observe.services.binder.BinderEngine;
import fr.ird.observe.services.binder.BinderSupport;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntities;

import javax.sql.rowset.serial.SerialBlob;
import java.io.Serializable;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class DataBinderSupport<E extends ObserveDataEntity, D extends DataDto> extends BinderSupport<E, D> {

    private static final Log log = LogFactory.getLog(DataBinderSupport.class);

    protected final ReferenceSetDefinition<D> definition;

    protected DataBinderSupport(Class<E> entityType, Class<D> dtoType) {
        super(entityType, dtoType);
        this.definition = ObserveDataReferenceSets.getDefinition(dtoType);
    }

    // -------------------------------------------------------------------------------------------------------------- //
    // -- ENTITY → DATA --------------------------------------------------------------------------------------------- //
    // -------------------------------------------------------------------------------------------------------------- //

    public D toData(ReferentialLocale referentialLocale, E entity) {

        D dto = newDto();
        copyToDto(referentialLocale, entity, dto);
        return dto;

    }

    protected void copyEntityDataFieldsToDto(E entity, D dto) {
        dto.setId(entity.getTopiaId());
        dto.setLastUpdateDate(entity.getLastUpdateDate());

        if (dto instanceof CommentableDto && entity instanceof CommentableEntity) {
            ((CommentableDto) dto).setComment(((CommentableEntity) entity).getComment());
        }
    }

    protected Blob byteArrayToBlob(byte[] bytes) {
        Blob result = null;

        try {
            result = new SerialBlob(bytes);
        } catch (SQLException e) {
            if (log.isErrorEnabled()) {
                log.error("unable to create blob ", e);
            }
        }

        return result;
    }

    // -------------------------------------------------------------------------------------------------------------- //
    // -- ENTITY → DATA REFERENCE ----------------------------------------------------------------------------------- //
    // -------------------------------------------------------------------------------------------------------------- //

    public DataReference<D> toDataReference(ReferentialLocale referentialLocale, E entity) {
        return toDataReference(entity);
    }

    protected DataReference<D> toDataReference(E entity, Serializable... values) {

        DataReference<D> reference = new DataReference<>();

        reference.setId(entity.getTopiaId());
        reference.setCreateDate(entity.getTopiaCreateDate());
        reference.setVersion(entity.getTopiaVersion());

        Objects.requireNonNull(definition, "Pas de définition trouvée pour " + this);
        reference.init(definition.getType(), definition.getPropertyNames(), values);

        return reference;
    }

    protected <EE extends ObserveDataEntity, DD extends DataDto> DataReference<DD> toDataReference(ReferentialLocale referentialLocale, EE entity, Class<DD> dtoType) {

        DataReference<DD> reference = null;
        if (entity != null) {

            DataBinderSupport<EE, DD> binder = BinderEngine.get().getDataBinder(dtoType);
            reference = binder.toDataReference(referentialLocale, entity);

        }
        return reference;

    }

    protected <EE extends ObserveDataEntity, DD extends DataDto> LinkedHashSet<DD> toLinkedHashSetData(ReferentialLocale referentialLocale, Collection<EE> entities, Class<DD> dtoType) {

        LinkedHashSet<DD> dtos = null;
        if (CollectionUtils.isNotEmpty(entities)) {

            DataBinderSupport<EE, DD> binder = BinderEngine.get().getDataBinder(dtoType);

            dtos = new LinkedHashSet<>(entities.size());
            for (EE entity : entities) {

                DD dto = binder.toData(referentialLocale, entity);
                dtos.add(dto);

            }

        }
        return dtos;

    }

    protected <EE extends ObserveDataEntity, DD extends DataDto> List<DD> toListData(ReferentialLocale referentialLocale, Collection<EE> entities, Class<DD> dtoType) {

        List<DD> dtos = null;
        if (CollectionUtils.isNotEmpty(entities)) {

            DataBinderSupport<EE, DD> binder = BinderEngine.get().getDataBinder(dtoType);

            dtos = new ArrayList<>(entities.size());
            for (EE entity : entities) {

                DD dto = binder.toData(referentialLocale, entity);
                dtos.add(dto);

            }

        }
        return dtos;

    }

    // -------------------------------------------------------------------------------------------------------------- //
    // -- DATA → DATA REFERENCE ------------------------------------------------------------------------------------- //
    // -------------------------------------------------------------------------------------------------------------- //

    public DataReference<D> toDataReference(ReferentialLocale referentialLocale, D dto) {
        return toDataReference(dto);
    }

    protected DataReference<D> toDataReference(D dto, Serializable... values) {

        DataReference<D> reference = new DataReference<>();

        reference.setId(dto.getId());
//        reference.setCreateDate(dto.getCreateDate());
//        reference.setVersion(dto.getTestPropertyAsVersion());

        reference.init(definition.getType(), definition.getPropertyNames(), values);

        return reference;
    }

    // -------------------------------------------------------------------------------------------------------------- //
    // -- DATA → ENTITIY -------------------------------------------------------------------------------------------- //
    // -------------------------------------------------------------------------------------------------------------- //

    public E toEntity(ReferentialLocale referentialLocale, D data) {
        E entity = newEntity();
        copyToEntity(referentialLocale, data, entity);
        return entity;
    }

    protected void copyDtoDataFieldsToEntity(D dto, E entity) {
        entity.setTopiaId(dto.getId());
        Date lastUpdateDate = dto.getLastUpdateDate();
        if (lastUpdateDate == null) {
            lastUpdateDate = new Date();
        }
        entity.setLastUpdateDate(lastUpdateDate);
        if (dto instanceof CommentableDto && entity instanceof CommentableEntity) {
            ((CommentableEntity) entity).setComment(((CommentableDto) dto).getComment());
        }
    }

    protected <DD extends DataDto, EE extends ObserveDataEntity, C extends Set<EE>> C toEntitySet(ReferentialLocale referentialLocale, Collection<DD> dtos, Class<EE> entityType, C entityList) {

        if (entityList == null) {

            entityList = (C) new LinkedHashSet();
        }

        fillEntityCollection(referentialLocale, dtos, entityList);

        return entityList;

    }

    protected <DD extends DataDto, EE extends ObserveDataEntity> List<EE> toEntityList(ReferentialLocale referentialLocale, Collection<DD> dtos, Class<EE> entityType, List<EE> entityList) {

        if (entityList == null) {

            entityList = new LinkedList<>();
        }

        fillEntityCollection(referentialLocale, dtos, entityList);

        return entityList;

    }

    protected <DD extends DataDto, EE extends ObserveDataEntity> Collection<EE> toEntityCollection(ReferentialLocale referentialLocale, Collection<DD> dtos, Class<EE> entityType, Collection<EE> entityList) {

        if (entityList == null) {

            entityList = new LinkedHashSet<>();
        }

        fillEntityCollection(referentialLocale, dtos, entityList);

        return entityList;

    }

    protected <DD extends DataDto, EE extends ObserveDataEntity, C extends Collection<EE>> void fillEntityCollection(ReferentialLocale referentialLocale, Collection<DD> dtos, C entityList) {

        ImmutableMap<String, EE> entitiesById = Maps.uniqueIndex(entityList, TopiaEntities.getTopiaIdFunction());

        entityList.clear();

        if (CollectionUtils.isNotEmpty(dtos)) {


            DD firstDto = Iterables.get(dtos, 0, null);
            DataBinderSupport<EE, DD> binder = BinderEngine.get().getDataBinder(firstDto);

            for (DD dto : dtos) {

                EE entity = entitiesById.get(dto.getId());
                if (entity == null) {

                    // Create new entity
                    entity = binder.toEntity(referentialLocale, dto);
                    entityList.add(entity);

                } else {

                    // Reuse existing entity
                    binder.copyToEntity(referentialLocale, dto, entity);
                    entityList.add(entity);

                }

            }

        }

    }

    // -------------------------------------------------------------------------------------------------------------- //
    // -- DATA REFERENCE → ENTITY ----------------------------------------------------------------------------------- //
    // -------------------------------------------------------------------------------------------------------------- //

    public E toEntity(DataReference<D> reference) {
        E entity = newEntity();
        entity.setTopiaId(reference.getId());
        entity.setTopiaCreateDate(reference.getCreateDate());
        entity.setTopiaVersion(reference.getVersion());
        entity.setLastUpdateDate(reference.getLastUpdateDate());
        return entity;
    }


    protected <DD extends DataDto, EE extends ObserveDataEntity> EE toEntity(DataReference<DD> reference, Class<EE> entityType) {

        EE entity = null;
        if (reference != null) {
            Class<DD> type = reference.getType();
            DataBinderSupport<EE, DD> binder = BinderEngine.get().getDataBinder(type);
            entity = binder.toEntity(reference);
        }
        return entity;
    }

}
