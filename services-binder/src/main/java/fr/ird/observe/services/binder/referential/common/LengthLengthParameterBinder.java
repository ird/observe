package fr.ird.observe.services.binder.referential.common;

/*-
 * #%L
 * ObServe :: Services Binder
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.referentiel.LengthLengthParameter;
import fr.ird.observe.entities.referentiel.SizeMeasureType;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.referential.LengthLengthParameterDto;
import fr.ird.observe.services.dto.reference.ReferentialReference;
import fr.ird.observe.services.dto.referential.SizeMeasureTypeDto;
import fr.ird.observe.services.binder.referential.ReferentialBinderSupport;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class LengthLengthParameterBinder extends ReferentialBinderSupport<LengthLengthParameter, LengthLengthParameterDto> {

    public LengthLengthParameterBinder() {
        super(LengthLengthParameter.class, LengthLengthParameterDto.class);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, LengthLengthParameterDto dto, LengthLengthParameter entity) {

        copyDtoReferentialFieldsToEntity(dto, entity);
        entity.setInputOutputFormula(dto.getInputOutputFormula());
        entity.setOutputInputFormula(dto.getOutputInputFormula());
        entity.setInputSizeMeasureType(toEntity(dto.getInputSizeMeasureType(), SizeMeasureType.class));
        entity.setOutputSizeMeasureType(toEntity(dto.getOutputSizeMeasureType(), SizeMeasureType.class));
        entity.setCoefficients(dto.getCoefficients());
        entity.setSource(dto.getSource());

    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, LengthLengthParameter entity, LengthLengthParameterDto dto) {

        copyEntityReferentialFieldsToDto(entity, dto);
        dto.setInputOutputFormula(entity.getInputOutputFormula());
        dto.setOutputInputFormula(entity.getOutputInputFormula());
        dto.setInputSizeMeasureType(toReferentialReference(referentialLocale, entity.getInputSizeMeasureType(), SizeMeasureTypeDto.class));
        dto.setOutputSizeMeasureType(toReferentialReference(referentialLocale, entity.getOutputSizeMeasureType(), SizeMeasureTypeDto.class));
        dto.setCoefficients(entity.getCoefficients());
        dto.setSource(entity.getSource());

    }

    @Override
    public ReferentialReference<LengthLengthParameterDto> toReferentialReference(ReferentialLocale referentialLocale, LengthLengthParameter entity) {

        return toReferentialReference(entity,
                                      entity.getCode(),
                                      getLabel(referentialLocale, entity.getInputSizeMeasureType()),
                                      getLabel(referentialLocale, entity.getOutputSizeMeasureType()),
                                      entity.getInputOutputFormula(),
                                      entity.getOutputInputFormula());

    }

    @Override
    public ReferentialReference<LengthLengthParameterDto> toReferentialReference(ReferentialLocale referentialLocale, LengthLengthParameterDto dto) {

        return toReferentialReference(dto,
                                      dto.getCode(),
                                      getLabel(referentialLocale, dto.getInputSizeMeasureType()),
                                      getLabel(referentialLocale, dto.getOutputSizeMeasureType()),
                                      dto.getInputOutputFormula(),
                                      dto.getOutputInputFormula());

    }
}
