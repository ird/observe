package fr.ird.observe.services.binder.referential.common;

/*-
 * #%L
 * ObServe :: Services Binder
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.referentiel.Country;
import fr.ird.observe.entities.referentiel.ShipOwner;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.referential.CountryDto;
import fr.ird.observe.services.dto.reference.ReferentialReference;
import fr.ird.observe.services.dto.referential.ShipOwnerDto;
import fr.ird.observe.services.binder.referential.ReferentialBinderSupport;

/**
 * Created on 02/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class ShipOwnerBinder extends ReferentialBinderSupport<ShipOwner, ShipOwnerDto> {

    public ShipOwnerBinder() {
        super(ShipOwner.class, ShipOwnerDto.class);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, ShipOwnerDto dto, ShipOwner entity) {


        copyDtoReferentialFieldsToEntity(dto, entity);
        entity.setCountry(toEntity(dto.getCountry(), Country.class));
        entity.setLabel(dto.getLabel());

    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, ShipOwner entity, ShipOwnerDto dto) {

        copyEntityReferentialFieldsToDto(entity, dto);
        dto.setLabel(entity.getLabel());
        dto.setCountry(toReferentialReference(referentialLocale, entity.getCountry(), CountryDto.class));

    }

    @Override
    public ReferentialReference<ShipOwnerDto> toReferentialReference(ReferentialLocale referentialLocale, ShipOwner entity) {

        return toReferentialReference(entity,
                                      entity.getCode(), entity.getLabel());

    }

    @Override
    public ReferentialReference<ShipOwnerDto> toReferentialReference(ReferentialLocale referentialLocale, ShipOwnerDto dto) {

        return toReferentialReference(dto,
                                      dto.getCode(), dto.getLabel());

    }
}
