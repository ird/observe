package fr.ird.observe.services.binder.data.seine;

/*-
 * #%L
 * ObServe :: Services Binder
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.referentiel.Sex;
import fr.ird.observe.entities.referentiel.SizeMeasureType;
import fr.ird.observe.entities.referentiel.Species;
import fr.ird.observe.entities.seine.TargetLength;
import fr.ird.observe.services.dto.reference.DataReference;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.referential.SexDto;
import fr.ird.observe.services.dto.referential.SizeMeasureTypeDto;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.seine.TargetLengthDto;
import fr.ird.observe.services.binder.data.DataBinderSupport;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class TargetLengthBinder extends DataBinderSupport<TargetLength, TargetLengthDto> {

    public TargetLengthBinder() {
        super(TargetLength.class, TargetLengthDto.class);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, TargetLengthDto dto, TargetLength entity) {

        copyDtoDataFieldsToEntity(dto, entity);

        entity.setSpecies(toEntity(dto.getSpecies(), Species.class));
        entity.setSex(toEntity(dto.getSex(), Sex.class));
        entity.setSizeMeasureType(toEntity(dto.getSizeMeasureType(), SizeMeasureType.class));
        entity.setLength(dto.getLength());
        entity.setIsLengthComputed(dto.isIsLengthComputed());
        entity.setCount(dto.getCount());
        entity.setWeight(dto.getWeight());
        entity.setIsWeightComputed(dto.isIsWeightComputed());
        entity.setAcquisitionMode(dto.getAcquisitionMode());
        entity.setTagNumber(dto.getTagNumber());

    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, TargetLength entity, TargetLengthDto dto) {

        copyEntityDataFieldsToDto(entity, dto);

        dto.setSpecies(toReferentialReference(referentialLocale, entity.getSpecies(), SpeciesDto.class));
        dto.setSex(toReferentialReference(referentialLocale, entity.getSex(), SexDto.class));
        dto.setSizeMeasureType(toReferentialReference(referentialLocale,entity.getSizeMeasureType(), SizeMeasureTypeDto.class));
        dto.setLength(entity.getLength());
        dto.setIsLengthComputed(entity.isIsLengthComputed());
        dto.setCount(entity.getCount());
        dto.setWeight(entity.getWeight());
        dto.setIsWeightComputed(entity.isIsWeightComputed());
        dto.setAcquisitionMode(entity.getAcquisitionMode());
        dto.setTagNumber(entity.getTagNumber());

    }

    @Override
    public DataReference<TargetLengthDto> toDataReference(ReferentialLocale referentialLocale, TargetLength entity) {

        return toDataReference(entity,
                               getLabel(referentialLocale, entity.getSpecies()),
                               entity.getLength(),
                               entity.getCount());

    }

    @Override
    public DataReference<TargetLengthDto> toDataReference(ReferentialLocale referentialLocale, TargetLengthDto dto) {

        return toDataReference(dto,
                               getLabel(referentialLocale, dto.getSpecies()),
                               dto.getLength(),
                               dto.getCount());

    }
}
