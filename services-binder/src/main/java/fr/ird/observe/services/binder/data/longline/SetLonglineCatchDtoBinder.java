package fr.ird.observe.services.binder.data.longline;

/*-
 * #%L
 * ObServe :: Services Binder
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.longline.CatchLongline;
import fr.ird.observe.entities.longline.SetLongline;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.longline.CatchLonglineDto;
import fr.ird.observe.services.dto.longline.SetLonglineCatchDto;
import fr.ird.observe.services.binder.data.DataBinderSupport;

/**
 * Created on 24/11/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class SetLonglineCatchDtoBinder extends DataBinderSupport<SetLongline, SetLonglineCatchDto> {

    public SetLonglineCatchDtoBinder() {
        super(SetLongline.class, SetLonglineCatchDto.class);
    }

    @Override
    public void copyToEntity(ReferentialLocale referentialLocale, SetLonglineCatchDto dto, SetLongline entity) {

        copyDtoDataFieldsToEntity(dto, entity);

        entity.setCatchLongline(toEntityList(referentialLocale, dto.getCatchLongline(), CatchLongline.class, entity.getCatchLongline()));

    }

    @Override
    public void copyToDto(ReferentialLocale referentialLocale, SetLongline entity, SetLonglineCatchDto dto) {

        copyEntityDataFieldsToDto(entity, dto);

        dto.setCatchLongline(toListData(referentialLocale, entity.getCatchLongline(), CatchLonglineDto.class));

    }
}
