package fr.ird.observe.services.service.data;

/*-
 * #%L
 * ObServe :: Services
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Multimap;
import fr.ird.observe.services.dto.ObserveDto;
import fr.ird.observe.services.dto.longline.TripLonglineDto;
import fr.ird.observe.services.dto.reference.DataReference;
import fr.ird.observe.services.dto.reference.ReferentialReference;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.seine.TripSeineDto;

/**
 * Created on 22/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class NavigationResult implements ObserveDto {

    private final ImmutableSet<ReferentialReference<ProgramDto>> programs;
    private final ArrayListMultimap<ReferentialReference<ProgramDto>, DataReference<TripSeineDto>> tripsSeineByProgram;
    private final ArrayListMultimap<ReferentialReference<ProgramDto>, DataReference<TripLonglineDto>> tripsLonglineByProgram;

    public NavigationResult(ImmutableSet<ReferentialReference<ProgramDto>> programs, ArrayListMultimap<ReferentialReference<ProgramDto>, DataReference<TripSeineDto>> tripsSeineByProgram, ArrayListMultimap<ReferentialReference<ProgramDto>, DataReference<TripLonglineDto>> tripsLonglineByProgram) {
        this.programs = programs;
        this.tripsSeineByProgram = tripsSeineByProgram;
        this.tripsLonglineByProgram = tripsLonglineByProgram;
    }

    public ImmutableSet<ReferentialReference<ProgramDto>> getPrograms() {
        return programs;
    }

    public Multimap<ReferentialReference<ProgramDto>, DataReference<TripSeineDto>> getTripsSeineByProgram() {
        return tripsSeineByProgram;
    }

    public Multimap<ReferentialReference<ProgramDto>, DataReference<TripLonglineDto>> getTripsLonglineByProgram() {
        return tripsLonglineByProgram;
    }
}
