package fr.ird.observe.services.gson;

/*
 * #%L
 * ObServe :: Services
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author Arnaud Thimel - thimel@codelutin.com
 */
public class ClassAdapter implements JsonSerializer<Class<?>>, JsonDeserializer<Class<?>> {

    private static final Map<String, Class<?>> CLASS_CACHE = new TreeMap<>();

    static {
        CLASS_CACHE.put("boolean", boolean.class);
        CLASS_CACHE.put("byte", byte.class);
        CLASS_CACHE.put("char", char.class);
        CLASS_CACHE.put("short", short.class);
        CLASS_CACHE.put("int", int.class);
        CLASS_CACHE.put("long", long.class);
        CLASS_CACHE.put("float", float.class);
        CLASS_CACHE.put("double", double.class);
    }

    @Override
    public JsonElement serialize(Class<?> src, Type typeOfSrc, JsonSerializationContext context) {
        return context.serialize(src.getCanonicalName(), String.class);

    }

    @Override
    public Class<?> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        String className = json.getAsString();
        return CLASS_CACHE.computeIfAbsent(className, k -> loadClass(className));

    }

    protected Class<?> loadClass(String className) {

        try {
            return Class.forName(className);
        } catch (ClassNotFoundException e) {
            throw new JsonParseException("Class not found: " + className, e);
        }
    }


}
