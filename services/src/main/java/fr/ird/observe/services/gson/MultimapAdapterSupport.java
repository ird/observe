package fr.ird.observe.services.gson;

/*
 * #%L
 * ObServe :: Services
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Multimap;
import com.google.common.reflect.TypeToken;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public abstract class MultimapAdapterSupport<M extends Multimap> implements JsonDeserializer<M>, JsonSerializer<M> {

    protected abstract M createMultimap();

    @Override
    public M deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {

        Type[] actualTypeArguments = ((ParameterizedType) type).getActualTypeArguments();
        Type keyType = actualTypeArguments[0];
        Type valueType = actualTypeArguments[1];

        Type type2 = ObserveDtoGsonSupplier.multimapOf(TypeToken.of(keyType), TypeToken.of(valueType)).getType();
        Map map = context.deserialize(json, type2);

        M result = createMultimap();
        for (Object key : map.keySet()) {
            List multimapValues = (List) map.get(key);
            result.putAll(key, multimapValues);
        }
        return result;
    }

    @Override
    public JsonElement serialize(M src, Type typeOfSrc, JsonSerializationContext context) {
        return context.serialize(src.asMap());
    }
}
