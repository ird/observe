package fr.ird.observe.services.dto.referential;

/*-
 * #%L
 * ObServe :: Services
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.common.FormulaHelper;
import fr.ird.observe.common.WithFormula;
import fr.ird.observe.services.dto.WithComment;

import java.util.Map;

public class LengthLengthParameterDto extends GeneratedLengthLengthParameterDto implements WithFormula {

    private static final long serialVersionUID = 1L;

    private Map<String, Double> coefficientsValues;

    @Override
    public String getComment() {
        return getSource();
    }

    @Override
    public void setComment(String comment) {
        setSource(comment);
        firePropertyChange(WithComment.PROPERTY_COMMENT, comment);
    }

    @Override
    public void setCoefficients(String coefficients) {
        super.setCoefficients(coefficients);
        this.coefficientsValues = null;
        revalidateFormulaOne();
        revalidateFormulaTwo();
    }

    @Override
    public void setInputOutputFormula(String inputOutputFormula) {
        super.setInputOutputFormula(inputOutputFormula);
        revalidateFormulaOne();
    }

    @Override
    public void setOutputInputFormula(String outputInputFormula) {
        super.setOutputInputFormula(outputInputFormula);
        revalidateFormulaTwo();
    }

    @Override
    public Double getCoefficientValue(String coefficientName) {
        return getCoefficientValues().get(coefficientName);
    }

    public void revalidateFormulaOne() {
        boolean result = FormulaHelper.validateRelation(this, inputOutputFormula, FormulaHelper.VARIABLE_INPUT);
        setInputOutputFormulaValid(result);
    }

    public void revalidateFormulaTwo() {
        boolean result = FormulaHelper.validateRelation(this, outputInputFormula, FormulaHelper.VARIABLE_OUTPUT);
        setOutputInputFormulaValid(result);
    }

    @Override
    public Map<String, Double> getCoefficientValues() {
        if (coefficientsValues == null) {
            coefficientsValues = FormulaHelper.getCoefficientValues(this);
        }
        return coefficientsValues;
    }
}
