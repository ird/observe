package fr.ird.observe.services.dto.longline;

/*-
 * #%L
 * ObServe :: Services
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.services.dto.reference.DataReference;

public class ActivityLonglineHelper extends GeneratedActivityLonglineHelper {

    public final static String FISHING_OPERATION_ID =
            "fr.ird.observe.entities.referentiel.longline.VesselActivityLongline#1239832686138#0.1"; // operation de peche
    public final static ImmutableSet<String> SENSOR_USED_ID = ImmutableSet.of(
            FISHING_OPERATION_ID, // operation de peche
            "fr.ird.observe.entities.referentiel.longline.VesselActivityLongline#1239832686138#0.3" // station oceanographique
    );
    public final static ImmutableSet<String> ENCOUNTERS_ID = ImmutableSet.of(
            FISHING_OPERATION_ID, // operation de peche
            "fr.ird.observe.entities.referentiel.longline.VesselActivityLongline#1239832686138#0.4" // rencontre interaction
    );

    public static DataReference<SetLonglineDto> getSetLongline(DataReference<ActivityLonglineDto> data) {
        return (DataReference<SetLonglineDto>) data.getPropertyValue(ActivityLonglineDto.PROPERTY_SET_LONGLINE);
    }

    public static String getVeseelActivityId(DataReference<ActivityLonglineDto> data) {
        return (String) data.getPropertyValue(ActivityLonglineDto.PROPERTY_VESSEL_ACTIVITY_LONGLINE + "Id");
    }
} 
