package fr.ird.observe.services.dto;

/*-
 * #%L
 * ObServe :: Services
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Comparator;

public class ObserveDbUserHelper extends GeneratedObserveDbUserHelper {

    protected static final UserDtoComparator USER_DTO_COMPARATOR = new UserDtoComparator();

    public static UserDtoComparator getUserDtoComparator() {
        return USER_DTO_COMPARATOR;
    }

    protected static class UserDtoComparator implements Comparator<ObserveDbUserDto> {

        @Override
        public int compare(ObserveDbUserDto user1, ObserveDbUserDto user2) {
            return user1.getName().compareTo(user2.getName());
        }
    }


}
