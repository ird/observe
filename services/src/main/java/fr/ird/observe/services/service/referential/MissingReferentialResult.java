package fr.ird.observe.services.service.referential;

/*-
 * #%L
 * ObServe :: Services
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.SetMultimap;
import fr.ird.observe.services.dto.referential.ReferentialDto;

/**
 * Created on 07/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class MissingReferentialResult {

    public static MissingReferentialResult of(SetMultimap<Class<? extends ReferentialDto>, String> missingReferentialIds, byte[] sqlCode) {
        return new MissingReferentialResult(missingReferentialIds, sqlCode);
    }

    private final SetMultimap<Class<? extends ReferentialDto>, String> missingIds;
    private final byte[] sqlCode;

    private MissingReferentialResult(SetMultimap<Class<? extends ReferentialDto>, String> missingIds, byte[] sqlCode) {
        this.missingIds = missingIds;
        this.sqlCode = sqlCode;
    }

    public SetMultimap<Class<? extends ReferentialDto>, String> getMissingIds() {
        return missingIds;
    }

    public byte[] getSqlCode() {
        return sqlCode;
    }
}
