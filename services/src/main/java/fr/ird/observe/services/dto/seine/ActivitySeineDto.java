package fr.ird.observe.services.dto.seine;

/*-
 * #%L
 * ObServe :: Services
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.reference.ReferentialReference;
import fr.ird.observe.services.dto.referential.seine.VesselActivitySeineDto;
import fr.ird.observe.services.dto.referential.seine.VesselActivitySeineHelper;
import org.nuiton.util.DateUtil;

import java.util.Date;

public class ActivitySeineDto extends GeneratedActivitySeineDto {

    public static final String PROPERTY_SET_SEINE = "setSeine";

    public static final String PROPERTY_SET_OPERATION = "setOperation";

    public static final String PROPERTY_DCP_OPERATION = "dcpOperation";

    private static final long serialVersionUID = 1L;

    public boolean isActivityFinDeVeille() {
        return VesselActivitySeineHelper.isActivityFinDeVeille(vesselActivitySeine);
    }

    public boolean isDcpOperation() {
        return VesselActivitySeineHelper.isDcpOperation(vesselActivitySeine);
    }

    public boolean isSetOperation() {
        return VesselActivitySeineHelper.isSetOperation(vesselActivitySeine);
    }

    @Override
    public void setVesselActivitySeine(ReferentialReference<VesselActivitySeineDto> vesselActivitySeine) {
        super.setVesselActivitySeine(vesselActivitySeine);
        firePropertyChange(PROPERTY_DCP_OPERATION, null, isDcpOperation());
        firePropertyChange(PROPERTY_SET_OPERATION, null, isSetOperation());
    }

    public Date getTimeSecond() {
        return DateUtil.getTime(time, false, false);
    }
}
