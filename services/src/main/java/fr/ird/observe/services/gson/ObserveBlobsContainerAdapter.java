package fr.ird.observe.services.gson;

/*-
 * #%L
 * ObServe :: Services
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import com.google.common.reflect.TypeToken;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import fr.ird.observe.services.dto.ObserveBlobsContainer;

import java.lang.reflect.Type;
import java.util.Map;

/**
 * Created on 10/12/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.1
 */
public class ObserveBlobsContainerAdapter implements JsonDeserializer<ObserveBlobsContainer>, JsonSerializer<ObserveBlobsContainer> {
    @Override
    public ObserveBlobsContainer deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();
        String tableName = jsonObject.get("tableName").getAsString();
        String columnName = jsonObject.get("columnName").getAsString();
        Type mapType = ObserveDtoGsonSupplier.mapOf(TypeToken.of(String.class), TypeToken.of(byte[].class)).getType();
        Map<String, byte[]> blobsById = context.deserialize(jsonObject.get("blobsById"), mapType);
        return new ObserveBlobsContainer(tableName, columnName, ImmutableMap.copyOf(blobsById));
    }

    @Override
    public JsonElement serialize(ObserveBlobsContainer src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject element = new JsonObject();
        element.add("tableName", context.serialize(src.getTableName()));
        element.add("columnName", context.serialize(src.getColumnName()));
        element.add("blobsById", context.serialize(src.getBlobsById().asMultimap().asMap()));
        return element;
    }
}
