package fr.ird.observe.services.dto.referential.seine;

/*-
 * #%L
 * ObServe :: Services
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.reference.ReferentialReference;

public class VesselActivitySeineHelper extends GeneratedVesselActivitySeineHelper {

    private static final String VESSEL_ACTIVITY_ID_FOR_SET = "fr.ird.observe.entities.referentiel.seine.VesselActivitySeine#1239832675369#0.12552908048322586";

    public static final String VESSEL_ACTIVITY_ID_CHANGED_ZONE = "fr.ird.observe.entities.referentiel.seine.VesselActivitySeine#1379684416896#0.38648073770690594";

    public static final String ACTIVITY_FIN_DE_VEILLE = "16";

    public static final String ACTIVITY_DEBUT_DE_PECHE = "6";

    public static boolean isSetOpreration(String id) {
        return VESSEL_ACTIVITY_ID_FOR_SET.equals(id);
    }

    public static boolean isSetOperation(ReferentialReference<VesselActivitySeineDto> reference) {
        return reference != null && isSetOpreration(reference.getId());
    }

    public static boolean isDcpOperation(ReferentialReference<VesselActivitySeineDto> reference) {
        boolean result = reference != null;
        if (result) {
            result = (boolean) reference.getPropertyValue(VesselActivitySeineDto.PROPERTY_ALLOW_FAD);
        }
        return result;
    }

    public static boolean isActivityFinDeVeille(ReferentialReference<VesselActivitySeineDto> reference) {
        return reference != null
                && ACTIVITY_FIN_DE_VEILLE.equals(reference.getPropertyValue(VesselActivitySeineDto.PROPERTY_CODE));
    }

    public static boolean isActivityPeche(ReferentialReference<VesselActivitySeineDto> reference) {
        return reference != null
                && ACTIVITY_DEBUT_DE_PECHE.equals(reference.getPropertyValue(VesselActivitySeineDto.PROPERTY_CODE));
    }


    public static boolean isChangedZoneOperation(String id) {
        return VESSEL_ACTIVITY_ID_CHANGED_ZONE.equals(id);
    }

    public static boolean isChangedZoneOperation(ReferentialReference<VesselActivitySeineDto> vesselActivitySeineRef) {
        return vesselActivitySeineRef != null && isChangedZoneOperation(vesselActivitySeineRef.getId());
    }

} //VesselActivitySeineHelper
