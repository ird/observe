package fr.ird.observe.services.dto.referential;

/*-
 * #%L
 * ObServe :: Services
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.services.dto.reference.ReferentialReference;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class SizeMeasureTypeHelper extends GeneratedSizeMeasureTypeHelper {

    public static final ImmutableSet<String> SEINE_LD1_SIZE_MEASURE_TYPE_ID = ImmutableSet.of("PDL", "LD1");
    public static final ImmutableSet<String> SEINE_LF_SIZE_MEASURE_TYPE_ID = ImmutableSet.of("FL", "LF");

    public static List<ReferentialReference<SizeMeasureTypeDto>> filterForSeine(Collection<ReferentialReference<SizeMeasureTypeDto>> incoming) {
        return incoming.stream()
                       .filter(d -> isLd1(d) || isLf(d))
                       .collect(Collectors.toList());
    }

    public static boolean isLd1(ReferentialReference<SizeMeasureTypeDto> incoming) {
        return SEINE_LD1_SIZE_MEASURE_TYPE_ID.contains(incoming.getCode());
    }

    public static boolean isLf(ReferentialReference<SizeMeasureTypeDto> incoming) {
        return SEINE_LF_SIZE_MEASURE_TYPE_ID.contains(incoming.getCode());
    }

    public static ReferentialReference<SizeMeasureTypeDto> getLd1(Collection<ReferentialReference<SizeMeasureTypeDto>> incoming) {
        return incoming.stream()
                       .filter(SizeMeasureTypeHelper::isLd1)
                       .findAny().orElse(null);

    }

    public static ReferentialReference<SizeMeasureTypeDto> getLf(Collection<ReferentialReference<SizeMeasureTypeDto>> incoming) {
        return incoming.stream()
                       .filter(SizeMeasureTypeHelper::isLf)
                       .findAny().orElse(null);

    }
}
