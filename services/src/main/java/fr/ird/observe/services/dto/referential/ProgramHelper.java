package fr.ird.observe.services.dto.referential;

/*-
 * #%L
 * ObServe :: Services
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.common.constants.GearType;
import fr.ird.observe.services.dto.reference.ReferentialReference;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class ProgramHelper extends GeneratedProgramHelper {

    public static boolean isProgramLongline(ReferentialReference<ProgramDto> programDtoRef) {

        boolean result = false;

        if (programDtoRef.getPropertyNames().contains(ProgramDto.PROPERTY_GEAR_TYPE)) {

            GearType gearType = (GearType) programDtoRef.getPropertyValue(ProgramDto.PROPERTY_GEAR_TYPE);

            result = GearType.longline.equals(gearType);
        }

        return result;
    }

    public static boolean isProgramSeine(ReferentialReference<ProgramDto> programDtoRef) {

        boolean result = false;

        if (programDtoRef.getPropertyNames().contains(ProgramDto.PROPERTY_GEAR_TYPE)) {

            GearType gearType = (GearType) programDtoRef.getPropertyValue(ProgramDto.PROPERTY_GEAR_TYPE);

            result = GearType.seine.equals(gearType);
        }

        return result;
    }

    public static int getTripCount(ReferentialReference<ProgramDto> reference) {
        return (int) reference.getPropertyValue(ProgramDto.PROPERTY_TRIP_COUNT);
    }

    public static List<ReferentialReference<ProgramDto>> sort(List<ReferentialReference<ProgramDto>> data) {
        Map<Object, ReferentialReference<ProgramDto>> map = new TreeMap<>();
        for (ReferentialReference<ProgramDto> datum : data) {
            map.put(datum.getPropertyValue(ProgramDto.PROPERTY_GEAR_TYPE) + "" + datum.getPropertyValue(ProgramDto.PROPERTY_LABEL), datum);
        }
        return new ArrayList<>(map.values());
    }

    public static List<ReferentialReference<ProgramDto>> filterGearType(List<ReferentialReference<ProgramDto>> programs, GearType gearType) {

        return programs.stream()
                       .filter(r -> gearType.equals(r.getPropertyValue(ProgramDto.PROPERTY_GEAR_TYPE)))
                .collect(Collectors.toList());
    }

}
