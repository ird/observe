package fr.ird.observe.services.dto.referential;

/*-
 * #%L
 * ObServe :: Services
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.common.FormulaHelper;
import fr.ird.observe.common.WithFormula;
import fr.ird.observe.services.dto.WithComment;

import java.util.Map;

public class LengthWeightParameterDto extends GeneratedLengthWeightParameterDto implements WithFormula {

    private static final long serialVersionUID = 1L;

    private Map<String, Double> coefficientsValues;

    @Override
    public String getComment() {
        return getSource();
    }

    @Override
    public void setComment(String comment) {
        setSource(comment);
        firePropertyChange(WithComment.PROPERTY_COMMENT, comment);
    }

    @Override
    public void setCoefficients(String coefficients) {
        super.setCoefficients(coefficients);
        this.coefficientsValues = null;
        revalidateFormulaOne();
        revalidateFormulaTwo();
    }

    @Override
    public void setLengthWeightFormula(String value) {
        super.setLengthWeightFormula(value);
        revalidateFormulaOne();
    }

    @Override
    public void setWeightLengthFormula(String value) {
        super.setWeightLengthFormula(value);
        revalidateFormulaTwo();
    }

    @Override
    public Double getCoefficientValue(String coefficientName) {
        return getCoefficientValues().get(coefficientName);
    }

    @Override
    public Map<String, Double> getCoefficientValues() {
        if (coefficientsValues == null) {
            coefficientsValues = FormulaHelper.getCoefficientValues(this);
        }
        return coefficientsValues;
    }

    public void revalidateFormulaOne() {
        boolean result = FormulaHelper.validateRelation(this, lengthWeightFormula, FormulaHelper.VARIABLE_LENGTH);
        setLengthWeightFormulaValid(result);
    }

    public void revalidateFormulaTwo() {
        boolean result = FormulaHelper.validateRelation(this, weightLengthFormula, FormulaHelper.VARIABLE_WEIGHT);
        setWeightLengthFormulaValid(result);
    }
}
