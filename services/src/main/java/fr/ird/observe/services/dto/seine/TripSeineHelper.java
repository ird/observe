package fr.ird.observe.services.dto.seine;

/*-
 * #%L
 * ObServe :: Services
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.reference.DataReference;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class TripSeineHelper extends GeneratedTripSeineHelper {

    public static int getRouteCount(DataReference<TripSeineDto> data) {
        return (int) data.getPropertyValue(TripSeineDto.PROPERTY_ROUTE_COUNT);
    }

    public static String getProgramId(DataReference<TripSeineDto> data) {
        return (String) data.getPropertyValue(TripSeineDto.PROPERTY_PROGRAM + "Id");
    }

    public static List<DataReference<TripSeineDto>> sort(List<DataReference<TripSeineDto>> data) {
        Map<Object, DataReference<TripSeineDto>> map = new TreeMap<>();
        for (DataReference<TripSeineDto> datum : data) {
            map.put(datum.getPropertyValue(TripSeineDto.PROPERTY_START_DATE), datum);
        }
        return new ArrayList<>(map.values());
    }
}
