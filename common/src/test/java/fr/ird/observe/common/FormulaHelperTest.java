package fr.ird.observe.common;

/*-
 * #%L
 * ObServe :: Common
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import org.junit.Assert;
import org.junit.Test;

import java.util.Map;

/**
 * Created on 05/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class FormulaHelperTest {

    @Test
    public void testComputeValue() {

        WithFormula parametrage = new WithFormula() {

            @Override
            public String getCoefficients() {
                return "a=3.8e-5:b=2.78 ";
            }

            @Override
            public Double getCoefficientValue(String coefficientName) {
                return getCoefficientValues().get(coefficientName);
            }

            @Override
            public Map<String, Double> getCoefficientValues() {
                return ImmutableMap.of("a", 3.8e-5, "b", 2.78);
            }
        };

        Float weight = FormulaHelper.computeValue(parametrage, "a * Math.pow(L, b)", null, "L", 84.0f);
        Assert.assertNotNull(weight);

        Float excepted = (float) (Math.pow(84.0, 2.78) * 3.8e-5);
        Assert.assertEquals(excepted, weight, 2);
    }
}
