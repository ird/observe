package fr.ird.observe.common;

/*-
 * #%L
 * ObServe :: Common
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.NumberUtil;

import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created on 05/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class FormulaHelper {

    /** Logger */
    private static final Log log = LogFactory.getLog(FormulaHelper.class);

    private static final Pattern COEFFICIENTS_PATTERN = Pattern.compile("(.+)=(.+)");

    /** variable weight à utiliser dans la relation taille */
    public static final String VARIABLE_WEIGHT = "P";

    /** variable taille à utiliser dans la relation weight */
    public static final String VARIABLE_LENGTH = "L";

    public static final String VARIABLE_INPUT = "I";

    public static final String VARIABLE_OUTPUT = "O";

    public static final String COEFFICIENT_A = "a";

    public static final String COEFFICIENT_B = "b";

    /** moteur d'évaluation d'expression */
    protected static ScriptEngine scriptEngine;


    protected static ScriptEngine getScriptEngine() {
        if (scriptEngine == null) {
            ScriptEngineManager factory = new ScriptEngineManager();

            scriptEngine = factory.getEngineByExtension("js");
        }
        return scriptEngine;
    }

    public static Map<String, Double> getCoefficientValues(WithFormula parametrage) {

        Map<String, Double> result = new TreeMap<>();
        String coefficients = parametrage.getCoefficients();
        if (coefficients != null) {
            for (String coefficientDef : coefficients.split(":")) {
                Matcher matcher = COEFFICIENTS_PATTERN.matcher(coefficientDef.trim());
                if (log.isDebugEnabled()) {
                    log.debug("constant to test = " + coefficientDef);
                }
                if (matcher.matches()) {

                    String key = matcher.group(1);
                    String val = matcher.group(2);
                    try {
                        Double d = Double.valueOf(val);
                        result.put(key, d);
                        if (log.isDebugEnabled()) {
                            log.debug("detects coefficient " + key + '=' + val);
                        }
                    } catch (NumberFormatException e) {
                        // pas pu recupere le count...
                        if (log.isWarnEnabled()) {
                            log.warn("could not parse double " + val + " for coefficient " + key);
                        }
                    }
                }
            }
        }
        return result;
    }

    public static boolean validateRelation(WithFormula parametrage, String relation, String variable) {
        boolean result = false;
        if (!StringUtils.isEmpty(relation)) {

            Map<String, Double> coeffs = parametrage.getCoefficientValues();

            ScriptEngine engine = getScriptEngine();
            Bindings bindings = engine.createBindings();
            addBindings(coeffs, bindings, variable, 1);

            try {
                engine.setBindings(bindings, ScriptContext.ENGINE_SCOPE);
                Double o = (Double) engine.eval("parseFloat(" + relation + ")");
                if (log.isDebugEnabled()) {
                    log.debug("evaluation ok : " + relation + " (" + variable + "=1) = " + o);
                }
                result = true;
            } catch (Exception e) {
                if (log.isDebugEnabled()) {
                    log.debug("evalution ko : " + relation + ", reason : " + e.getMessage());
                }
            }
        }
        return result;
    }

    public static Float computeValue(WithFormula parametrage, String formula, String coefficientName, String variableName, float data) {
        if (coefficientName != null) {
            Double b = parametrage.getCoefficientValue(coefficientName);
            if (b == 0) {

                // ce cas limite ne permet pas de calculer la taille a partir du weight
                return null;
            }
        }
        Float o = computeValue(parametrage, formula, variableName, data);

        if (o != null) {
            o = NumberUtil.roundOneDigit(o);
        }
        return o;
    }

    private static Float computeValue(WithFormula parametrage, String relation, String variable, float taille) {
        Map<String, Double> coeffs = parametrage.getCoefficientValues();
        ScriptEngine engine = getScriptEngine();
        Bindings bindings = engine.createBindings();
        addBindings(coeffs, bindings, variable, taille);

        engine.setBindings(bindings, ScriptContext.ENGINE_SCOPE);
        Double o = null;
        try {
            o = (Double) engine.eval("parseFloat(" + relation + ")");
        } catch (ScriptException e) {
            if (log.isErrorEnabled()) {
                log.error("Could not compute value from " + relation);
            }
        }
        return o == null ? null : o.floatValue();
    }

    private static void addBindings(Map<String, Double> coeffs, Bindings bindings, String variable, float taille) {
        for (Map.Entry<String, Double> entry : coeffs.entrySet()) {
            String key = entry.getKey();
            Double value = entry.getValue();
            bindings.put(key, value);

            if (log.isDebugEnabled()) {
                log.debug("add constant " + key + '=' + value);
            }
        }
        bindings.put(variable, taille);
    }


}
