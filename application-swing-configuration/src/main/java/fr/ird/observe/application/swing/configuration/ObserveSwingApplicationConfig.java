/*
 * #%L
 * ObServe :: Application Swing Configuration
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.configuration;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import com.google.gson.Gson;
import fr.ird.observe.application.swing.configuration.constants.CreationMode;
import fr.ird.observe.application.swing.configuration.constants.DbMode;
import fr.ird.observe.services.dto.presets.RemoteDataSourceConfiguration;
import fr.ird.observe.services.dto.presets.ServerDataSourceConfiguration;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.config.ArgumentsParserException;
import org.nuiton.jaxx.widgets.gis.CoordinateFormat;
import org.nuiton.version.Version;
import org.nuiton.version.Versions;

import java.beans.PropertyChangeListener;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * La configuration de l'application.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class ObserveSwingApplicationConfig extends GeneratedObserveSwingApplicationConfig {

    /** le pattern du fichier de sauvegarde d'une base locale */
    private static final String BACKUP_DB_PATTERN = "obstuna-local-%1$tF--%1$tk-%1$tM-%1$tS.sql.gz";

    public static final String DB_NAME = "obstuna";

    private static final String APPLICATION_VERSION = "application.version";

    private static final String VERSION = "version";

    private static final String PROPERTY_LOCAL_STORAGE_EXIST = "localStorageExist";

    private static final String PROPERTY_INITIAL_DUMP_EXIST = "initialDumpExist";

    private static final String PROPERTY_MAIN_STORAGE_OPENED = "mainStorageOpened";

    /** Logger */
    private static final Log log = LogFactory.getLog(ObserveSwingApplicationConfig.class);

    static final String DEFAULT_OBSERVE_SWING_CONFIGURATION_FILENAME = "observeSwing.conf";

    /**
     * un drapeau pour savoir s'il faut lancer l'interface graphique. Cette
     * valeur peut être programmées lors des actions.
     */
    private boolean displayMainUI = true;

    /** drapeau pour savoir si une base locale existe */
    private boolean localStorageExist;

    /**
     * drapeau pour savoir si le dump initial a ete chargée depuis la base
     * centrale
     */
    private boolean initialDumpExist;

    /** drapeau pour savoir si une source de données est ouverte */
    private boolean mainStorageOpened;

    /**
     * drapeau pour savoir si la source ouverte est locale (attention, lorsqu'il
     * n'y a pas de storage d'ouvert, la valeur est null).
     */
    private Boolean mainStorageOpenedLocal;

    /**
     * drapeau pour savoir si on peut utiliser des ui dans l'environnement.
     *
     * Par défaut, on suppose qu'on peut utiliser l'environnement graphique et
     * si on désactive explicitement ou si pas d'environnement graphique
     * trouvé.
     */
    private boolean canUseUI = true;

    /** La version de l'application */
    private Version version;

    /** Texte du copyright (calculé dynamiquement). */
    private String copyrightText;

    /**
     * Liste des options qu'on ne peut pas sauvegarder (les mots de passes,
     * les options d'admin pour les simples utilisateurs,...).
     *
     * @since 1.5
     */
    private final String[] unsavables = new String[]{
//            ObserveSwingApplicationConfigOption.OBSTUNA_PASSWORD.getKey(),
            ObserveSwingApplicationConfigOption.H2_PASSWORD.getKey(),
            ObserveSwingApplicationConfigOption.OBSTUNA_CAN_MIGRATE.getKey(),
    };

    //FIXME
    private final ImmutableSet<String> longlinVesselTypeIds = ImmutableSet.copyOf(
            Sets.newHashSet("fr.ird.observe.entities.referentiel.VesselType#1239832675735#0.044156847891821505",
                            "fr.ird.observe.entities.referentiel.VesselType#1239832675736#0.8708229847859869",
                            "fr.ird.observe.entities.referentiel.VesselType#1239832686137#0.1"));

    private final ImmutableSet<String> seineVesselTypeIds = ImmutableSet.copyOf(
            Sets.newHashSet("fr.ird.observe.entities.referentiel.VesselType#1239832675735#0.044156847891821505",
                            "fr.ird.observe.entities.referentiel.VesselType#1239832675735#0.307197212385357",
                            "fr.ird.observe.entities.referentiel.VesselType#1239832675735#0.7380146830307519",
                            "fr.ird.observe.entities.referentiel.VesselType#1239832675735#0.9086075071905084",
                            "fr.ird.observe.entities.referentiel.VesselType#1239832675737#0.43324169605639407"));

    public ObserveSwingApplicationConfig() {
        this(DEFAULT_OBSERVE_SWING_CONFIGURATION_FILENAME);
    }

    public ObserveSwingApplicationConfig(String confFileName) {
        ApplicationConfig delegate = get();
        delegate.setEncoding(StandardCharsets.UTF_8.name());
        delegate.setConfigFileName(confFileName);
    }

    public void initConfig(Properties p, String... args) throws ArgumentsParserException {

        Version version = null;
        ApplicationConfig applicationConfig = get();

        for (Object k : p.keySet()) {
            String key = String.valueOf(k);
            Object value = p.get(k);
            if (log.isDebugEnabled()) {
                log.debug("install property [" + k + "] : " + value);
            }
            String strValue = String.valueOf(value);
            if (APPLICATION_VERSION.equals(key)) {
                version = Versions.valueOf(strValue);
                if (version.isSnapshot()) {
                    version = Versions.removeSnapshot(version);
                }

            } else {

                applicationConfig.setDefaultOption(key, strValue);
            }
        }

        if (version == null) {
            throw new IllegalStateException("No application.version found in application configuration.");
        }

        setVersion(version);
        applicationConfig.setDefaultOption(VERSION, version.getVersion());
        applicationConfig.setDefaultOption(APPLICATION_VERSION, version.getVersion());

        applicationConfig.parse(args);

    }

    @Override
    public DbMode getDefaultDbMode() {
        return DbMode.valueOf(get().getOption(ObserveSwingApplicationConfigOption.DEFAULT_DB_MODE.getKey()));
    }

    @Override
    public CreationMode getDefaultCreationMode() {
        return CreationMode.valueOf(get().getOption(ObserveSwingApplicationConfigOption.DEFAULT_CREATION_MODE.getKey()));
    }

    @Override
    public CoordinateFormat getCoordinateFormat() {
        return CoordinateFormat.valueOf(get().getOption(ObserveSwingApplicationConfigOption.COORDINATE_FORMAT.getKey()));
    }

    public String getCopyrightText() {
        if (copyrightText == null) {
            Date date = new Date();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            int year = calendar.get(Calendar.YEAR);
            copyrightText = "Version " + getVersion() + " IRD @ 2008-" + year;
        }
        return copyrightText;
    }

    public Version getVersion() {
        return version;
    }

    public boolean isShowTimeEditorSlider() {
        return isShowDateTimeEditorSlider();
    }

    public File getLocalDBDirectory() {
        return getDbDirectory();
    }

    public boolean isDisplayMainUI() {
        return displayMainUI;
    }

    public boolean isCanUseUI() {
        return canUseUI;
    }

    public boolean isLocalStorageExist() {
        return localStorageExist;
    }

    public boolean isInitialDumpExist() {
        return initialDumpExist;
    }

    public boolean isMainStorageOpened() {
        return mainStorageOpened;
    }

    public Boolean getMainStorageOpenedLocal() {
        return mainStorageOpenedLocal;
    }

    public void setLocalStorageExist(boolean newValue) {
        localStorageExist = newValue;
        // always force propagation
        firePropertyChange(PROPERTY_LOCAL_STORAGE_EXIST, newValue);
    }

    public void setInitialDumpExist(boolean newValue) {
        initialDumpExist = newValue;
        // always force propagation
        firePropertyChange(PROPERTY_INITIAL_DUMP_EXIST, newValue);
    }

    public void setMainStorageOpened(boolean newValue) {
        mainStorageOpened = newValue;
        if (!newValue) {
            // on force la reinitialisation
            setMainStorageOpenedLocal(null);
        }
        // always force propagation
        firePropertyChange(PROPERTY_MAIN_STORAGE_OPENED, newValue);
    }

    public void setMainStorageOpenedLocal(Boolean newValue) {
        mainStorageOpenedLocal = newValue;
        // always force propagation
        firePropertyChange(PROPERTY_MAIN_STORAGE_OPENED + "Local", newValue);
    }

    public void setDisplayMainUI(boolean b) {
        displayMainUI = b;
    }

    public void setCanUseUI(boolean canUseUI) {
        this.canUseUI = canUseUI;
        if (!canUseUI) {
            // on ne pourra pas lancer l'ui principale
            setDisplayMainUI(false);
        }
    }

    public static final List<ObserveSwingApplicationConfigOption> MAP_LAYERS = ImmutableList.of(
            ObserveSwingApplicationConfigOption.MAP_LAYER1,
            ObserveSwingApplicationConfigOption.MAP_LAYER2,
            ObserveSwingApplicationConfigOption.MAP_LAYER3,
            ObserveSwingApplicationConfigOption.MAP_LAYER4,
            ObserveSwingApplicationConfigOption.MAP_LAYER5,
            ObserveSwingApplicationConfigOption.MAP_LAYER6,
            ObserveSwingApplicationConfigOption.MAP_LAYER7,
            ObserveSwingApplicationConfigOption.MAP_LAYER8,
            ObserveSwingApplicationConfigOption.MAP_LAYER9,
            ObserveSwingApplicationConfigOption.MAP_LAYER10);

    public List<File> getMapLayerFiles() {
        List<File> layers = new LinkedList<>();

        for (ObserveSwingApplicationConfigOption layerOption : MAP_LAYERS) {
            File layerFile = get().getOptionAsFile(layerOption.getKey());
            if (layerFile != null && layerFile.exists()) {
                layers.add(layerFile);
            }
        }
        return layers;
    }

    public String[] getTreeOpenNodeIds() {
        String ids = getTreeOpenNodes();
        String[] result = null;
        if (ids != null) {
            result = ids.split(",");
        }
        return result;
    }

    public void saveTreeOpenNodeIds(String... ids) {
        setTreeOpenNodes(StringUtils.join(ids, ","));
        saveForUser();
    }

    public File newBackupDataFile() {
        return new File(getBackupDirectory(), String.format(BACKUP_DB_PATTERN, new Date()));
    }

    public void saveForUser() {
        if (log.isInfoEnabled()) {
            log.info(t("observe.message.save.configuration", get().getUserConfigFile()));
        }
        get().saveForUser(unsavables);
    }

    public void removeJaxxPropertyChangeListener() {
//        pcs.getPropertyChangeListeners();
//        List<String> tmp = new ArrayList<>();
//        for (ObserveSwingApplicationConfigOption option : ObserveSwingApplicationConfigOption.values()) {
//            String propertyName = option.getPropertyKey();
//            if (propertyName != null) {
//                tmp.add(propertyName);
//            }
//        }
//        if (log.isDebugEnabled()) {
//            log.debug("property names to seek for options : " + tmp);
//        }
//        String[] propertyNames = tmp.toArray(
//                new String[tmp.size()]);
//
//        PropertyChangeListener[] toRemove =
//                SwingUtil.findJaxxPropertyChangeListener(
//                        propertyNames,
//                        getPropertyChangeListeners());
//        if (toRemove == null || toRemove.length == 0) {
//            return;
//        }
        if (log.isDebugEnabled()) {
            log.debug("before remove : " + pcs.getPropertyChangeListeners().length);
//            log.debug("toRemove : " + toRemove.length);
        }
        for (PropertyChangeListener listener : pcs.getPropertyChangeListeners()) {
            removePropertyChangeListener(listener);
        }
        if (log.isDebugEnabled()) {
            log.debug("after remove : " + pcs.getPropertyChangeListeners().length);
        }
    }

    private void setVersion(Version version) {
        this.version = version;
    }

    public Set<String> getSeineVesselTypeIds() {
        return seineVesselTypeIds;
    }

    public Set<String> getLonglineVesselTypeIds() {
        return longlinVesselTypeIds;
    }

    @Override
    public RemoteDataSourceConfiguration[] getRemoteDataSourceConfigurations() {
        String option = get().getOption(ObserveSwingApplicationConfigOption.REMOTE_DATA_SOURCE_CONFIGURATIONS.getKey());
        return option == null ? new RemoteDataSourceConfiguration[0] : new Gson().fromJson(option, RemoteDataSourceConfiguration[].class);
    }

    public void addRemoteDataSourceConfiguration(RemoteDataSourceConfiguration remoteDataSourceConfiguration) {
        List<RemoteDataSourceConfiguration> remoteDataSourceConfigurations = new ArrayList<>(Arrays.asList(getRemoteDataSourceConfigurations()));
        remoteDataSourceConfigurations.add(remoteDataSourceConfiguration);
        setRemoteDataSourceConfigurations(remoteDataSourceConfigurations.toArray(new RemoteDataSourceConfiguration[remoteDataSourceConfigurations.size()]));
        saveForUser();
    }

    public void updateRemoteDataSourceConfiguration(RemoteDataSourceConfiguration remoteDataSourceConfiguration) {
        List<RemoteDataSourceConfiguration> remoteDataSourceConfigurations = new ArrayList<>(Arrays.asList(getRemoteDataSourceConfigurations()));
        remoteDataSourceConfigurations.remove(remoteDataSourceConfiguration);
        remoteDataSourceConfigurations.add(remoteDataSourceConfiguration);
        setRemoteDataSourceConfigurations(remoteDataSourceConfigurations.toArray(new RemoteDataSourceConfiguration[remoteDataSourceConfigurations.size()]));
        saveForUser();
    }

    public void removeRemoteDataSourceConfiguration(RemoteDataSourceConfiguration remoteDataSourceConfiguration) {
        List<RemoteDataSourceConfiguration> remoteDataSourceConfigurations = new ArrayList<>(Arrays.asList(getRemoteDataSourceConfigurations()));
        remoteDataSourceConfigurations.remove(remoteDataSourceConfiguration);
        setRemoteDataSourceConfigurations(remoteDataSourceConfigurations.toArray(new RemoteDataSourceConfiguration[remoteDataSourceConfigurations.size()]));
        saveForUser();
    }

    @Override
    public void setRemoteDataSourceConfigurations(RemoteDataSourceConfiguration[] remoteDataSourceConfigurations) {
        String option = new Gson().toJson(remoteDataSourceConfigurations);
        get().setOption(ObserveSwingApplicationConfigOption.REMOTE_DATA_SOURCE_CONFIGURATIONS.getKey(), option);
    }

    @Override
    public ServerDataSourceConfiguration[] getServerDataSourceConfigurations() {
        String option = get().getOption(ObserveSwingApplicationConfigOption.SERVER_DATA_SOURCE_CONFIGURATIONS.getKey());
        return option == null ? new ServerDataSourceConfiguration[0] : new Gson().fromJson(option, ServerDataSourceConfiguration[].class);
    }

    public void addServerDataSourceConfiguration(ServerDataSourceConfiguration remoteDataSourceConfiguration) {
        List<ServerDataSourceConfiguration> remoteDataSourceConfigurations = new ArrayList<>(Arrays.asList(getServerDataSourceConfigurations()));
        remoteDataSourceConfigurations.add(remoteDataSourceConfiguration);
        setServerDataSourceConfigurations(remoteDataSourceConfigurations.toArray(new ServerDataSourceConfiguration[remoteDataSourceConfigurations.size()]));
        saveForUser();
    }

    public void updateServerDataSourceConfiguration(ServerDataSourceConfiguration remoteDataSourceConfiguration) {
        List<ServerDataSourceConfiguration> remoteDataSourceConfigurations = new ArrayList<>(Arrays.asList(getServerDataSourceConfigurations()));
        remoteDataSourceConfigurations.remove(remoteDataSourceConfiguration);
        remoteDataSourceConfigurations.add(remoteDataSourceConfiguration);
        setServerDataSourceConfigurations(remoteDataSourceConfigurations.toArray(new ServerDataSourceConfiguration[remoteDataSourceConfigurations.size()]));
        saveForUser();
    }

    public void removeServerDataSourceConfiguration(ServerDataSourceConfiguration serverDataSourceConfiguration) {
        List<ServerDataSourceConfiguration> serverDataSourceConfigurations = new ArrayList<>(Arrays.asList(getServerDataSourceConfigurations()));
        serverDataSourceConfigurations.remove(serverDataSourceConfiguration);
        setServerDataSourceConfigurations(serverDataSourceConfigurations.toArray(new ServerDataSourceConfiguration[serverDataSourceConfigurations.size()]));
        saveForUser();
    }

    @Override
    public void setServerDataSourceConfigurations(ServerDataSourceConfiguration[] serverDataSourceConfigurations) {
        String option = new Gson().toJson(serverDataSourceConfigurations);
        get().setOption(ObserveSwingApplicationConfigOption.SERVER_DATA_SOURCE_CONFIGURATIONS.getKey(), option);
    }

    private final SimpleDateFormat dateFormat = new SimpleDateFormat();

    @Override
    public Date getBuildDate() {
        String dateStr = get().getOption(ObserveSwingApplicationConfigOption.BUILD_DATE.getKey());
        try {
            return dateFormat.parse(dateStr);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    //////////////////////////////////////////////////
    // Toutes les étapes d'actions
    //////////////////////////////////////////////////

    public enum Step {

        AfterInit, BeforeExit
    }

}
