package org.nuiton.topia.service.sql.batch;

/*
 * #%L
 * ObServe :: ToPIA Extension
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import org.nuiton.topia.persistence.TopiaApplicationContext;
import org.nuiton.topia.persistence.metadata.TopiaMetadataModel;
import org.nuiton.topia.service.sql.batch.actions.AbstractSchemaRequest;
import org.nuiton.topia.service.sql.batch.actions.AbstractSqlRequest;
import org.nuiton.topia.service.sql.batch.actions.AbstractTablesRequest;
import org.nuiton.topia.service.sql.batch.actions.CreateSchemaRequest;
import org.nuiton.topia.service.sql.batch.actions.DeleteTablesRequest;
import org.nuiton.topia.service.sql.batch.actions.DropSchemaRequest;
import org.nuiton.topia.service.sql.batch.actions.ReplicateTablesRequest;
import org.nuiton.topia.service.sql.batch.actions.TopiaSqlTableSelectArgument;
import org.nuiton.topia.service.sql.batch.actions.UpdateTablesRequest;
import org.nuiton.topia.service.sql.batch.tables.TopiaSqlTables;

import java.io.Writer;
import java.nio.file.Path;
import java.util.Iterator;

/**
 * A {@link SqlRequests} is a container of requests.
 *
 * Created on 04/01/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0.1
 */
public class SqlRequests implements Iterable<AbstractSqlRequest> {

    protected final ImmutableSet<AbstractSqlRequest> requests;

    protected final boolean showSql;

    protected SqlRequests(ImmutableSet<AbstractSqlRequest> requests, boolean showSql) {
        this.requests = requests;
        this.showSql = showSql;
    }

    public static Builder builder() {
        return new BuilderImpl();
    }

    public static <R extends AbstractSqlRequest> SqlRequests of(R request) {
        return builder()
                .from(request.getSourceTopiaApplicationContext())
                .to(request.getTargetTopiaApplicationContext())
                .to(request.getWriter())
                .addRequest(request)
                .build();
    }

    @Override
    public Iterator<AbstractSqlRequest> iterator() {
        return requests.iterator();
    }

    public boolean isShowSql() {
        return showSql;
    }

    public interface Builder extends BuilderAddRequestStep {

        Builder showSql();

        Builder from(TopiaApplicationContext sourceTopiaApplicationContext);

        Builder to(TopiaApplicationContext targetTopiaApplicationContext);

        Builder to(Writer writer);

    }

    interface BuilderAddRequestStep {

        CreateSchemaRequestBuilder createSchemaBuilder();

        DropSchemaRequestBuilder dropSchemaBuilder();

        ReplicateTablesRequestBuilder replicateTablesBuilder(TopiaMetadataModel metadataModel);

        UpdateTablesRequestBuilder updateTablesBuilder(TopiaMetadataModel metadataModel);

        DeleteTablesRequestBuilder deleteTablesBuilder();

        <R extends AbstractSqlRequest> BuilderAddRequestStep addRequest(R request);

        Builder flush();

        SqlRequests build();

    }

    protected static class BuilderImpl implements Builder {

        protected final ImmutableSet.Builder<AbstractSqlRequest> requestsBuilder = ImmutableSet.builder();
        protected TopiaApplicationContext sourceTopiaApplicationContext;
        protected TopiaApplicationContext targetTopiaApplicationContext;
        protected Writer writer;
        private boolean showSql;

        @Override
        public Builder showSql() {
            showSql = true;
            return this;
        }

        @Override
        public Builder from(TopiaApplicationContext sourceTopiaApplicationContext) {
            this.sourceTopiaApplicationContext = sourceTopiaApplicationContext;
            return this;
        }

        @Override
        public Builder to(TopiaApplicationContext targetTopiaApplicationContext) {
            this.targetTopiaApplicationContext = targetTopiaApplicationContext;
            return this;
        }

        @Override
        public Builder to(Writer writer) {
            this.writer = writer;
            return this;
        }

        @Override
        public SqlRequests build() {
            return new SqlRequests(requestsBuilder.build(), showSql);
        }

        @Override
        public <R extends AbstractSqlRequest> Builder addRequest(R request) {
            requestsBuilder.add(request);
            return this;
        }

        @Override
        public CreateSchemaRequestBuilder createSchemaBuilder() {
            return new CreateSchemaRequestBuilder(this, initBuilder(CreateSchemaRequest.builder()));
        }

        @Override
        public DropSchemaRequestBuilder dropSchemaBuilder() {
            return new DropSchemaRequestBuilder(this, initBuilder(DropSchemaRequest.builder()));
        }

        @Override
        public ReplicateTablesRequestBuilder replicateTablesBuilder(TopiaMetadataModel metadataModel) {
            ReplicateTablesRequest.Builder builder = new ReplicateTablesRequest.Builder();
            builder.setTopiaMetaModel(metadataModel);
            return new ReplicateTablesRequestBuilder(this, initBuilder(builder));
        }

        @Override
        public UpdateTablesRequestBuilder updateTablesBuilder(TopiaMetadataModel metadataModel) {
            UpdateTablesRequest.Builder builder = new UpdateTablesRequest.Builder();
            builder.setTopiaMetaModel(metadataModel);
            return new UpdateTablesRequestBuilder(this, initBuilder(builder));
        }

        @Override
        public DeleteTablesRequestBuilder deleteTablesBuilder() {
            return new DeleteTablesRequestBuilder(this, initBuilder(new DeleteTablesRequest.Builder()));
        }

        @Override
        public Builder flush() {
            return this;
        }

        protected <B extends AbstractSqlRequest.AbstractSqlRequestBuilder<B, ?>> B initBuilder(B builder) {
            return builder.from(sourceTopiaApplicationContext)
                          .to(targetTopiaApplicationContext)
                          .to(writer);
        }
    }

    public static class CreateSchemaRequestBuilder extends AbstractSchemaRequestBuilder<CreateSchemaRequest.Builder, CreateSchemaRequestBuilder> {

        public CreateSchemaRequestBuilder(BuilderImpl builder, CreateSchemaRequest.Builder delegate) {
            super(builder, delegate);
            delegate.setAddSchema(true);
        }

    }

    public static class DropSchemaRequestBuilder extends AbstractSchemaRequestBuilder<DropSchemaRequest.Builder, DropSchemaRequestBuilder> {

        public DropSchemaRequestBuilder(BuilderImpl builder, DropSchemaRequest.Builder delegate) {
            super(builder, delegate);
            delegate.setDropSchema(true);
        }

    }

    public static class ReplicateTablesRequestBuilder extends AbstractTablesRequestBuilder<ReplicateTablesRequest.Builder, ReplicateTablesRequestBuilder> {

        public ReplicateTablesRequestBuilder(BuilderImpl builder, ReplicateTablesRequest.Builder delegate) {
            super(builder, delegate);
        }

    }

    public static class UpdateTablesRequestBuilder extends AbstractTablesRequestBuilder<UpdateTablesRequest.Builder, UpdateTablesRequestBuilder> {

        public UpdateTablesRequestBuilder(BuilderImpl builder, UpdateTablesRequest.Builder delegate) {
            super(builder, delegate);
        }

    }

    public static class DeleteTablesRequestBuilder extends AbstractTablesRequestBuilder<DeleteTablesRequest.Builder, DeleteTablesRequestBuilder> {

        public DeleteTablesRequestBuilder(BuilderImpl builder, DeleteTablesRequest.Builder delegate) {
            super(builder, delegate);
        }

    }

    protected static abstract class RequestBuilderImpl<R extends AbstractSqlRequest.AbstractSqlRequestBuilder, B extends RequestBuilderImpl> implements BuilderAddRequestStep {

        protected final Builder builder;
        protected final R delegate;

        protected RequestBuilderImpl(BuilderImpl builder, R delegate) {
            this.builder = builder;
            this.delegate = delegate;
        }

        @Override
        public CreateSchemaRequestBuilder createSchemaBuilder() {
            return flush().createSchemaBuilder();
        }

        @Override
        public DropSchemaRequestBuilder dropSchemaBuilder() {
            return flush().dropSchemaBuilder();
        }

        @Override
        public ReplicateTablesRequestBuilder replicateTablesBuilder(TopiaMetadataModel metadataModel) {
            return flush().replicateTablesBuilder(metadataModel);
        }

        @Override
        public UpdateTablesRequestBuilder updateTablesBuilder(TopiaMetadataModel metadataModel) {
            return flush().updateTablesBuilder(metadataModel);
        }

        @Override
        public DeleteTablesRequestBuilder deleteTablesBuilder() {
            return flush().deleteTablesBuilder();
        }

        @Override
        public <RR extends AbstractSqlRequest> BuilderAddRequestStep addRequest(RR request) {
            return builder.addRequest(request);
        }

        @Override
        public SqlRequests build() {
            return flush().build();
        }

        @Override
        public Builder flush() {
            addRequest(delegate.build());
            return builder;
        }

        protected B returnThis() {
            return (B) this;
        }

    }

    protected static class AbstractSchemaRequestBuilder<R extends AbstractSchemaRequest.AbstractSchemaRequestBuilder, B extends AbstractSchemaRequestBuilder> extends RequestBuilderImpl<R, B> {

        protected AbstractSchemaRequestBuilder(BuilderImpl builder, R delegate) {
            super(builder, delegate);
        }

        public B forH2() {
            delegate.forH2();
            return returnThis();
        }

        public B forPostgres() {
            delegate.forPostgres();
            return returnThis();
        }

        public B setTemporaryPath(Path temporaryPath) {
            delegate.setTemporaryPath(temporaryPath);
            return returnThis();
        }

    }

    protected static class AbstractTablesRequestBuilder<R extends AbstractTablesRequest.AbstractTablesRequestBuilder, B extends AbstractTablesRequestBuilder> extends RequestBuilderImpl<R, B> {

        protected AbstractTablesRequestBuilder(BuilderImpl builder, R delegate) {
            super(builder, delegate);
        }

        public B setTables(TopiaSqlTables tables) {
            delegate.setTables(tables);
            return returnThis();
        }

        public B setSelectArgument(TopiaSqlTableSelectArgument arg) {
            delegate.setSelectArgument(arg);
            return returnThis();
        }

    }

}
