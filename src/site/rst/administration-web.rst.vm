.. -
.. * #%L
.. * ObServe
.. * %%
.. * Copyright (C) 2008 - 2010 IRD, Codelutin, Tony Chemit
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the
.. * License, or (at your option) any later version.
.. *
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. *
.. * You should have received a copy of the GNU General Public
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

Installation de l'application web
---------------------------------

L'application web est une application web classique à installer dans un conteneur web (tomcat).

Configuration de l'application web
----------------------------------

La configuration de l'application web est regroupé dans un seul fichier à placer ici

::

  /etc/observeweb.conf

La configuration de l'application possède des valeurs par défaut pour toutes les options, si vous utiliser ces valeurs par défaut, un minimum de configuration est requise.

Deux options doivent être modifiées :

  - **observeweb.adminApiKey** : la clef à utiliser pour accéder aux services d'administration
  - **observeweb.apiUrl** : l'url publique d'accès aux services web

Consulter la `page des configurations`_ pour connaitre l'ensemble des options de cette configuration.

Par défaut, l'application utilise le répertoire **/var/local/observeweb** pour y stoquer ses données.

Voici le contenu de ce dossier suite à un premier démarrage :

::

  /var/local/observeweb/
  |-- databases.yml  # configuration des bases de données
  |-- log
  |   `-- observeweb-${project.version}.log # logs de l'application
  |-- observeweb-log4j.conf # configuration des logs
  |-- temp # répertoire temporaire
  `-- users.yml # configuration des utilisateurs

Lors du premier démarrage, l'application génère des fichiers exemples de «*databases.yml*» et «*users.yml*».

Vous pouvez ensuite les modifier et recharger la configuration à chaud.

Configuration sous Windows
~~~~~~~~~~~~~~~~~~~~~~~~~~

Le fichier de configuration peut être placé dans le répertoire racine du tomcat.

Attention, à bien penser à échapper les <<\>> par des <<\\>> dans les options de répertoires.

Par exemple :

::


  observeweb.adminApiKey=a
  observeweb.apiUrl=http://localhost:8080/observeweb/api/v1
  observeweb.baseDirectory=C:\\var\\local\\observeweb
  observeweb.sessionExpirationDelay=90

Configuration des bases données
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Le fichier databases.yml liste les bases de données connues par l'application.

Il s'agit donc ici bien uniquement de configuration de type postgresql.

Par exemple :

::

  databases:
  - name: production
    defaultDatabase: true
    roles:
    - login: technicien
      password: a
    - login: referentiel
      password: a
    url: jdbc:postgresql://localhost:5432/obstuna-production
  - name: test
    roles:
    - login: technicien
      password: a
    - login: referentiel
      password: a
    url: jdbc:postgresql://localhost:5432/obstuna-test

On décrit ici deux bases «*production*» (base par défaut) et «*test*» qui pointent sur les bases postgresql
«*obstuna-production*» et «*obstuna-test*».

Sur ces deux bases, on utilise deux utilisateurs (role postgres) «*technicien*» et «*referentiel*».

Configuration des utilisateurs de l'application
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Le fichier users.yml définit les utilisateurs de l'applications web ainsi que leur niveau de droit (mapping avec les droits des bases de données).

Par exemple :

::

  users:
  - login: utilisateur-technicien
    password: a
    permissions:
    - database: production
      role: technicien
    - database: test
      role: technicien
  - login: utilisateur-referentiel
    password: a
    permissions:
    - database: production
      role: referentiel

L'utilisateur «*utilisateur-technicien*» aura le droit d'accéder à la base de production et de test avec le rôle «*technicien*».

L'utilisateur «*utilisateur-referentiel*» aura le droit d'accéder à la base de production uniquement avec le rôle «*referentiel*».

Tester l'application
~~~~~~~~~~~~~~~~~~~~

Par défaut, l'application est déployée à l'adresse suivante :
::

  http://localhost:8080/observeweb

Vous pouvez accéder à une seule page qui regroupe l'ensemble des configurations de l'application :
::

  http://localhost:8080/observeweb/admin/configuration?adminApiKey=changeme (ou la clef que vous avez configuré)

Si vous arrivez sur cette page, l'application est fonctionnelle.

Recharger les configurations
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Vous pouvez recharger les configurations à chaud via l'adresse suivante :
::

  http://localhost:8080/observeweb/admin/reloadConfiguration?adminApiKey=changeme (ou la clef que vous avez configuré)

Supprimer les sessions utilisateurs
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Vous pouvez supprimer toutes les sessions utilisateurs via l'adresse suivante :
::

  http://localhost:8080/observeweb/admin/resetAuthenticationTokens?adminApiKey=changeme (ou la clef que vous avez configuré)


.. _page des configurations: ./config-report.html#detail_observeWeb