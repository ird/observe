package fr.ird.observe.maven.plugins.toolbox;

/*-
 * #%L
 * ObServe :: Toolbox Maven plugin
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.nuiton.plugin.AbstractPlugin;

import java.io.File;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Properties;

/**
 * Created on 19/01/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
@Mojo(name = "merge-i18n-bundle", defaultPhase = LifecyclePhase.PROCESS_RESOURCES)
public class MergeI18nBundleMojo extends AbstractPlugin {

    @Parameter(defaultValue = "${project}", required = true, readonly = true)
    protected MavenProject project;

    @Parameter(property = "merge.bundle", required = true)
    protected File bundle;

    @Parameter(property = "merge.locale", required = true)
    protected String locale;

    /**
     * A flag to set verbose logs.
     */
    @Parameter(property = "merge.verbose", defaultValue = "${maven.verbose}")
    protected boolean verbose;

    /**
     * A flag to skip the goal.
     */
    @Parameter(property = "merge.skip", defaultValue = "false")
    protected boolean skip;

    /**
     * Encoding used to read and write files.
     */
    @Parameter(property = "merge.encoding", defaultValue = "${project.build.sourceEncoding}", required = true)
    protected String encoding;

    private Path i18nFile;

    @Override
    protected void init() throws Exception {

        if (skip) {
            return;
        }
        if (getLog().isDebugEnabled()) {
            setVerbose(true);
        }

        if (!bundle.exists()) {
            throw new MojoExecutionException("Can't find bundle at " + bundle);
        }

        i18nFile = project.getBasedir().toPath()
                          .resolve("src")
                          .resolve("main")
                          .resolve("resources")
                          .resolve("i18n")
                          .resolve(String.format("%s_%s.properties", project.getArtifactId(), locale));

        if (!Files.exists(i18nFile)) {
            if (isVerbose()) {
                getLog().info("No i18n file found at " + i18nFile);
            }
            i18nFile = null;
        }
    }

    @Override
    protected boolean checkSkip() {
        if (skip) {
            getLog().info("Skipping goal (skip flag is on).");
            return false;
        }

        if (i18nFile == null) {
            getLog().warn("Skipping goal (No matching i18n file found).");
            return false;
        }

        return super.checkSkip();
    }

    @Override
    public void doAction() throws Exception {

        if (isVerbose()) {
            getLog().info("project = " + project);
        }

        Charset charset = Charset.forName(encoding);

        Properties source = new Properties();
        source.load(Files.newBufferedReader(bundle.toPath(), charset));

        Properties target = new Properties();
        target.load(Files.newBufferedReader(i18nFile, charset));

        int modified = 0;
        for (Object sourceKey : source.keySet()) {
            if (target.containsKey(sourceKey)) {
                target.put(sourceKey, source.get(sourceKey));
                modified++;
            }
        }

        if (modified > 0) {
            getLog().info(modified + "key(s) modified, save the file.");

            target.store(Files.newBufferedWriter(i18nFile, charset), "Modified by " + getClass().getName());
        } else {

            getLog().info("File is up-to-date.");
        }

    }

    @Override
    public MavenProject getProject() {
        return project;
    }

    @Override
    public boolean isVerbose() {
        return verbose;
    }

    @Override
    public void setProject(MavenProject project) {
        this.project = project;
    }

    @Override
    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

    private int methodCount;

    private void checkClass(Class<?> sourceClass, Class<?> targetClass) throws MissingMethodException, MismatchMethodParameterNameException, MissingClassException {

        Method[] sourceDeclaredMethods = sourceClass.getDeclaredMethods();

        if (isVerbose()) {

            getLog().info("Check " + sourceClass.getName());

        }

        if (targetClass == null) {
            throw new MissingClassException(sourceClass.getName());
        }

        for (Method sourceMethod : sourceDeclaredMethods) {

            methodCount++;

            Method targetMethod;
            try {
                targetMethod = targetClass.getDeclaredMethod(sourceMethod.getName(), (Class<?>[]) sourceMethod.getParameterTypes());
            } catch (NoSuchMethodException e) {
                throw new MissingMethodException("Could not find method " + sourceMethod.getName() + " on target class: " + targetClass);
            }

            if (isVerbose()) {

                getLog().info("Check " + sourceClass.getName() + "#" + sourceMethod.getName());

            }

            java.lang.reflect.Parameter[] sourceParameters = sourceMethod.getParameters();
            java.lang.reflect.Parameter[] targetParameters = targetMethod.getParameters();

            for (int i = 0, max = sourceParameters.length; i < max; i++) {
                java.lang.reflect.Parameter sourceParameter = sourceParameters[i];
                java.lang.reflect.Parameter targetParameter = targetParameters[i];

                if (isVerbose()) {

                    getLog().info("Check " + sourceClass.getName() + "#" + sourceMethod.getName() + "→" + sourceParameter.getName() + " vs " + targetParameter.getName());

                }

                if (!sourceParameter.getName().equals(targetParameter.getName())) {

                    throw new MismatchMethodParameterNameException(sourceClass.getName(),
                                                                   sourceMethod.getName(),
                                                                   sourceParameter.getName(),
                                                                   i,
                                                                   targetClass.getName(),
                                                                   targetMethod.getName(),
                                                                   targetParameter.getName());
                }

            }

        }

        if (isVerbose()) {
            getLog().info(targetClass.getName() + " is conform to #" + sourceClass.getName());
        }

    }

}

