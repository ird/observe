---
-- #%L
-- ObServe :: Persistence
-- %%
-- Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
CREATE TABLE observe_common.SIZEMEASURETYPE( topiaid character varying(255) NOT NULL,topiaversion BIGINT NOT NULL, topiacreatedate DATE, lastupdatedate TIMESTAMP NOT NULL, code character varying(255), status INTEGER DEFAULT 1, needComment BOOLEAN DEFAULT false, uri character varying(255), label1 character varying(255), label2 character varying(255), label3 character varying(255), label4 character varying(255), label5 character varying(255), label6 character varying(255), label7 character varying(255), label8 character varying(255));
ALTER TABLE observe_common.SIZEMEASURETYPE ADD CONSTRAINT PK_SIZEMEASURETYPE PRIMARY KEY(TOPIAID);
INSERT INTO observe_common.SIZEMEASURETYPE(topiaId, topiaversion, topiacreatedate, lastupdatedate, code, status, needcomment, uri, label1, label2, label3, label4, label5, label6, label7, label8) (SELECT topiaId, topiaversion, topiacreatedate, lastupdatedate, code, status, needcomment, uri, label1, label2, label3, label4, label5, label6, label7, label8 FROM OBSERVE_LONGLINE.SIZEMEASURETYPE);

CREATE TABLE observe_common.WEIGHTMEASURETYPE( topiaid character varying(255) NOT NULL,topiaversion BIGINT NOT NULL, topiacreatedate DATE, lastupdatedate TIMESTAMP NOT NULL, code character varying(255), status INTEGER DEFAULT 1, needComment BOOLEAN DEFAULT false, uri character varying(255), label1 character varying(255), label2 character varying(255), label3 character varying(255), label4 character varying(255), label5 character varying(255), label6 character varying(255), label7 character varying(255), label8 character varying(255));
ALTER TABLE observe_common.WEIGHTMEASURETYPE ADD CONSTRAINT PK_WEIGHTMEASURETYPE PRIMARY KEY(TOPIAID);
INSERT INTO observe_common.WEIGHTMEASURETYPE( topiaid, topiaversion, topiacreatedate, lastupdatedate, code, status, needcomment, uri, label1, label2, label3, label4, label5, label6, label7, label8) (SELECT topiaId, topiaversion, topiacreatedate, lastupdatedate, code, status, needcomment, uri, label1, label2, label3, label4, label5, label6, label7, label8 FROM OBSERVE_LONGLINE.WEIGHTMEASURETYPE);

UPDATE observe_common.LASTUPDATEDATE SET TYPE = 'fr.ird.observe.entities.referentiel.SizeMeasureType' WHERE TYPE = 'fr.ird.observe.entities.referentiel.longline.SizeMeasureType';
UPDATE observe_common.LASTUPDATEDATE SET TYPE = 'fr.ird.observe.entities.referentiel.WeightMeasureType' WHERE TYPE = 'fr.ird.observe.entities.referentiel.longline.WeightMeasureType';

ALTER TABLE observe_seine.targetlength ADD COLUMN sizeMeasureType character varying(255);
UPDATE observe_seine.targetlength SET sizeMeasureType = NULL WHERE measureType = 0;
UPDATE observe_seine.targetlength SET sizeMeasureType = (select topiaid from observe_longline.sizemeasuretype where code='PDL')  WHERE measureType = 1;
UPDATE observe_seine.targetlength SET sizeMeasureType =(select topiaid from observe_longline.sizemeasuretype where code='FL')  WHERE measureType = 2;
ALTER TABLE observe_seine.targetlength DROP COLUMN measureType CASCADE;

ALTER TABLE observe_seine.targetlength ADD COLUMN sizeMeasureType2 character varying(255);
ALTER TABLE observe_longline.sizeMeasure ADD COLUMN sizeMeasureType2 character varying(255);
ALTER TABLE observe_longline.weightMeasure ADD COLUMN weightMeasureType2 character varying(255);
