package fr.ird.observe.entities.referentiel;

/*
 * #%L
 * ObServe :: Persistence
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.common.FormulaHelper;
import fr.ird.observe.persistence.ObserveTopiaDaoSupplier;
import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created on 28/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class LengthWeightParameters {

    /**
     * Recherche d'un {@link LengthWeightParameter} à partir des paramètres donnés.
     *
     * La recherche peut ne peut être aussi exacte que les paramètres donnés :
     *
     * Dans le cas d'une espèce faune, si non trouvé alors on recherche sur son
     * speciesGroup d'espèce.
     *
     * Si non trouvé pour l'océan donné (et que celui-ci est non null), alors on
     * recherche avec un ocean vide.
     *
     * Si non trouvé sur le gender (et que le gender n'est pas indéterminé) , alors
     * on recherche avec le gender indéterminé (gender=0).
     *
     * @param daoSupplier     la transaction en cours d'utilisation
     * @param sizeMeasureType le type de mensuration recherché
     * @param species         l'espèce sur lequel on recherche le paramétrage
     * @param ocean           l'ocean recherché (peut être null)
     * @param sex             le sexe recherché (on essayera sans sexe (sexe.code=0) si non trouvé)
     * @param date            le jour recherché
     * @return le paramétrage adéquate
     * @since 1.5
     */
    public static LengthWeightParameter findLengthWeightParameter(ObserveTopiaDaoSupplier daoSupplier,
                                                                  SizeMeasureType sizeMeasureType,
                                                                  Species species,
                                                                  Ocean ocean,
                                                                  Sex sex,
                                                                  Date date) {

        Sex unknownSex = getUnknownSex(daoSupplier);

        if (sex == null) {

            // on utilise le sexe indéterminé
            sex = unknownSex;
        }

        List<LengthWeightParameter> list = findLengthWeightParameter0(daoSupplier, species, ocean, sex, date);

        if (CollectionUtils.isEmpty(list) && !unknownSex.equals(sex)) {

            // on essaye avec le sexe indéterminé
            sex = unknownSex;
            list = findLengthWeightParameter0(daoSupplier, species, ocean, sex, date);
        }

        if (CollectionUtils.isEmpty(list)) {
            return null;
        }

        if (sizeMeasureType != null) {

            // on filtre par le type de mensuration entrant
            List<LengthWeightParameter> list2 = filterBySizeMeasureType(list, sizeMeasureType);

            if (!list2.isEmpty()) {

                // ok pas trouvé avec le bon type de mensuration, on fera la conversion plus tard
                // on tente cependante d'utiliser la RTP trouvé
                list = list2;
            }
        }

        // au final il ne devrait en rester qu'un

        if (list.size() > 1) {
            throw new DuplicateLengthWeightParameterException(species, ocean, sex, date, list);
        }

        return list.get(0);

    }

    /**
     * Recherche de la liste des {@link LengthWeightParameter} à partir des paramètres donnés.
     *
     * La recherche peut ne peut être aussi exacte que les paramètres donnés :
     *
     * Dans le cas d'une espèce faune, si non trouvé alors on recherche sur son
     * speciesGroup d'espèce.
     *
     * Si non trouvé pour l'océan donné (et que celui-ci est non null), alors on
     * recherche avec un ocean vide.
     *
     * @param daoSupplier la transaction en cours d'utilisation
     * @param species     l'espèce sur lequel on recherche le paramétrage
     * @param ocean       l'ocean recherché (peut être null)
     * @param sex         le sexe recherché (on essayera sans sexe (sexe.code=0) si non trouvé)
     * @param date        le jour recherché
     * @return le paramétrage adéquate
     * @since 1.5
     */
    public static List<LengthWeightParameter> findLengthWeightParameter0(ObserveTopiaDaoSupplier daoSupplier,
                                                                         Species species,
                                                                         Ocean ocean,
                                                                         Sex sex,
                                                                         Date date) {

        Objects.requireNonNull(daoSupplier, "daoSupplier parameter can't be null");
        Objects.requireNonNull(species, "species parameter can't be null");
        Objects.requireNonNull(sex, "sex parameter can't be null");

        List<LengthWeightParameter> list = findBySpecies(daoSupplier, species);

        if (CollectionUtils.isEmpty(list)) {

            // aucun parametrage pour le type donne
            return null;
        }

        // filtrage par ocean
        List<LengthWeightParameter> filterByOcean = filterByOcean(list, ocean);

        if (CollectionUtils.isEmpty(filterByOcean) && ocean != null) {

            // filtre par ocean null
            filterByOcean = filterByOcean(list, null);
        }
        list = filterByOcean;

        if (CollectionUtils.isEmpty(list)) {

            // pas d'ocean adequate
            return null;
        }

        // filtrage par sexe
        list = filterBySexe(list, sex);

        if (CollectionUtils.isEmpty(list)) {

            // pas de sexe adequate
            return null;
        }

        // filtrage par startDate de validite
        list = filterByDateDebutValidite(list, date);

        if (CollectionUtils.isEmpty(list)) {

            // pas de date de debut adequate
            return null;
        }

        // filtrage par endDate de validite
        list = filterByDateFinValidite(list, date);

        if (CollectionUtils.isEmpty(list)) {

            // pas de date de fin adequate
            return null;
        }

        return list;

    }

    protected static Sex getUnknownSex(ObserveTopiaDaoSupplier daoSupplier) {
        return daoSupplier.getSexDao().forCodeEquals("0").findUnique();
    }

    public static List<LengthWeightParameter> findBySpecies(ObserveTopiaDaoSupplier daoSupplier, Species species) {

        LengthWeightParameterTopiaDao dao = daoSupplier.getLengthWeightParameterDao();

        List<LengthWeightParameter> list = dao.forSpeciesEquals(species).findAll();

        // on supprime les paramétrages qui ont a=0 ou a=null ou b=0 ou b = null
        Iterator<LengthWeightParameter> itr = list.iterator();
        while (itr.hasNext()) {
            LengthWeightParameter p = itr.next();
            Double a = p.getCoefficientValue(FormulaHelper.COEFFICIENT_A);
            if (a == null || a == 0) {
                itr.remove();
                continue;
            }

            Double b = p.getCoefficientValue(FormulaHelper.COEFFICIENT_B);
            // on autorise d'avoir b à 0 (mais cela ne permet plus de calculer la taille à partir du poids)
//            if (b == null || b == 0) {
            if (b == null) {
                itr.remove();
            }
        }
        return list;

    }

    public static List<LengthWeightParameter> filterByOcean(List<LengthWeightParameter> list, Ocean ocean) {
        List<LengthWeightParameter> result = new ArrayList<>();
        if (ocean == null) {

            // on n'accepte que les parametrage sans ocean
            for (LengthWeightParameter parametrageLengthWeight : list) {
                if (parametrageLengthWeight.getOcean() == null) {
                    result.add(parametrageLengthWeight);
                }
            }
        } else {
            for (LengthWeightParameter parametrageLengthWeight : list) {
                if (ocean.equals(parametrageLengthWeight.getOcean())) {
                    result.add(parametrageLengthWeight);
                }
            }
        }
        return result;
    }

    public static List<LengthWeightParameter> filterBySexe(List<LengthWeightParameter> list, Sex sex) {
        return list.stream().filter(l -> l.getSex() == sex).collect(Collectors.toList());
    }

    public static List<LengthWeightParameter> filterBySizeMeasureType(List<LengthWeightParameter> list, SizeMeasureType sizeMeasureType) {
        return list.stream().filter(l -> l.getSizeMeasureType() == sizeMeasureType).collect(Collectors.toList());
    }

    public static List<LengthWeightParameter> filterByDateDebutValidite(List<LengthWeightParameter> list, Date startDate) {
        return list.stream()
                   .filter(l -> l.getStartDate() == null || l.getStartDate().before(startDate) || l.getStartDate().equals(startDate))
                   .collect(Collectors.toList());
    }

    public static List<LengthWeightParameter> filterByDateFinValidite(List<LengthWeightParameter> list, Date endDate) {
        List<LengthWeightParameter> result = new ArrayList<>();

        if (endDate == null) {
            // on n'accepte que les parametrages selon les critères suivants :
            // - sans date de fin (i.e en cours de validite)
            for (LengthWeightParameter parametrageLengthWeight : list) {

                Date date = parametrageLengthWeight.getEndDate();
                if (date == null) {
                    result.add(parametrageLengthWeight);
                }
            }
        } else {
            // on n'accepte que les parametrages selon les critères suivants :
            // - sans date de fin (i.e en cours de validite)
            // - ceux dont la date de fin est avant la date de fin donnée
            for (LengthWeightParameter parametrageLengthWeight : list) {

                Date date = parametrageLengthWeight.getEndDate();
                if (date == null ||
                        date.after(endDate) ||
                        date.equals(endDate)) {
                    result.add(parametrageLengthWeight);
                }
            }
        }
        return result;
    }

}
