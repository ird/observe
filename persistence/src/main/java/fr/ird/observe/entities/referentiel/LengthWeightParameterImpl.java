/*
 * #%L
 * ObServe :: Persistence
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.entities.referentiel;

import fr.ird.observe.common.FormulaHelper;

public class LengthWeightParameterImpl extends LengthWeightParameterAbstract {

    @Override
    public void setLengthWeightFormula(String value) {
        super.setLengthWeightFormula(value);
        revalidateFormulaOne();
    }

    @Override
    public void setWeightLengthFormula(String value) {
        super.setWeightLengthFormula(value);
        revalidateFormulaTwo();
    }

    @Override
    public void revalidateFormulaOne() {
        boolean result = FormulaHelper.validateRelation(this, getLengthWeightFormula(), FormulaHelper.VARIABLE_LENGTH);
        setFormulaOneValid(result);
    }

    @Override
    public void revalidateFormulaTwo() {
        boolean result = FormulaHelper.validateRelation(this, getWeightLengthFormula(), FormulaHelper.VARIABLE_WEIGHT);
        setFormulaTwoValid(result);
    }

    @Override
    public String getFormulaOne() {
        return lengthWeightFormula;
    }

    @Override
    public String getFormulaTwo() {
        return weightLengthFormula;
    }

    @Override
    public Float computeFromFomulaOne(float data) {
        return FormulaHelper.computeValue(this, lengthWeightFormula, null, FormulaHelper.VARIABLE_LENGTH, data);
    }

    @Override
    public Float computeFromFomulaTwo(float data) {
        return FormulaHelper.computeValue(this, weightLengthFormula, FormulaHelper.COEFFICIENT_B, FormulaHelper.VARIABLE_WEIGHT, data);
    }
}
