package fr.ird.observe.entities.referentiel;

/*-
 * #%L
 * ObServe :: Persistence
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.entities.longline.CatchLongline;
import fr.ird.observe.persistence.ObserveEntityEnum;
import org.hibernate.Session;
import org.nuiton.topia.persistence.internal.support.HibernateTopiaJpaSupport;
import org.nuiton.topia.persistence.support.TopiaHibernateSupport;

import java.util.List;

public class SpeciesTopiaDao extends AbstractSpeciesTopiaDao<Species> {

    @Override
    public void delete(Species entity) {

        TopiaHibernateSupport hibernateSupport = ((HibernateTopiaJpaSupport) topiaJpaSupport).getHibernateSupport();
        Session hibernateSession = hibernateSupport.getHibernateSession();

        //FIXME Cela devrait être généré dans Topia.
        {
            String sql =
                    "SELECT main.* FROM OBSERVE_LONGLINE.catch main, OBSERVE_LONGLINE.catch_predator secondary " +
                    " WHERE main.topiaId=secondary.catch AND secondary.predator='" + entity.getTopiaId() + "'";
            List<CatchLongline> list = hibernateSession
                    .createSQLQuery(sql)
                    .addEntity("main", ObserveEntityEnum.CatchLongline.getImplementation())
                    .list();

            for (CatchLongline item : list) {
                item.removePredator(entity);
            }
        }

        super.delete(entity);

    }
}
