package fr.ird.observe.entities.seine;

/*-
 * #%L
 * ObServe :: Persistence
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Joiner;
import fr.ird.observe.entities.referentiel.GearCaracteristic;

import java.util.stream.Collectors;

public class GearUseFeaturesSeineImpl extends GearUseFeaturesSeineAbstract {

    private static final long serialVersionUID = 1L;

    @Override
    public String getMeasurementsAsString() {
        return Joiner.on(", ").join(
                getGearUseFeaturesMeasurement().stream()
                                               .map(g -> {
                                                   GearCaracteristic gearCaracteristic = g.getGearCaracteristic();
                                                   String code = gearCaracteristic.getCode();
                                                   String unit = gearCaracteristic.getUnit();
                                                   String measurementValue = g.getMeasurementValue();
                                                   boolean isBoolean = false;
                                                   if ("true".equals(measurementValue)) {
                                                       measurementValue = "Oui";
                                                       isBoolean = true;
                                                   } else if ("false".equals(measurementValue)) {
                                                       measurementValue = "Non";
                                                       isBoolean = true;
                                                   }
                                                   return "( " + (code == null ? " Aucun code" : code)
                                                           + " - " + gearCaracteristic.getLabel2()
                                                           + (isBoolean ? "" : (" [ " + (unit == null ? "pas d'unité" : unit) + " ]"))
                                                           + " = " + measurementValue + " )";
                                               })
                                               .collect(Collectors.toList()));
    }
}
