package fr.ird.observe.entities.referentiel;

/*-
 * #%L
 * ObServe :: Persistence
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.common.FormulaHelper;
import fr.ird.observe.common.WithFormula;
import org.apache.commons.lang3.BooleanUtils;

import java.util.Map;
import java.util.Set;

/**
 * Created on 05/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public abstract class FormulaSupportImpl extends FormulaSupportAbstract implements WithFormula {

    private static final long serialVersionUID = 1L;

    private Map<String, Double> coefficientValues;
    private Boolean formulaOneValid;
    private Boolean formulaTwoValid;

    @Override
    public String getCode() {
        // pas utilise
        return null;
    }

    @Override
    public void setCode(String code) {
        // pas utilise
    }

    @Override
    public void setCoefficients(String value) {
        super.setCoefficients(value);
        coefficientValues = null;
        revalidateFormulaOne();
        revalidateFormulaTwo();
    }

    @Override
    public Set<String> getCoefficientNames() {
        return getCoefficientValues().keySet();
    }

    @Override
    public Double getCoefficientValue(String coefficientName) {
        return getCoefficientValues().get(coefficientName);
    }

    @Override
    public Map<String, Double> getCoefficientValues() {
        if (coefficientValues == null) {
            coefficientValues = FormulaHelper.getCoefficientValues(this);
        }
        return coefficientValues;
    }


    @Override
    public boolean isFormulaOneValid() {
        if (formulaOneValid == null) {
            revalidateFormulaOne();
        }
        return BooleanUtils.isTrue(formulaOneValid);
    }

    @Override
    public boolean isFormulaTwoValid() {
        if (formulaTwoValid == null) {
            revalidateFormulaTwo();
        }
        return BooleanUtils.isTrue(formulaTwoValid);
    }

    @Override
    public void setFormulaOneValid(boolean formulaOneValid) {
        this.formulaOneValid = formulaOneValid;
        fireOnPostWrite(PROPERTY_FORMULA_ONE_VALID, null, formulaOneValid);
    }

    @Override
    public void setFormulaTwoValid(boolean formulaTwoValid) {
        this.formulaTwoValid = formulaTwoValid;
        fireOnPostWrite(PROPERTY_FORMULA_TWO_VALID, null, formulaTwoValid);
    }

}
