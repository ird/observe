package fr.ird.observe.entities.longline;

/*
 * #%L
 * ObServe :: Persistence
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ird.observe.common.TripMapPoint;
import fr.ird.observe.common.constants.TripMapPointType;
import fr.ird.observe.entities.referentiel.Harbour;
import fr.ird.observe.entities.referentiel.Program;
import org.nuiton.topia.persistence.support.TopiaSqlQuery;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;
import org.nuiton.util.DateUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;

public class TripLonglineTopiaDao extends AbstractTripLonglineTopiaDao<TripLongline> {

    public static final String ACTIVITY_INTERACTION_ID = "fr.ird.observe.entities.referentiel.longline.VesselActivityLongline#1239832686138#0.4";

    public static final String ACTIVITY_STATION_ID = "fr.ird.observe.entities.referentiel.longline.VesselActivityLongline#1239832686138#0.3";

    public int findPositionByProgramId(String programId, String tripId) {
        int result = 0;
        //FIXME Bien vérifier que cela fonctionne
        List<TripLongline> list = forProgramEquals(topiaDaoSupplier.getDao(Program.class).forTopiaIdEquals(programId).findUnique()).findAll();
        for (TripLongline e : list) {
            if (tripId.equals(e.getTopiaId())) {
                return result;
            }
            result++;
        }

        // not found
        return -1;
    }

    public LinkedHashSet<TripMapPoint> extractTripMapActivityPoints(String tripId) {

        TripLongline tripLongline = forTopiaIdEquals(tripId).findUnique();

        LinkedHashSet<TripMapPoint> tripMapPoints = new LinkedHashSet<>();

        // add departure harbours
        Harbour departureHarbour = tripLongline.getDepartureHarbour();
        if (departureHarbour != null
                && departureHarbour.getLatitude() != null
                && departureHarbour.getLongitude() != null) {
            TripMapPoint departurePoint = new TripMapPoint();
            departurePoint.setTime(tripLongline.getStartDate());
            departurePoint.setLatitude(departureHarbour.getLatitude());
            departurePoint.setLongitude(departureHarbour.getLongitude());
            departurePoint.setType(TripMapPointType.longlineDepartureHarbour);
            tripMapPoints.add(departurePoint);
        }

        // Add Activities
        TripMapActivityPointQuery tripMapActivityPointQuery = new TripMapActivityPointQuery(tripId);
        List<List<TripMapPoint>> results = topiaSqlSupport.findMultipleResult(tripMapActivityPointQuery);
        for (List<TripMapPoint> result : results) {
            tripMapPoints.addAll(result);
        }

        // add landing harbours
        Harbour landingHarbour = tripLongline.getLandingHarbour();
        if (landingHarbour != null
                && landingHarbour.getLatitude() != null
                && landingHarbour.getLongitude() != null) {
            TripMapPoint landingPoint = new TripMapPoint();
            landingPoint.setTime(tripLongline.getEndDate());
            landingPoint.setLatitude(landingHarbour.getLatitude());
            landingPoint.setLongitude(landingHarbour.getLongitude());
            landingPoint.setType(TripMapPointType.longlineLandingHarbour);
            tripMapPoints.add(landingPoint);
        }

        return tripMapPoints;
    }

    private static class TripMapActivityPointQuery extends TopiaSqlQuery<List<TripMapPoint>> {

        private static final String SQL = "SELECT" +
                " a.timestamp, " +
                " a.latitude," +
                " a.longitude," +
                " a.vesselActivity," +
                " s.settingStartTimestamp," +
                " s.settingStartLatitude," +
                " s.settingStartLongitude," +
                " s.settingEndTimestamp," +
                " s.settingEndLatitude," +
                " s.settingEndLongitude," +
                " s.haulingStartTimestamp," +
                " s.haulingStartLatitude," +
                " s.haulingStartLongitude," +
                " s.haulingEndTimestamp," +
                " s.haulingEndLatitude," +
                " s.haulingEndLongitude" +
                " FROM observe_longLine.activity a" +
                " LEFT OUTER JOIN observe_longLine.set s" +
                " ON s.topiaId = a.set" +
                " WHERE a.trip = ?" +
                " ORDER BY a.timestamp";

        protected final String tripId;

        public TripMapActivityPointQuery(String tripId) {
            this.tripId = tripId;
        }

        @Override
        public PreparedStatement prepareQuery(Connection connection) throws SQLException {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
            preparedStatement.setString(1, tripId);
            return preparedStatement;
        }

        @Override
        public List<TripMapPoint> prepareResult(ResultSet resultSet) throws SQLException {

            List<TripMapPoint> result = Lists.newLinkedList();

            if (resultSet.getString(5) == null) {

                // activity
                TripMapPoint activity = new TripMapPoint();
                activity.setTime(resultSet.getDate(1));
                activity.setLatitude(resultSet.getFloat(2));
                activity.setLongitude(resultSet.getFloat(3));

                String vesselActivityId = resultSet.getString(4);
                if (vesselActivityId == null) {
                    activity.setType(TripMapPointType.longlineActivity);
                } else if (ACTIVITY_INTERACTION_ID.equals(vesselActivityId)) {
                    activity.setType(TripMapPointType.longlineActivityWithInteraction);
                } else if (ACTIVITY_STATION_ID.equals(vesselActivityId)) {
                    activity.setType(TripMapPointType.longlineActivityWithStation);
                } else {
                    activity.setType(TripMapPointType.longlineActivity);
                }

                result.add(activity);
            }

            // settingStart
            if (resultSet.getString(5) != null) {
                TripMapPoint settingStart = new TripMapPoint();
                settingStart.setTime(resultSet.getDate(5));
                settingStart.setLatitude(resultSet.getFloat(6));
                settingStart.setLongitude(resultSet.getFloat(7));
                settingStart.setType(TripMapPointType.longlineActivityWithSettingStart);
                result.add(settingStart);
            }

            // settingEnd
            if (resultSet.getString(8) != null) {
                TripMapPoint settingEnd = new TripMapPoint();
                settingEnd.setTime(resultSet.getDate(8));
                settingEnd.setLatitude(resultSet.getFloat(9));
                settingEnd.setLongitude(resultSet.getFloat(10));
                settingEnd.setType(TripMapPointType.longlineActivityWithSettingEnd);
                result.add(settingEnd);
            }

            // haulingStart
            if (resultSet.getString(11) != null) {
                TripMapPoint haulingStart = new TripMapPoint();
                haulingStart.setTime(resultSet.getDate(11));
                haulingStart.setLatitude(resultSet.getFloat(12));
                haulingStart.setLongitude(resultSet.getFloat(13));
                haulingStart.setType(TripMapPointType.longlineActivityWithHaulingStart);
                result.add(haulingStart);
            }

            // haulingEnd
            if (resultSet.getString(14) != null) {
                TripMapPoint haulingEnd = new TripMapPoint();
                haulingEnd.setTime(resultSet.getDate(14));
                haulingEnd.setLatitude(resultSet.getFloat(15));
                haulingEnd.setLongitude(resultSet.getFloat(16));
                haulingEnd.setType(TripMapPointType.longlineActivityWithHaulingEnd);
                result.add(haulingEnd);
            }

            return result;
        }
    }

    public boolean updateEndDate(TripLongline trip) {

        boolean wasUpdated = false;

        // la date de fin theorique (date de la dernière activité de la marée)
        Date theoricalEndDate = getTheoricalEndDate(trip);

        // la date de fin actuelle
        Date realEndDate = trip.getEndDate();

        if (realEndDate == null || theoricalEndDate.after(realEndDate)) {

            // on utilise la nouvelle date theorique car l'ancienne n'existe pas
            // ou est antérieure à la date de fin théoriquue
            trip.setEndDate(theoricalEndDate);
            wasUpdated = true;
        }

        return wasUpdated;

    }

    public Date getTheoricalEndDate(TripLongline trip) {

        Date d;

        Timestamp lastActivityDate = TheoricalEndOfDateSqlQuery.find(topiaSqlSupport, trip.getTopiaId());

        if (lastActivityDate == null) {

            // pas d'activité, donc la date de fin est la date de debut
            d = trip.getStartDate();

        } else {

            // date de la dernière activité de la marée
            d = lastActivityDate;

        }

        // on conserve la date epuree (pas de notion de temps dans la date)
        d = DateUtil.getEndOfDay(d);
        return d;

    }

    private static class TheoricalEndOfDateSqlQuery extends TopiaSqlQuery<Timestamp> {

        private final String sql;

        private final String tripId;

        TheoricalEndOfDateSqlQuery(String sql, String tripId) {
            this.sql = sql;
            this.tripId = tripId;
        }

        public static Timestamp find(TopiaSqlSupport context, String tripId) {

            String sql = "SELECT max(a.timestamp)" +
                    " FROM observe_longline.activity a" +
                    " WHERE a.trip = ?";

            TheoricalEndOfDateSqlQuery request = new TheoricalEndOfDateSqlQuery(sql, tripId);
            return context.findSingleResult(request);

        }

        @Override
        public PreparedStatement prepareQuery(Connection connection) throws SQLException {

            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, tripId);
            return preparedStatement;

        }

        @Override
        public Timestamp prepareResult(ResultSet set) throws SQLException {

            return set.getTimestamp(1);

        }

    }
}
