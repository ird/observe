package fr.ird.observe.entities.referentiel;

/*
 * #%L
 * ObServe :: Persistence
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Helper class around {@link Species}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0
 */
public class Species2 {

    protected Species2() {
        // avoid instance
    }

    public static Predicate<Species> newSpeciesByOceanPredicate(Ocean ocean) {
        return o -> o.containsOcean(ocean);
    }

    public static <E extends Species> List<E> filterByOcean(Collection<E> speciess, Ocean ocean) {

        return speciess.stream().filter(newSpeciesByOceanPredicate(ocean)).collect(Collectors.toList());
    }

    public static String decorate(Species species) {

        StringBuilder builder = new StringBuilder();
        builder.append(species.getFaoCode());
        if (species.getScientificLabel() == null) {
            builder.append(" - ").append("xx");

        } else {
            builder.append(" - ").append(species.getScientificLabel());
        }

        return builder.toString();

    }

}
