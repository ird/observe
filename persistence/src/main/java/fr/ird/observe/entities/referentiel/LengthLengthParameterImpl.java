package fr.ird.observe.entities.referentiel;

/*-
 * #%L
 * ObServe :: Persistence
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.common.FormulaHelper;
import fr.ird.observe.common.WithFormula;

/**
 * Created on 05/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class LengthLengthParameterImpl extends LengthLengthParameterAbstract implements WithFormula {

    @Override
    public void setInputOutputFormula(String inputOutputFormula) {
        super.setInputOutputFormula(inputOutputFormula);
        revalidateFormulaOne();
    }

    @Override
    public void setOutputInputFormula(String outputInputFormula) {
        super.setOutputInputFormula(outputInputFormula);
        revalidateFormulaTwo();
    }

    @Override
    public Float computeFromFomulaOne(float data) {
        return FormulaHelper.computeValue(this, inputOutputFormula, null, FormulaHelper.VARIABLE_INPUT, data);
    }

    @Override
    public Float computeFromFomulaTwo(float data) {
        return FormulaHelper.computeValue(this, outputInputFormula, FormulaHelper.COEFFICIENT_B, FormulaHelper.VARIABLE_OUTPUT, data);
    }

    @Override
    public String getFormulaOne() {
        return inputOutputFormula;
    }

    @Override
    public String getFormulaTwo() {
        return outputInputFormula;
    }

    @Override
    public void revalidateFormulaOne() {
        boolean result = FormulaHelper.validateRelation(this, inputOutputFormula, FormulaHelper.VARIABLE_INPUT);
        setFormulaOneValid(result);
    }

    @Override
    public void revalidateFormulaTwo() {
        boolean result = FormulaHelper.validateRelation(this, outputInputFormula, FormulaHelper.VARIABLE_OUTPUT);
        setFormulaTwoValid(result);
    }

}
