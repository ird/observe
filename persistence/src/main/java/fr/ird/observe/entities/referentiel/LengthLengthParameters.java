package fr.ird.observe.entities.referentiel;

/*-
 * #%L
 * ObServe :: Persistence
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.persistence.ObserveTopiaDaoSupplier;

import java.util.Optional;

/**
 * Created on 05/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class LengthLengthParameters {

    public static Optional<LengthLengthParameter> findLengthLengthParameter(ObserveTopiaDaoSupplier daoSupplier,
                                                                            SizeMeasureType inputSizeMeasureType,
                                                                            SizeMeasureType outputSizeMeasureType) {
        return daoSupplier.getLengthLengthParameterDao().find(inputSizeMeasureType, outputSizeMeasureType);
    }

}
