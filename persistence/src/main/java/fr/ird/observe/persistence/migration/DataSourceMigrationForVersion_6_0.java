package fr.ird.observe.persistence.migration;

/*-
 * #%L
 * ObServe :: Persistence
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.support.TopiaSqlQuery;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Created on 27/10/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
@MigrationVersion(version = "6.0")
public class DataSourceMigrationForVersion_6_0 extends AbstractObserveMigrationCallBack {

    /** Logger */
    private static final Log log = LogFactory.getLog(DataSourceMigrationForVersion_6_0.class);

    public DataSourceMigrationForVersion_6_0(AbstractDataSourceMigration callBack, String scriptSuffix) {
        super(DataSourceMigrationForVersion_6_0.class, callBack, scriptSuffix);
    }

    @Override
    protected void prepareMigrationScript(TopiaSqlSupport topiaSqlSupport, List<String> queries, boolean showSql, boolean showProgression) {

        addScript("01", "evol_8374_delete_sonarUsed", queries);
        addScript("02", "evol_7886_catch_add_set_idx", queries);
        addScript("03", "evol_6997_delete_supportVesselName", queries);
        addScript("04", "evol_6832_branchlineComposition_add_tracelineLength", queries);
        addScript("05", "evol_6999_nonTargetCatch_add_well", queries);
        addScript("06", "evol_8375_setSeine_add_booleans", queries);
        addScript("07", "evol_8389_rename_source_columns", queries);

        addScript("08", "evol_8390_measure_type", queries);
        evol8390(topiaSqlSupport, queries);
        addScript("08_1", "evol_8390_measure_type", queries);

        addScript("09", "evol_7877_targetLength_add_sex", queries);
        addScript("10", "evol_8391_species_add_sizeMeasureType", queries);
        evol8391(topiaSqlSupport, queries);

        addScript("11", "evol_8571_length_add_tagNumber", queries);
        addScript("12", "evol_8475_vessel_add_fields", queries);
        addScript("13", "evol_7901_add_armateur_table", queries);
        addScript("14", "evol_8196_lengthWeightParameter_add_source", queries);
        addScript("15", "evol_8578_nonTargetLength_add_speciesFate", queries);
        addScript("16", "evol_8377_speciesFate_add_discard", queries);
        addScript("17", "evol_8376_vesselActivitySeine_add_allowFad", queries);
        addScript("18", "evol_8404_lengthLengthParameter", queries);
        addScript("19", "ano_8554_brancheline_add_section", queries);
        addScript("20", "evol_8391_species_drop_lengthmeasuretype", queries);

    }

    private void evol8391(TopiaSqlSupport topiaSqlSupport, List<String> queries) {

        ImmutableMap<String, String> missingCodesMapping = ImmutableMap.<String, String>builder()
                .put("LT ?", "TL")
                .put("LF", "FL")
                .put("L1", "PDL")
                .put("LC", "DL")
                .put("LT", "TL")
                .put("", "TL")
                .build();
        List<String> lengthMeasureTypes = topiaSqlSupport.findMultipleResult(new TopiaSqlQuery<String>() {

            @Override
            public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                return connection.prepareStatement("SELECT DISTINCT(lengthmeasuretype) FROM observe_common.species WHERE lengthmeasuretype IS NOT NULL");
            }

            @Override
            public String prepareResult(ResultSet set) throws SQLException {
                return set.getString(1);
            }
        });
        List<String> existingLengthMeasureTypes = topiaSqlSupport.findMultipleResult(new TopiaSqlQuery<String>() {

            @Override
            public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                // Attention à ce moment là ce référentiel est encore dans le schema longline
                return connection.prepareStatement("SELECT DISTINCT(code) FROM observe_longline.sizemeasuretype ");
            }

            @Override
            public String prepareResult(ResultSet set) throws SQLException {
                return set.getString(1);
            }
        });

        Set<String> missingLengthMeasureTypeCodes = new HashSet<>();
        for (String lengthMeasureType : lengthMeasureTypes) {
            lengthMeasureType = lengthMeasureType.trim();
            String realCode = missingCodesMapping.get(lengthMeasureType);
            if (realCode == null) {
                realCode = lengthMeasureType;
            }
            missingLengthMeasureTypeCodes.add(realCode);
        }
        missingLengthMeasureTypeCodes.removeAll(existingLengthMeasureTypes);

        ImmutableMap.Builder<String, String> extraTopiaIdsByCodeBuilder = ImmutableMap.builder();

        if (!missingLengthMeasureTypeCodes.isEmpty()) {

            // il manque des référentiels on va les ajouter
            ImmutableMap<String, String> sqlMappingByCode =
                    ImmutableMap.<String, String>builder()
                            .put("FL", "INSERT INTO observe_common.sizemeasuretype (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, status, uri, label1, label2, label3, label4, label5, label6, label7, label8, needcomment) VALUES ('fr.ird.observe.entities.referentiel.SizeMeasureType#1433499465700#0.0902433863375336', 2, '2015-06-05', '2016-10-18 11:51:54.099140','FL', 1, null, 'Fork Length', 'Fork Length', 'Fork Length', null, null, null, null, null, false);")
                            .put("CFL", "INSERT INTO observe_common.sizemeasuretype (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, status, uri, label1, label2, label3, label4, label5, label6, label7, label8, needcomment) VALUES ('fr.ird.observe.entities.referentiel.SizeMeasureType#1433499465999#0.707568018231541', 3, '2015-06-05', '2016-10-18 11:51:54.099140', 'CFL', 1, null, 'Curved Fork Length', 'Curved Fork Length', 'Curved Fork Length', null, null, null, null, null, false);")
                            .put("PAL", "INSERT INTO observe_common.sizemeasuretype (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, status, uri, label1, label2, label3, label4, label5, label6, label7, label8, needcomment) VALUES ('fr.ird.observe.entities.referentiel.SizeMeasureType#1433499466255#0.444246932631359', 2, '2015-06-05', '2016-10-18 11:51:54.099140', 'PAL', 1, null, 'Pectoral-Anal Length', 'Pectoral-Anal Length', 'Pectoral-Anal Length', null, null, null, null, null, false);")
                            .put("TL", "INSERT INTO observe_common.sizemeasuretype (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, status, uri, label1, label2, label3, label4, label5, label6, label7, label8, needcomment) VALUES ('fr.ird.observe.entities.referentiel.SizeMeasureType#1433499466532#0.844473292818293', 2, '2015-06-05', '2016-10-18 11:51:54.099140', 'TL', 1, null, 'Total Length', 'Total Length', 'Total Length', null, null, null, null, null, false);")
                            .put("PDL", "INSERT INTO observe_common.sizemeasuretype (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, status, uri, label1, label2, label3, label4, label5, label6, label7, label8, needcomment) VALUES ('fr.ird.observe.entities.referentiel.SizeMeasureType#1433499466774#0.529249255312607', 2, '2015-06-05', '2016-10-18 11:51:54.099140', 'PDL', 1, null, 'Predorsal Length', 'Predorsal Length', 'Predorsal Length', null, null, null, null, null, false);")
                            .put("SL", "INSERT INTO observe_common.sizemeasuretype (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, status, uri, label1, label2, label3, label4, label5, label6, label7, label8, needcomment) VALUES ('fr.ird.observe.entities.referentiel.SizeMeasureType#1433499467059#0.563144549960271', 2, '2015-06-05', '2016-10-18 11:51:54.099140', 'SL', 1, null, 'Standard Length', 'Standard Length', 'Standard Length', null, null, null, null, null, false);")
                            .put("LJFL", "INSERT INTO observe_common.sizemeasuretype (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, status, uri, label1, label2, label3, label4, label5, label6, label7, label8, needcomment) VALUES ('fr.ird.observe.entities.referentiel.SizeMeasureType#1433499467326#0.537910396233201', 2, '2015-06-05', '2016-10-18 11:51:54.099140', 'LJFL', 1, null, 'Lower Jaw-Fork Length', 'Lower Jaw-Fork Length', 'Lower Jaw-Fork Length', null, null, null, null, null, false);")
                            .put("CLJFL", "INSERT INTO observe_common.sizemeasuretype (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, status, uri, label1, label2, label3, label4, label5, label6, label7, label8, needcomment) VALUES ('fr.ird.observe.entities.referentiel.SizeMeasureType#1433499467638#0.409153908025473', 2, '2015-06-05', '2016-10-18 11:51:54.099140', 'CLJFL', 1, null, 'Curved Lower Jaw-Fork Length', 'Curved Lower Jaw-Fork Length', 'Curved Lower Jaw-Fork Length', null, null, null, null, null, false);")
                            .put("IDL", "INSERT INTO observe_common.sizemeasuretype (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, status, uri, label1, label2, label3, label4, label5, label6, label7, label8, needcomment) VALUES ('fr.ird.observe.entities.referentiel.SizeMeasureType#1433499467945#0.249704661779106', 2, '2015-06-05', '2016-10-18 11:51:54.099140', 'IDL', 1, null, 'Interdorsal Length', 'Interdorsal Length', 'Interdorsal Length', null, null, null, null, null, false);")
                            .put("EFL", "INSERT INTO observe_common.sizemeasuretype (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, status, uri, label1, label2, label3, label4, label5, label6, label7, label8, needcomment) VALUES ('fr.ird.observe.entities.referentiel.SizeMeasureType#1433499468283#0.152317869942635', 2, '2015-06-05', '2016-10-18 11:51:54.099140', 'EFL', 1, null, 'Eye-Fork Length', 'Eye-Fork Length', 'Eye-Fork Length', null, null, null, null, null, false);")
                            .put("DW", "INSERT INTO observe_common.sizemeasuretype (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, status, uri, label1, label2, label3, label4, label5, label6, label7, label8, needcomment) VALUES ('fr.ird.observe.entities.referentiel.SizeMeasureType#1433499468646#0.710986209334806', 2, '2015-06-05', '2016-10-18 11:51:54.099140', 'DW', 1, null, 'Disk Width', 'Disk Width', 'Disk Width', null, null, null, null, null, false);")
                            .put("DL", "INSERT INTO observe_common.sizemeasuretype (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, status, uri, label1, label2, label3, label4, label5, label6, label7, label8, needcomment) VALUES ('fr.ird.observe.entities.referentiel.SizeMeasureType#1433499469115#0.42996318358928', 2, '2015-06-05', '2016-10-18 11:51:54.099140',  'DL', 1, null, 'Disk Length', 'Disk Length', 'Disk Length', null, null, null, null, null, false);")
                            .put("TW", "INSERT INTO observe_common.sizemeasuretype (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, status, uri, label1, label2, label3, label4, label5, label6, label7, label8, needcomment) VALUES ('fr.ird.observe.entities.referentiel.SizeMeasureType#1433499469572#0.969587777974084', 2, '2015-06-05', '2016-10-18 11:51:54.099140', 'TW', 1, null, 'Total Weight', 'Total Weight', 'Total Weight', null, null, null, null, null, false);")
                            .put("CTL", "INSERT INTO observe_common.sizemeasuretype (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, status, uri, label1, label2, label3, label4, label5, label6, label7, label8, needcomment) VALUES ('fr.ird.observe.entities.referentiel.SizeMeasureType#1433499470528#0.813851526239887', 2, '2015-06-05', '2016-10-18 11:51:54.099140', 'CTL', 1, null, 'Curved Total Length', 'Curved Total Length', 'Curved Total Length', null, null, null, null, null, false);")
                            .put("DML", "INSERT INTO observe_common.sizemeasuretype (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, status, uri, label1, label2, label3, label4, label5, label6, label7, label8, needcomment) VALUES ('fr.ird.observe.entities.referentiel.SizeMeasureType#1433499470887#0.950930784922093', 2, '2015-06-05', '2016-10-18 11:51:54.099140', 'DML', 1, null, 'Dorsal Mantle Length', 'Dorsal Mantle Length', 'Dorsal Mantle Length', null, null, null, null, null, false);")
                            .put("CDML", "INSERT INTO observe_common.sizemeasuretype (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, status, uri, label1, label2, label3, label4, label5, label6, label7, label8, needcomment) VALUES ('fr.ird.observe.entities.referentiel.SizeMeasureType#1433499471278#0.425988764036447', 2, '2015-06-05', '2016-10-18 11:51:54.099140', 'CDML', 1, null, 'Curved Dorsal Mantle Length', 'Curved Dorsal Mantle Length', 'Curved Dorsal Mantle Length', null, null, null, null, null, false);")
                            .put("PFL", "INSERT INTO observe_common.sizemeasuretype (topiaid, topiaversion, topiacreatedate, lastupdatedate, code, status, uri, label1, label2, label3, label4, label5, label6, label7, label8, needcomment) VALUES ('fr.ird.observe.entities.referentiel.SizeMeasureType#1433499472220#0.885759855154902', 2, '2015-06-05', '2016-10-18 11:51:54.099140', 'PFL', 1, null, 'Pectoral-Fork Length', 'Pectoral-Fork Length', 'Pectoral-Fork Length', null, null, null, null, null, false);")
                            .build();

            Iterator<String> iterator = missingLengthMeasureTypeCodes.iterator();
            while (iterator.hasNext()) {
                String code = iterator.next();
                String sql = sqlMappingByCode.get(code);
                if (sql != null) {
                    queries.add(sql);
                    iterator.remove();
                    int i = sql.indexOf("'");
                    String topiaId = sql.substring(i + 1);
                    int j = topiaId.indexOf("'");
                    topiaId = topiaId.substring(0, j);
                    if (log.isInfoEnabled()) {
                        log.info("Add " + code + "::" + topiaId);
                    }
                    extraTopiaIdsByCodeBuilder.put(code, topiaId);
                }
            }

            if (!missingLengthMeasureTypeCodes.isEmpty()) {

                // migration impossible, il manque encore des référentiels
                // ce cas ne devrait jamais arrivé
                throw new IllegalStateException("Les types de mesure de taille pour les codes suivants :\n\t" + Joiner.on("\n\t").join(missingLengthMeasureTypeCodes) + " sont requis pour la migration mais n'ont pas été trouvés en base.");
            }

        }

        ImmutableMap<String, String> extraTopiaIdsByCode = extraTopiaIdsByCodeBuilder.build();

        Set<String> invalidCodes = new LinkedHashSet<>();
        for (String lengthMeasureType : lengthMeasureTypes) {

            lengthMeasureType = lengthMeasureType.trim();
            if (lengthMeasureType.isEmpty()) {
                continue;
            }
            if (missingCodesMapping.containsKey(lengthMeasureType)) {
                lengthMeasureType = missingCodesMapping.get(lengthMeasureType);
            }

            String finalLengthMeasureType = lengthMeasureType;
            String sizeMeasureTypeId = extraTopiaIdsByCode.get(lengthMeasureType);
            if (sizeMeasureTypeId == null) {
                sizeMeasureTypeId = topiaSqlSupport.findSingleResult(new TopiaSqlQuery<String>() {

                    @Override
                    public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                        return connection.prepareStatement("SELECT topiaId FROM observe_longline.sizeMeasureType WHERE code = '" + finalLengthMeasureType + "'");
                    }

                    @Override
                    public String prepareResult(ResultSet set) throws SQLException {
                        return set.getString(1);
                    }
                });
            }

            if (sizeMeasureTypeId == null) {

                if (log.isWarnEnabled()) {
                    log.warn("Impossible de trouver la type de mesure de taille pour le code: " + lengthMeasureType);
                }
                invalidCodes.add(lengthMeasureType);
            } else {
                sizeMeasureTypeId = StringUtils.replace(sizeMeasureTypeId, ".longline", "");
                queries.add(String.format("UPDATE observe_common.species SET sizeMeasureType = '%s' WHERE lengthMeasureType = '%s'", sizeMeasureTypeId, lengthMeasureType));
            }
        }

        if (!invalidCodes.isEmpty()) {

            throw new IllegalStateException("Impossible de trouver la type de mesure de taille pour les codes suivants :\n\t" + Joiner.on("\n\t").join(invalidCodes));
        }

    }

    private void evol8390(TopiaSqlSupport topiaSqlSupport, List<String> queries) {
        {
            List<String> oldIds = topiaSqlSupport.findMultipleResult(new TopiaSqlQuery<String>() {

                @Override
                public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                    return connection.prepareStatement("SELECT topiaId FROM observe_longline.sizemeasuretype");
                }

                @Override
                public String prepareResult(ResultSet set) throws SQLException {
                    return set.getString(1);
                }
            });

            for (String oldId : oldIds) {
                String newId = StringUtils.replace(oldId, ".longline", "");
                queries.add(String.format("UPDATE observe_common.sizeMeasureType SET topiaId = '%s' WHERE topiaId = '%s'", newId, oldId));
                queries.add(String.format("UPDATE observe_longline.sizeMeasure SET sizeMeasureType2 = '%s' WHERE sizeMeasureType = '%s'", newId, oldId));
                queries.add(String.format("UPDATE observe_seine.targetLength SET sizeMeasureType = '%s' WHERE sizeMeasureType = '%s'", newId, oldId));
            }
        }
        {
            List<String> oldIds = topiaSqlSupport.findMultipleResult(new TopiaSqlQuery<String>() {

                @Override
                public PreparedStatement prepareQuery(Connection connection) throws SQLException {
                    return connection.prepareStatement("SELECT topiaId FROM observe_longline.weightmeasuretype");
                }

                @Override
                public String prepareResult(ResultSet set) throws SQLException {
                    return set.getString(1);
                }
            });

            for (String oldId : oldIds) {
                String newId = StringUtils.replace(oldId, ".longline", "");
                queries.add(String.format("UPDATE observe_common.weightMeasureType SET topiaId = '%s' WHERE topiaId = '%s'", newId, oldId));
                queries.add(String.format("UPDATE observe_longline.weightMeasure SET weightMeasureType2 = '%s' WHERE weightMeasureType = '%s'", newId, oldId));
            }
        }

    }

    public static class H2DataSourceMigrationForVersion extends DataSourceMigrationForVersion_6_0 {

        public H2DataSourceMigrationForVersion(AbstractDataSourceMigration callBack) {
            super(callBack, H2DataSourceMigration.TYPE);
        }

    }

    public static class PGDataSourceMigrationForVersion extends DataSourceMigrationForVersion_6_0 {

        public PGDataSourceMigrationForVersion(AbstractDataSourceMigration callBack) {
            super(callBack, PGDataSourceMigration.TYPE);
        }

    }

}

