package fr.ird.observe.persistence.migration;

/*-
 * #%L
 * ObServe :: Persistence
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.migration.TopiaMigrationCallbackByClass;
import org.nuiton.version.Version;
import org.nuiton.version.VersionBuilder;
import org.reflections.Reflections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created on 01/08/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0
 */
public final class ObserveMigrationConfigurationProvider {

    /** Logger */
    private static final Log log = LogFactory.getLog(ObserveMigrationConfigurationProvider.class);

    private static ObserveMigrationConfigurationProvider INSTANCE;

    private static Map<Version, Class<?>> migrationForVersionTypes;

    private final Version[] availableVersions;

    public static Map<Version, Class<?>> getMigrationForVersionTypes() {
        if (migrationForVersionTypes == null) {

            TreeMap<Version, Class<?>> builder = new TreeMap<>();

            Set<Class<?>> types = new Reflections("fr.ird.observe.persistence.migration").getTypesAnnotatedWith(MigrationVersion.class);
            for (Class<?> type : types) {
                Version version = getVersion(type);
                builder.put(version, type);
                if (log.isInfoEnabled()) {
                    log.info("Found migration version: " + version);
                }
            }
            migrationForVersionTypes = ImmutableMap.copyOf(builder);

        }
        return migrationForVersionTypes;
    }

    public static Version getVersion(Class type) {

        Objects.requireNonNull(type);
        if (type.isMemberClass()) {
            type = type.getEnclosingClass();
        }
        MigrationVersion annotation = (MigrationVersion) type.getDeclaredAnnotation(MigrationVersion.class);
        Objects.requireNonNull(annotation);
        String version = annotation.version();
        return VersionBuilder.create(version).build();

    }

    public ObserveMigrationConfigurationProvider() {
        Set<Version> versionsSet = getMigrationForVersionTypes().keySet();
        List<Version> versions = new ArrayList<>(versionsSet);
        Collections.sort(versions);
        this.availableVersions = versions.toArray(new Version[versions.size()]);
    }

    public Version[] getAvailableVersions() {
        return availableVersions;
    }

    public Version getLastVersion() {
        Version[] values = getAvailableVersions();
        int lastIndex = values.length - 1;
        return values[lastIndex];
    }


    public ImmutableList<Version> getVersionsAfter(Version current) {

        ImmutableList.Builder<Version> builder = ImmutableList.builder();

        for (Version version : getAvailableVersions()) {

            if (version.after(current)) {
                builder.add(version);
            }

        }

        return builder.build();

    }

    public Version getMinimumVersion() {
        return VersionBuilder.create("3.0").build();
    }

    public Class<? extends TopiaMigrationCallbackByClass> getMigrationClassBack(boolean h2) {
        return h2 ? H2DataSourceMigration.class : PGDataSourceMigration.class;
    }

    public static ObserveMigrationConfigurationProvider get() {
        if (INSTANCE == null) {
            INSTANCE = new ObserveMigrationConfigurationProvider();
        }
        return INSTANCE;
    }

}
