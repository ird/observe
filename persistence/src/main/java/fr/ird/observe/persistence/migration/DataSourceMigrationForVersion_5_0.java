package fr.ird.observe.persistence.migration;

/*
 * #%L
 * ObServe :: Persistence
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.support.TopiaSqlSupport;
import org.nuiton.version.VersionBuilder;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created on 25/08/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@MigrationVersion(version = "5.0")
public abstract class DataSourceMigrationForVersion_5_0 extends AbstractObserveMigrationCallBack {

    public DataSourceMigrationForVersion_5_0(AbstractDataSourceMigration callBack, String scriptSuffix) {
        super(DataSourceMigrationForVersion_5_0.class, callBack, scriptSuffix);
    }

    @Override
    protected void prepareMigrationScript(TopiaSqlSupport topiaSqlSupport,
                                          List<String> queries,
                                          boolean showSql,
                                          boolean showProgression) {

        // Suppression du champ open sur les entités anciennement du type Openable
        // See https://forge.codelutin.com/issues/7469
        addScript("01", "remove_open_field", queries);

        // Ajout des champs lastUpdate
        // See https://forge.codelutin.com/issues/7470
        addScript("02", "add_lastUpdateDate_field", queries);

        // Ajout de la table lastUpdateDate
        // See https://forge.codelutin.com/issues/7470
        addScript("03", "add_lastUpdateDate_table", queries);


        // Ajout de la table lastUpdateDate
        // See https://forge.codelutin.com/issues/7470
        addScript("04", "remove_unit_field", queries);
        addScript("05", "remove_gender_field", queries);
        addScript("06", "remove_duplicate_rows", queries);


        addScript("07", "drop_not_null_constraint_gearUseFeatures", queries);
        addScript("08", "drop_not_null_constraint_hauling_identifier", queries);
        addScript("09", "add_non_target_length_sex_fk", queries);
    }

    public static class H2DataSourceMigrationForVersion extends DataSourceMigrationForVersion_5_0 {

        public H2DataSourceMigrationForVersion(AbstractDataSourceMigration callBack) {
            super(callBack, H2DataSourceMigration.TYPE);
        }

        @Override
        protected void prepareMigrationScript(TopiaSqlSupport topiaSqlSupport, List<String> queries, boolean showSql, boolean showProgression) {

            super.prepareMigrationScript(topiaSqlSupport, queries, showSql, showProgression);

            Set<String> forkeignKeys = H2DataSourceMigration.getForeignKeyConstraintNames(topiaSqlSupport, "nontargetlength");
            Set<String> collect = forkeignKeys.stream().map(String::toLowerCase).collect(Collectors.toSet());
            if (!collect.contains("fk_nontargetlength_sex")) {
                addScript(VersionBuilder.create("4.906").build(), "01", "add_non_target_length_sex_fk", queries);
            }
        }

    }

    public static class PGDataSourceMigrationForVersion extends DataSourceMigrationForVersion_5_0 {

        public PGDataSourceMigrationForVersion(AbstractDataSourceMigration callBack) {
            super(callBack, PGDataSourceMigration.TYPE);
        }

        @Override
        protected void prepareMigrationScript(TopiaSqlSupport topiaSqlSupport, List<String> queries, boolean showSql, boolean showProgression) {

            super.prepareMigrationScript(topiaSqlSupport, queries, showSql, showProgression);

            Set<String> forkeignKeys = PGDataSourceMigration.getForeignKeyConstraintNames(topiaSqlSupport, "nontargetlength");
            Set<String> collect = forkeignKeys.stream().map(String::toLowerCase).collect(Collectors.toSet());
            if (!collect.contains("fk_nontargetlength_sex")) {
                addScript(VersionBuilder.create("4.906").build(), "01", "add_non_target_length_sex_fk", queries);
            }
        }


    }

}
