package fr.ird.observe.persistence.migration.old;

/*
 * #%L
 * ObServe :: Persistence
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.persistence.migration.AbstractDataSourceMigration;
import fr.ird.observe.persistence.migration.AbstractObserveMigrationCallBack;
import fr.ird.observe.persistence.migration.H2DataSourceMigration;
import fr.ird.observe.persistence.migration.MigrationVersion;
import fr.ird.observe.persistence.migration.PGDataSourceMigration;
import org.nuiton.topia.persistence.support.TopiaSqlSupport;

import java.util.List;

/**
 * Created on 6/8/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 4.0.1
 */
@MigrationVersion(version = "4.0.2")
public abstract class DataSourceMigrationForVersion_4_0_2 extends AbstractObserveMigrationCallBack {

    public DataSourceMigrationForVersion_4_0_2(AbstractDataSourceMigration callBack, String scriptSuffix) {
        super(DataSourceMigrationForVersion_4_0_2.class, callBack, scriptSuffix);
    }

    public static class H2DataSourceMigrationForVersion extends DataSourceMigrationForVersion_4_0_2 {

        public H2DataSourceMigrationForVersion(AbstractDataSourceMigration callBack) {
            super(callBack, H2DataSourceMigration.TYPE);
        }

        @Override
        protected void prepareMigrationScript(TopiaSqlSupport tx, List<String> queries, boolean showSql, boolean showProgression) {
            // Rien à faire (déjà fait en 4.0.1)
        }

    }

    public static class PGDataSourceMigrationForVersion extends DataSourceMigrationForVersion_4_0_2 {

        public PGDataSourceMigrationForVersion(AbstractDataSourceMigration callBack) {
            super(callBack, PGDataSourceMigration.TYPE);
        }

        @Override
        protected void prepareMigrationScript(TopiaSqlSupport tx, List<String> queries, boolean showSql, boolean showProgression) {

            // See http://forge.codelutin.com/issues/7467
            addScript("01", "remove-gender-column", queries);

        }

    }

}
