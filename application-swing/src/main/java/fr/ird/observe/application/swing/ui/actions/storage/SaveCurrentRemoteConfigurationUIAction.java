package fr.ird.observe.application.swing.ui.actions.storage;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ui.ObserveKeyStrokes;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.actions.AbstractUIAction;
import fr.ird.observe.application.swing.ui.storage.StorageUIModel;
import fr.ird.observe.application.swing.ui.storage.tabs.ConfigUI;
import fr.ird.observe.services.dto.presets.RemoteDataSourceConfiguration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.AbstractButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 18/12/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class SaveCurrentRemoteConfigurationUIAction extends AbstractUIAction {

    /** Logger */
    private static final Log log = LogFactory.getLog(SaveCurrentRemoteConfigurationUIAction.class);

    public static final String ACTION_NAME = SaveCurrentRemoteConfigurationUIAction.class.getName();

    public SaveCurrentRemoteConfigurationUIAction(ObserveMainUI mainUI) {
        super(mainUI, ACTION_NAME, t("observe.storage.remoteConfiguration.presets.save"), t("observe.storage.remoteConfiguration.presets.save"), "save", ObserveKeyStrokes.KEY_STROKE_SAVE);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        AbstractButton value = (AbstractButton) getValue(EDITOR);
        ConfigUI configUI = (ConfigUI) value.getClientProperty(CLIENT_PROPERTY_UI);
        StorageUIModel model = configUI.getModel();

        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(0, 2));

        JTextField question = new JTextField();
        panel.add(new JLabel(t("observe.storage.remote.configuration.name")));
        panel.add(question);

        panel.add(new JLabel(t("observe.storage.remote.url")));
        JTextField url = new JTextField(model.getRemoteUrl());
        url.setEditable(false);
        panel.add(url);

        panel.add(new JLabel(t("observe.storage.remote.login")));
        JTextField login = new JTextField(model.getRemoteLogin());
        login.setEditable(false);
        panel.add(login);

        panel.add(new JLabel(t("observe.storage.remote.password")));
        JTextField password = new JTextField(new String(model.getRemotePassword()));
        password.setEditable(false);
        panel.add(password);

        JCheckBox databaseName = new JCheckBox(t("observe.storage.remote.useSll"), model.isUseSsl());
        databaseName.setEnabled(false);
        panel.add(databaseName);

        question.setPreferredSize(new Dimension(300, 30));
        int response = UIHelper.askUser(getMainUI(), t("observe.storage.remote.configuration.save.title"), panel, JOptionPane.QUESTION_MESSAGE, new String[]{
                t("observe.action.save"),
                t("observe.action.cancel")
        }, 0);
        String configurationName = question.getText().trim();

        if (response != 0 || configurationName.isEmpty()) {
            if (log.isInfoEnabled()) {
                log.info("User skip save of configuration");
            }
            return;
        }
        if (log.isInfoEnabled()) {
            log.info("Will add remote configuration: " + configurationName);
        }

        RemoteDataSourceConfiguration configuration = new RemoteDataSourceConfiguration();
        configuration.setName(configurationName);
        configuration.setUrl(model.getRemoteUrl());
        configuration.setLogin(model.getRemoteLogin());
        configuration.setPassword(new String(model.getRemotePassword()));
        configuration.setUseSsl(model.isUseSsl());
        getMainUI().getConfig().addRemoteDataSourceConfiguration(configuration);
        configUI.getHandler().addRemoteConfiguration(configuration);
    }
}
