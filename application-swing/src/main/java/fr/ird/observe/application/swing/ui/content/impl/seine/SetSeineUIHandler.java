/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.content.impl.seine;

import fr.ird.observe.application.swing.I18nEnumHelper;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.DataContext;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.db.constants.DataContextType;
import fr.ird.observe.application.swing.ui.content.ContentMode;
import fr.ird.observe.application.swing.ui.content.ContentUIHandler;
import fr.ird.observe.application.swing.ui.content.ContentUIModel;
import fr.ird.observe.application.swing.ui.tree.navigation.NavigationTree;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.NavigationTreeNodeSupport;
import fr.ird.observe.common.constants.seine.SchoolType;
import fr.ird.observe.services.dto.form.Form;
import fr.ird.observe.services.dto.reference.DataReference;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.dto.seine.ActivitySeineDto;
import fr.ird.observe.services.dto.seine.RouteDto;
import fr.ird.observe.services.dto.seine.SetSeineDto;
import fr.ird.observe.services.dto.seine.SetSeineHelper;
import fr.ird.observe.services.service.data.seine.RouteService;
import fr.ird.observe.services.service.data.seine.SetSeineService;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.widgets.hidor.HidorButton;
import org.nuiton.jaxx.validator.swing.SwingValidatorMessage;
import org.nuiton.jaxx.validator.swing.SwingValidatorMessageTableModel;
import org.nuiton.jaxx.validator.swing.SwingValidatorUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.DateUtil;
import org.nuiton.validator.NuitonValidatorScope;

import javax.swing.event.TableModelListener;
import java.beans.PropertyChangeListener;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class SetSeineUIHandler extends ContentUIHandler<SetSeineDto, SetSeineUI> implements UIHandler<SetSeineUI> {

    /** Logger */
    private static final Log log = LogFactory.getLog(SetSeineUIHandler.class);

    // Compute valid state of tab from the error table
    protected final TableModelListener computeTabValidStateListener;

    // Change toogle target visible on all TimeEditors
    protected final PropertyChangeListener onToogleTimeEditorSliderChangedListener;

    protected boolean toogleTimeEditorSliderIsChanging;

    public SetSeineUIHandler() {
        super(DataContextType.ActivitySeine, DataContextType.SetSeine);
        computeTabValidStateListener = e -> {

            SwingValidatorMessageTableModel source = (SwingValidatorMessageTableModel) e.getSource();
            computeTabValidState(source);

        };
        onToogleTimeEditorSliderChangedListener = evt -> {
            Boolean newValue = (Boolean) evt.getNewValue();
            onToogleTimeEditorSliderChanged(newValue);
        };
    }

    public String updateTypeValue(SchoolType schoolType) {
        if (schoolType == null) {
            return t("observe.content.setSeine.schoolType.not.fill");
        }
        return I18nEnumHelper.getLabel(schoolType);
    }

    @Override
    protected boolean computeCanWrite(ObserveSwingDataSource source) {
        return source.canWriteData();
    }

    @Override
    protected ContentMode getContentMode(DataContext dataContext) {

        String setId = getSelectedId();

        if (setId == null) {

            // mode creation
            return ContentMode.CREATE;
        }

        if (getOpenDataManager().isOpenActivitySeine(dataContext.getSelectedActivitySeineId())) {

            // l'activity est ouverte, mode édition
            return ContentMode.UPDATE;
        }

        // l'activity n'est pas ouverte, mode lecture
        addMessage(getUi(),
                   NuitonValidatorScope.INFO,
                   getTypeI18nKey(ActivitySeineDto.class),
                   t("observe.storage.activitySeine.message.not.open"));
        return ContentMode.READ;
    }

    @Override
    public void afterInit(SetSeineUI ui) {
        super.afterInit(ui);

        // To be sure always remove listener (could prevent some leaks)
        ui.getErrorTableModel().removeTableModelListener(computeTabValidStateListener);
        // listen messages to see if required to add
        ui.getErrorTableModel().addTableModelListener(computeTabValidStateListener);

        ui.getStartTime().getSliderHidor().addPropertyChangeListener(HidorButton.PROPERTY_TARGET_VISIBLE, onToogleTimeEditorSliderChangedListener);
        ui.getEndPursingTimeStamp().getSliderHidor().addPropertyChangeListener(HidorButton.PROPERTY_TARGET_VISIBLE, onToogleTimeEditorSliderChangedListener);
        ui.getEndSetTimeStamp().getSliderHidor().addPropertyChangeListener(HidorButton.PROPERTY_TARGET_VISIBLE, onToogleTimeEditorSliderChangedListener);

    }

    @Override
    public void openUI() {
        super.openUI();

        String activityId = getSelectedParentId();
        String setId = getSelectedId();
        String selectedRouteId = getDataContext().getSelectedRouteId();

        if (log.isInfoEnabled()) {
            log.info("activityId = " + activityId);
            log.info("setId    = " + setId);
        }
        ContentMode mode = computeContentMode();

        SetSeineDto bean = getBean();

        Form<SetSeineDto> form;
        if (setId == null) {

            // create mode
            form = getSetSeineService().preCreate(selectedRouteId, activityId);

        } else {

            // update mode
            form = getSetSeineService().loadForm(setId);

        }

        // utilisation du mode requis
        setContentMode(mode);

        loadReferentialReferenceSetsInModel(form);

        getModel().setForm(form);
        SetSeineHelper.copySetSeineDto(form.getObject(), bean);

        RouteService routeService = ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newRouteService();

        RouteDto route = routeService.loadDto(selectedRouteId);

        Date time = bean.getStartTime();

        Date date = route.getDate();

        Date dateAndTime = DateUtil.getDateAndTime(date, time, false, false);

        getUi().getStartTime().setDate(dateAndTime);

        SwingValidatorMessageTableModel errorTableModel = getUi().getErrorTableModel();
        computeTabValidState(errorTableModel);

        if (mode != ContentMode.READ) {
            getUi().startEdit(null);
        }
    }

    @Override
    public void startEditUI(String... binding) {
        ContentUIModel<SetSeineDto> model = getModel();
        boolean create = model.getMode() == ContentMode.CREATE;
        String contextName = getValidatorContextName(model.getMode());
        getUi().getValidator().setContext(contextName);
        if (create) {
            addInfoMessage(t("observe.content.setSeine.message.creating"));
        } else {
            addInfoMessage(t("observe.content.setSeine.message.updating"));
        }

        super.startEditUI(SetSeineUI.BINDING_SUPPORT_VESSEL_NAME_TEXT,
                          SetSeineUI.BINDING_COMMENT_MODEL,
                          SetSeineUI.BINDING_START_TIME_DATE,
                          SetSeineUI.BINDING_END_SET_TIME_STAMP_DATE,
                          SetSeineUI.BINDING_END_SET_TIME_STAMP_DATE,
                          SetSeineUI.BINDING_END_PURSING_TIME_STAMP_DATE,
                          SetSeineUI.BINDING_CURRENT_SPEED_NUMBER_VALUE,
                          SetSeineUI.BINDING_CURRENT_DIRECTION_NUMBER_VALUE,
                          SetSeineUI.BINDING_REASON_FOR_NULL_SET_SELECTED_ITEM,
                          SetSeineUI.BINDING_CURRENT_SPEED_NUMBER_VALUE,
                          SetSeineUI.BINDING_NON_TARGET_DISCARDED_BOOLEAN_VALUE,
                          SetSeineUI.BINDING_TARGET_DISCARDED_BOOLEAN_VALUE,
                          SetSeineUI.BINDING_SCHOOL_THICKNESS_NUMBER_VALUE,
                          SetSeineUI.BINDING_SCHOOL_TYPE_TEXT,
                          SetSeineUI.BINDING_MAX_GEAR_DEPTH_NUMBER_VALUE,
                          SetSeineUI.BINDING_SCHOOL_MEAN_DEPTH_NUMBER_VALUE,
                          SetSeineUI.BINDING_SCHOOL_TOP_DEPTH_NUMBER_VALUE);
        model.setModified(create);
    }

    @Override
    protected boolean doSave(SetSeineDto bean) {

        String activityId = getSelectedParentId();
        boolean notPersisted = bean.isNotPersisted();

        SaveResultDto saveResult = getSetSeineService().save(activityId, bean);
        saveResult.toDto(bean);

        if (notPersisted) {
            getOpenDataManager().openSetSeine(activityId, bean.getId());
        }

        return true;
    }

    @Override
    protected void afterSave(boolean refresh) {

        super.afterSave(refresh);

        SetSeineDto bean = getBean();

        SwingValidatorUtil.setValidatorChanged(getUi(), false);

        NavigationTree treeHelper = getTreeHelper(getUi());

        NavigationTreeNodeSupport node = treeHelper.getSelectedNode();
        boolean create = node.getId() == null;

        if (create) {
            getModel().setMode(ContentMode.UPDATE);

            // remove old node and recreate new node
            NavigationTreeNodeSupport parentNode = node.getParent();
            treeHelper.removeNode(node);

            DataReference<SetSeineDto> beanRef = ObserveSwingApplicationContext.get().getReferenceBinderEngine().transformDataDtoToReference(getDecoratorService().getReferentialLocale(), bean);
            node = treeHelper.addSetSeine(parentNode, beanRef);

            stopEditUI();
            treeHelper.selectNode(node);
        } else {
            // select ancestor node
//            treeHelper.refreshNode(node, false);
            treeHelper.reloadSelectedNode(false, false);
        }
    }

    protected boolean doDelete(SetSeineDto bean) {

        if (askToDelete(bean)) {
            return false;
        }

        String activityId = getSelectedParentId();
        getSetSeineService().delete(activityId, bean.getId());
        return true;
    }

    protected void computeTabValidState(SwingValidatorMessageTableModel errorTableModel) {

        Set<String> errorProperties = new HashSet<>();
        int rowCount = errorTableModel.getRowCount();
        for (int i = 0; i < rowCount; i++) {

            SwingValidatorMessage row = errorTableModel.getRow(i);
            if (NuitonValidatorScope.ERROR.equals(row.getScope())) {
                errorProperties.add(row.getField());
            }
        }

        boolean generalTabValid = !errorProperties.removeAll(SetSeineUIModel.GENERAL_TAB_PROPERTIES);
        boolean measurementsTabValid = !errorProperties.removeAll(SetSeineUIModel.MEASUREMENTS_TAB_PROPERTIES);

        SetSeineUIModel model = (SetSeineUIModel) getModel();
        model.setGeneralTabValid(generalTabValid);
        model.setMeasurementsTabValid(measurementsTabValid);

    }

    protected void onToogleTimeEditorSliderChanged(boolean newValue) {

        if (!toogleTimeEditorSliderIsChanging) {

            toogleTimeEditorSliderIsChanging = true;

            try {

                getUi().getStartTime().getSliderHidor().setSelected(!newValue);
                getUi().getStartTime().getSliderHidor().setTargetVisible(newValue);

                getUi().getEndPursingTimeStamp().getSliderHidor().setSelected(!newValue);
                getUi().getEndPursingTimeStamp().getSliderHidor().setTargetVisible(newValue);

                getUi().getEndSetTimeStamp().getSliderHidor().setSelected(!newValue);
                getUi().getEndSetTimeStamp().getSliderHidor().setTargetVisible(newValue);

            } finally {

                toogleTimeEditorSliderIsChanging = false;

            }

        }

    }

    protected SetSeineService getSetSeineService() {
        return ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newSetSeineService();
    }
}
