package fr.ird.observe.application.swing.ui.tree.menu;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.decoration.DecoratorService;
import fr.ird.observe.application.swing.decoration.decorators.DataReferenceDecorator;
import fr.ird.observe.application.swing.ui.tree.actions.ChangeActivityTripActionListener;
import fr.ird.observe.application.swing.ui.tree.actions.NodeChangeActionListener;
import fr.ird.observe.application.swing.ui.tree.navigation.NavigationTree;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.NavigationTreeNodeSupport;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.longline.TripLonglineNavigationTreeNode;
import fr.ird.observe.application.swing.ui.util.DecoratedNodeEntity;
import fr.ird.observe.services.dto.IdHelper;
import fr.ird.observe.services.dto.longline.TripLonglineDto;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 5.0
 */
public class MoveActivityLonglineNodeMenuPopulator extends MoveNodeMenuPopulator {

    @Override
    public NodeChangeActionListener createChangeActionListener(NavigationTree tree,
                                                               String id,
                                                               String parentId) {
        return new ChangeActivityTripActionListener(tree, id, parentId);
    }

    @Override
    public List<DecoratedNodeEntity> getPossibleParentNodes(NavigationTreeNodeSupport activityLonglineNode) {

        // noeud de marée parent
        NavigationTreeNodeSupport parentNode = activityLonglineNode.getParent().getParent();

        // noeud de route de la marée sans le parent actuel
        List<DecoratedNodeEntity> possibleParents = new ArrayList<>();

        // noeud du programme
        NavigationTreeNodeSupport programNode = parentNode.getParent();

        DecoratorService decoratorService = ObserveSwingApplicationContext.get().getDecoratorService();
        DataReferenceDecorator<TripLonglineDto> tripDecorator = decoratorService.getDataReferenceDecorator(TripLonglineDto.class);

        for (int i = 0, n = programNode.getChildCount(); i < n; i++) {

            TripLonglineNavigationTreeNode tripNode = (TripLonglineNavigationTreeNode) programNode.getChildAt(i);
            String tripId = tripNode.getId();

            // si le noeud de marée n'est pas le même que le parent actuel
            // si le noeud est bien un noeud de marée longline
            if (!parentNode.equals(tripNode) && IdHelper.isTripLonglineId(tripId)) {

                possibleParents.add(DecoratedNodeEntity.newDecoratedNodeEntity(tripNode, tripDecorator));

            }
        }

        return possibleParents;
    }
}
