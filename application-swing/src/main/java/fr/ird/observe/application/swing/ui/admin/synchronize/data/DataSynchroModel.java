package fr.ird.observe.application.swing.ui.admin.synchronize.data;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.ui.admin.AdminActionModel;
import fr.ird.observe.application.swing.ui.admin.AdminStep;
import fr.ird.observe.application.swing.ui.admin.AdminUI;
import fr.ird.observe.application.swing.ui.admin.config.ConfigUI;
import fr.ird.observe.application.swing.ui.admin.synchronize.data.task.DataSynchronizeTaskSupport;
import fr.ird.observe.application.swing.ui.tree.selection.SelectionTreeModel;
import fr.ird.observe.application.swing.ui.util.ProgressModel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.DefaultListModel;
import javax.swing.border.TitledBorder;

/**
 * Created on 02/08/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0
 */
public class DataSynchroModel extends AdminActionModel {

    /** Logger. */
    private static final Log log = LogFactory.getLog(DataSynchroModel.class);

    public static final String LEFT_SOURCE_PROPERTY_NAME = "leftSource";
    public static final String RIGHT_SOURCE_PROPERTY_NAME = "rightSource";

    public static final String LEFT_SELECTION_MODEL_CHANGED_PROPERTY_NAME = "leftSelectionModelChanged";
    public static final String RIGHT_SELECTION_MODEL_CHANGED_PROPERTY_NAME = "rightSelectionModelChanged";

    /** la source sur le panneau de gauche. */
    protected ObserveSwingDataSource leftSource;

    /** la source sur le panneau de droite. */
    protected ObserveSwingDataSource rightSource;

    /** les données sélectionnées sur le panneau de gauche. */
    protected SelectionTreeModel leftSelectionDataModel;

    /** les données sélectionnées sur le panneau de droite. */
    protected SelectionTreeModel rightSelectionDataModel;

    protected final DefaultListModel<DataSynchronizeTaskSupport> tasks;

    private final ProgressModel progressModel = new ProgressModel();

    public DataSynchroModel() {
        super(AdminStep.DATA_SYNCHRONIZE);

//        leftSelectionDataModel = new SelectionTreeModel();
//        leftSelectionDataModel.setLoadLongline(true);
//        leftSelectionDataModel.setLoadSeine(true);
//        leftSelectionDataModel.setLoadReferential(false);
//        leftSelectionDataModel.setShowEmptyTrips(false);
//
//        rightSelectionDataModel = new SelectionTreeModel();
//        rightSelectionDataModel.setLoadLongline(true);
//        rightSelectionDataModel.setLoadSeine(true);
//        rightSelectionDataModel.setLoadReferential(false);
//        rightSelectionDataModel.setShowEmptyTrips(false);
        tasks = new DefaultListModel<>();

    }

    @Override
    public void destroy() {
        super.destroy();
        leftSelectionDataModel = null;
        rightSelectionDataModel = null;
        tasks.clear();
        leftSource = null;
        rightSource = null;
    }

    public void setLeftSelectionDataModel(SelectionTreeModel leftSelectionDataModel) {
        this.leftSelectionDataModel = leftSelectionDataModel;
    }

    public void setRightSelectionDataModel(SelectionTreeModel rightSelectionDataModel) {
        this.rightSelectionDataModel = rightSelectionDataModel;
    }

    public ObserveSwingDataSource getLeftSource() {
        return leftSource;
    }

    public void setLeftSource(ObserveSwingDataSource leftSource) {
        this.leftSource = leftSource;
        firePropertyChange(LEFT_SOURCE_PROPERTY_NAME, leftSource);
    }

    public ObserveSwingDataSource getRightSource() {
        return rightSource;
    }

    public void setRightSource(ObserveSwingDataSource rightSource) {
        this.rightSource = rightSource;
        firePropertyChange(RIGHT_SOURCE_PROPERTY_NAME, rightSource);
    }

    public SelectionTreeModel getLeftSelectionDataModel() {
        return leftSelectionDataModel;
    }

    public SelectionTreeModel getRightSelectionDataModel() {
        return rightSelectionDataModel;
    }

    public void populateLeftSelectionModel() {
        populateSelectionModel(leftSource, leftSelectionDataModel, LEFT_SELECTION_MODEL_CHANGED_PROPERTY_NAME);
    }

    public void populateRightSelectionModel() {
        populateSelectionModel(rightSource, rightSelectionDataModel, RIGHT_SELECTION_MODEL_CHANGED_PROPERTY_NAME);
    }

    public DefaultListModel<DataSynchronizeTaskSupport> getTasks() {
        return tasks;
    }

    public void addTask(DataSynchronizeTaskSupport task) {
        tasks.addElement(task);
    }

    private void populateSelectionModel(ObserveSwingDataSource dataSource, SelectionTreeModel selectionDataModel, String propertyName) {

        try {

            selectionDataModel.populate(dataSource);

        } catch (Exception e) {

            if (log.isErrorEnabled()) {
                log.error("could not populate selected model", e);
            }

        } finally {

            // on notifie que le modèle de sélection a changé
            // (il faut donc recalculé l'arbre de sélection)
            firePropertyChange(propertyName, selectionDataModel);

        }
    }

    public void start(AdminUI ui) {

        ConfigUI configUI = (ConfigUI) ui.getStepUI(AdminStep.CONFIG);
        configUI.getLocalSourceConfig().setBorder(new TitledBorder(ui.getModel().getConfigModel().getLocalSourceLabel()));
        configUI.getCentralSourceConfig().setBorder(new TitledBorder(ui.getModel().getConfigModel().getCentralSourceLabel()));

    }

    public ProgressModel getProgressModel() {
        return progressModel;
    }
}
