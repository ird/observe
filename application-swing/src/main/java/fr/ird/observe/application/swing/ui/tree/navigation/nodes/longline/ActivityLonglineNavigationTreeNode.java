package fr.ird.observe.application.swing.ui.tree.navigation.nodes.longline;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ui.content.open.impl.longline.ActivityLonglineUI;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.ReferenceNavigationTreeNodeSupport;
import fr.ird.observe.services.dto.longline.ActivityLonglineDto;
import fr.ird.observe.services.dto.longline.ActivityLonglineHelper;
import fr.ird.observe.services.dto.longline.SetLonglineDto;
import fr.ird.observe.services.dto.reference.DataReference;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 14/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class ActivityLonglineNavigationTreeNode extends ReferenceNavigationTreeNodeSupport<ActivityLonglineDto, DataReference<ActivityLonglineDto>> {

    public ActivityLonglineNavigationTreeNode(DataReference<ActivityLonglineDto> data) {
        super(data, true);
        if (isPersisted()) {
            DataReference<SetLonglineDto> setLonglineRef = ActivityLonglineHelper.getSetLongline(data);
            if (setLonglineRef != null) {
                add(new SetLonglineNavigationTreeNode(setLonglineRef));
            }

            String vesselActivityId = ActivityLonglineHelper.getVeseelActivityId(data);

            if (ActivityLonglineHelper.ENCOUNTERS_ID.contains(vesselActivityId)) {
                add(new EncounterLonglineNavigationTreeNode());
            }
            if (ActivityLonglineHelper.SENSOR_USED_ID.contains(vesselActivityId)) {
                add(new SensorUsedLonglineNavigationTreeNode());
            }
        }
    }

    @Override
    public boolean isLeaf() {
        return !isPersisted();
    }

    @Override
    public void reload() {
        DataReference<ActivityLonglineDto> data = getMainDataSourceServicesProvider().newActivityLonglineService().loadReferenceToRead(getId());
        setData(data);
    }

    @Override
    public String getText() {
        return isPersisted() ? super.getText() : t("observe.type.activityLongline.unsaved");
    }

    @Override
    public Class<ActivityLonglineUI> getContentClass() {
        return ActivityLonglineUI.class;
    }

}
