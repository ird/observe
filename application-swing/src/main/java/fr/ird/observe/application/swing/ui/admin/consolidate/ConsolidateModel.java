/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.admin.consolidate;

import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.ui.admin.AdminActionModel;
import fr.ird.observe.application.swing.ui.admin.AdminStep;
import fr.ird.observe.application.swing.ui.admin.AdminUIModel;
import fr.ird.observe.application.swing.ui.util.ProgressModel;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardState;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.beans.PropertyChangeListener;

/**
 * Modele pour preparer une validation de donnees d'une base.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.5
 */
public class ConsolidateModel extends AdminActionModel {

    /** Logger */
    private static final Log log = LogFactory.getLog(ConsolidateModel.class);

    /** la source de données où effectuer la consolidation des données */
    private ObserveSwingDataSource source;
    private final ProgressModel progressModel = new ProgressModel();

    public ConsolidateModel() {
        super(AdminStep.CONSOLIDATE);
    }

    public ObserveSwingDataSource getSource() {
        return source;
    }

    public void setSource(ObserveSwingDataSource source) {
        this.source = source;
    }

    @Override
    public void destroy() {
        super.destroy();
        source = null;
    }

    public void start(AdminUIModel uiModel) {

        // Anything to init ?

        PropertyChangeListener listenConsolidateModified = evt -> {
            ConsolidateModel source = (ConsolidateModel) evt.getSource();
            if (log.isDebugEnabled()) {
                log.debug("consolidate model [" + source + "] changed on " + evt.getPropertyName() + ", new value = " + evt.getNewValue());
            }
            uiModel.validate();
        };

        removePropertyChangeListener(listenConsolidateModified);
        addPropertyChangeListener(listenConsolidateModified);
    }

    public boolean validate(AdminUIModel uiModel) {
        return uiModel.validate(AdminStep.SELECT_DATA) && uiModel.getStepState(step) == WizardState.SUCCESSED;
    }

    public ProgressModel getProgressModel() {
        return progressModel;
    }
}
