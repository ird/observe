<!--
  #%L
  ObServe :: Application Swing
  %%
  Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

<!-- ***************************************** -->
<!-- L'écran de configuration de la source     -->
<!-- ***************************************** -->

<StorageTabUI>

  <import>
    fr.ird.observe.application.swing.I18nEnumHelper
    fr.ird.observe.application.swing.db.constants.ConnexionStatus
    fr.ird.observe.application.swing.ui.actions.storage.SaveCurrentRemoteConfigurationUIAction
    fr.ird.observe.application.swing.ui.actions.storage.SaveCurrentServerConfigurationUIAction
    fr.ird.observe.application.swing.ui.storage.StorageStep

    java.awt.Color
    java.awt.Dimension

    java.io.File

    javax.swing.border.LineBorder
    javax.swing.text.html.HTMLEditorKit

    static org.nuiton.i18n.I18n.n
  </import>

  <StorageStep id='step' initializer='StorageStep.CONFIG'/>

  <fr.ird.observe.application.swing.ui.storage.StorageUIModel id='model'/>

  <CardLayout id='configLayout'/>

  <JPanel id='content' layout='{new BorderLayout()}'>
    <JPanel id='configContent' constraints='BorderLayout.CENTER'>

      <!-- configurer creation base locale a partir d'une sauvegarde -->
      <Table id='IMPORT_EXTERNAL_DUMP' constraints='"IMPORT_EXTERNAL_DUMP"'>
        <row>
          <cell>
            <JLabel text='observe.storage.importExternalDump.config'/>
          </cell>
        </row>
        <row weighty='1'>
          <cell weightx='1' fill="horizontal">
            <JTextField id="dumpFile"
                        onKeyReleased='getModel().setDumpFile(new File(((JTextField)event.getSource()).getText()))'/>
          </cell>
          <cell anchor="east">
            <JButton id="fileChooserAction" onActionPerformed="handler.chooseDumpFile()"/>
          </cell>
        </row>
      </Table>

      <!-- configurer creation base locale et import referentiel depuis base distante  -->
      <Table id='IMPORT_REMOTE_STORAGE' fill="both" constraints='"IMPORT_REMOTE_STORAGE"'>
        <row>
          <cell>
            <JLabel text='observe.storage.importRemoteStorage.config'/>
          </cell>
        </row>
        <row>
          <cell weighty='1' weightx='1'>
            <JPanel id='IMPORT_REMOTE_STORAGE_content'/>
          </cell>
        </row>

      </Table>

      <!-- configurer creation base locale et import referentiel depuis un server distant  -->
      <Table id='IMPORT_SERVER_STORAGE' fill="both" constraints='"IMPORT_SERVER_STORAGE"'>
        <row>
          <cell>
            <JLabel text='observe.storage.importServerStorage.config'/>
          </cell>
        </row>
        <row>
          <cell weighty='1' weightx='1'>
            <JPanel id='IMPORT_SERVER_STORAGE_content'/>
          </cell>
        </row>

      </Table>

      <!-- configurer connexion a base distante  -->
      <Table id='USE_REMOTE' constraints='"USE_REMOTE"' fill='both'>
        <row>
          <cell>
            <JPanel layout="{new BorderLayout()}">
              <JLabel constraints='BorderLayout.WEST' text='observe.storage.useRemoteStorage.config'/>
              <JMenuBar id='remoteMenuBar' constraints='BorderLayout.EAST'>
                <JMenu id='remoteMenu'>
                  <JMenuItem id="saveRemoteConfiguration"/>
                  <JSeparator/>
                </JMenu>
              </JMenuBar>
            </JPanel>
          </cell>
        </row>
        <row>
          <cell weighty='1' weightx='1'>
            <JPanel id='USE_REMOTE_content' minimumSize='{new Dimension(20,300)}'/>
          </cell>
        </row>
      </Table>

      <!-- configurer connexion au server base distante  -->
      <Table id='USE_SERVER' constraints='"USE_SERVER"' fill='both'>
        <row>
          <cell>
            <JPanel layout="{new BorderLayout()}">
              <JLabel constraints='BorderLayout.WEST' text='observe.storage.useServerStorage.config'/>
              <JMenuBar id='serverMenuBar' constraints='BorderLayout.EAST'>
                <JMenu id='serverMenu'>
                  <JMenuItem id="saveServerConfiguration"/>
                  <JSeparator/>
                </JMenu>
              </JMenuBar>
            </JPanel>
          </cell>
        </row>
        <row>
          <cell weighty='1' weightx='1'>
            <JPanel id='USE_SERVER_content' minimumSize='{new Dimension(20,300)}'/>
          </cell>
        </row>
      </Table>

      <JPanel constraints='"$$hideme$$"' visible='false'>
        <JPanel id='remoteConfig'>
          <Table constraints='BorderLayout.NORTH' fill="both">
            <row>
              <cell anchor='west'>
                <JLabel text='observe.storage.remote.url'/>
              </cell>
              <cell weightx='1' fill="both" anchor='east'>
                <JTextField id="remoteUrl"
                            onKeyReleased='getModel().setRemoteUrl(((JTextField)event.getSource()).getText())'/>
              </cell>
            </row>
            <row>
              <cell anchor='west'>
                <JLabel text='observe.storage.remote.login'/>
              </cell>
              <cell weightx='1' fill="both" anchor='east'>
                <JTextField id="remoteLogin"
                            onKeyReleased='getModel().setRemoteLogin(((JTextField)event.getSource()).getText())'/>
              </cell>
            </row>
            <row>
              <cell anchor='west'>
                <JLabel text='observe.storage.remote.password'/>
              </cell>
              <cell weightx='1' fill="both" anchor='east'>
                <JPasswordField id="remotePassword"
                                onKeyReleased='getModel().setRemotePassword(((JPasswordField)event.getSource()).getPassword())'/>
              </cell>
            </row>
            <row>
              <cell anchor='west'>
                <JLabel id="serverDataBaseLabel" text='observe.storage.server.dataBase'/>
              </cell>
              <cell weightx='1' fill="both" anchor='east'>
                <JTextField id="serverDataBase"
                            onKeyReleased='getModel().setServerDatabase(((JTextField)event.getSource()).getText())'/>
              </cell>
            </row>
            <row>
              <cell columns='2'>
                <JCheckBox id="remoteUseSsl"
                           onActionPerformed='getModel().setUseSsl(((JCheckBox)event.getSource()).isSelected())'/>
              </cell>
            </row>

            <!-- tester la connexion -->
            <row>
              <cell columns='2'>
                <JButton id="testRemoteAction" onActionPerformed='getModel().testRemote()'/>
              </cell>
            </row>
          </Table>
          <!-- résumé du test de connexion -->
          <Table constraints='BorderLayout.CENTER' fill='both' insets='0'>
            <row>
              <cell weightx='1' weighty='1'>
                <JScrollPane id="connexionStatusPanel" columnHeaderView="{connexionStatusLabel}">
                  <JTextPane id='connexionStatus'/>
                  <JLabel id="connexionStatusLabel"/>
                </JScrollPane>
              </cell>
            </row>
          </Table>
        </JPanel>
      </JPanel>
    </JPanel>
  </JPanel>

</StorageTabUI>
