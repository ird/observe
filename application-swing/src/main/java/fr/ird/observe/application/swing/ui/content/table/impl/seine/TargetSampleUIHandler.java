/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.content.table.impl.seine;

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.DataContext;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.content.ContentMode;
import fr.ird.observe.application.swing.ui.content.table.ContentTableModel;
import fr.ird.observe.application.swing.ui.content.table.ContentTableUI;
import fr.ird.observe.services.dto.form.Form;
import fr.ird.observe.services.dto.reference.ReferentialReference;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.SexDto;
import fr.ird.observe.services.dto.referential.SizeMeasureTypeDto;
import fr.ird.observe.services.dto.referential.SizeMeasureTypeHelper;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.dto.seine.SetSeineDto;
import fr.ird.observe.services.dto.seine.TargetLengthDto;
import fr.ird.observe.services.dto.seine.TargetSampleDto;
import fr.ird.observe.services.dto.seine.TargetSampleHelper;
import fr.ird.observe.services.service.data.seine.TargetSampleService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.validator.NuitonValidatorScope;

import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class TargetSampleUIHandler<U extends ContentTableUI<TargetSampleDto, TargetLengthDto, U>> extends AbstractSampleUIHandler<TargetSampleDto, TargetLengthDto, U> {

    /** Logger */
    private static final Log log = LogFactory.getLog(TargetSampleUIHandler.class);

    /**
     * Pour différencier positionner l'invariant de l'écran
     * {@link TargetSampleDto#getDiscarded()}.
     *
     * @since 1.5
     */
    protected final boolean discarded;

    public TargetSampleUIHandler(boolean discarded) {
        this.discarded = discarded;
    }

    @Override
    public void afterInit(U ui) {
        super.afterInit(ui);

    }

    @Override
    public void resetIsWeightComputed() {
        getTableEditBean().setIsWeightComputed(false);
        ((TargetSampleUI) getUi()).getWeight().grabFocus();
    }

    @Override
    public void resetIsLengthComputed() {
        getTableEditBean().setIsLengthComputed(false);
        ((TargetSampleUI) getUi()).getLength().grabFocus();
    }

    @Override
    protected String getEditBeanIdToLoad() {
        return getDataContext().getSelectedSetId();
    }

    @Override
    protected void onSpeciesChanged(ReferentialReference<SpeciesDto> species) {

        // on utilise le code par defaut de l'espèce
        Optional<ReferentialReference<SizeMeasureTypeDto>> sizeMeasureType = getSpeciesDefaultSizeMeasureType(species);

        ((TargetSampleUI) getUi()).getModel().setDefaultSizeMeasureType(sizeMeasureType.orElse(null));

    }

    @Override
    protected void onWeightChanged(Float newValue) {
        getTableEditBean().setIsWeightComputed(false);
    }

    @Override
    protected void onLengthChanged(Float newValue) {
        getTableEditBean().setIsLengthComputed(false);
    }

    @Override
    protected void onSelectedRowChanged(int editingRow, TargetLengthDto bean, boolean create) {

        ContentTableModel<TargetSampleDto, TargetLengthDto> model = getTableModel();
        TargetSampleUI ui = (TargetSampleUI) getUi();
        if (!model.isEditable()) {
            return;
        }

        TargetLengthDto tableEditBean = getTableEditBean();

        tableEditBean.removePropertyChangeListener(TargetLengthDto.PROPERTY_WEIGHT, weightChanged);
        tableEditBean.removePropertyChangeListener(TargetLengthDto.PROPERTY_LENGTH, lengthChanged);
        tableEditBean.removePropertyChangeListener(TargetLengthDto.PROPERTY_SPECIES, speciesChanged);

        ReferentialReference<SpeciesDto> species = bean.getSpecies();

        JComponent requestFocus;

        ModeSaisieEchantillonEnum modeSaisieEchantillonEnum;
        Optional<ReferentialReference<SizeMeasureTypeDto>> sizeMeasureType = Optional.empty();

        if (create) {

            modeSaisieEchantillonEnum = ModeSaisieEchantillonEnum.byEffectif;
            if (model.isCreate()) {

                if (editingRow > 0) {
                    // on recupere l'species de la ligne precedente
                    TargetLengthDto editBean = model.getValueAt(editingRow - 1);
                    species = editBean.getSpecies();
                    modeSaisieEchantillonEnum = ModeSaisieEchantillonEnum.valueOf(editBean.getAcquisitionMode());
                }

                // on utilise le code par defaut de l'espèce
                sizeMeasureType = getSpeciesDefaultSizeMeasureType(species);
                if (!sizeMeasureType.isPresent()) {
                    // au cas où, on repasse toujours sur lf
                    sizeMeasureType = Optional.of(SizeMeasureTypeHelper.getLf(ui.getSizeMeasureType().getData()));
                }
                ui.getModel().setDefaultSizeMeasureType(sizeMeasureType.orElse(null));
            }

            requestFocus = ui.getSpecies();

        } else {

            requestFocus = ui.getCount();

            int acquisitionMode = bean.getAcquisitionMode();
            modeSaisieEchantillonEnum = ModeSaisieEchantillonEnum.valueOf(acquisitionMode);

            sizeMeasureType = Optional.ofNullable(bean.getSizeMeasureType());

        }

        ui.getAcquisitionModeGroup().setSelectedValue(null);
        ui.getAcquisitionModeGroup().setSelectedValue(modeSaisieEchantillonEnum);

        ui.getSizeMeasureType().setSelectedItem(null);
        sizeMeasureType.ifPresent(sizeMeasureTypeDtoReferentialReference -> ui.getSizeMeasureType().setSelectedItem(sizeMeasureTypeDtoReferentialReference));

        // on met a jour l'espece
        ui.getSpecies().setSelectedItem(null);
        if (species != null) {
            if (log.isDebugEnabled()) {
                log.debug("species to use " + species);
            }
            ui.getSpecies().setSelectedItem(species);
        }
        UIHelper.askFocus(requestFocus);

        tableEditBean.addPropertyChangeListener(TargetLengthDto.PROPERTY_WEIGHT, weightChanged);
        tableEditBean.addPropertyChangeListener(TargetLengthDto.PROPERTY_LENGTH, lengthChanged);
        tableEditBean.addPropertyChangeListener(TargetLengthDto.PROPERTY_SPECIES, speciesChanged);
    }

    @Override
    public void openUI() {
        super.openUI();

        List<ReferentialReference<SizeMeasureTypeDto>> sizeMeasureTypes = new ArrayList<>(getDataSource().getReferentialReferences(SizeMeasureTypeDto.class));
        List<ReferentialReference<SizeMeasureTypeDto>> data = SizeMeasureTypeHelper.filterForSeine(sizeMeasureTypes);
        if (log.isInfoEnabled()) {
            log.info("Using " + data.size() + " size measure type(s).");
        }
        if (data.size() != 2) {
            throw new IllegalStateException("WHY???");
        }
        ((TargetSampleUI) getUi()).getSizeMeasureType().setData(data);

    }

    @Override
    protected void initTableUI(DefaultTableCellRenderer renderer) {

        JTable table = getUi().getTable();

        UIHelper.setI18nTableHeaderRenderer(
                table,
                n("observe.content.targetSample.table.speciesThon"),
                n("observe.content.targetSample.table.speciesThon.tip"),
                n("observe.content.targetSample.table.sex"),
                n("observe.content.targetSample.table.sex.tip"),
                n("observe.content.targetSample.table.sizeMeasureType"),
                n("observe.content.targetSample.table.sizeMeasureType.tip"),
                n("observe.content.targetSample.table.length"),
                n("observe.content.targetSample.table.length.tip"),
                n("observe.content.targetSample.table.meanWeight"),
                n("observe.content.targetSample.table.meanWeight.tip"),
                n("observe.content.targetSample.table.count"),
                n("observe.content.targetSample.table.count.tip"),
                n("observe.content.targetSample.table.totalWeight"),
                n("observe.content.targetSample.table.totalWeight.tip"));

        UIHelper.setTableColumnRenderer(table, 0, UIHelper.newReferentialReferenceDecorateTableCellRenderer(renderer, SpeciesDto.class));
        UIHelper.setTableColumnRenderer(table, 1, UIHelper.newReferentialReferenceDecorateTableCellRenderer(renderer, SexDto.class));
        UIHelper.setTableColumnRenderer(table, 2, UIHelper.newReferentialReferenceDecorateTableCellRenderer(renderer, SizeMeasureTypeDto.class));
        UIHelper.setTableColumnRenderer(table, 3, UIHelper.newEmptyNumberTableCellRenderer(renderer));
        UIHelper.setTableColumnRenderer(table, 4, UIHelper.newEmptyNumberTableCellRenderer(renderer));
        UIHelper.setTableColumnRenderer(table, 5, UIHelper.newEmptyNumberTableCellRenderer(renderer));
        UIHelper.setTableColumnRenderer(table, 6, UIHelper.newEmptyNumberTableCellRenderer(renderer));
    }

    @Override
    protected ContentMode getContentMode(DataContext dataContext) {
        ContentMode mode = super.getContentMode(dataContext);

        String setId = dataContext.getSelectedSetId();

        boolean showData = getTargetSampleService().canUseTargetSample(setId, discarded);

        getModel().setShowData(showData);

        if (mode == ContentMode.UPDATE && !showData) {

            mode = ContentMode.READ;

            addMessage(getUi(),
                       NuitonValidatorScope.INFO,
                       getTypeI18nKey(SetSeineDto.class),
                       discarded ?
                               t("observe.content.setSeine.message.no.targetCatch") :
                               t("observe.content.setSeine.message.no.targetDiscarded")
            );
        }
        return mode;
    }

    /**
     * Le mode de saisie a été mis à jour.
     *
     * @param newMode le nouveau de mode de saisie à utiliser
     * @since 1.8
     */
    public static void updateModeSaisie(TargetSampleUI ui, ModeSaisieEchantillonEnum newMode) {

        if (log.isDebugEnabled()) {
            log.debug("Change mode saisie to " + newMode);
        }
        if (newMode == null) {

            // mode null (cela peut arriver avec les bindings)
            return;
        }
        boolean createMode = ui.getTableModel().isCreate();

        TargetLengthDto editBean = ui.getTableEditBean();
        switch (newMode) {

            case byEffectif:

                // le weight n'est pas modifiable
                ui.getWeight().setEnabled(false);

                // l'count est modifiable
                ui.getCount().setEnabled(true);

                if (createMode) {

                    // on supprime le weight (si il a été saisie)
                    editBean.setWeight(null);
                    // on supprime aussi l'count (pour forcer la saisie)
                    editBean.setCount(null);
                }
                break;

            case byIndividu:

                // le weight est pas modifiable
                ui.getWeight().setEnabled(true);

                // l'count n'est pas modifiable et est toujours de 1
                ui.getCount().setEnabled(false);


                if (createMode) {

                    // on positionne l'count à 1 (seule valeur possible)
                    editBean.setCount(1);
                }
                break;
        }

        if (createMode) {

            // on propage le mode de saisie dans le bean
            editBean.setAcquisitionMode(newMode.ordinal());
        }
    }

    public void resetDefaultSizeMeasureType() {
        TargetSampleUI ui = (TargetSampleUI) getUi();
        ui.getSizeMeasureType().setSelectedItem(null);
        ui.getSizeMeasureType().setSelectedItem(ui.getModel().getDefaultSizeMeasureType());
    }

    @Override
    protected void doPersist(TargetSampleDto bean) {

        SaveResultDto saveResult = getTargetSampleService().save(getSelectedParentId(), bean);
        saveResult.toDto(bean);

    }

    @Override
    protected void loadEditBean(String beanId) {
        Form<TargetSampleDto> form = getTargetSampleService().loadForm(beanId, discarded);
        loadReferentialReferenceSetsInModel(form);
        getModel().setForm(form);
        TargetSampleHelper.copyTargetSampleDto(form.getObject(), getBean());
    }

    @Override
    protected <D extends ReferentialDto> Collection<ReferentialReference<D>> filterReferentialReferences(Class<D> dtoType, String propertyName, LinkedList<ReferentialReference<D>> incomingReferences) {

        Collection<ReferentialReference<D>> result = super.filterReferentialReferences(dtoType, propertyName, incomingReferences);

        switch (propertyName) {

            case TargetLengthDto.PROPERTY_SPECIES: {

                result = (List) getTargetSampleService().getSampleSpecies(getSelectedParentId(), discarded);

                if (log.isDebugEnabled()) {
                    log.debug(String.format("Filter referential references (type %s - property %s), final size: %d", dtoType.getSimpleName(), propertyName, incomingReferences.size()));
                }

            }

            break;

            case TargetLengthDto.PROPERTY_SIZE_MEASURE_TYPE: {

                result = SizeMeasureTypeHelper.filterForSeine((Collection) incomingReferences);
                if (log.isInfoEnabled()) {
                    log.info("Using " + result.size() + " size measure type(s).");
                }
                if (result.size() != 2) {
                    throw new IllegalStateException("WHY???");
                }
            }
        }


        return result;

    }

    protected TargetSampleService getTargetSampleService() {
        return ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newTargetSampleService();
    }

    protected Optional<ReferentialReference<SizeMeasureTypeDto>> getSpeciesDefaultSizeMeasureType(ReferentialReference<SpeciesDto> species) {
        Optional<ReferentialReference<SizeMeasureTypeDto>> result = Optional.empty();
        if (species != null) {

            String sizeMeasureId = (String) species.getPropertyValue(SpeciesDto.PROPERTY_SIZE_MEASURE_TYPE + "Id");
            result = ((TargetSampleUI) getUi()).getSizeMeasureType().getData().stream()
                                               .filter(s -> s.getId().equals(sizeMeasureId)).findFirst();
        }
        return result;
    }

}
