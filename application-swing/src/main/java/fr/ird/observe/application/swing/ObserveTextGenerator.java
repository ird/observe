package fr.ird.observe.application.swing;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import fr.ird.observe.application.swing.configuration.ObserveSwingApplicationConfig;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.ui.storage.StorageUIModel;
import fr.ird.observe.services.configuration.ObserveDataSourceInformation;
import freemarker.cache.ClassTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;

import java.io.File;
import java.io.StringWriter;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Sylvain Bavencoff - bavencoff@codelutin.com
 */
public class ObserveTextGenerator {

    protected static final String CONNEXION_TEST_RESULT_TEMPLATE = "connexionTestResult.ftl";

    protected static final String DATA_SOURCE_SELECT_MODE_RESUME_TEMPLATE = "dataSourceSelectModeResume.ftl";

    protected static final String DATA_SOURCE_CONNECTION_REPORT_TEMPLATE = "dataSourceConnectionReport.ftl";

    protected static final String DATA_SOURCE_INFORMATION_TEMPLATE = "dataSourceInformation.ftl";

    protected static final String DATA_SOURCE_POLICY_TEMPLATE = "dataSourcePolicy.ftl";

    protected static final String ABOUT_TEMPLATE = "about.ftl";

    protected static final String TRANSLATE_TEMPLATE = "translate.ftl";


    protected final Configuration freemarkerConfiguration;

    protected final ObserveSwingApplicationConfig observeConfiguration;

    public ObserveTextGenerator(ObserveSwingApplicationConfig observeConfiguration) {
        this.observeConfiguration = observeConfiguration;

        freemarkerConfiguration = new Configuration(Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS);

        // needed to overwrite "Defaults to default system encoding."
        // fix encoding issue on some systems
        freemarkerConfiguration.setEncoding(Locale.getDefault(), "UTF-8");

        // specific template loader to get template from jars (classpath)
        ClassTemplateLoader templateLoader = new ClassTemplateLoader(ObserveTextGenerator.class, "/ftl");
        freemarkerConfiguration.setTemplateLoader(templateLoader);
    }

    public String getConnexionTestResultMessage(StorageUIModel model) {
        return generateHtml(CONNEXION_TEST_RESULT_TEMPLATE, model);
    }

    public String getLoadDataSourceResume(StorageUIModel model) {
        return generateHtml(DATA_SOURCE_SELECT_MODE_RESUME_TEMPLATE, model);
    }

    public String getDataSourceConnectionReport(StorageUIModel model) {
        return generateHtml(DATA_SOURCE_CONNECTION_REPORT_TEMPLATE, model);
    }

    public String getDataSourceInfo(ObserveSwingDataSource source) {
        return generateHtml(DATA_SOURCE_INFORMATION_TEMPLATE, source);
    }

    public String getDataSourcePolicy(ObserveDataSourceInformation model) {
        return generateHtml(DATA_SOURCE_POLICY_TEMPLATE, model);
    }

    protected String generateHtml(String templateName, Object model) {
        return generateHtml(observeConfiguration.getLocale(), templateName, model);
    }

    public String getAbout(ObserveSwingApplicationConfig model) {
        return generateHtml(ABOUT_TEMPLATE, model);
    }

    public String getTranslate(File model) {
        try {
            URL url = model.toURI().toURL();
            return generateHtml(TRANSLATE_TEMPLATE, ImmutableMap.of("file", url));
        } catch (MalformedURLException e) {
            throw new ObserveSwingTechnicalException(e);
        }
    }

    protected String generateHtml(Locale locale, String templateName, Object model) {

        try {

            // Get freemarker template
            Template mapTemplate = freemarkerConfiguration.getTemplate(templateName, locale);

            Writer out = new StringWriter();
            mapTemplate.process(model, out);

            out.flush();

            return out.toString();


        } catch (Exception ex) {
            throw new ObserveSwingTechnicalException(t("observe.generateHtml.error", templateName), ex);
        }
    }
}
