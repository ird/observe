<!--
  #%L
  ObServe :: Application Swing
  %%
  Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

<fr.ird.observe.application.swing.ui.content.table.ContentTableUI
        superGenericType='ActivityLonglineSensorUsedDto, SensorUsedDto, SensorUsedUI'
        contentTitle='{n("observe.content.sensorUsed.title")}'
        saveNewEntryText='{n("observe.content.sensorUsed.action.create")}'
        saveNewEntryTip='{n("observe.content.sensorUsed.action.create.tip")}'>

  <style source="../../CommonTable.jcss"/>

  <import>
    fr.ird.observe.services.dto.longline.ActivityLonglineSensorUsedDto
    fr.ird.observe.services.dto.longline.SensorUsedDto
    fr.ird.observe.services.dto.reference.ReferentialReference
    fr.ird.observe.services.dto.referential.longline.SensorTypeDto
    fr.ird.observe.services.dto.referential.longline.SensorBrandDto
    fr.ird.observe.services.dto.referential.longline.SensorDataFormatDto
    fr.ird.observe.application.swing.ui.content.table.*
    fr.ird.observe.application.swing.ui.util.JComment

    org.nuiton.jaxx.widgets.select.BeanComboBox

    java.awt.Dimension

    static fr.ird.observe.application.swing.ui.UIHelper.getStringValue
    static org.nuiton.i18n.I18n.n
  </import>

  <!-- model -->
  <SensorUsedUIModel id='model'/>

  <!-- edit bean -->
  <ActivityLonglineSensorUsedDto id='bean'/>

  <!-- table edit bean -->
  <SensorUsedDto id='tableEditBean'/>

  <!-- table model -->
  <ContentTableModel id='tableModel'/>

  <!-- le validateur de l'écran -->
  <BeanValidator id='validator' autoField='true' context='ui-update'
                 beanClass='fr.ird.observe.services.dto.longline.ActivityLonglineSensorUsedDto'
                 errorTableModel='{getErrorTableModel()}'/>

  <!-- le validateur d'une entrée de tableau -->
  <BeanValidator id='validatorTable' autoField='true' context='ui-update'
                 beanClass='fr.ird.observe.services.dto.longline.SensorUsedDto'
                 errorTableModel='{getErrorTableModel()}'/>

  <Table id='editorPanel' fill='both' insets='1'>

    <!-- sensorType -->
    <row>
      <cell>
        <JLabel id='sensorTypeLabel'/>
      </cell>
      <cell weightx='1' anchor='east'>
        <BeanComboBox id='sensorType' constructorParams='this' genericType='ReferentialReference&lt;SensorTypeDto&gt;'
                      _entityClass='SensorTypeDto.class'/>
      </cell>
    </row>

    <!-- sensorDataFormat -->
    <row>
      <cell>
        <JLabel id='sensorDataFormatLabel'/>
      </cell>
      <cell weightx='1' anchor='east'>
        <BeanComboBox id='sensorDataFormat' constructorParams='this'
                      genericType='ReferentialReference&lt;SensorDataFormatDto&gt;'
                      _entityClass='SensorDataFormatDto.class'/>
      </cell>
    </row>

    <!-- sensorBrand -->
    <row>
      <cell>
        <JLabel id='sensorBrandLabel'/>
      </cell>
      <cell weightx='1' anchor='east'>
        <BeanComboBox id='sensorBrand' constructorParams='this' genericType='ReferentialReference&lt;SensorBrandDto&gt;'
                      _entityClass='SensorBrandDto.class'/>
      </cell>
    </row>

    <!-- sensorSerialNo -->
    <row>
      <cell>
        <JLabel id='sensorSerialNoLabel'/>
      </cell>
      <cell weightx='1' anchor='east'>
        <JPanel layout='{new BorderLayout()}'>
          <JToolBar id='sensorSerialNoToolbar' constraints='BorderLayout.WEST'>
            <JButton id='resetSensorSerialNo' styleClass='resetButton'/>
          </JToolBar>
          <JTextField id='sensorSerialNo' constraints='BorderLayout.CENTER'/>
        </JPanel>
      </cell>
    </row>

    <!-- dataLocation -->
    <row>
      <cell>
        <JLabel id='dataLocationLabel'/>
      </cell>
      <cell weightx='1' anchor='east'>
        <JPanel layout='{new BorderLayout()}'>
          <JToolBar id='dataLocationToolbar' constraints='BorderLayout.WEST'>
            <JButton id='resetDataLocation' styleClass='resetButton'/>
          </JToolBar>
          <JTextField id='dataLocation' constraints='BorderLayout.CENTER'/>
        </JPanel>
      </cell>
    </row>

    <!-- data -->
    <row>
      <cell>
        <JLabel id='dataLabel'/>
      </cell>
      <cell weightx='1' anchor='east'>
        <JPanel layout='{new GridLayout()}'>
          <JButton id="importDataButton" onActionPerformed="getHandler().importData()"/>
          <JButton id="exportDataButton" onActionPerformed="getHandler().exportData()"/>
          <JButton id="deleteDataButton" onActionPerformed="getHandler().deleteData()"/>
        </JPanel>
      </cell>
    </row>
  </Table>

  <Table id='extraZone' fill='both' weightx='1' insets='1'>
    <row>
      <cell weighty='1'>
        <JComment id="comment"/>
      </cell>
    </row>
  </Table>

</fr.ird.observe.application.swing.ui.content.table.ContentTableUI>
