package fr.ird.observe.application.swing.ui.tree.selection.nodes;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.collections4.EnumerationUtils;

import java.util.Iterator;

/**
 * Created on 18/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class RootSelectionTreeNode extends SelectionTreeNodeSupport<Void> implements Iterable<ProgramSelectionTreeNode> {

    public RootSelectionTreeNode() {
        super(null, true);
    }

    @Override
    public boolean isOpen() {
        return false;
    }

    @Override
    public String getId() {
        return null;
    }

    @Override
    public String getText() {
        return null;
    }

    @Override
    public String getIconPath() {
        return null;
    }

    @Override
    public Iterator<ProgramSelectionTreeNode> iterator() {
        return (Iterator) EnumerationUtils.toList(children()).stream().filter(c -> c instanceof ProgramSelectionTreeNode).iterator();
    }

    @Override
    public boolean isSelected() {
        return false;
    }

    @Override
    public void setSelected(boolean selected) {
        // rien a faire
    }

    public ReferentialsSelectionTreeNode[] getReferentialsNodes() {
        ReferentialsSelectionTreeNode[] result = new ReferentialsSelectionTreeNode[3];
        result[0] = (ReferentialsSelectionTreeNode) getChildAt(getChildCount() - 3);
        result[1] = (ReferentialsSelectionTreeNode) getChildAt(getChildCount() - 2);
        result[2] = (ReferentialsSelectionTreeNode) getChildAt(getChildCount() - 1);
        return result;
    }
}
