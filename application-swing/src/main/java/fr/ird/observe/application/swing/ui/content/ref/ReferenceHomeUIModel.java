package fr.ird.observe.application.swing.ui.content.ref;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import fr.ird.observe.application.swing.decoration.ObserveI18nDecoratorHelper;
import fr.ird.observe.application.swing.ui.content.ContentUIModel;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialHelper;

import java.util.ArrayList;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 9/28/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since XXX
 */
public abstract class ReferenceHomeUIModel extends ContentUIModel<ProgramDto> {

    public static class ReferenceHomeCommonUIModel extends ReferenceHomeUIModel {
        public ReferenceHomeCommonUIModel() {
            super(new ArrayList<>(ReferentialHelper.REFERENCE_COMMON_DTOS), t("observe.type.reference.common"));
        }
    }

    public static class ReferenceHomeSeineUIModel extends ReferenceHomeUIModel {
        public ReferenceHomeSeineUIModel() {
            super(new ArrayList<>(ReferentialHelper.REFERENCE_SEINE_DTOS), t("observe.type.reference.seine"));
        }
    }

    public static class ReferenceHomeLonglineUIModel extends ReferenceHomeUIModel {
        public ReferenceHomeLonglineUIModel() {
            super(new ArrayList<>(ReferentialHelper.REFERENCE_LONGLINE_DTOS), t("observe.type.reference.longline"));
        }
    }

    private static final long serialVersionUID = 1L;

    private final String nodeName;
    private final ImmutableList<Class<? extends ReferentialDto>> types;

    protected ReferenceHomeUIModel(List<Class<? extends ReferentialDto>> types, String nodeName) {
        super(ProgramDto.class);
        this.types = ImmutableList.copyOf(ObserveI18nDecoratorHelper.sortPluralTypes(types));
        this.nodeName = nodeName;
    }

    public ImmutableList<Class<? extends ReferentialDto>> getTypes() {
        return types;
    }

    public String getNodeName() {
        return nodeName;
    }
}
