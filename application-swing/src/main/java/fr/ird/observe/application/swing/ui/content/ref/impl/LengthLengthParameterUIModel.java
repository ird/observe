package fr.ird.observe.application.swing.ui.content.ref.impl;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.application.swing.ui.content.ref.ContentReferenceUIModel;
import fr.ird.observe.services.dto.referential.LengthLengthParameterDto;

import java.util.Set;

/**
 * Created on 11/5/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class LengthLengthParameterUIModel extends ContentReferenceUIModel<LengthLengthParameterDto> {

    public static final String PROPERTY_GENERAL_TAB_VALID = "generalTabValid";

    public static final String PROPERTY_OTHER_TAB_VALID = "otherTabValid";

    public static final Set<String> GENERAL_TAB_PROPERTIES =
            ImmutableSet.<String>builder().add(LengthLengthParameterDto.PROPERTY_URI,
                                               LengthLengthParameterDto.PROPERTY_CODE,
                                               LengthLengthParameterDto.PROPERTY_STATUS,
                                               LengthLengthParameterDto.PROPERTY_NEED_COMMENT,
                                               LengthLengthParameterDto.PROPERTY_SOURCE,
                                               LengthLengthParameterDto.PROPERTY_OUTPUT_SIZE_MEASURE_TYPE,
                                               LengthLengthParameterDto.PROPERTY_INPUT_SIZE_MEASURE_TYPE).build();

    public static final Set<String> OTHER_TAB_PROPERTIES =
            ImmutableSet.<String>builder().add(LengthLengthParameterDto.PROPERTY_INPUT_OUTPUT_FORMULA,
                                               LengthLengthParameterDto.PROPERTY_INPUT_OUTPUT_FORMULA_VALID,
                                               LengthLengthParameterDto.PROPERTY_OUTPUT_INPUT_FORMULA,
                                               LengthLengthParameterDto.PROPERTY_OUTPUT_INPUT_FORMULA_VALID,
                                               LengthLengthParameterDto.PROPERTY_COEFFICIENTS).build();

    private static final long serialVersionUID = 1L;

    protected boolean generalTabValid;

    protected boolean otherTabValid;

    public LengthLengthParameterUIModel() {
        super(LengthLengthParameterDto.class,
              null,
              new String[]{
                      LengthLengthParameterDto.PROPERTY_INPUT_SIZE_MEASURE_TYPE,
                      LengthLengthParameterDto.PROPERTY_OUTPUT_SIZE_MEASURE_TYPE,
              },
              new String[]{
                      LengthLengthParameterUI.BINDING_INPUT_SIZE_MEASURE_TYPE_SELECTED_ITEM,
                      LengthLengthParameterUI.BINDING_INPUT_OUTPUT_FORMULA_TEXT,
                      LengthLengthParameterUI.BINDING_OUTPUT_SIZE_MEASURE_TYPE_SELECTED_ITEM,
                      LengthLengthParameterUI.BINDING_OUTPUT_INPUT_FORMULA_TEXT,
              }

        );
    }

    public boolean isOtherTabValid() {
        return otherTabValid;
    }

    public void setOtherTabValid(boolean otherTabValid) {
        Object oldValue = isOtherTabValid();
        this.otherTabValid = otherTabValid;
        firePropertyChange(PROPERTY_OTHER_TAB_VALID, oldValue, otherTabValid);
    }

    public boolean isGeneralTabValid() {
        return generalTabValid;
    }

    public void setGeneralTabValid(boolean generalTabValid) {
        Object oldValue = isGeneralTabValid();
        this.generalTabValid = generalTabValid;
        firePropertyChange(PROPERTY_GENERAL_TAB_VALID, oldValue, generalTabValid);
    }

}
