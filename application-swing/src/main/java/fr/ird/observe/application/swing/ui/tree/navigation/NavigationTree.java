package fr.ird.observe.application.swing.ui.tree.navigation;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.DataContext;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.NavigationTreeNodeSupport;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.ReferenceNavigationTreeNodeSupport;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.RootNavigationTreeNode;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.longline.ActivitiesLonglineNavigationTreeNode;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.longline.ActivityLonglineNavigationTreeNode;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.longline.ProgramLonglineNavigationTreeNode;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.longline.SetLonglineNavigationTreeNode;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.longline.TripLonglineNavigationTreeNode;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.seine.ActivitiesSeineNavigationTreeNode;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.seine.ActivitySeineNavigationTreeNode;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.seine.FloatingObjectSeineNavigationTreeNode;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.seine.ProgramSeineNavigationTreeNode;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.seine.RouteSeineNavigationTreeNode;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.seine.RoutesSeineNavigationTreeNode;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.seine.SetSeineNavigationTreeNode;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.seine.TripSeineNavigationTreeNode;
import fr.ird.observe.services.binder.ReferenceBinderEngine;
import fr.ird.observe.services.dto.IdDto;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.dto.longline.ActivityLonglineDto;
import fr.ird.observe.services.dto.longline.SetLonglineDto;
import fr.ird.observe.services.dto.longline.TripLonglineDto;
import fr.ird.observe.services.dto.reference.DataReference;
import fr.ird.observe.services.dto.reference.ReferentialReference;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.referential.ProgramHelper;
import fr.ird.observe.services.dto.seine.ActivitySeineDto;
import fr.ird.observe.services.dto.seine.FloatingObjectDto;
import fr.ird.observe.services.dto.seine.RouteDto;
import fr.ird.observe.services.dto.seine.SetSeineDto;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.JXTree;

import javax.swing.SwingUtilities;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeWillExpandListener;
import javax.swing.tree.DefaultTreeSelectionModel;
import javax.swing.tree.ExpandVetoException;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * Created on 14/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class NavigationTree extends JXTree {

    /** Logger */
    private static final Log log = LogFactory.getLog(NavigationTree.class);

    public NavigationTree() {
        super(new NavigationTreeModel());
        setSelectionModel(new DefaultTreeSelectionModel() {
            @Override
            public void setSelectionPath(TreePath path) {
                boolean canChange = ObserveSwingApplicationContext.get().getContentUIManager().closeSelectedContentUI();
                if (!canChange) {
                    // cancel the change of node
                    return;
                }
                super.setSelectionPath(path);
            }
        });
        setLargeModel(true);
        setRootVisible(false);
        getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);

        addTreeWillExpandListener(new TreeWillExpandListener() {

            @Override
            public void treeWillExpand(TreeExpansionEvent event) throws ExpandVetoException {
                if (!(getModel() instanceof NavigationTreeModel)) {
                    return;
                }
                NavigationTreeModel model = getTreeModel();
                NavigationTreeNodeSupport node = (NavigationTreeNodeSupport) event.getPath().getLastPathComponent();
                model.open(node);

            }

            @Override
            public void treeWillCollapse(TreeExpansionEvent event) throws ExpandVetoException {
                if (!(getModel() instanceof NavigationTreeModel)) {
                    return;
                }
                NavigationTreeNodeSupport node = (NavigationTreeNodeSupport) event.getPath().getLastPathComponent();

                // le seul posant problème est la fermeture d'un noeud parent
                // du noeud courant : cela va changer la selection
                NavigationTreeNodeSupport selectedNode = getSelectedNode();
                if (selectedNode == null) {
                    // pas de noeud selectionne
                    return;
                }

                if (selectedNode.equals(node) || !selectedNode.isNodeAncestor(node)) {
                    return;
                }
                boolean canChange = ObserveSwingApplicationContext.get().getContentUIManager().closeSelectedContentUI();
                if (!canChange) {
                    throw new ExpandVetoException(event, "Can not collapse node " + event.getPath());
                }

            }
        });
        addTreeSelectionListener(e -> {
            if (!e.isAddedPath()) {
                return;
            }
            TreePath path = e.getPath();
            getTreeModel().open((NavigationTreeNodeSupport) path.getLastPathComponent());
        });

        NavigationTreeCellRenderer renderer = new NavigationTreeCellRenderer();
        setCellRenderer(renderer);

    }

    public NavigationTreeModel getTreeModel() {
        return (NavigationTreeModel) getModel();
    }

    public NavigationTreeNodeSupport getSelectedNode() {
        return isSelectionEmpty() ? null : (NavigationTreeNodeSupport) getSelectionPath().getLastPathComponent();
    }

    /**
     * Selects the given {@code node} in the registred tree.
     *
     * @param node the node to select
     */
    public void selectNode(NavigationTreeNodeSupport node) {
        if (log.isInfoEnabled()) {
            log.info("try to select node [" + node + "]");
        }
        TreePath path = new TreePath(getTreeModel().getPathToRoot(node));

        setSelectionPath(path);
        SwingUtilities.invokeLater(() -> scrollPathToVisible(path));
    }

    public NavigationTreeNodeSupport getChild(NavigationTreeNodeSupport node, String id) {
        return node.findChildById(id);
    }

    public void removeNode(NavigationTreeNodeSupport node) {
        getTreeModel().removeNodeFromParent(node);
    }

    public void insertNode(NavigationTreeNodeSupport parentNode, NavigationTreeNodeSupport node, int position) {
        getTreeModel().insertNodeInto(node, parentNode, position);
    }

    public void clearModel() {
        getTreeModel().setRoot(new RootNavigationTreeNode());
    }

    /**
     * Sélectionne le noeud dans l'arbre de navigation.
     *
     * <b>Note:</b> cette méthode doit être appelée après un rechargement du
     * modèle de navigation.
     */
    public void selectInitialNode() {

        if (log.isDebugEnabled()) {
            log.debug("Will select initial node...");
        }
        DataContext context = ObserveSwingApplicationContext.get().getDataContext();

        List<Object> selectedIds = ObserveSwingApplicationContext.get().getNodesToReselect();
        if (CollectionUtils.isNotEmpty(selectedIds)) {
            if (log.isDebugEnabled()) {
                log.debug("will select previous ids " + selectedIds);
            }

            NavigationTreeNodeSupport selectedNode = getTreeModel().findNode(getRootNode(), selectedIds.toArray());

            if (selectedNode != null) {


                if (log.isInfoEnabled()) {
                    log.info("Selected node: " + selectedNode);
                }

                try {

                    selectNode(selectedNode);
                } finally {

                    // nettoyage du context
                    ObserveSwingApplicationContext.get().setNodesToReselect(null);
                }
            }

        } else {

            // on trouve le meilleur noeud a selectionner.

            String id = context.getHigherOpenId();

            NavigationTreeNodeSupport selectedNode = null;
            if (id != null) {

                // on se positionne sur la donnée la plus haute ouverte
                if (context.isOpenTrip()) {

                    NavigationTreeNodeSupport tripNode = getTripNode(context.getOpenProgramId(), context.getOpenTripId());
                    selectedNode = tripNode;

                    if (context.isOpenRoute()) {

                        NavigationTreeNodeSupport routeNode = getRouteNode(tripNode, context.getOpenRouteId());
                        selectedNode = routeNode;
                        if (context.isOpenActivity()) {

                            NavigationTreeNodeSupport activitySeineNode = getActivitySeineNode(routeNode, context.getOpenActivityId());
                            selectedNode = activitySeineNode;
                            if (context.isOpenSet()) {
                                selectedNode = getSetSeineNode(activitySeineNode);

                            }
                        }
                    }

                    if (context.isOpenActivityLongline()) {

                        NavigationTreeNodeSupport activityLonglineNode = getActivityLonglineNode(tripNode, context.getOpenActivityId());
                        selectedNode = activityLonglineNode;
                        if (context.isOpenSet()) {
                            selectedNode = getSetLonglineNode(activityLonglineNode);

                        }
                    }
                }

            } else {
                selectedNode = getTreeModel().getRoot();
                if (!selectedNode.isLeaf()) {
                    selectedNode = (NavigationTreeNodeSupport) selectedNode.getFirstChild();
                }

            }


            if (selectedNode != null) {

                if (log.isInfoEnabled()) {
                    log.info("will selected open node: " + selectedNode);
                }

                selectNode(selectedNode);
            }

        }

        // navigation tree should acquire focus
        SwingUtilities.invokeLater(this::grabFocus);
    }

    public void reloadNodeSubTree(NavigationTreeNodeSupport node) {
        Objects.requireNonNull(node, "node is null, we can not reload its structure");

        // 1. Let's clear node structure
        while (node.getChildCount() > 0) {
            removeNode((NavigationTreeNodeSupport) node.getFirstChild());
        }

        // 3. Let's re-generate node's children by populating the node : this will call the child loaders.
        node.updateNode();

        // Fix bug (if no child in parent node, it will not expand...)
        fireTreeExpanded(new TreePath(node.getPath()));

    }

    public void reloadNode(NavigationTreeNodeSupport<?> node, boolean deep) {

        if (log.isDebugEnabled()) {
            log.debug("Will refresh (deep ? " + deep + ") node " + node);
        }
        getTreeModel().nodeChanged(node);
        if (deep) {
            // repaint childs nodes
            Enumeration<NavigationTreeNodeSupport<?>> e = node.children();
            while (e.hasMoreElements()) {
                NavigationTreeNodeSupport<?> child = e.nextElement();
                reloadNode(child, true);
            }
        }

    }

    public Object[] getSelectedIds() {
        List<Object> result = new ArrayList<>();
        NavigationTreeNodeSupport selectedNode = getSelectedNode();
        while (selectedNode != null && !selectedNode.isRoot()) {

            if (selectedNode instanceof ReferenceNavigationTreeNodeSupport) {
                result.add(selectedNode.getId());
            } else {
                result.add(selectedNode.getClass());
            }
            selectedNode = selectedNode.getParent();
        }
        Collections.reverse(result);
        return result.toArray();
    }

    public void reloadSelectedNode(boolean refreshFromParent, boolean refreshChilds) {
        NavigationTreeNodeSupport node = getSelectedNode();
        node.setLoaded(false);

        if (refreshFromParent) {
            node = node.getParent();
        }
        reloadNode(node, refreshChilds);
    }

    public NavigationTreeNodeSupport addUnsavedNode(NavigationTreeNodeSupport parentNode, Class<?> type) {

        // noeud en mode creation
        NavigationTreeNodeSupport result;

        if (TripSeineDto.class.equals(type)) {
            DataReference<TripSeineDto> data = new DataReference<>();
            data.init(TripSeineDto.class, new String[0]);
            result = new TripSeineNavigationTreeNode(data);
        } else if (RouteDto.class.equals(type)) {
            DataReference<RouteDto> data = new DataReference<>();
            data.init(RouteDto.class, new String[0]);
            result = new RouteSeineNavigationTreeNode(data);
        } else if (ActivitySeineDto.class.equals(type)) {
            DataReference<ActivitySeineDto> data = new DataReference<>();
            data.init(ActivitySeineDto.class, new String[0]);
            result = new ActivitySeineNavigationTreeNode(data);
        } else if (SetSeineDto.class.equals(type)) {
            DataReference<SetSeineDto> data = new DataReference<>();
            data.init(SetSeineDto.class, new String[0]);
            result = new SetSeineNavigationTreeNode(data);
        } else if (FloatingObjectDto.class.equals(type)) {
            DataReference<FloatingObjectDto> data = new DataReference<>();
            data.init(FloatingObjectDto.class, new String[0]);
            result = new FloatingObjectSeineNavigationTreeNode(data);
        } else if (TripLonglineDto.class.equals(type)) {
            DataReference<TripLonglineDto> data = new DataReference<>();
            data.init(TripLonglineDto.class, new String[0]);
            result = new TripLonglineNavigationTreeNode(data);
        } else if (ActivityLonglineDto.class.equals(type)) {
            DataReference<ActivityLonglineDto> data = new DataReference<>();
            data.init(ActivityLonglineDto.class, new String[0]);
            result = new ActivityLonglineNavigationTreeNode(data);
        } else {
            throw new IllegalStateException("Can't use type to create unsaved node: " + type.getName());
        }
        insertNode(parentNode, result, parentNode.getChildCount());

        // refresh parent node (render of parent can have changed)
        reloadNode(parentNode, true);

        // Fix bug (if no child in parent node, it will not expand...)
        fireTreeExpanded(new TreePath(result.getPath()));

        // Select new node
        selectNode(result);

        return result;
    }

    public <E extends IdDto> NavigationTreeNodeSupport addOpenable(NavigationTreeNodeSupport parentNode, E bean) {

        ReferenceBinderEngine referenceBinderEngine = ObserveSwingApplicationContext.get().getReferenceBinderEngine();
        ReferentialLocale referentialLocale = ObserveSwingApplicationContext.get().getDecoratorService().getReferentialLocale();
        if (bean instanceof TripSeineDto) {

            DataReference<TripSeineDto> ref = referenceBinderEngine.transformDataDtoToReference(referentialLocale, (TripSeineDto) bean);
            return addTripSeine(parentNode, ref);

        } else if (bean instanceof TripLonglineDto) {

            DataReference<TripLonglineDto> ref = referenceBinderEngine.transformDataDtoToReference(referentialLocale, (TripLonglineDto) bean);
            return addTripLongline(parentNode, ref);

        } else if (bean instanceof RouteDto) {

            DataReference<RouteDto> ref = referenceBinderEngine.transformDataDtoToReference(referentialLocale, (RouteDto) bean);
            return addRoute(parentNode, ref);

        } else if (bean instanceof ActivitySeineDto) {

            DataReference<ActivitySeineDto> ref = referenceBinderEngine.transformDataDtoToReference(referentialLocale, (ActivitySeineDto) bean);
            return addActivitySeine(parentNode, ref);

        } else if (bean instanceof ActivityLonglineDto) {

            DataReference<ActivityLonglineDto> ref = referenceBinderEngine.transformDataDtoToReference(referentialLocale, (ActivityLonglineDto) bean);
            return addActivityLongline(parentNode, ref);

        }
        throw new IllegalStateException("Can not come here!");
    }

    private TripSeineNavigationTreeNode addTripSeine(NavigationTreeNodeSupport parentNode, DataReference<TripSeineDto> bean) {
        TripSeineNavigationTreeNode result = new TripSeineNavigationTreeNode(bean);
        insertNode(parentNode, result);
        return result;
    }

    private NavigationTreeNodeSupport addTripLongline(NavigationTreeNodeSupport parentNode, DataReference<TripLonglineDto> bean) {
        NavigationTreeNodeSupport result = new TripLonglineNavigationTreeNode(bean);
        insertNode(parentNode, result);
        return result;
    }

    private NavigationTreeNodeSupport addRoute(NavigationTreeNodeSupport parentNode, DataReference<RouteDto> bean) {
        NavigationTreeNodeSupport result = new RouteSeineNavigationTreeNode(bean);
        insertNode(parentNode, result);
        return result;
    }

    private NavigationTreeNodeSupport addActivitySeine(NavigationTreeNodeSupport parentNode, DataReference<ActivitySeineDto> bean) {
        NavigationTreeNodeSupport result = new ActivitySeineNavigationTreeNode(bean);
        insertNode(parentNode, result);
        return result;
    }

    private NavigationTreeNodeSupport addActivityLongline(NavigationTreeNodeSupport parentNode, DataReference<ActivityLonglineDto> bean) {
        NavigationTreeNodeSupport result = new ActivityLonglineNavigationTreeNode(bean);
        insertNode(parentNode, result);
        return result;
    }

    public NavigationTreeNodeSupport addFloatingObject(NavigationTreeNodeSupport parentNode, DataReference<FloatingObjectDto> bean) {
        NavigationTreeNodeSupport result = new FloatingObjectSeineNavigationTreeNode(bean);
        insertNode(parentNode, result);
        return result;
    }

    public NavigationTreeNodeSupport addSetSeine(NavigationTreeNodeSupport parentNode, DataReference<SetSeineDto> bean) {
        NavigationTreeNodeSupport result = new SetSeineNavigationTreeNode(bean);
        insertNode(parentNode, result);
        return result;
    }

    public NavigationTreeNodeSupport addSetLongline(NavigationTreeNodeSupport parentNode, DataReference<SetLonglineDto> bean) {
        NavigationTreeNodeSupport result = new SetLonglineNavigationTreeNode(bean);
        insertNode(parentNode, result);
        return result;
    }

    public void insertNode(NavigationTreeNodeSupport parentNode, NavigationTreeNodeSupport childNode) {
        insertNode(parentNode, childNode, parentNode.getChildCount());
    }

    public NavigationTreeNodeSupport addProgram(ReferentialReference<ProgramDto> bean) {

        ObserveSwingDataSource mainDataSource = ObserveSwingApplicationContext.get().getDataSourcesManager().getMainDataSource();
        Set<ReferentialReference<ProgramDto>> programs = mainDataSource.getReferentialReferences(ProgramDto.class);
        List<ReferentialReference<ProgramDto>> data = new ArrayList<>(programs);
        int newIndex = 0;
        if (CollectionUtils.isNotEmpty(data)) {
            data = ProgramHelper.sort(data);
            newIndex = data.indexOf(bean);
        }

        NavigationTreeNodeSupport result;
        if (ProgramHelper.isProgramLongline(bean)) {
            result = new ProgramLonglineNavigationTreeNode(bean, Collections.emptyList());
        } else {
            result = new ProgramSeineNavigationTreeNode(bean, Collections.emptyList());
        }
        insertNode(getRootNode(), result, newIndex);
        return result;
    }

    public void removeProgram(String programId) {
        RootNavigationTreeNode rootNode = getRootNode();
        NavigationTreeNodeSupport result = getChild(rootNode, programId);
        Objects.requireNonNull(result, "Could not find program node with id: " + programId);
        removeNode(result);
    }

    public void updateProgram(ProgramDto bean) {
        RootNavigationTreeNode rootNode = getRootNode();
        NavigationTreeNodeSupport result = getChild(rootNode, bean.getId());
        Objects.requireNonNull(result, "Could not find program node with id: " + bean.getId());
        reloadNode(result, false);
        //FIXME voir a quoi ça servait
//        refreshNode(result, false);
    }

    public RootNavigationTreeNode getRootNode() {
        return getTreeModel().getRoot();
    }

    /**
     * Moves the given {@code node} to the new {@code position}.
     *
     * @param parentNode the parent node
     * @param node       the node to move
     * @param position   the new position of the node
     */
    public void moveNode(NavigationTreeNodeSupport parentNode, NavigationTreeNodeSupport node, int position) {
        parentNode.remove(node);
        parentNode.insert(node, position);
        getTreeModel().nodeStructureChanged(parentNode);
    }


    public NavigationTreeNodeSupport getReferentialNode(String name) {
        RootNavigationTreeNode root = getTreeModel().getRoot();
        return getTreeModel().findNode(root, name);
    }

    public NavigationTreeNodeSupport getProgramNode(String programId) {
        RootNavigationTreeNode root = getTreeModel().getRoot();
        return getTreeModel().findNode(root, programId);
    }

    public NavigationTreeNodeSupport getTripNode(String programId, String tripId) {
        return getTreeModel().findNode(getProgramNode(programId), tripId);
    }

    public NavigationTreeNodeSupport getTripNode(NavigationTreeNodeSupport programNode, String tripId) {
        return getTreeModel().findNode(programNode, tripId);
    }

    public NavigationTreeNodeSupport getRouteNode(NavigationTreeNodeSupport tripNode, String routeId) {
        NavigationTreeNodeSupport routesNode = getTreeModel().findNodeByType(tripNode, RoutesSeineNavigationTreeNode.class);
        return getTreeModel().findNode(routesNode, routeId);
    }

    public NavigationTreeNodeSupport getActivityLonglineNode(NavigationTreeNodeSupport tripNode, String activityId) {
        NavigationTreeNodeSupport activitiesNode = getTreeModel().findNodeByType(tripNode, ActivitiesLonglineNavigationTreeNode.class);
        return getTreeModel().findNode(activitiesNode, activityId);
    }

    public NavigationTreeNodeSupport getActivitySeineNode(NavigationTreeNodeSupport routeNode, String activityId) {
        NavigationTreeNodeSupport activitiesNode = getTreeModel().findNodeByType(routeNode, ActivitiesSeineNavigationTreeNode.class);
        return getTreeModel().findNode(activitiesNode, activityId);
    }

    public NavigationTreeNodeSupport getSetSeineNode(NavigationTreeNodeSupport activitySeineNode) {
        return getTreeModel().findNodeByType(activitySeineNode, SetSeineNavigationTreeNode.class);
    }

    public NavigationTreeNodeSupport getSetLonglineNode(NavigationTreeNodeSupport activityLonglineNode) {
        return getTreeModel().findNodeByType(activityLonglineNode, SetLonglineNavigationTreeNode.class);
    }

    public NavigationTreeNodeSupport<?> getRouteNode(String programId, String tripSeineId, String routeId) {
        NavigationTreeNodeSupport tripNode = getTripNode(programId, tripSeineId);
        return getRouteNode(tripNode, routeId);
    }

    public NavigationTreeNodeSupport<?> getActivityLonglineNode(String programId, String tripLonglineId, String activityLonglineId) {
        NavigationTreeNodeSupport tripNode = getTripNode(programId, tripLonglineId);
        return getActivityLonglineNode(tripNode, activityLonglineId);
    }

    public NavigationTreeNodeSupport<?> getActivitySeineNode(String programId, String tripSeineId, String routeId, String activitySeineId) {
        NavigationTreeNodeSupport routeNode = getRouteNode(programId, tripSeineId, routeId);
        return getActivitySeineNode(routeNode, activitySeineId);
    }

    public void refreshSelectedNode() {
        NavigationTreeNodeSupport selectedNode = getSelectedNode();
        selectedNode.reload();
        getTreeModel().nodeChanged(selectedNode);
    }
}
