/*
 * #%L
 * ObServe :: Swing
 * %%
 * Copyright (C) 2008 - 2010 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

#generalTab {
  title:{t("observe.content.lengthLengthParameter.tab.general")};
  icon:{getHandler().getErrorIconIfFalse(model.isGeneralTabValid())};
}

#otherTab {
  title:{t("observe.content.lengthLengthParameter.tab.other")};
  icon:{getHandler().getErrorIconIfFalse(model.isOtherTabValid())};
}

#speciesLabel {
  text:"observe.common.species";
  font-style:"italic";
  labelFor:{species};
}

#species {
  property:{LengthLengthParameterDto.PROPERTY_SPECIES};
  selectedItem:{bean.getSpecies()};
  enabled:{model.isCreatingMode()};
  _entityClass:{SpeciesDto.class};
}

#inputSizeMeasureTypeLabel {
  text:"observe.common.inputSizeMeasureType";
  font-style:"italic";
  labelFor:{inputSizeMeasureType};
}

#inputSizeMeasureType {
  property:{LengthLengthParameterDto.PROPERTY_INPUT_SIZE_MEASURE_TYPE};
  selectedItem:{getBean().getInputSizeMeasureType()};
  enabled:{model.isCreatingMode()};
  _entityClass:{SizeMeasureTypeDto.class};
}

#outputSizeMeasureTypeLabel {
  text:"observe.common.outputSizeMeasureType";
  font-style:"italic";
  labelFor:{outputSizeMeasureType};
}

#outputSizeMeasureType {
  property:{LengthLengthParameterDto.PROPERTY_OUTPUT_SIZE_MEASURE_TYPE};
  selectedItem:{getBean().getOutputSizeMeasureType()};
  enabled:{model.isCreatingMode()};
  _entityClass:{SizeMeasureTypeDto.class};
}

#relationTable {
  border:{new TitledBorder(t("observe.common.equation"))};
}

#inputOutputFormulaInformation {
  font-size:11;
  text:"observe.content.label.inputOutputFormula.info";
  actionIcon:"information";
}

#inputOutputFormulaLabel {
  text:"observe.common.inputOutputFormula";
  labelFor:{inputOutputFormula};
}

#inputOutputFormula {
  text:{getStringValue(getBean().getInputOutputFormula())};
  _propertyName:{LengthLengthParameterDto.PROPERTY_INPUT_OUTPUT_FORMULA};
}

#outputInputFormulaInformation {
  font-size:11;
  text:"observe.content.label.outputInputFormula.info";
  actionIcon:"information";
}
#outputInputFormulaLabel {
  text:"observe.common.outputInputFormula";
  labelFor:{outputInputFormula};
}

#outputInputFormula {
  text:{getStringValue(getBean().getOutputInputFormula())};
  _propertyName:{LengthLengthParameterDto.PROPERTY_OUTPUT_INPUT_FORMULA};
}

#coefficientsInformation {
  font-size:11;
  text:"observe.content.label.coefficients.info";
  actionIcon:"information";
}
#coefficientsLabel {
  text:"observe.common.coefficients";
  labelFor:{coefficients};
}

#coefficients {
  text:{getStringValue(bean.getCoefficients())};
  _propertyName:{LengthLengthParameterDto.PROPERTY_COEFFICIENTS};
}

#sourceField {
  title:"observe.common.source";
  model:{getBean()};
}