/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.actions.content;

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.ui.ObserveKeyStrokes;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.content.ContentUI;
import fr.ird.observe.application.swing.ui.content.list.impl.longline.ActivityLonglinesUI;
import fr.ird.observe.application.swing.ui.content.list.impl.longline.TripLonglinesUI;
import fr.ird.observe.application.swing.ui.content.list.impl.seine.ActivitySeinesUI;
import fr.ird.observe.application.swing.ui.content.list.impl.seine.RoutesUI;
import fr.ird.observe.application.swing.ui.content.list.impl.seine.TripSeinesUI;
import fr.ird.observe.application.swing.ui.content.open.ContentOpenableUI;
import fr.ird.observe.application.swing.ui.tree.navigation.NavigationTree;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.NavigationTreeNodeSupport;

import javax.swing.SwingUtilities;

import static org.nuiton.i18n.I18n.n;

/**
 * Action pour fermer l'objet sous-jacent à l'écran.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public class CloseOpenUIAction extends AbstractContentUIAction {

    private static final long serialVersionUID = 1L;

    public static final String ACTION_NAME = "closeOpen";

    public CloseOpenUIAction(ObserveMainUI mainUI) {
        super(mainUI,
              ACTION_NAME,
              n("observe.action.close.open"),
              n("observe.action.close.open.tip"),
              "close",
              ObserveKeyStrokes.KEY_STROKE_CLOSE_OPEN
        );
    }

    @Override
    public void actionPerformed(ContentUI<?, ?> ui) {
        if (ui instanceof ContentOpenableUI) {
            ((ContentOpenableUI<?, ?>) ui).closeData();
            return;
        }

        ObserveSwingApplicationContext applicationContext = ObserveSwingApplicationContext.get();

        if (ui instanceof TripSeinesUI) {
            String higherOpenId = applicationContext.getDataContext().getOpenTripSeineId();
            closeData(applicationContext, ui, higherOpenId);
            return;
        }
        if (ui instanceof RoutesUI) {
            String higherOpenId = applicationContext.getDataContext().getOpenRouteId();
            closeData(applicationContext, ui, higherOpenId);
            return;
        }
        if (ui instanceof ActivitySeinesUI) {
            String higherOpenId = applicationContext.getDataContext().getOpenActivitySeineId();
            closeData(applicationContext, ui, higherOpenId);
            return;
        }

        if (ui instanceof TripLonglinesUI) {
            String higherOpenId = applicationContext.getDataContext().getOpenTripLonglineId();
            closeData(applicationContext, ui, higherOpenId);
            return;
        }
        if (ui instanceof ActivityLonglinesUI) {
            String higherOpenId = applicationContext.getDataContext().getOpenActivityLonglineId();
            closeData(applicationContext, ui, higherOpenId);
            return;
        }

        throw new IllegalStateException("Can not come here!");

    }

    private void closeData(ObserveSwingApplicationContext applicationContext, ContentUI<?, ?> ui, String id) {

        NavigationTree tree = getMainUI().getNavigation();
        NavigationTreeNodeSupport selectedNode = tree.getSelectedNode();

        NavigationTreeNodeSupport node = tree.getChild(selectedNode, id);

        tree.selectNode(node);

        ContentOpenableUI selectedUI = (ContentOpenableUI) applicationContext.getContentUIManager().getSelectedContentUI();

        // on demande la fermeture de la donnée
        selectedUI.closeData();

        SwingUtilities.invokeLater(() -> {

            // retour sur le noeud parent
            tree.selectNode(selectedNode);

        });

    }

}
