/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.content.impl.seine;

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.DataContext;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.db.constants.DataContextType;
import fr.ird.observe.application.swing.ui.content.ContentMode;
import fr.ird.observe.application.swing.ui.content.ContentUIHandler;
import fr.ird.observe.application.swing.ui.content.ContentUIModel;
import fr.ird.observe.application.swing.ui.tree.navigation.NavigationTree;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.NavigationTreeNodeSupport;
import fr.ird.observe.services.dto.form.Form;
import fr.ird.observe.services.dto.reference.DataReference;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.dto.seine.ActivitySeineDto;
import fr.ird.observe.services.dto.seine.FloatingObjectDto;
import fr.ird.observe.services.dto.seine.FloatingObjectHelper;
import fr.ird.observe.services.service.data.seine.FloatingObjectService;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.validator.NuitonValidatorScope;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class FloatingObjectUIHandler extends ContentUIHandler<FloatingObjectDto, FloatingObjectUI> implements UIHandler<FloatingObjectUI> {

    /** Logger */
    private static final Log log = LogFactory.getLog(FloatingObjectUIHandler.class);

    public FloatingObjectUIHandler() {
        super(DataContextType.ActivitySeine, DataContextType.FloatingObject);
    }

    @Override
    protected boolean computeCanWrite(ObserveSwingDataSource source) {
        return source.canWriteData();
    }

    @Override
    protected ContentMode getContentMode(DataContext dataContext) {

        String dcpId = getSelectedId();

        if (dcpId == null) {

            // mode création
            return ContentMode.CREATE;
        }

        // dcp existant

        if (getOpenDataManager().isOpenActivitySeine(dataContext.getSelectedActivitySeineId())) {

            //  mode mise a jour
            return ContentMode.UPDATE;
        }

        // l'activité n'est pas ouverte
        addMessage(
                getUi(),
                NuitonValidatorScope.INFO,
                getTypeI18nKey(ActivitySeineDto.class),
                t("observe.storage.activitySeine.message.not.open"));

        return ContentMode.READ;
    }

    @Override
    public void openUI() {
        super.openUI();

        String activityId = getSelectedParentId();
        String dcpId = getSelectedId();

        if (log.isInfoEnabled()) {
            log.info("activityId = " + activityId);
            log.info("dcpId      = " + dcpId);
        }

        ContentMode mode = computeContentMode();

        FloatingObjectDto bean = getBean();

        Form<FloatingObjectDto> form;
        if (dcpId == null) {

            // create mode
            form = getFloatingObjectService().preCreate(activityId);

        } else {

            // update mode
            form = getFloatingObjectService().loadForm(dcpId);

        }
        loadReferentialReferenceSetsInModel(form);

        getModel().setForm(form);
        FloatingObjectHelper.copyFloatingObjectDto(form.getObject(), bean);

        // utilisation du mode requis
        setContentMode(mode);

        if (mode != ContentMode.READ) {
            getUi().startEdit(null);
        }
    }

    @Override
    public void startEditUI(String... binding) {
        ContentUIModel<FloatingObjectDto> contentUIModel = getModel();
        boolean create = contentUIModel.getMode() == ContentMode.CREATE;
        String contextName = getValidatorContextName(contentUIModel.getMode());
        getUi().getValidator().setContext(contextName);
        if (create) {
            addMessage(getUi(),
                       NuitonValidatorScope.INFO,
                       getTypeI18nKey(FloatingObjectDto.class),
                       t("observe.content.floatingObject.message.creating"));
        } else {
            addMessage(getUi(),
                       NuitonValidatorScope.INFO,
                       getTypeI18nKey(FloatingObjectDto.class),
                       t("observe.content.floatingObject.message.updating"));
        }
        super.startEditUI(FloatingObjectUI.BINDING_DAYS_AT_SEA_COUNT_NUMBER_VALUE,
                          FloatingObjectUI.BINDING_OBJECT_TYPE_SELECTED_ITEM,
                          FloatingObjectUI.BINDING_OBJECT_FATE_SELECTED_ITEM,
                          FloatingObjectUI.BINDING_COMMENT_MODEL);
        contentUIModel.setModified(create);
    }

    @Override
    protected boolean doSave(FloatingObjectDto bean) {

        String activityId = getSelectedParentId();

        SaveResultDto saveResult = getFloatingObjectService().save(activityId, bean);
        saveResult.toDto(bean);

        return true;

    }

    @Override
    protected void afterSave(boolean refresh) {
        super.afterSave(refresh);
        FloatingObjectDto bean = getBean();

        NavigationTree treeHelper = getTreeHelper(getUi());

        NavigationTreeNodeSupport node = treeHelper.getSelectedNode();
        boolean create = node.getId() == null;

        if (create) {

            getModel().setMode(ContentMode.UPDATE);

            // remove old node and recreate new node
            NavigationTreeNodeSupport parentNode = node.getParent();
            treeHelper.removeNode(node);

            DataReference<FloatingObjectDto> beanRef = ObserveSwingApplicationContext.get().getReferenceBinderEngine().transformDataDtoToReference(getDecoratorService().getReferentialLocale(), bean);
            node = treeHelper.addFloatingObject(parentNode, beanRef);
            stopEditUI();
            if (refresh) {
                treeHelper.selectNode(node);
            }
        } else {

            treeHelper.refreshSelectedNode();
        }
    }

    @Override
    protected boolean doDelete(FloatingObjectDto bean) {

        if (askToDelete(bean)) {
            return false;
        }

        if (bean.getId() != null) {

            String activityId = getSelectedParentId();
            getFloatingObjectService().delete(activityId, bean.getId());
        }
        return true;
    }

    protected FloatingObjectService getFloatingObjectService() {
        return getDataSource().newFloatingObjectService();
    }
}
