package fr.ird.observe.application.swing.ui.tree.selection;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.jaxx.runtime.spi.UIHandler;

import javax.swing.SwingUtilities;
import java.beans.PropertyChangeListener;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class SelectionTreeHeaderHandler implements UIHandler<SelectionTreeHeader> {

    private SelectionTreeHeader ui;

    public SelectionTreeHeader getUi() {
        return ui;
    }

    private SelectionTree getTree() {
        return getUi().getTreeTable();
    }

    @Override
    public void beforeInit(SelectionTreeHeader selectionTreeHeader) {
        this.ui = selectionTreeHeader;
    }

    @Override
    public void afterInit(SelectionTreeHeader selectionTreeHeader) {

        PropertyChangeListener propertyChangeListener = e -> onTripCountChanged((int) e.getNewValue());
        selectionTreeHeader.addPropertyChangeListener(SelectionTreeHeader.PROPERTY_TREE_TABLE, e -> {
            SelectionTreeModel newValue = ((SelectionTree) e.getNewValue()).getTreeModel();
            newValue.removePropertyChangeListener(SelectionTreeModel.SELECTED_COUNT, propertyChangeListener);
            newValue.addPropertyChangeListener(SelectionTreeModel.SELECTED_COUNT, propertyChangeListener);
        });
    }

    private void onTripCountChanged(int newValue) {

        String text;
        if (newValue == 0) {
            text = t("observe.selection.no.trips.selected");
        } else {
            text = t("observe.selection.selected.trips", newValue);
        }
        getUi().setLabelText(text);
    }

    public void collapseAll() {
        SelectionTree tree = getTree();
        tree.collapseAll();
    }

    public void expandAll() {
        SelectionTree tree = getTree();
        tree.expandAll();
    }

    public void selectAll() {
        SelectionTree selectionTree = getTree();
        selectionTree.getTreeModel().selectAllTrips();
        SwingUtilities.invokeLater(selectionTree::repaint);
    }

    public void unselectAll() {
        SelectionTree selectionTree = getTree();
        selectionTree.getTreeModel().unselectAllTrips();
        SwingUtilities.invokeLater(selectionTree::repaint);
    }
}
