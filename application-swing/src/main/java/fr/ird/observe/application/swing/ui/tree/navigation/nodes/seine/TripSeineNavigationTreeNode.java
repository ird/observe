package fr.ird.observe.application.swing.ui.tree.navigation.nodes.seine;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ui.content.open.impl.seine.TripSeineUI;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.ReferenceNavigationTreeNodeSupport;
import fr.ird.observe.services.dto.reference.DataReference;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import fr.ird.observe.services.dto.seine.TripSeineHelper;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 14/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class TripSeineNavigationTreeNode extends ReferenceNavigationTreeNodeSupport<TripSeineDto, DataReference<TripSeineDto>> {

    public TripSeineNavigationTreeNode(DataReference<TripSeineDto> data) {
        super(data, true);
        if (isPersisted()) {
            add(new GearUseFeaturesSeineNavigationTreeNode());
            add(new RoutesSeineNavigationTreeNode());
        }
    }

    @Override
    public void reload() {
        DataReference<TripSeineDto> data = getMainDataSourceServicesProvider().newTripSeineService().loadReferenceToRead(getId());
        setData(data);
    }

    @Override
    public boolean isLeaf() {
        return !isPersisted();
    }

    @Override
    public String getText() {
        return isPersisted() ? (super.getText() + " (" + TripSeineHelper.getRouteCount(getData()) + ")") : t("observe.type.tripSeine.unsaved");
    }

    @Override
    public Class<TripSeineUI> getContentClass() {
        return TripSeineUI.class;
    }
}
