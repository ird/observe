package fr.ird.observe.application.swing.ui;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.ui.actions.AbstractUIAction;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.jaxx.runtime.JAXXObject;

import javax.swing.AbstractButton;
import javax.swing.JTabbedPane;
import javax.swing.KeyStroke;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

import static fr.ird.observe.application.swing.ui.content.ContentUIInitializer.GLOBAL_ACTION;
import static javax.swing.Action.ACCELERATOR_KEY;
import static javax.swing.Action.NAME;
import static javax.swing.Action.SHORT_DESCRIPTION;
import static org.nuiton.i18n.I18n.t;

/**
 * Created on 23/12/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public abstract class ObserveKeyStrokes {

    public static final KeyStroke KEY_STROKE_STORAGE_DO_CONFIGURE_REMOTE_SERVER = KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.CTRL_MASK);
    public static final KeyStroke KEY_STROKE_STORAGE_DO_BACKUP = KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK);
    public static final KeyStroke KEY_STROKE_STORAGE_DO_CHOOSE_DIRECTORY = KeyStroke.getKeyStroke(KeyEvent.VK_D, InputEvent.CTRL_MASK);
    public static final KeyStroke KEY_STROKE_STORAGE_DO_USE_SSL = KeyStroke.getKeyStroke(KeyEvent.VK_U, InputEvent.CTRL_MASK);
    public static final KeyStroke KEY_STROKE_STORAGE_DO_CHOOSE_FILE = KeyStroke.getKeyStroke(KeyEvent.VK_F, InputEvent.CTRL_MASK);
    public static final KeyStroke KEY_STROKE_ESCAPE = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
    public static final KeyStroke KEY_STROKE_ENTER = KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0);
    public static final KeyStroke KEY_STROKE_SHOW_SEINE = KeyStroke.getKeyStroke("ctrl pressed F1");
    public static final KeyStroke KEY_STROKE_SHOW_LONGLINE = KeyStroke.getKeyStroke("ctrl pressed F2");
    public static final KeyStroke KEY_STROKE_SHOW_REFERENTIAL = KeyStroke.getKeyStroke("ctrl pressed F3");
    public static final KeyStroke KEY_STROKE_SHOW_EMPTY_PROGRAM = KeyStroke.getKeyStroke("ctrl pressed F4");
    public static final KeyStroke KEY_STROKE_CONFIGURE_LOCAL_SOURCE = KeyStroke.getKeyStroke("ctrl pressed L");
    public static final KeyStroke KEY_STROKE_CONFIGURE_REMOTE_SOURCE = KeyStroke.getKeyStroke("ctrl pressed R");
    public static final KeyStroke KEY_STROKE_SAVE_TABLE_ENTRY = KeyStroke.getKeyStroke("shift ctrl pressed S");
    public static final KeyStroke KEY_STROKE_PRESSED_ENTER = KeyStroke.getKeyStroke("pressed ENTER");
    public static final KeyStroke KEY_STROKE_RESET_DATA = KeyStroke.getKeyStroke("pressed F7");
    public static final KeyStroke KEY_STROKE_NEW_NEXT_DATA = KeyStroke.getKeyStroke("pressed F4");
    public static final KeyStroke KEY_STROKE_DELETE_DATA_GLOBAL = KeyStroke.getKeyStroke("pressed F6");
    public static final KeyStroke KEY_STROKE_OPEN_CLOSE_DATA = KeyStroke.getKeyStroke("pressed F3");
    public static final KeyStroke KEY_STROKE_SAVE_DATA = KeyStroke.getKeyStroke("pressed F5");
    public static final KeyStroke KEY_STROKE_CHANGE_FOCUS = KeyStroke.getKeyStroke("pressed F1");
    public static final KeyStroke KEY_STROKE_SAVE = KeyStroke.getKeyStroke("ctrl pressed S");
    public static final KeyStroke KEY_STROKE_SHIFT_ALT_ENTER = KeyStroke.getKeyStroke("alt shift pressed ENTER");
    public static final KeyStroke KEY_STROKE_OPEN = KeyStroke.getKeyStroke("ctrl pressed O");
    public static final KeyStroke KEY_STROKE_GO_DOWN = KeyStroke.getKeyStroke("alt pressed D");
    public static final KeyStroke KEY_STROKE_NEW_TABLE_ENTRY = KeyStroke.getKeyStroke("ctrl pressed N");
    public static final KeyStroke KEY_STROKE_ADD_DCP = KeyStroke.getKeyStroke("ctrl pressed P");
    public static final KeyStroke KEY_STROKE_CREATE = KeyStroke.getKeyStroke("ctrl pressed N");
    public static final KeyStroke KEY_STROKE_ADD_SET_LONGLINE = KeyStroke.getKeyStroke("ctrl pressed L");
    public static final KeyStroke KEY_STROKE_GO_UP = KeyStroke.getKeyStroke("alt pressed U");
    public static final KeyStroke KEY_STROKE_DELETE_REFERENCE = KeyStroke.getKeyStroke("ctrl pressed X");
    public static final KeyStroke KEY_STROKE_DELETE_DATA = KeyStroke.getKeyStroke("ctrl pressed X");
    public static final KeyStroke KEY_STROKE_EDIT_REFERENTIAL = KeyStroke.getKeyStroke("ctrl pressed M");
    public static final KeyStroke KEY_STROKE_DELETE_TABLE_ENTRY = KeyStroke.getKeyStroke("ctrl pressed X");
    public static final KeyStroke KEY_STROKE_RESET_EDIT = KeyStroke.getKeyStroke("ctrl pressed R");
    public static final KeyStroke KEY_STROKE_RESET_TABLE_ENTRY = KeyStroke.getKeyStroke("ctrl shift pressed R");
    public static final KeyStroke KEY_STROKE_ADD_SET_SEINE = KeyStroke.getKeyStroke("ctrl pressed L");
    public static final KeyStroke KEY_STROKE_ALT_ENTER = KeyStroke.getKeyStroke("alt pressed ENTER");
    public static final KeyStroke KEY_STROKE_BACK_TO_REFERENTIAL_LIST = KeyStroke.getKeyStroke("ctrl pressed B");
    public static final KeyStroke KEY_STROKE_CLOSE_OPEN = KeyStroke.getKeyStroke("ctrl pressed C");
    public static final KeyStroke KEY_STROKE_CLOSE_AND_CREATE = KeyStroke.getKeyStroke("ctrl pressed N");
    public static final KeyStroke KEY_STROKE_RESET = KeyStroke.getKeyStroke("ctrl pressed D");
    public static final KeyStroke KEY_STROKE_SHOW_DECORATOR_POPUP = KeyStroke.getKeyStroke("ctrl pressed F");
    public static final KeyStroke KEY_STROKE_GO_DOWN_TABLE_ENTRY = KeyStroke.getKeyStroke("shift pressed W");
    public static final KeyStroke KEY_STROKE_GO_UP_TABLE_ENTRY = KeyStroke.getKeyStroke("shift pressed Q");
    public static final KeyStroke KEY_STROKE_GO_TAB_1 = KeyStroke.getKeyStroke("ctrl pressed F5");
    public static final KeyStroke KEY_STROKE_GO_TAB_2 = KeyStroke.getKeyStroke("ctrl pressed F6");
    public static final KeyStroke KEY_STROKE_GO_TAB_3 = KeyStroke.getKeyStroke("ctrl pressed F7");
    public static final KeyStroke KEY_STROKE_GO_TAB_4 = KeyStroke.getKeyStroke("ctrl pressed F8");
    public static final KeyStroke KEY_STROKE_GO_TAB_5 = KeyStroke.getKeyStroke("ctrl pressed F9");
    public static final KeyStroke KEY_STROKE_GO_TAB_6 = KeyStroke.getKeyStroke("ctrl pressed F10");

    public static final KeyStroke KEY_STROKE_GO_SUB_TAB_1 = KeyStroke.getKeyStroke("shift ctrl pressed F5");
    public static final KeyStroke KEY_STROKE_GO_SUB_TAB_2 = KeyStroke.getKeyStroke("shift ctrl pressed F6");
    public static final KeyStroke KEY_STROKE_GO_SUB_TAB_3 = KeyStroke.getKeyStroke("shift ctrl pressed F7");

    public static final ImmutableList<KeyStroke> KEY_STROKE_GO_TABS = ImmutableList.of(
            KEY_STROKE_GO_TAB_1,
            KEY_STROKE_GO_TAB_2,
            KEY_STROKE_GO_TAB_3,
            KEY_STROKE_GO_TAB_4,
            KEY_STROKE_GO_TAB_5,
            KEY_STROKE_GO_TAB_6
    );
    public static final ImmutableList<KeyStroke> KEY_STROKE_GO_SUB_TABS = ImmutableList.of(
            KEY_STROKE_GO_SUB_TAB_1,
            KEY_STROKE_GO_SUB_TAB_2,
            KEY_STROKE_GO_SUB_TAB_3
    );

    private static String keyStrokeToStr(KeyStroke actionKey) {
        String result = "";
        if (actionKey != null) {
            result = " (" + StringUtils.removeStart(actionKey.toString().replace("pressed", "+"), "+ ") + ")";
        }
        return result;
    }

    public static void addKeyStroke(AbstractButton component, KeyStroke actionKey) {

        boolean showMnemonic = isShowMnemonic();
        if (showMnemonic) {
            String actionStr = keyStrokeToStr(actionKey);
            if (component.getText() != null && !component.getText().contains(actionStr)) {
                String text = component.getText() + actionStr;
                component.setText(text);
            }
            if (component.getToolTipText() != null && !component.getToolTipText().contains(actionStr)) {
                String tip = component.getToolTipText() + actionStr;
                component.setToolTipText(tip);
            }
        }
    }

    protected static boolean isShowMnemonic() {
        return ObserveSwingApplicationContext.get().getConfig().isShowMnemonic();
    }

    public static void addKeyStore(AbstractUIAction action, KeyStroke keyStroke, String label, String shortDescription) {

        String acceleratorStr = "";
        if (keyStroke != null) {
            action.putValue(ACCELERATOR_KEY, keyStroke);
            acceleratorStr = isShowMnemonic() ? keyStrokeToStr(keyStroke) : "";
        }

        action.putValue(NAME, t(label) + acceleratorStr);
        action.putValue(SHORT_DESCRIPTION, t(shortDescription) + acceleratorStr);
    }

    public static String suffixTextWithKeyStroke(String text, KeyStroke keyStroke) {
        boolean showMnemonic = isShowMnemonic();
        if (showMnemonic) {
            text += keyStrokeToStr(keyStroke);
        }
        return text;
    }

    public static void addKeyStroke2(AbstractButton editor, KeyStroke keyStroke) {
        String tip = (String) editor.getClientProperty("toolTipText");
        boolean showMnemonic = isShowMnemonic();
        if (tip != null) {
            if (keyStroke != null && showMnemonic) {
                tip += keyStrokeToStr(keyStroke);
            }
            editor.setToolTipText(tip);
        }

        String text = (String) editor.getClientProperty("text");
        if (text != null) {
            if (keyStroke != null && showMnemonic) {
                text += keyStrokeToStr(keyStroke);
            }
            editor.setText(text);
        }

        if (text != null || tip != null) {
            String actionId = (String) editor.getClientProperty(GLOBAL_ACTION);
            if (actionId != null) {
                AbstractUIAction action = (AbstractUIAction) ObserveSwingApplicationContext.get().getActionMap().get(actionId);
                Objects.requireNonNull(action, "action [" + actionId + "] not found");
                KeyStroke acceleratorKey = action.getAcceleratorKey();
                addKeyStroke(editor, acceleratorKey);
            }
        }

    }

    public static void addKeyStrokeFromMnemonic(AbstractButton editor) {
        int mnemonic = editor.getMnemonic();
        if (mnemonic > 0) {
            if (isShowMnemonic()) {
                String accelerator = " (Alt + " + (char) mnemonic + ")";
                if (editor.getText() != null) {
                    editor.setText(editor.getText() + accelerator);
                }
                if (editor.getToolTipText() != null) {
                    editor.setToolTipText(editor.getToolTipText() + accelerator);
                }
            }
        }
    }

    public static void addKeyStrokeFromMnemonic(JAXXObject jaxxObject) {
        Set<JAXXObject> done = new LinkedHashSet<>();
        addKeyStrokeFromMnemonic(jaxxObject, done);
    }

    public static void addKeyStrokeToMainTabbedPane(JTabbedPane tabbedPane) {
        if (isShowMnemonic()) {
            int tabCount = tabbedPane.getTabCount();
            for (int i = 0; i < tabCount; i++) {
                String titleAt = tabbedPane.getTitleAt(i);
                tabbedPane.setTitleAt(i, titleAt + keyStrokeToStr(KEY_STROKE_GO_TABS.get(i)));
            }
        }
    }

    public static void addKeyStrokeToSubTabbedPane(JTabbedPane tabbedPane) {
        if (isShowMnemonic()) {
            int tabCount = tabbedPane.getTabCount();
            for (int i = 0; i < tabCount; i++) {
                String titleAt = tabbedPane.getTitleAt(i);
                tabbedPane.setTitleAt(i, titleAt + keyStrokeToStr(KEY_STROKE_GO_SUB_TABS.get(i)));
            }
        }
    }

    protected static void addKeyStrokeFromMnemonic(JAXXObject jaxxObject, Set<JAXXObject> done) {

        if (done.contains(jaxxObject)) {
            return;
        }
        done.add(jaxxObject);

        jaxxObject.get$objectMap().values().stream().filter(o -> o instanceof AbstractButton).forEach(
                o -> addKeyStrokeFromMnemonic((AbstractButton) o)
        );

        jaxxObject.get$objectMap().values().stream().filter(o -> o instanceof JAXXObject).forEach(
                o -> addKeyStrokeFromMnemonic((JAXXObject) o, done)
        );
    }
}
