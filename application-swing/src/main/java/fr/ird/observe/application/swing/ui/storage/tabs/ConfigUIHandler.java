package fr.ird.observe.application.swing.ui.storage.tabs;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.ObserveTextGenerator;
import fr.ird.observe.application.swing.configuration.constants.DbMode;
import fr.ird.observe.application.swing.db.constants.ConnexionStatus;
import fr.ird.observe.application.swing.ui.ObserveKeyStrokes;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.actions.AbstractUIAction;
import fr.ird.observe.application.swing.ui.actions.storage.UseRemoteConfigurationUIAction;
import fr.ird.observe.application.swing.ui.actions.storage.UseServerConfigurationUIAction;
import fr.ird.observe.application.swing.ui.content.ObserveActionMap;
import fr.ird.observe.application.swing.ui.storage.StorageStep;
import fr.ird.observe.application.swing.ui.storage.StorageUIModel;
import fr.ird.observe.services.dto.presets.RemoteDataSourceConfiguration;
import fr.ird.observe.services.dto.presets.ServerDataSourceConfiguration;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.ActionMap;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.Objects;

import static fr.ird.observe.application.swing.ui.content.ContentUIInitializer.OBSERVE_ACTION;
import static javax.swing.JComponent.WHEN_IN_FOCUSED_WINDOW;
import static org.nuiton.i18n.I18n.t;

/**
 * Created on 27/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since X
 */
public class ConfigUIHandler extends StorageTabUIHandler<ConfigUI> implements UIHandler<ConfigUI> {

    /** Logger */
    private static final Log log = LogFactory.getLog(ConfigUIHandler.class);

    public static final String ACTION_DO_CHOOSE_FILE = "doChooseFile";
    public static final String ACTION_DO_USE_SSL = "doUseSsl";

    @Override
    public void afterInit(ConfigUI ui) {

        PropertyChangeListener listener = evt -> {
            StorageUIModel model = (StorageUIModel) evt.getSource();
            String propertyName = evt.getPropertyName();
            if (StorageUIModel.CREATION_MODE_PROPERTY_NAME.equals(propertyName) ||
                    StorageUIModel.DB_MODE_PROPERTY_NAME.equals(propertyName)) {
                String id = null;
                if (model.getDbMode() == DbMode.USE_REMOTE) {
                    id = DbMode.USE_REMOTE.name();
                } else if (model.getDbMode() == DbMode.USE_SERVER) {
                    id = DbMode.USE_SERVER.name();
                } else {
                    if (model.getCreationMode() != null) {
                        id = model.getCreationMode().name();
                    }
                }
                if (id != null) {
                    refreshConfig(ui, id);
                }
            }
        };
        ui.getModel().addPropertyChangeListener(listener);

        ActionMap actionMap = ui.getActionMap();
        actionMap.put(ACTION_DO_CHOOSE_FILE, new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (StorageStep.CONFIG == ui.getModel().getStep()) {
                    ui.getFileChooserAction().doClick();
                }
            }
        });
        ui.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(ObserveKeyStrokes.KEY_STROKE_STORAGE_DO_CHOOSE_FILE, ACTION_DO_CHOOSE_FILE);
        ObserveKeyStrokes.addKeyStroke(ui.getFileChooserAction(), ObserveKeyStrokes.KEY_STROKE_STORAGE_DO_CHOOSE_FILE);

        actionMap.put(ACTION_DO_USE_SSL, new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (StorageStep.CONFIG == ui.getModel().getStep()) {
                    ui.getRemoteUseSsl().doClick();
                }
            }
        });
        ui.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(ObserveKeyStrokes.KEY_STROKE_STORAGE_DO_USE_SSL, ACTION_DO_USE_SSL);
        ObserveKeyStrokes.addKeyStroke(ui.getRemoteUseSsl(), ObserveKeyStrokes.KEY_STROKE_STORAGE_DO_USE_SSL);

        ObserveActionMap observeActionMap = ObserveSwingApplicationContext.get().getMainUI().getObserveActionMap();
        init(observeActionMap, ui.getSaveRemoteConfiguration());
        init(observeActionMap, ui.getSaveServerConfiguration());
        init(observeActionMap, ui.getServerMenu());
        init(observeActionMap, ui.getRemoteMenu());

        for (RemoteDataSourceConfiguration configuration : ObserveSwingApplicationContext.get().getMainUI().getConfig().getRemoteDataSourceConfigurations()) {
            addRemoteConfiguration(configuration);
        }

        for (ServerDataSourceConfiguration configuration : ObserveSwingApplicationContext.get().getMainUI().getConfig().getServerDataSourceConfigurations()) {
            addServerConfiguration(configuration);
        }
    }

    public void addRemoteConfiguration(RemoteDataSourceConfiguration configuration) {
        ObserveActionMap observeActionMap = ObserveSwingApplicationContext.get().getMainUI().getObserveActionMap();
        JMenuItem item = new JMenuItem(configuration.getName());
        item.putClientProperty("configuration", configuration);
        item.putClientProperty("text", configuration.getName());
        item.putClientProperty("toolTipText", configuration.getName());
        item.putClientProperty(OBSERVE_ACTION, UseRemoteConfigurationUIAction.ACTION_NAME);
        AbstractUIAction action = init(observeActionMap, item);
        action.updateAction(ui, item);
        ui.getRemoteMenu().add(item);
    }

    public void addServerConfiguration(ServerDataSourceConfiguration configuration) {
        ObserveActionMap observeActionMap = ObserveSwingApplicationContext.get().getMainUI().getObserveActionMap();
        JMenuItem item = new JMenuItem(configuration.getName());
        item.putClientProperty("configuration", configuration);
        item.putClientProperty("text", configuration.getName());
        item.putClientProperty("toolTipText", configuration.getName());
        item.putClientProperty(OBSERVE_ACTION, UseServerConfigurationUIAction.ACTION_NAME);
        AbstractUIAction action = init(observeActionMap, item);
        action.updateAction(ui, item);
        ui.getServerMenu().add(item);
    }

    public void refreshConfig(ConfigUI ui, String configId) {
        JComponent c = (JComponent) ui.getObjectById(configId);
        if (c != null) {
            if (log.isDebugEnabled()) {
                log.debug(configId);
            }
            ui.configLayout.show(ui.configContent, configId);
            String text = (String) c.getClientProperty("description");
            ui.setDescriptionText(t(text));
            if (c.equals(ui.IMPORT_REMOTE_STORAGE)) {
                ui.IMPORT_REMOTE_STORAGE_content.add(ui.remoteConfig, BorderLayout.CENTER);
            } else if (c.equals(ui.IMPORT_SERVER_STORAGE)) {
                ui.IMPORT_SERVER_STORAGE_content.add(ui.remoteConfig, BorderLayout.CENTER);
            } else if (c.equals(ui.USE_REMOTE)) {
                ui.USE_REMOTE_content.add(ui.remoteConfig, BorderLayout.CENTER);
            } else if (c.equals(ui.USE_SERVER)) {
                ui.USE_SERVER_content.add(ui.remoteConfig, BorderLayout.CENTER);
            }
        }
    }


    public Icon updateConnexionStatutIcon(ConnexionStatus status) {
        return (Icon) ui.getConnexionStatus().getClientProperty(status.name().toLowerCase() + "Icon");
    }

    public Color updateConnexionStatutColor(ConnexionStatus status) {
        return (Color) ui.getConnexionStatus().getClientProperty(status.name().toLowerCase() + "Color");
    }

    public String updateConnexionStatutText(@SuppressWarnings("unused") ConnexionStatus status) {
        ObserveSwingApplicationContext applicationContext = ObserveSwingApplicationContext.get();
        ObserveTextGenerator textGenerator = applicationContext.getTextGenerator();
        return textGenerator.getConnexionTestResultMessage(ui.getModel());
    }

    public void chooseDumpFile() {
        File f = UIHelper.chooseFile(ui,
                                     t("observe.title.choose.db.dump"),
                                     t("observe.action.choose.db.dump"),
                                     ui.getModel().getDumpFile(),
                                     "^.+\\.sql\\.gz$",
                                     t("observe.action.choose.db.dump.description"));
        ui.getModel().setDumpFile(f);
    }


    protected AbstractUIAction init(ActionMap actionMap, AbstractButton editor) {
        String actionId = (String) editor.getClientProperty(OBSERVE_ACTION);
        if (actionId == null) {
            // le boutton n'est pas commun
            return null;
        }

        // on a trouve une action commune
        AbstractUIAction action = (AbstractUIAction) actionMap.get(actionId);
        Objects.requireNonNull(action, "action [" + actionId + "] not found for ui " + ui.getClass().getName());

        if (log.isDebugEnabled()) {
            log.debug("init common action " + actionId);
        }

        action.initAction(ui, editor);
        return action;
    }
}
