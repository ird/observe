/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.admin.export;

import com.google.common.collect.ImmutableList;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.decoration.decorators.DataReferenceDecorator;
import fr.ird.observe.application.swing.decoration.decorators.ReferentialReferenceDecorator;
import fr.ird.observe.application.swing.ui.admin.AdminActionModel;
import fr.ird.observe.application.swing.ui.admin.AdminStep;
import fr.ird.observe.application.swing.ui.admin.AdminUIModel;
import fr.ird.observe.application.swing.ui.util.ProgressModel;
import fr.ird.observe.services.dto.longline.TripLonglineDto;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardState;

import java.util.ArrayList;
import java.util.List;

/**
 * Le modèle d'une opération d'export de données observers.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public class ExportModel extends AdminActionModel {

    /** les index des marées à exporter. */
    protected int[] exportDataSelectedIndex;

    /** les données exportables */
    protected List<TripEntry> data;

    protected ReferentialReferenceDecorator<ProgramDto> programDecorator;

    protected DataReferenceDecorator<TripSeineDto> tripSeineDecorator;

    protected DataReferenceDecorator<TripLonglineDto> tripLonglineDecorator;

    protected ObserveSwingDataSource source;

    protected ObserveSwingDataSource centralSource;

    protected boolean insertMissingReferentials;
    private final ProgressModel progressModel = new ProgressModel();

    public ExportModel() {
        super(AdminStep.EXPORT_DATA);
    }

    public void setExportDataSelectedIndex(int... exportDataSelectedIndex) {
        this.exportDataSelectedIndex = exportDataSelectedIndex;
    }

    @Override
    public void destroy() {
        super.destroy();
        exportDataSelectedIndex = null;
        tripSeineDecorator = null;
        data = null;
        centralSource = null;
        source = null;
    }

    public List<TripEntry> getData() {
        return data;
    }

    public void setData(ImmutableList<TripEntry> data) {
        this.data = data;
    }

    public ObserveSwingDataSource getSource() {
        return source;
    }

    public void setSource(ObserveSwingDataSource source) {
        this.source = source;
    }

    public ObserveSwingDataSource getCentralSource() {
        return centralSource;
    }

    public void setCentralSource(ObserveSwingDataSource centralSource) {
        this.centralSource = centralSource;
    }

    public void setProgramDecorator(ReferentialReferenceDecorator<ProgramDto> programDecorator) {
        this.programDecorator = programDecorator;
    }

    public void setTripSeineDecorator(DataReferenceDecorator<TripSeineDto> mareeDecorator) {
        this.tripSeineDecorator = mareeDecorator;
    }

    public void setTripLonglineDecorator(DataReferenceDecorator<TripLonglineDto> tripLonglineDecorator) {
        this.tripLonglineDecorator = tripLonglineDecorator;
    }

    public List<TripEntry> getSelectedTrips() {

        List<TripEntry> entries = new ArrayList<>();

        for (int index : exportDataSelectedIndex) {
            entries.add(data.get(index));
        }
        return entries;

    }

    public boolean isInsertMissingReferentials() {
        return insertMissingReferentials;
    }

    public void setInsertMissingReferentials(boolean insertMissingReferentials) {
        this.insertMissingReferentials = insertMissingReferentials;
        firePropertyChange("insertMissingReferentials", null, insertMissingReferentials);
    }

    public boolean validate(AdminUIModel uiModel) {
        return uiModel.validate(AdminStep.SELECT_DATA) && uiModel.getStepState(step) == WizardState.SUCCESSED;
    }

    public ProgressModel getProgressModel() {
        return progressModel;
    }
}
