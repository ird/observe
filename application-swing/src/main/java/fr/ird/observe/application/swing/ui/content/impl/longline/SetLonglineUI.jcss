/*
 * #%L
 * ObServe :: Swing
 * %%
 * Copyright (C) 2008 - 2014 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

#model {
  editable:true;
  modified:{validator.isChanged()};
  valid:{validator.isValid()};
  enabled:{!model.isReadingMode()};
}

/* ***************************************************************************** */
/*  SETTING TAB **************************************************************** */
/* ***************************************************************************** */

#settingTab {
  title:{t("observe.content.setLongline.tab.setting")};
  icon:{handler.getErrorIconIfFalse(model.isSettingTabValid())};
}

#homeIdLabel {
  text:"observe.content.setLongline.homeId";
  labelFor:{homeId};
}

#homeId {
  text: {getStringValue(bean.getHomeId())};
  _propertyName: {SetLonglineDto.PROPERTY_HOME_ID};
  _validatorLabel:{t("observe.content.setLongline.homeId")};
}

#resetHomeId {
  _resetPropertyName: {SetLonglineDto.PROPERTY_HOME_ID};
  toolTipText:"observe.content.setLongline.action.reset.homeId.tip";
}

#numberLabel {
  text:"observe.content.setLongline.number";
  labelFor:{number};
}

#number {
  property: {SetLonglineDto.PROPERTY_NUMBER};
  numberValue:{bean.getNumber()};
  _validatorLabel:{t("observe.content.setLongline.number")};
}

#settingStartTimeStamp {
  label:{t("observe.content.setLongline.settingStartTimeStamp")};
  date:{bean.getSettingStartTimeStamp()};
  propertyDate: {SetLonglineDto.PROPERTY_SETTING_START_TIME_STAMP};
  _validatorLabel:{t("observe.content.setLongline.settingStartTimeStamp")};
}

#settingStartCoordinatesLabel {
  text:"observe.content.setLongline.settingStartCoordinates";
}

#settingStartCoordinates {
  propertyLatitude:{SetLonglineDto.PROPERTY_SETTING_START_LATITUDE};
  propertyLongitude:{SetLonglineDto.PROPERTY_SETTING_START_LONGITUDE};
  propertyQuadrant:{SetLonglineDto.PROPERTY_SETTING_START_QUADRANT};
  latitude:{bean.getSettingStartLatitude()};
  longitude:{bean.getSettingStartLongitude()};
  quadrant:{bean.getSettingStartQuadrant()};
  _validatorLabel:{t("observe.content.setLongline.settingStartCoordinates")};
}

#settingEndTimeStamp {
  label:{t("observe.content.setLongline.settingEndTimeStamp")};
  date:{bean.getSettingEndTimeStamp()};
  propertyDate: {SetLonglineDto.PROPERTY_SETTING_END_TIME_STAMP};
  _validatorLabel:{t("observe.content.setLongline.settingEndTimeStamp")};
}

#settingEndCoordinatesLabel {
  text:"observe.content.setLongline.settingEndCoordinates";
}

#settingEndCoordinates {
  propertyLatitude:{SetLonglineDto.PROPERTY_SETTING_END_LATITUDE};
  propertyLongitude:{SetLonglineDto.PROPERTY_SETTING_END_LONGITUDE};
  propertyQuadrant:{SetLonglineDto.PROPERTY_SETTING_END_QUADRANT};
  latitude:{bean.getSettingEndLatitude()};
  longitude:{bean.getSettingEndLongitude()};
  quadrant:{bean.getSettingEndQuadrant()};
  _validatorLabel:{t("observe.content.setLongline.settingEndCoordinates")};
}

/* ***************************************************************************** */
/*  SETTING CARACTERISTICS TAB ************************************************* */
/* ***************************************************************************** */

#settingCaracteristicsTab {
  title:{t("observe.content.setLongline.tab.settingCaracteristics")};
  icon:{handler.getErrorIconIfFalse(model.isSettingCaracteristicsTabValid())};
}

#settingVesselSpeedLabel {
  text:"observe.content.setLongline.settingVesselSpeed";
  labelFor:{settingVesselSpeed};
}

#settingVesselSpeed {
  property:{SetLonglineDto.PROPERTY_SETTING_VESSEL_SPEED};
  numberValue:{bean.getSettingVesselSpeed()};
  _validatorLabel:{t("observe.content.setLongline.settingVesselSpeed")};
}

#shooterUsed {
  text:"observe.content.setLongline.shooterUsed";
  horizontalTextPosition:{JCheckBox.LEFT};
  _propertyName:{SetLonglineDto.PROPERTY_SHOOTER_USED};
  selected:{BooleanUtils.isTrue(bean.getShooterUsed())};
}

#shooterSpeedLabel {
  text:"observe.content.setLongline.shooterSpeed";
  labelFor:{shooterSpeed};
}

#shooterSpeed {
  enabled:{BooleanUtils.isTrue(bean.getShooterUsed())};
  property:{SetLonglineDto.PROPERTY_SHOOTER_SPEED};
  numberValue:{bean.getShooterSpeed()};
  _validatorLabel:{t("observe.content.setLongline.shooterSpeed")};
}

#settingShapeLabel {
  text:"observe.content.setLongline.settingShape";
  labelFor:{settingShape};
}

#settingShape {
  property:{SetLonglineDto.PROPERTY_SETTING_SHAPE};
  selectedItem:{bean.getSettingShape()};
  _validatorLabel:{t("observe.content.setLongline.settingShape")};
}

#weightedSwivel {
  horizontalTextPosition:{JCheckBox.LEFT};
  text:"observe.content.setLongline.weightedSwivel";
  _propertyName:{SetLonglineDto.PROPERTY_WEIGHTED_SWIVEL};
  selected:{BooleanUtils.isTrue(bean.getWeightedSwivel())};
  _validatorLabel:{t("observe.content.setLongline.weightedSwivel")};
}

#swivelWeightLabel {
  text:"observe.content.setLongline.swivelWeight";
  labelFor:{swivelWeight};
}

#swivelWeight {
  enabled:{BooleanUtils.isTrue(bean.getWeightedSwivel())};
  property:{SetLonglineDto.PROPERTY_SWIVEL_WEIGHT};
  numberValue:{bean.getSwivelWeight()};
  _validatorLabel:{t("observe.content.setLongline.swivelWeight")};
}

#weightedSnap {
  horizontalTextPosition:{JCheckBox.LEFT};
  text:"observe.content.setLongline.weightedSnap";
  _propertyName:{SetLonglineDto.PROPERTY_WEIGHTED_SNAP};
  selected:{BooleanUtils.isTrue(bean.getWeightedSnap())};
  _validatorLabel:{t("observe.content.setLongline.weightedSnap")};
}

#snapWeightLabel {
  text:"observe.content.setLongline.snapWeight";
  labelFor:{snapWeight};
}

#snapWeight {
  enabled:{BooleanUtils.isTrue(bean.getWeightedSnap())};
  property:{SetLonglineDto.PROPERTY_SNAP_WEIGHT};
  numberValue:{bean.getSnapWeight()};
  _validatorLabel:{t("observe.content.setLongline.snapWeight")};
}

#lineTypeLabel {
  text:"observe.content.setLongline.lineType";
  labelFor:{lineType};
}

#lineType {
  property:{SetLonglineDto.PROPERTY_LINE_TYPE};
  selectedItem:{bean.getLineType()};
  _validatorLabel:{t("observe.content.setLongline.lineType")};
}

#maxDepthTargetedLabel {
  text:"observe.content.setLongline.maxDepthTargeted";
  labelFor:{maxDepthTargeted};
}

#maxDepthTargeted {
  property:{SetLonglineDto.PROPERTY_MAX_DEPTH_TARGETED};
  numberValue:{bean.getMaxDepthTargeted()};
  _validatorLabel:{t("observe.content.setLongline.maxDepthTargeted")};
}

#monitored {
  horizontalTextPosition:{JCheckBox.LEFT};
  text:"observe.content.setLongline.monitored";
  _propertyName:{SetLonglineDto.PROPERTY_MONITORED};
  selected:{BooleanUtils.isTrue(bean.getMonitored())};
  _validatorLabel:{t("observe.content.setLongline.monitored")};
}

#lightsticksTypeLabel {
  text:"observe.content.setLongline.lightsticksType";
  labelFor:{lightsticksType};
}

#lightsticksType {
  property:{SetLonglineDto.PROPERTY_LIGHTSTICKS_TYPE};
  selectedItem:{bean.getLightsticksType()};
  enabled:{bean.getLightsticksPerBasketCount() !=null && bean.getLightsticksPerBasketCount() > 0};
  _validatorLabel:{t("observe.content.setLongline.lightsticksType")};
}

#lightsticksColorLabel {
  text:"observe.content.setLongline.lightsticksColor";
  labelFor:{lightsticksColor};
}

#lightsticksColor {
  property:{SetLonglineDto.PROPERTY_LIGHTSTICKS_COLOR};
  selectedItem:{bean.getLightsticksColor()};
  enabled:{bean.getLightsticksPerBasketCount() !=null && bean.getLightsticksPerBasketCount() > 0};
  _validatorLabel:{t("observe.content.setLongline.lightsticksColor")};
}

#timeBetweenHooksLabel {
  text:"observe.content.setLongline.timeBetweenHooks";
  labelFor:{timeBetweenHooks};
}

#timeBetweenHooks {
  property:{SetLonglineDto.PROPERTY_TIME_BETWEEN_HOOKS};
  numberValue:{bean.getTimeBetweenHooks()};
  _validatorLabel:{t("observe.content.setLongline.timeBetweenHooks")};
}

#lightsticksPerBasketCountLabel {
  text:"observe.content.setLongline.lightsticksPerBasketCount";
  labelFor:{lightsticksPerBasketCount};
}

#lightsticksPerBasketCount {
  property:{SetLonglineDto.PROPERTY_LIGHTSTICKS_PER_BASKET_COUNT};
  numberValue:{bean.getLightsticksPerBasketCount()};
  _validatorLabel:{t("observe.content.setLongline.lightsticksPerBasketCount")};
}

#basketsPerSectionCountLabel {
  text:"observe.content.setLongline.basketsPerSectionCount";
  labelFor:{basketsPerSectionCount};
}

#basketsPerSectionCount {
  property:{SetLonglineDto.PROPERTY_BASKETS_PER_SECTION_COUNT};
  numberValue:{bean.getBasketsPerSectionCount()};
  _validatorLabel:{t("observe.content.setLongline.basketsPerSectionCount")};
}

#totalBasketsCountLabel {
  text:"observe.content.setLongline.totalBasketsCount";
  labelFor:{totalBasketsCount};
}

#totalBasketsCount {
  property:{SetLonglineDto.PROPERTY_TOTAL_BASKETS_COUNT};
  numberValue:{bean.getTotalBasketsCount()};
  _validatorLabel:{t("observe.content.setLongline.totalBasketsCount")};
}

#branchlinesPerBasketCountLabel {
  text:"observe.content.setLongline.branchlinesPerBasketCount";
  labelFor:{branchlinesPerBasketCount};
}

#branchlinesPerBasketCount {
  property:{SetLonglineDto.PROPERTY_BRANCHLINES_PER_BASKET_COUNT};
  numberValue:{bean.getBranchlinesPerBasketCount()};
  _validatorLabel:{t("observe.content.setLongline.branchlinesPerBasketCount")};
}

#totalSectionsCountLabel {
  text:"observe.content.setLongline.totalSectionsCount";
  labelFor:{totalSectionsCount};
}

#totalSectionsCount {
  property:{SetLonglineDto.PROPERTY_TOTAL_SECTIONS_COUNT};
  numberValue:{bean.getTotalSectionsCount()};
  _validatorLabel:{t("observe.content.setLongline.totalSectionsCount")};
}

#totalHooksCountLabel {
  text:"observe.content.setLongline.totalHooksCount";
  labelFor:{totalHooksCount};
}

#totalHooksCount {
  property:{SetLonglineDto.PROPERTY_TOTAL_HOOKS_COUNT};
  numberValue:{bean.getTotalHooksCount()};
  _validatorLabel:{t("observe.content.setLongline.totalHooksCount")};
}

/* ***************************************************************************** */
/*  HAULING TAB **************************************************************** */
/* ***************************************************************************** */

#haulingTab {
  title:{t("observe.content.setLongline.tab.hauling")};
  icon:{handler.getErrorIconIfFalse(model.isHaulingTabValid())};
}

#haulingDirectionSameAsSettingLabel {
  text:"observe.content.setLongline.haulingDirectionSameAsSetting";
  labelFor:{haulingDirectionSameAsSetting};
}

#haulingDirectionSameAsSetting {
  booleanValue:{bean.getHaulingDirectionSameAsSetting()};
  _propertyName: {SetLonglineDto.PROPERTY_HAULING_DIRECTION_SAME_AS_SETTING};
  _validatorLabel:{t("observe.content.setLongline.haulingDirectionSameAsSetting")};
}

#haulingStartTimeStamp {
  label:{t("observe.content.setLongline.haulingStartTimeStamp")};
  date:{bean.getHaulingStartTimeStamp()};
  propertyDate: {SetLonglineDto.PROPERTY_HAULING_START_TIME_STAMP};
  _validatorLabel:{t("observe.content.setLongline.haulingStartTimeStamp")};
}

#haulingStartCoordinates {
  propertyLatitude:{SetLonglineDto.PROPERTY_HAULING_START_LATITUDE};
  propertyLongitude:{SetLonglineDto.PROPERTY_HAULING_START_LONGITUDE};
  propertyQuadrant:{SetLonglineDto.PROPERTY_HAULING_START_QUADRANT};
  latitude:{bean.getHaulingStartLatitude()};
  longitude:{bean.getHaulingStartLongitude()};
  quadrant:{bean.getHaulingStartQuadrant()};
  _validatorLabel:{t("observe.content.setLongline.haulingStartCoordinates")};
}

#haulingEndTimeStamp {
  label:{t("observe.content.setLongline.haulingEndTimeStamp")};
  date:{bean.getHaulingEndTimeStamp()};
  propertyDate: {SetLonglineDto.PROPERTY_HAULING_END_TIME_STAMP};
  _validatorLabel:{t("observe.content.setLongline.haulingEndTimeStamp")};
}

#haulingEndCoordinates {
  propertyLatitude:{SetLonglineDto.PROPERTY_HAULING_END_LATITUDE};
  propertyLongitude:{SetLonglineDto.PROPERTY_HAULING_END_LONGITUDE};
  propertyQuadrant:{SetLonglineDto.PROPERTY_HAULING_END_QUADRANT};
  latitude:{bean.getHaulingEndLatitude()};
  longitude:{bean.getHaulingEndLongitude()};
  quadrant:{bean.getHaulingEndQuadrant()};
  _validatorLabel:{t("observe.content.setLongline.haulingEndCoordinates")};
}

#haulingBreaksLabel {
  text:"observe.content.setLongline.haulingBreaks";
  labelFor:{haulingBreaks};
}

#haulingBreaks {
  property:{SetLonglineDto.PROPERTY_HAULING_BREAKS};
  numberValue:{bean.getHaulingBreaks()};
  _validatorLabel:{t("observe.content.setLongline.haulingBreaks")};
}

#delete {
  _globalAction:{DeleteDataGlobalUIAction.ACTION_NAME};
}

#save {
  _globalAction:{SaveDataGlobalUIAction.ACTION_NAME};
}

#reset {
  _globalAction:{ResetDataGlobalUIAction.ACTION_NAME};
}
