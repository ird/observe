/*
 * #%L
 * ObServe :: Swing
 * %%
 * Copyright (C) 2008 - 2010 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

#model {
  editable:true;
  modified:{validator.isChanged()};
  valid:{validator.isValid()};
}

#body {
  layout:{new BorderLayout()};
}

#extraActions {
  visible:{model.isUpdatingMode()};
}

#timeStamp {
  propertyDate:{ActivityLonglineDto.PROPERTY_TIME_STAMP};
  propertyDayDate:{ActivityLonglineUIModel.PROPERTY_DATE};
  propertyTimeDate:{ActivityLonglineUIModel.PROPERTY_TIME};
  label:{t("observe.storage.activityLongline.timeStamp")};
  date:{bean.getTimeStamp()};
}

#coordinatesEditor {
  propertyLatitude:{ActivityLonglineDto.PROPERTY_LATITUDE};
  propertyLongitude:{ActivityLonglineDto.PROPERTY_LONGITUDE};
  propertyQuadrant:{ActivityLonglineDto.PROPERTY_QUADRANT};
}

#vesselActivityLonglineLabel {
  text:"observe.storage.activityLongline.vesselActivityLongline";
  labelFor:{vesselActivityLongline};
}

#vesselActivityLongline {
  property:{ActivityLonglineDto.PROPERTY_VESSEL_ACTIVITY_LONGLINE};
  selectedItem:{bean.getVesselActivityLongline()};
  enabled:{!model.isUpdatingMode()};
}

#fpaZoneLabel {
  text:"observe.storage.activityLongline.fpaZone";
  labelFor:{fpaZone};
}

#fpaZone {
  property:{ActivityLonglineDto.PROPERTY_FPA_ZONE};
  selectedItem:{bean.getFpaZone()};
}

#seaSurfaceTemperatureLabel {
  text:"observe.storage.activityLongline.seaSurfaceTemperature";
  labelFor:{seaSurfaceTemperature};
}

#seaSurfaceTemperature {
  property:{ActivityLonglineDto.PROPERTY_SEA_SURFACE_TEMPERATURE};
  numberValue:{bean.getSeaSurfaceTemperature()};
}

#reopen {
  _globalAction:{"openCloseDataGlobal"};
  _toolTipText:{t("observe.content.action.reopen.activity.tip")};
}

#close {
  _globalAction:{"openCloseDataGlobal"};
  enabled:{!model.isModified() && (model.isHistoricalData() || model.isValid())};
  _toolTipText:{t("observe.action.close.activity.tip")};
}

#closeAndCreate {
  _globalAction:{"newNextGlobal"};
  enabled:{!model.isModified() && (model.isHistoricalData() || model.isValid())};
  _text:{t("observe.content.action.closeAndCreate.activity")};
  _toolTipText:{t("observe.content.action.closeAndCreate.activity.tip")};
}

#delete {
  _globalAction:{"deleteDataGlobal"};
  _toolTipText:{t("observe.action.delete.activity.tip")};
}

#addSet {
  actionIcon:add;
  text:"observe.action.add.setLongline";
  toolTipText:"observe.action.add.setLongline.tip";
  enabled:{!model.isModified() && model.isValid() && model.isSetOperation() && ! bean.isHasSetLongline()};
  _observeAction:{AddActivityLonglineSetUIAction.ACTION_NAME};

}
