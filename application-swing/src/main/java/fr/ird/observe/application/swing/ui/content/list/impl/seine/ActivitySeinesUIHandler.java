/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.content.list.impl.seine;

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.DataContext;
import fr.ird.observe.application.swing.db.constants.DataContextType;
import fr.ird.observe.application.swing.ui.content.ContentMode;
import fr.ird.observe.application.swing.ui.content.list.ContentListUIHandler;
import fr.ird.observe.application.swing.ui.tree.navigation.NavigationTree;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.NavigationTreeNodeSupport;
import fr.ird.observe.services.dto.reference.DataReference;
import fr.ird.observe.services.dto.seine.ActivitySeineDto;
import fr.ird.observe.services.dto.seine.RouteDto;
import org.nuiton.jaxx.runtime.spi.UIHandler;

import static org.nuiton.i18n.I18n.n;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class ActivitySeinesUIHandler extends ContentListUIHandler<RouteDto, ActivitySeineDto, ActivitySeinesUI> implements UIHandler<ActivitySeinesUI> {

    public ActivitySeinesUIHandler() {
        super(DataContextType.Route, DataContextType.ActivitySeine);
    }

    @Override
    protected boolean computeCanGotoOpen() {
        // il faut que l'activité ouverte soit dans cette route
        boolean result = ObserveSwingApplicationContext.get().getDataContext().isOpenActivitySeine();
        if (result) {

            String id = ObserveSwingApplicationContext.get().getDataContext().getOpenActivitySeineId();
            result = false;
            for (DataReference<ActivitySeineDto> reference : ui.getModel().getData()) {
                if (id.equals(reference.getId())) {
                    result = true;
                    break;
                }
            }
        }
        return result;
    }

    @Override
    protected boolean computeCanClose() {
        return computeCanGotoOpen();
    }

    @Override
    public NavigationTreeNodeSupport<?> getOpenNode() {
        DataContext dataContext = ObserveSwingApplicationContext.get().getDataContext();
        NavigationTree treeHelper = getTreeHelper(ui);
        return treeHelper.getActivitySeineNode(dataContext.getOpenProgramId(), dataContext.getOpenTripSeineId(), dataContext.getOpenRouteId(), dataContext.getOpenActivitySeineId());
    }

    @Override
    protected ContentMode getContentMode(DataContext dataContext) {

        String openRouteId = dataContext.getOpenRouteId();

        if (openRouteId == null) {

            // pas de route ouverte, donc on ne peut pas ouvrir une activité
            addInfoMessage(n("observe.content.route.message.no.active.found"));
            return ContentMode.READ;
        }

        //
        // il existe une route ouverte
        //

        boolean openActivity = dataContext.isOpenActivity();


        if (dataContext.isSelectedOpen(RouteDto.class)) {

            //
            // la route courante est ouverte
            //

            // la route courante est ouverte
            if (openActivity) {

                // il existe une activité ouverte dans la route courante
                addInfoMessage(n("observe.storage.activitySeine.message.active.found"));
                return ContentMode.UPDATE;
            }

            // pas d'activité ouverte, on peut en ouvrir une
            addInfoMessage(n("observe.storage.activitySeine.message.no.active.found"));
            return ContentMode.CREATE;
        }

        //
        // la route ouverte est dans une autre maree
        //

        if (openActivity) {

            // il existe une activité ouverte dans la route ouverte
            addInfoMessage(n("observe.storage.activitySeine.message.active.found.for.other.route"));
        } else {

            // il n'existe pas d'activité ouverte dans la route ouverte
            addInfoMessage(n("observe.storage.activitySeine.message.no.active.found.for.other.route"));
        }

        return ContentMode.READ;

    }

}
