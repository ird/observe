package fr.ird.observe.application.swing;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.jaxx.runtime.swing.session.SwingSession;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.awt.Component;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;

/**
 * Created on 07/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class ObserveSwingSessionHelper implements Closeable {

    /** Logger */
    private static final Log log = LogFactory.getLog(ObserveSwingSessionHelper.class);

    private SwingSession session;

    public ObserveSwingSessionHelper(File file) {
        session = new SwingSession(file, false);
    }

    public void addComponent(Component c, boolean replace) {
        session.add(c, replace);
    }

    public void removeComponent(Component c) {
        session.remove(c);
    }

    public void save() {
        try {
            session.save();
        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Could not save swing session", e);
            }
        }
    }

    @Override
    public void close() {
        save();
    }

}
