/*
 * #%L
 * ObServe :: Swing
 * %%
 * Copyright (C) 2008 - 2010 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

JMenuBar {
  opaque:true;
  borderPainted:false;
  margin:{new Insets(0, 0, 0, 0)};
  border: {null};
}

JMenu {
  opaque:true;
  borderPainted:false;
  border: {null};
  margin:{new Insets(0, 0, 0, 0)};
}

#configContent {
  layout:{configLayout};
}

#IMPORT_EXTERNAL_DUMP {
  _description:{n("observe.storage.importExternalDump.description")};
}

#dumpFile {
  text:{model.getDumpFile() + ""};
}

#fileChooserAction {
  actionIcon:"fileChooser";
}

#IMPORT_REMOTE_STORAGE {
  _description:{n("observe.storage.importRemoteStorage.description")};
}

#IMPORT_REMOTE_STORAGE_content {
  layout:{new BorderLayout()};
}

#IMPORT_SERVER_STORAGE {
  _description:{n("observe.storage.importServerStorage.description")};
}

#IMPORT_SERVER_STORAGE_content {
  layout:{new BorderLayout()};
}

#USE_REMOTE {
  _description:{n("observe.storage.useRemoteStorage.description")};
}

#USE_REMOTE_content {
  layout:{new BorderLayout()};
}

#USE_SERVER {
  _description:{n("observe.storage.useServerStorage.description")};
}

#USE_SERVER_content {
  layout:{new BorderLayout()};
}

#remoteConfig {
  layout:{new BorderLayout()};
}

#remoteUrl {
  text:{model.getRemoteUrl()};
}

#remoteLogin {
  text:{model.getRemoteLogin()};
}

#remotePassword {
  text:{model.getRemotePassword()==null ? "" : new String(model.getRemotePassword())};
}

#serverDataBaseLabel {
  visible: {model.isEditServerConfig()};
}

#serverDataBase {
  visible: {model.isEditServerConfig()};
  text:{SwingUtil.getStringValue(model.getServerDatabase())};
}

#remoteUseSsl {
  visible: {model.isEditRemoteConfig()};
  text:"observe.storage.remote.useSll";
  selected:{model.isUseSsl()};
}

#testRemoteAction {
  text:"observe.action.test.remote";
  toolTipText:"observe.action.test.remote.tip";
  actionIcon:"connect_creating";
  mnemonic:V;
  enabled:{model.getConnexionStatus() != ConnexionStatus.SUCCESS};
}

#connexionStatusPanel {
  columnHeaderView:{connexionStatusLabel};
  border:{new LineBorder(handler.updateConnexionStatutColor(model.getConnexionStatus()),2)};
}

#connexionStatusLabel {
  text:{I18nEnumHelper.getLabel(model.getConnexionStatus())};
  icon:{handler.updateConnexionStatutIcon(model.getConnexionStatus())};
}

#connexionStatus {
  height : 70;
  focusable:false;
  font-size:11;
  contentType : "text/html";
  editorKit : {new HTMLEditorKit()};
  text:{handler.updateConnexionStatutText(model.getConnexionStatus())};
  _untestedColor:{Color.GRAY};
  _successColor:{Color.GREEN};
  _failedColor:{Color.RED};
  _untestedIcon:{SwingUtil.getUIManagerActionIcon("connect_untested")};
  _successIcon:{SwingUtil.getUIManagerActionIcon("connect_ok")};
  _failedIcon:{SwingUtil.getUIManagerActionIcon("connect_no")};
}

#serverMenu {
  text:"observe.storage.serverConfiguration.presets";
  actionIcon:db-server;
  mnemonic:S;
}

#remoteMenu {
  text:"observe.storage.remoteConfiguration.presets";
  actionIcon:db-remote;
  mnemonic:R;
}

#saveRemoteConfiguration {
  _observeAction:{SaveCurrentRemoteConfigurationUIAction.ACTION_NAME};
}

#saveServerConfiguration {
  _observeAction:{SaveCurrentServerConfigurationUIAction.ACTION_NAME};
}