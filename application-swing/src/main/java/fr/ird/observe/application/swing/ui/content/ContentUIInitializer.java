package fr.ird.observe.application.swing.ui.content;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import fr.ird.observe.application.swing.I18nEnumHelper;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.decoration.DecoratorService;
import fr.ird.observe.application.swing.decoration.ObserveI18nDecoratorHelper;
import fr.ird.observe.application.swing.decoration.decorators.DataReferenceDecorator;
import fr.ird.observe.application.swing.decoration.decorators.ReferentialReferenceDecorator;
import fr.ird.observe.application.swing.ui.ObserveKeyStrokes;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.actions.AbstractUIAction;
import fr.ird.observe.application.swing.ui.actions.content.GoToSubTab1UIAction;
import fr.ird.observe.application.swing.ui.actions.content.GoToSubTab2UIAction;
import fr.ird.observe.application.swing.ui.actions.content.GoToSubTab3UIAction;
import fr.ird.observe.application.swing.ui.actions.content.GoToTab1UIAction;
import fr.ird.observe.application.swing.ui.actions.content.GoToTab2UIAction;
import fr.ird.observe.application.swing.ui.actions.content.GoToTab3UIAction;
import fr.ird.observe.application.swing.ui.actions.content.GoToTab4UIAction;
import fr.ird.observe.application.swing.ui.actions.content.GoToTab5UIAction;
import fr.ird.observe.application.swing.ui.actions.content.GoToTab6UIAction;
import fr.ird.observe.application.swing.ui.actions.content.GoToTabUIActionSupport;
import fr.ird.observe.application.swing.ui.util.BooleanEditor;
import fr.ird.observe.application.swing.ui.util.tripMap.ObserveMapPane;
import fr.ird.observe.application.swing.validation.ObserveSwingValidator;
import fr.ird.observe.services.dto.DataDto;
import fr.ird.observe.services.dto.IdDto;
import fr.ird.observe.services.dto.reference.DataReference;
import fr.ird.observe.services.dto.reference.ReferentialReference;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.JXMonthView;
import org.jdesktop.swingx.autocomplete.ComboBoxCellEditor;
import org.jdesktop.swingx.autocomplete.ObjectToStringConverter;
import org.nuiton.decorator.Decorator;
import org.nuiton.jaxx.runtime.swing.editor.EnumEditor;
import org.nuiton.jaxx.runtime.swing.renderer.DecoratorListCellRenderer;
import org.nuiton.jaxx.runtime.swing.renderer.EnumEditorRenderer;
import org.nuiton.jaxx.validator.JAXXValidator;
import org.nuiton.jaxx.validator.swing.SwingValidatorUtil;
import org.nuiton.jaxx.widgets.datetime.DateTimeEditor;
import org.nuiton.jaxx.widgets.datetime.TimeEditor;
import org.nuiton.jaxx.widgets.gis.absolute.CoordinatesEditor;
import org.nuiton.jaxx.widgets.number.NumberCellEditor;
import org.nuiton.jaxx.widgets.number.NumberEditor;
import org.nuiton.jaxx.widgets.select.BeanComboBox;
import org.nuiton.jaxx.widgets.select.BeanListHeader;
import org.nuiton.jaxx.widgets.select.BeanUIUtil;
import org.nuiton.jaxx.widgets.select.FilterableDoubleList;
import org.nuiton.util.DateUtil;

import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.InputMap;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollBar;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.ListCellRenderer;
import javax.swing.SwingUtilities;
import javax.swing.border.LineBorder;
import javax.swing.table.TableCellEditor;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * To initialize ui.
 *
 * Created on 9/26/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.7
 */
public class ContentUIInitializer<E extends IdDto, UI extends ContentUI<E, UI>> {

    public static final String OBSERVE_ACTION = "observeAction";

    public static final String GLOBAL_ACTION = "globalAction";

    public static final String CLIENT_PROPERTY_PROPERTY_NAME = "propertyName";

    public static final String CLIENT_PROPERTY_RESET_PROPERTY_NAME = "resetPropertyName";

    public static final String CLIENT_PROPERTY_NOT_BLOCKING = "notBlocking";

    /** Logger. */
    private static final Log log = LogFactory.getLog(ContentUIInitializer.class);

    protected final UI ui;

    protected final DecoratorService decoratorService;

    public ContentUIInitializer(UI ui) {
        this.ui = ui;
        this.decoratorService = ObserveSwingApplicationContext.get().getDecoratorService();
    }

    public static TableCellEditor newFloatColumnEditor() {

        NumberCellEditor<Float> editor = new NumberCellEditor<>(Float.class, false);
        editor.getNumberEditor().setSelectAllTextOnError(true);
        editor.getNumberEditor().getTextField().setBorder(new LineBorder(Color.GRAY, 2));
        editor.getNumberEditor().setNumberPattern("\\d{0,6}(\\.\\d{0,4})?");
        return editor;

    }

    public static TableCellEditor newIntegerColumnEditor() {

        NumberCellEditor<Integer> editor = new NumberCellEditor<>(Integer.class, false);
        editor.getNumberEditor().setSelectAllTextOnError(true);
        editor.getNumberEditor().getTextField().setBorder(new LineBorder(Color.GRAY, 2));
        editor.getNumberEditor().setNumberPattern("\\d{0,4}?");
        return editor;

    }

    public static <B> ComboBoxCellEditor newDataColumnEditor(List<B> data, Decorator<B> decorator) {

        JComboBox<B> comboBox = new JComboBox<>();

        return newDataColumnEditor(comboBox, data, decorator);

    }

    public static <B> ComboBoxCellEditor newDataColumnEditor(JComboBox<B> comboBox, List<B> data, Decorator<B> decorator) {

        comboBox.setRenderer(new DecoratorListCellRenderer<>(decorator));

        prepareComboBoxData(comboBox, data);

        ObjectToStringConverter converter = BeanUIUtil.newDecoratedObjectToStringConverter(decorator);
        BeanUIUtil.decorate(comboBox, converter);
        return new ComboBoxCellEditor(comboBox);

    }

    public static <B> void prepareComboBoxData(JComboBox comboBox, List<B> data) {
        List<B> dataToList = Lists.newArrayList(data);

        // add a null value at first position
        if (!dataToList.isEmpty() && dataToList.get(0) != null) {
            dataToList.add(0, null);
        }
        UIHelper.fillComboBox(comboBox, dataToList, null);
    }

    protected E getBean() {

        return ui.getModel().getBean();

    }

    public void initUI() {

        if (log.isDebugEnabled()) {
            log.debug("ui " + getClass());
        }

        ObserveActionMap actionMap = ObserveSwingApplicationContext.get().getActionMap();
        InputMap inputMap = ObserveSwingApplicationContext.get().getMainUI().getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        // initialisation des éditeurs

        Set<String> doNotBlockComponentIds = new HashSet<>();

        ui.getActionMap().setParent(actionMap);
        ui.setInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW, inputMap);

        for (String name : ui.get$objectMap().keySet()) {
            Object o = ui.getObjectById(name);

            if (o == null) {
                continue;
            }

            if (o instanceof JComponent) {

                init((JComponent) o, doNotBlockComponentIds);
            }

            if (o instanceof JCheckBox) {
                init((JCheckBox) o);
            }

            if (o instanceof AbstractButton) {
                init(actionMap, (AbstractButton) o);
                continue;
            }

            if (o instanceof NumberEditor) {
                init((NumberEditor) o);
                continue;
            }

            if (o instanceof BeanComboBox<?>) {
                init((BeanComboBox<?>) o);
                continue;
            }

            if (o instanceof BeanListHeader<?>) {
                init((BeanListHeader) o);
                continue;
            }

            if (o instanceof FilterableDoubleList<?>) {
                init((FilterableDoubleList<?>) o);
                continue;
            }

            if (o instanceof JXDatePicker) {
                init((JXDatePicker) o);
                continue;
            }

            if (o instanceof TimeEditor) {
                init((TimeEditor) o);
                continue;
            }

            if (o instanceof DateTimeEditor) {
                init((DateTimeEditor) o);
                continue;
            }

            if (o instanceof CoordinatesEditor) {
                init((CoordinatesEditor) o);
                continue;
            }

            if (o instanceof BooleanEditor) {
                init((BooleanEditor) o);
                continue;
            }

            if (o instanceof EnumEditor) {
                init((EnumEditor<?>) o);
                continue;
            }

            if (o instanceof ObserveSwingValidator<?>) {
                init(ui, (ObserveSwingValidator<?>) o);
                continue;
            }

            if (o instanceof JTextField) {
                init((JTextField) o);
                continue;
            }

            if (o instanceof JTextArea) {
                init((JTextArea) o);
                continue;
            }
            if (o instanceof JTabbedPane) {
                init((JTabbedPane) o);
            }

        }

        if (!doNotBlockComponentIds.isEmpty()) {

            String[] acceptedComponentNames = doNotBlockComponentIds.toArray(new String[doNotBlockComponentIds.size()]);

            initBlockLayerUI(acceptedComponentNames);

        }
    }

    protected void initBlockLayerUI(String... doNotBlockComponentIds) {
        ui.getBlockLayerUI().setAcceptedComponentTypes(ObserveMapPane.class, JScrollBar.class);
        ui.getBlockLayerUI().setAcceptedComponentNames(doNotBlockComponentIds);

    }

    protected void init(UI ui, ObserveSwingValidator<?> validator) {
        SwingValidatorUtil.listenValidatorContextNameAndRefreshFields(validator, (JAXXValidator) ui);
    }

    protected void init(ObserveActionMap actionMap, AbstractButton editor) {
        String actionId = (String) editor.getClientProperty(OBSERVE_ACTION);
        if (actionId == null) {
            // le boutton n'est pas commun

            final String propertyName = (String) editor.getClientProperty(CLIENT_PROPERTY_RESET_PROPERTY_NAME);
            if (propertyName != null) {
                editor.addActionListener(e -> UIHelper.setProperty(getBean(), propertyName, null));
            }

            return;
        }

        // on a trouve une action commune
        AbstractUIAction action = (AbstractUIAction) actionMap.get(actionId);
        Objects.requireNonNull(action, "action [" + actionId + "] not found for ui " + ui.getClass().getName());

        if (log.isDebugEnabled()) {
            log.debug("init common action " + actionId);
        }

        action.initAction(ui, editor);

        actionId = (String) editor.getClientProperty(GLOBAL_ACTION);
        if (actionId != null) {
            action = (AbstractUIAction) actionMap.get(actionId);
            Objects.requireNonNull(action, "action [" + actionId + "] not found for ui " + ui.getClass().getName());
            KeyStroke acceleratorKey = action.getAcceleratorKey();
            ObserveKeyStrokes.addKeyStroke(editor, acceleratorKey);
        }

    }

    protected void init(NumberEditor editor) {
        if (log.isDebugEnabled()) {
            log.debug("init number editor " + editor.getName());
        }
        editor.init();
    }

    protected void init(BeanComboBox beanComboBox) {
        if (log.isDebugEnabled()) {
            log.debug("init combobox for " + beanComboBox.getBeanType());
        }
        beanComboBox.setI18nPrefix("observe.common.");
        beanComboBox.setMinimumSize(new Dimension(0, 24));
        beanComboBox.getResetButton().setToolTipText(beanComboBox.getResetButton().getToolTipText() + " (ctrl + D)");
        beanComboBox.getChangeDecorator().setToolTipText(beanComboBox.getChangeDecorator().getToolTipText() + " (ctrl + F)");
        JComponent editorComponent = (JComponent) beanComboBox.getCombobox().getEditor().getEditorComponent();

        Action resetAction = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                beanComboBox.doActionPerformed__on__resetButton(null);
                SwingUtilities.invokeLater(beanComboBox::grabFocus);
            }
        };

        editorComponent.getInputMap(JComponent.WHEN_FOCUSED).put(ObserveKeyStrokes.KEY_STROKE_RESET, "resetAction");
        editorComponent.getActionMap().put("resetAction", resetAction);

        Action showPopupAction = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!beanComboBox.getCombobox().isPopupVisible()) {
                    beanComboBox.getCombobox().showPopup();
                } else {
                    beanComboBox.setSelectedItem(beanComboBox.getCombobox().getEditor().getItem());
                    beanComboBox.getCombobox().hidePopup();
                }
            }
        };
        editorComponent.getInputMap(JComponent.WHEN_FOCUSED).put(ObserveKeyStrokes.KEY_STROKE_ENTER, "showPopupAction");
        editorComponent.getActionMap().put("showPopupAction", showPopupAction);

        Action showDecoratorPopupAction = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                beanComboBox.getPopup().show(beanComboBox, 0, 0);
            }
        };
        editorComponent.getInputMap(JComponent.WHEN_FOCUSED).put(ObserveKeyStrokes.KEY_STROKE_SHOW_DECORATOR_POPUP, "showDecoratorPopupAction");
        editorComponent.getActionMap().put("showDecoratorPopupAction", showDecoratorPopupAction);

        Class dtoClass = getDtoClass(beanComboBox);

        if (ReferentialDto.class.isAssignableFrom(dtoClass)) {
            prepareReferentialBeanComboBox(dtoClass, beanComboBox);
        } else {
            prepareDataBeanComboBox(dtoClass, beanComboBox);
        }

    }

    protected void init(BeanListHeader beanList) {

        beanList.setI18nPrefix("observe.common.");

        if (log.isInfoEnabled()) {
            log.info("init list for " + beanList.getBeanType());
        }

        Class dtoClass = getDtoClass(beanList);

        if (ReferentialDto.class.isAssignableFrom(dtoClass)) {
            prepareReferentialEntityList(dtoClass, beanList);
        } else {
            prepareDataEntityList(dtoClass, beanList);
        }

    }

    protected void init(FilterableDoubleList beanList) {

        beanList.setI18nPrefix("observe.common.");
        beanList.getAddButton().setFocusable(false);
        beanList.getRemoveButton().setFocusable(false);

        if (log.isInfoEnabled()) {
            log.info("init list for " + beanList.getBeanType());
        }

        Class dtoClass = getDtoClass(beanList);

        if (ReferentialDto.class.isAssignableFrom(dtoClass)) {
            prepareReferentialFilterableDoubleList(dtoClass, beanList);
        } else {
            prepareDataFilterableDoubleList(dtoClass, beanList);
        }

    }

    protected void init(TimeEditor editor) {
        if (log.isDebugEnabled()) {
            log.debug("init time editor " + editor.getName());
        }
        editor.init();
        if (isAutoSelectOnFocus(editor)) {

            addAutoSelectOnFocus((JSpinner.DateEditor) editor.getHourEditor().getEditor());
            addAutoSelectOnFocus((JSpinner.DateEditor) editor.getMinuteEditor().getEditor());

        }
    }

    protected void init(DateTimeEditor editor) {
        if (log.isDebugEnabled()) {
            log.debug("init date time editor " + editor.getName());
        }
        editor.init();
        editor.getSliderHidor().setFocusable(false);
        if (isAutoSelectOnFocus(editor)) {

            addAutoSelectOnFocus((JSpinner.DateEditor) editor.getHourEditor().getEditor());
            addAutoSelectOnFocus((JSpinner.DateEditor) editor.getMinuteEditor().getEditor());
            addAutoSelectOnFocus(editor.getDayDateEditor().getEditor());
            editor.getDayDateEditor().getEditor().addFocusListener(new FocusAdapter() {

                private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");

                @Override
                public void focusLost(FocusEvent e) {

                    JFormattedTextField source = (JFormattedTextField) e.getSource();
                    if (source.isEditValid()) {
                        try {
                            Date newDate = simpleDateFormat.parse(editor.getDayDateEditor().getEditor().getText());
                            Object oldValue = UIHelper.getProperty(editor.getModel().getBean(), editor.getModel().getPropertyDayDate());
                            if (!Objects.equals(oldValue, newDate)) {
                                editor.getDayDateEditor().commitEdit();
                                if (log.isInfoEnabled()) {
                                    log.info("quit date editor, commit value: " + source.getValue());
                                }
                            }
                        } catch (ParseException e1) {
                            // l'édition est valide donc pas de problème ici
                        }
                    }
                    super.focusLost(e);
                }
            });

        }
    }

    private void addAutoSelectOnFocus(JSpinner.DateEditor hourEditor) {
        addAutoSelectOnFocus(hourEditor.getTextField());
    }

    protected void init(CoordinatesEditor editor) {
        if (log.isDebugEnabled()) {
            log.debug("init coordinates editor " + editor.getName());
        }
        editor.setFormat(ObserveSwingApplicationContext.get().getConfig().getCoordinateFormat());
        editor.init();
        editor.getDmdFormat().setFocusable(false);
        editor.getDmsFormat().setFocusable(false);
        editor.getDdFormat().setFocusable(false);

    }

    protected void init(JTextField editor) {

        final String propertyName = (String) editor.getClientProperty(CLIENT_PROPERTY_PROPERTY_NAME);
        if (propertyName != null) {
            editor.addKeyListener(new KeyAdapter() {

                @Override
                public void keyReleased(KeyEvent e) {
                    JTextField source = (JTextField) e.getSource();
                    String text = source.getText();
                    text = text.isEmpty() ? null : text;
                    if (text == null && UIHelper.getProperty(getBean(), propertyName) == null) {
                        // On n'envoie pas la demande, car cela va modifier ensuite le formulaire
                        // car avec oldValue=null et newValue=null, les pcs sont déclanchés...
                        return;
                    }
                    UIHelper.setProperty(getBean(), propertyName, text);
                }
            });
        }
    }

    protected void init(JTextArea editor) {

        final String propertyName = (String) editor.getClientProperty(CLIENT_PROPERTY_PROPERTY_NAME);
        if (propertyName != null) {
            editor.addKeyListener(new KeyAdapter() {

                @Override
                public void keyReleased(KeyEvent e) {
                    JTextArea source = (JTextArea) e.getSource();
                    String text = source.getText();
                    UIHelper.setProperty(getBean(), propertyName, text);
                }
            });
        }
    }

    protected void init(JTabbedPane tabbedPane) {
        boolean mainTabbedPane = GoToTabUIActionSupport.MAIN_TABBED_PANE.equals(tabbedPane.getName());
        boolean subTabbedPane = GoToTabUIActionSupport.SUB_TABBED_PANE.equals(tabbedPane.getName());
        if (!mainTabbedPane && !subTabbedPane) {
            return;
        }
        int tabCount = tabbedPane.getTabCount();
        InputMap inputMap = ui.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        if (mainTabbedPane) {
            ObserveKeyStrokes.addKeyStrokeToMainTabbedPane(tabbedPane);
            if (tabCount > 0) {
                inputMap.put(ObserveKeyStrokes.KEY_STROKE_GO_TAB_1, GoToTab1UIAction.ACTION_NAME);
            }
            if (tabCount > 1) {
                inputMap.put(ObserveKeyStrokes.KEY_STROKE_GO_TAB_2, GoToTab2UIAction.ACTION_NAME);
            }
            if (tabCount > 2) {
                inputMap.put(ObserveKeyStrokes.KEY_STROKE_GO_TAB_3, GoToTab3UIAction.ACTION_NAME);
            }
            if (tabCount > 3) {
                inputMap.put(ObserveKeyStrokes.KEY_STROKE_GO_TAB_4, GoToTab4UIAction.ACTION_NAME);
            }
            if (tabCount > 4) {
                inputMap.put(ObserveKeyStrokes.KEY_STROKE_GO_TAB_5, GoToTab5UIAction.ACTION_NAME);
            }
            if (tabCount > 5) {
                inputMap.put(ObserveKeyStrokes.KEY_STROKE_GO_TAB_6, GoToTab6UIAction.ACTION_NAME);
            }
        }
        if (subTabbedPane) {
            ObserveKeyStrokes.addKeyStrokeToSubTabbedPane(tabbedPane);
            if (tabCount > 0) {
                inputMap.put(ObserveKeyStrokes.KEY_STROKE_GO_SUB_TAB_1, GoToSubTab1UIAction.ACTION_NAME);
            }
            if (tabCount > 1) {
                inputMap.put(ObserveKeyStrokes.KEY_STROKE_GO_SUB_TAB_2, GoToSubTab2UIAction.ACTION_NAME);
            }
            if (tabCount > 2) {
                inputMap.put(ObserveKeyStrokes.KEY_STROKE_GO_SUB_TAB_3, GoToSubTab3UIAction.ACTION_NAME);
            }
        }

    }

    protected void init(BooleanEditor editor) {
        if (log.isDebugEnabled()) {
            log.debug("init boolean editor " + editor.getName());
        }
        final String propertyName = (String) editor.getClientProperty(CLIENT_PROPERTY_PROPERTY_NAME);
        if (propertyName != null) {
            editor.addItemListener(event -> {
                if (event.getStateChange() == ItemEvent.SELECTED) {

                    Boolean newValue = ((BooleanEditor) event.getSource()).getBooleanValue();
                    UIHelper.setProperty(getBean(), propertyName, newValue);
                }
            });
        }
    }

    protected void init(JComponent editor, Set<String> notBlockingComponents) {
        final Boolean propertyName = (Boolean) editor.getClientProperty(CLIENT_PROPERTY_NOT_BLOCKING);
        if (propertyName != null) {

            if (editor instanceof FilterableDoubleList) {

                notBlockingComponents.add("universeListPane");
                notBlockingComponents.add("selectedListPane");

            } else {

                notBlockingComponents.add(editor.getName());

            }
        }
    }

    protected void init(JCheckBox editor) {
        if (log.isDebugEnabled()) {
            log.debug("init simple boolean editor " + editor.getName());
        }
        final String propertyName = (String) editor.getClientProperty(CLIENT_PROPERTY_PROPERTY_NAME);
        if (propertyName != null) {
            editor.addItemListener(event -> {
                Boolean newValue = ((JCheckBox) event.getSource()).isSelected();
                UIHelper.setProperty(getBean(), propertyName, newValue);
            });
        }
    }

    protected void init(JXDatePicker picker) {

        if (log.isDebugEnabled()) {
            log.debug("init JXDatePicker editor" + picker.getName());
        }picker.setLocale(ObserveSwingApplicationContext.get().getConfig().getLocale());
        JFormattedTextField editor = picker.getEditor();
        editor.setEditable(true);
        JXMonthView monthView = new JXMonthView();
        monthView.setLowerBound(DateUtil.createDate(1, 1, 1970));
        monthView.setTraversable(true);
        monthView.setZoomable(true);
        picker.setMonthView(monthView);

        final String propertyName = (String) picker.getClientProperty(CLIENT_PROPERTY_PROPERTY_NAME);
        if (propertyName != null) {
            picker.addActionListener(e -> {
                JXDatePicker source = (JXDatePicker) e.getSource();
                Date date = source.getDate();
                if (Objects.equals(date, UIHelper.getProperty(getBean(), propertyName))) {
                    // On n'envoie pas la demande, car cela va modifier ensuite le formulaire
                    // car avec oldValue=null et newValue=null, les pcs sont déclanchés...
                    return;
                }
                UIHelper.setProperty(getBean(), propertyName, date);
            });
        }

        addAutoSelectOnFocus(editor);
        editor.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {

                JFormattedTextField source = (JFormattedTextField) e.getSource();
                if (source.isEditValid()) {
                    try {
                        picker.commitEdit();
                        if (log.isInfoEnabled()) {
                            log.info("quit date editor, commit value: " + source.getValue());
                        }
                    } catch (ParseException e1) {
                        // l'édition est valide donc pas de problème ici
                    }
                }
                super.focusLost(e);
            }
        });
    }

    protected <B extends Enum<B>> void init(EnumEditor<B> editor) {
        if (log.isDebugEnabled()) {
            log.debug("init enumEditor editor " + editor.getName());
        }
        ImmutableMap.Builder<B, String> labelsBuilder = ImmutableMap.builder();
        for (B e : EnumSet.allOf(editor.getType())) {
            String label = I18nEnumHelper.getLabel(e);
            labelsBuilder.put(e, label);
        }
        editor.setRenderer(new EnumEditorRenderer<>(labelsBuilder.build()));
    }

    protected boolean isAutoSelectOnFocus(JComponent comp) {
        Boolean selectOnFocus = (Boolean) comp.getClientProperty("selectOnFocus");
        return BooleanUtils.isTrue(selectOnFocus);
    }

    protected void addAutoSelectOnFocus(JTextField jTextField) {
        jTextField.addFocusListener(new FocusAdapter() {

            @Override
            public void focusGained(final FocusEvent e) {
                SwingUtilities.invokeLater(() -> {
                    JTextField source = (JTextField) e.getSource();
                    source.selectAll();
                });

            }
        });
    }

    /**
     * Remplit le modèle d'une liste graphique avec la liste des entités d'un
     * type donné sur un service de persistance donné.
     *
     * @param list le component graphique à initialiser
     * @since 1.5
     */
    protected <D extends DataDto> void prepareDataFilterableDoubleList(Class<D> dtoClass, FilterableDoubleList<DataReference<D>> list) {

        DataReferenceDecorator<D> decorator = decoratorService.getDataReferenceDecorator(dtoClass);
        if (log.isDebugEnabled()) {
            log.debug("Will use decorator " + decorator);
        }

        String entityLabel = t(ObserveI18nDecoratorHelper.getTypeI18nKey(dtoClass));
        list.getPopupSortLabel().setText(t("observe.content.type.data", entityLabel));

        list.putClientProperty("decorator", decorator);

        list.init(decorator, new ArrayList<>(), new ArrayList<>());

        JList<DataReference<D>> selectedList = list.getSelectedList();
        ListCellRenderer<? super DataReference<D>> renderer = selectedList.getCellRenderer();

        selectedList.setCellRenderer(new DataReferenceListCellRenderer<>(renderer));
        list.getUniverseList().setCellRenderer(new DataReferenceListCellRenderer<>(renderer));

    }

    /**
     * Remplit le modèle d'une liste graphique avec la liste des entités d'un
     * type donné sur un service de persistance donné.
     *
     * @param list le component graphique à initialiser
     * @since 1.5
     */
    protected <D extends ReferentialDto> void prepareReferentialFilterableDoubleList(Class<D> dtoClass, FilterableDoubleList<ReferentialReference<D>> list) {

        ReferentialReferenceDecorator<D> decorator = decoratorService.getReferentialReferenceDecorator(dtoClass);
        if (log.isDebugEnabled()) {
            log.debug("Will use decorator " + decorator);
        }

        String entityLabel = t(ObserveI18nDecoratorHelper.getTypeI18nKey(dtoClass));
        list.getPopupSortLabel().setText(t("observe.content.type.referential", entityLabel));

        list.putClientProperty("decorator", decorator);

        list.init(decorator, new ArrayList<>(), new ArrayList<>());

        JList<ReferentialReference<D>> selectedList = list.getSelectedList();
        ListCellRenderer<? super ReferentialReference<D>> renderer = selectedList.getCellRenderer();

        selectedList.setCellRenderer(new ReferentialReferenceListCellRenderer<>(renderer));
        list.getUniverseList().setCellRenderer(new ReferentialReferenceListCellRenderer<>(renderer));

    }

    /**
     * Remplit le modèle d'une liste graphique avec la liste des entités d'un
     * type donné sur un service de persistance donné.
     *
     * @param list le component graphique à initialiser
     * @since 1.5
     */
    protected <D extends DataDto> void prepareDataEntityList(Class<D> dtoClass, BeanListHeader<DataReference<D>> list) {

        DataReferenceDecorator<D> decorator = decoratorService.getDataReferenceDecorator(dtoClass);
        if (log.isDebugEnabled()) {
            log.debug("Will use decorator " + decorator);
        }

        String entityLabel = t(ObserveI18nDecoratorHelper.getTypeI18nKey(dtoClass));
        list.setPopupTitleText(t("observe.content.type.data", entityLabel));

        list.putClientProperty("decorator", decorator);

        list.init(decorator, Collections.emptyList());

        JList<DataReference<D>> list1 = list.getList();
        ListCellRenderer<? super DataReference<D>> renderer = list1.getCellRenderer();
        list1.setCellRenderer(new DataReferenceListCellRenderer<>(renderer));

    }

    /**
     * Remplit le modèle d'une liste graphique avec la liste des entités d'un
     * type donné sur un service de persistance donné.
     *
     * @param list le component graphique à initialiser
     * @since 1.5
     */
    protected <D extends ReferentialDto> void prepareReferentialEntityList(Class<D> dtoClass, BeanListHeader<ReferentialReference<D>> list) {

        ReferentialReferenceDecorator<D> decorator = decoratorService.getReferentialReferenceDecorator(dtoClass);
        if (log.isDebugEnabled()) {
            log.debug("Will use decorator " + decorator);
        }

        String entityLabel = t(ObserveI18nDecoratorHelper.getTypeI18nKey(dtoClass));
        list.setPopupTitleText(t("observe.content.type.referential", entityLabel));

        list.putClientProperty("decorator", decorator);

        list.init(decorator, Collections.emptyList());

        JList<ReferentialReference<D>> list1 = list.getList();
        ListCellRenderer<? super ReferentialReference<D>> renderer = list1.getCellRenderer();
        list1.setCellRenderer(new ReferentialReferenceListCellRenderer<>(renderer));

    }

    /**
     * Prépare un component de choix d'entités pour un type d'entité donné et pour un service de persistance donné.
     *
     * @param <D>      le type de l'entité
     * @param comboBox le component graphique à initialiser
     */
    protected <D extends DataDto> void prepareDataBeanComboBox(Class<D> dtoClass, BeanComboBox<DataReference<D>> comboBox) {

        DataReferenceDecorator<D> decorator = decoratorService.getDataReferenceDecorator(dtoClass);

        String entityLabel = t(ObserveI18nDecoratorHelper.getTypeI18nKey(dtoClass));
        comboBox.setPopupTitleText(t("observe.content.type.data", entityLabel));

        comboBox.init(decorator, Collections.emptyList());

        JComboBox<DataReference<D>> combobox = comboBox.getCombobox();

        ListCellRenderer<DataReference<D>> toolTipRenderer = new ComboBoxListCellRenderer<>(combobox.getRenderer());
        combobox.setRenderer(toolTipRenderer);
        if (log.isDebugEnabled()) {
            log.debug("combo  list [" + dtoClass.getName() + "] : " + comboBox.getData().size());
        }

    }

    /**
     * Prépare un component de choix d'entités pour un type d'entité donné et pour un service de persistance donné.
     *
     * @param <D>      le type de l'entité
     * @param comboBox le component graphique à initialiser
     */
    protected <D extends ReferentialDto> void prepareReferentialBeanComboBox(Class<D> dtoClass, BeanComboBox<ReferentialReference<D>> comboBox) {

        ReferentialReferenceDecorator<D> decorator = decoratorService.getReferentialReferenceDecorator(dtoClass);

        String entityLabel = t(ObserveI18nDecoratorHelper.getTypeI18nKey(dtoClass));
        comboBox.setPopupTitleText(t("observe.content.type.referential", entityLabel));

        comboBox.init(decorator, Collections.emptyList());

        JComboBox<ReferentialReference<D>> combobox = comboBox.getCombobox();

        ListCellRenderer<ReferentialReference<D>> toolTipRenderer = new ComboBoxListCellRenderer<>(combobox.getRenderer());
        combobox.setRenderer(toolTipRenderer);
        if (log.isDebugEnabled()) {
            log.debug("combo  list [" + dtoClass.getName() + "] : " + comboBox.getData().size());
        }

    }

    protected <D extends IdDto> Class<D> getDtoClass(JComponent list) {
        Object clientProperty = list.getClientProperty(ObserveContentUI.CLIENT_PROPERTY_ENTITY_CLASS);
        return (Class<D>) clientProperty;
    }

    private static class ComboBoxListCellRenderer<E> implements ListCellRenderer<E> {

        private final ListCellRenderer<? super E> renderer;

        public ComboBoxListCellRenderer(ListCellRenderer<? super E> renderer) {
            this.renderer = renderer;
        }

        @Override
        public Component getListCellRendererComponent(JList<? extends E> list,
                                                      E value,
                                                      int index,
                                                      boolean isSelected,
                                                      boolean cellHasFocus) {
            Component comp = renderer.getListCellRendererComponent(
                    list,
                    value,
                    index,
                    isSelected,
                    cellHasFocus
            );
            if (comp instanceof JLabel) {
                JLabel jcomp = (JLabel) comp;
                jcomp.setToolTipText(jcomp.getText());
            }
            return comp;
        }
    }
}
