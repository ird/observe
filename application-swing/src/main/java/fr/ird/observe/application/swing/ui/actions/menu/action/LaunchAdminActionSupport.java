package fr.ird.observe.application.swing.ui.actions.menu.action;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.I18nEnumHelper;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.actions.AbstractUIAction;
import fr.ird.observe.application.swing.ui.admin.AdminStep;
import fr.ird.observe.application.swing.ui.admin.AdminUILauncher;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.awt.event.ActionEvent;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 1/17/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.13
 */
public abstract class LaunchAdminActionSupport extends AbstractUIAction {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(LaunchAdminActionSupport.class);

    private final AdminStep action;

    public LaunchAdminActionSupport(ObserveMainUI ui, String actionName, AdminStep action, char mnemonic) {

        super(ui, actionName, t(action.getOperationLabel()), t(action.getOperationDescription()), action.getIconName(), null);
        this.action = action;
        putValue(MNEMONIC_KEY, (int) mnemonic);

    }

    @Override
    public void actionPerformed(ActionEvent event) {

        boolean canContinue = ObserveSwingApplicationContext.get().getContentUIManager().closeSelectedContentUI();
        if (canContinue) {

            if (log.isInfoEnabled()) {
                log.info("Will start admin action: " + I18nEnumHelper.getLabel(action));
            }
            AdminUILauncher launcher = AdminUILauncher.newLauncher(getMainUI(), action);
            launcher.start();
        }

    }

}
