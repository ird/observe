package fr.ird.observe.application.swing.ui.tree.navigation.nodes.longline;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.decoration.ObserveI18nDecoratorHelper;
import fr.ird.observe.application.swing.ui.content.list.impl.longline.ActivityLonglinesUI;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.ClassNavigationTreeNode;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.WithChildsToReload;
import fr.ird.observe.services.dto.longline.ActivityLonglineDto;
import fr.ird.observe.services.dto.reference.DataReference;
import fr.ird.observe.services.dto.reference.DataReferenceSet;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 14/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class ActivitiesLonglineNavigationTreeNode extends ClassNavigationTreeNode<ActivityLonglineDto> implements WithChildsToReload {

    public ActivitiesLonglineNavigationTreeNode() {
        super(ActivityLonglineDto.class, true);
    }

    @Override
    public String getId() {
        return getParent().getId();
    }

    @Override
    public String getIconPath() {
        return "navigation." + getData().getName();
    }

    @Override
    public Class<ActivityLonglinesUI> getContentClass() {
        return ActivityLonglinesUI.class;
    }

    @Override
    public String getText() {
        return t(ObserveI18nDecoratorHelper.getTypePluralI18nKey(getData()));
    }

    @Override
    public void reloadChilds() {
        DataReferenceSet<ActivityLonglineDto> referenceSet = getMainDataSourceServicesProvider().newActivityLonglineService().getActivityLonglineByTripLongline(getId());
        for (DataReference<ActivityLonglineDto> reference : referenceSet.getReferences()) {
            add(new ActivityLonglineNavigationTreeNode(reference));
        }
    }
}
