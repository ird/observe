/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui;

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import org.nuiton.jaxx.runtime.spi.UIHandler;

/**
 * Le handler de l'ui principale.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @see ObserveMainUI
 * @since 1.0
 */
public class ObserveMainUIHandler implements UIHandler<ObserveMainUI> {

    @Override
    public void beforeInit(ObserveMainUI ui) {
        ObserveMainUIModel model = new ObserveMainUIModel();
        model.init(ObserveSwingApplicationContext.get().getConfig());
        ui.setContextValue(model);
    }

    @Override
    public void afterInit(ObserveMainUI ui) {
        ui.getInitializer().initialize();
    }
}
