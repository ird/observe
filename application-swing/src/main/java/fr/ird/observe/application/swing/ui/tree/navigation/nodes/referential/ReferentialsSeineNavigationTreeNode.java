package fr.ird.observe.application.swing.ui.tree.navigation.nodes.referential;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ui.content.ref.ReferenceHomeSeineUI;
import fr.ird.observe.services.dto.referential.ReferentialHelper;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 14/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class ReferentialsSeineNavigationTreeNode extends ReferentialsNavigationTreeNodeSupport {

    public ReferentialsSeineNavigationTreeNode() {
        super(t("observe.type.reference.seine"), ReferentialHelper.REFERENCE_SEINE_DTOS);
    }

    @Override
    public Class<ReferenceHomeSeineUI> getContentClass() {
        return ReferenceHomeSeineUI.class;
    }

}
