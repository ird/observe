/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.admin.validate;

import com.google.common.base.Charsets;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.decoration.DecoratorService;
import fr.ird.observe.application.swing.decoration.ObserveI18nDecoratorHelper;
import fr.ird.observe.application.swing.ui.ObserveKeyStrokes;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.admin.AdminStep;
import fr.ird.observe.application.swing.ui.admin.AdminTabUIHandler;
import fr.ird.observe.application.swing.ui.admin.AdminUIModel;
import fr.ird.observe.application.swing.ui.admin.config.ConfigUI;
import fr.ird.observe.application.swing.ui.admin.config.SelectDataUI;
import fr.ird.observe.application.swing.ui.tree.selection.SelectionTreeModel;
import fr.ird.observe.application.swing.ui.util.ObserveValidationMessageTableRenderer;
import fr.ird.observe.application.swing.ui.util.ProgressModel;
import fr.ird.observe.application.swing.validation.ValidationModelMode;
import fr.ird.observe.services.dto.IdDto;
import fr.ird.observe.services.dto.reference.AbstractReference;
import fr.ird.observe.services.dto.reference.DataReference;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import fr.ird.observe.services.service.actions.validate.ValidateDataRequest;
import fr.ird.observe.services.service.actions.validate.ValidateDataResult;
import fr.ird.observe.services.service.actions.validate.ValidateReferentialsRequest;
import fr.ird.observe.services.service.actions.validate.ValidateReferentialsResult;
import fr.ird.observe.services.service.actions.validate.ValidateResultForDto;
import fr.ird.observe.services.service.actions.validate.ValidateResultForDtoHelper;
import fr.ird.observe.services.service.actions.validate.ValidateResultForDtoType;
import fr.ird.observe.services.service.actions.validate.ValidateService;
import fr.ird.observe.services.service.actions.validate.ValidationMessage;
import fr.ird.observe.services.service.actions.validate.ValidatorDto;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.decorator.Decorator;
import org.nuiton.jaxx.runtime.context.JAXXInitialContext;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardState;
import org.nuiton.validator.NuitonValidatorScope;

import javax.swing.DefaultListModel;
import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Le controleur des onglets.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public class ValidateUIHandler extends AdminTabUIHandler<ValidateUI> implements UIHandler<ValidateUI> {

    public static final String LINE = "--------------------------------------------------------------------------------";

    /** Logger */
    private static final Log log = LogFactory.getLog(ValidateUIHandler.class);

    protected ObserveSwingDataSource source;

    protected Decorator<TripSeineDto> dTrip;

    protected Decorator<ProgramDto> dProgram;

    @Override
    public void afterInit(ValidateUI ui) {
        super.afterInit(ui);

        JTable messageTable = ui.getMessageTable();

        messageTable.setDefaultRenderer(Object.class, new ObserveValidationMessageTableRenderer());
        messageTable.getRowSorter().setSortKeys(
                Collections.singletonList(new RowSorter.SortKey(0, SortOrder.ASCENDING)));
        UIHelper.setI18nTableHeaderRenderer(
                messageTable,
                n("observe.actions.validate.validator.scope.header"),
                n("observe.actions.validate.validator.scope.header.tip"),
                n("observe.actions.validate.validator.field.header"),
                n("observe.actions.validate.validator.field.header.tip"),
                n("observe.actions.validate.validator.message.header"),
                n("observe.actions.validate.validator.message.header.tip"));
        UIHelper.fixTableColumnWidth(messageTable, 0, 25);

        ValidateEntityListCellRenderer listRenderer = new ValidateEntityListCellRenderer(ui.getStepModel());
        ui.getTypeList().setCellRenderer(listRenderer);
        ui.getRefList().setCellRenderer(listRenderer);

        ui.getStartButton().setText(t("observe.actions.synchro.launch.operation", t(ui.getStep().getOperationLabel())));

        ObserveKeyStrokes.addKeyStrokeFromMnemonic(ui.getStartButton());
        ObserveKeyStrokes.addKeyStrokeFromMnemonic(ui.getSaveReport());

        // initialisation de l'ui de configuration
        if (log.isInfoEnabled()) {
            log.info("Init extra configuration for " + ui.getName());
        }

        ConfigUI configUI = (ConfigUI) parentUI.getStepUI(AdminStep.CONFIG);
        JAXXInitialContext tx = new JAXXInitialContext().add(configUI).add(this);
        ValidateConfigUI extraConfig = new ValidateConfigUI(tx);
        configUI.getExtraConfig().add(extraConfig);

        final SelectDataUI selectTabUI = (SelectDataUI) parentUI.getStepUI(AdminStep.SELECT_DATA);

        AdminUIModel model = ui.getModel();
        model.getValidateModel().addPropertyChangeListener(ValidateModel.PROPERTY_MODEL_MODE, evt -> {
            ValidationModelMode value = (ValidationModelMode) evt.getNewValue();
            if (value == null) {
                // rien a faire pour le moment...
                return;
            }
            SelectionTreeModel selectDataModel = selectTabUI.getSelectTree().getTreeModel();
            selectDataModel.setLoadLongline(false);
            selectDataModel.setLoadSeine(false);
            selectDataModel.setLoadReferential(false);
            if (log.isDebugEnabled()) {
                log.debug("validation model changed to " + value);
            }
            switch (value) {
                case REFERENTIEL:

                    selectDataModel.setLoadReferential(true);
                    break;
                case DATA:
                    selectDataModel.setLoadLongline(true);
                    selectDataModel.setLoadSeine(true);
                    break;
                case ALL:
                    selectDataModel.setLoadReferential(true);
                    selectDataModel.setLoadLongline(true);
                    selectDataModel.setLoadSeine(true);
                    break;
            }
            //updateModel();
        });

        ImmutableSet<ValidatorDto> validators = ObserveSwingApplicationContext.get().getValidators();

        ValidateModel stepModel = model.getValidateModel();

        stepModel.setAllValidators(validators);
        stepModel.getProgressModel().installUI(ui.getProgressBar());
    }

    public void updateState(ValidateUI tabUI, WizardState newState) {

        super.updateState(tabUI, newState);

        if (newState == WizardState.NEED_FIX) {
            updateTypes();
            tabUI.resumeLabel.setText(t("observe.actions.validate.save.reportFile", tabUI.getStepModel().getReportFile()));
            String actionText;
            if (tabUI.getStepModel().isGenerateReport()) {
                actionText = t("observe.actions.validate.save");
            } else {
                actionText = t("observe.actions.validate.continue.with.no.save.report");
            }
            tabUI.saveReport.setText(actionText);
            return;
        }
        if (newState == WizardState.RUNNING) {
            tabUI.typeList.clearSelection();
            tabUI.refList.clearSelection();
            tabUI.messageTable.clearSelection();
            tabUI.typeModel.clear();
            tabUI.refModel.clear();
            tabUI.messagesModel.clear();
        }
    }

    public void updateTypes() {

        ValidateUI tabUI = ui;

        DefaultListModel<Class<?>> typeModel = tabUI.typeModel;
        tabUI.typeSelectionModel.clearSelection();
        typeModel.clear();

        Set<Class<? extends IdDto>> messageTypes = tabUI.getStepModel().getMessageTypes();

        List<Class<? extends IdDto>> classes = ObserveI18nDecoratorHelper.sortTypes(messageTypes);

        for (Class e : classes) {

            typeModel.addElement(e);

        }

        tabUI.typeList.setSelectedIndex(0);
    }

    public void updateSelectedType() {


        ValidateUI tabUI = ui;

        ui.getModel().setBusy(true);
        // on nettoye le modele des refs
        try {
            tabUI.refSelectionModel.clearSelection();
            tabUI.refModel.clear();

            Object o = tabUI.typeList.getSelectedValue();
            if (log.isInfoEnabled()) {
                log.info("new selected type = " + o);
            }
            if (o == null) {
                return;
            }

            Class<?> type = (Class<?>) o;

            List<AbstractReference> refs = tabUI.getStepModel().getMessagesDto(type);
            for (AbstractReference<?> ref : Iterables.limit(refs, 100)) {
                if (log.isDebugEnabled()) {
                    log.debug("add ref = " + ref);
                }
                tabUI.refModel.addElement(ref);
            }
            tabUI.refList.setSelectedIndex(0);
        } finally {
            ui.getModel().setBusy(false);
        }

    }

    public void updateSelectedRef() {


        ValidateUI tabUI = ui;

        // on nettoye le modele des messages
        tabUI.messageTable.clearSelection();
        tabUI.messagesModel.clear();

        Object o = tabUI.refList.getSelectedValue();

        if (log.isDebugEnabled()) {
            log.debug("new selected ref = " + o);
        }

        if (o == null) {
            return;
        }

        AbstractReference<?> ref = (AbstractReference) o;

        if (log.isDebugEnabled()) {
            log.debug(ref);
        }

        ValidateResultForDto<?> resultForDto = tabUI.getStepModel().getMessages(ref);

        ImmutableSet<ValidationMessage> messages = resultForDto.getMessages();

        tabUI.messagesModel.setMessages(messages);


    }

    public void startAction() {

        addAdminWorker(ui.getStartButton().getToolTipText(), this::doAction);
    }

    public WizardState doAction() throws Exception {
        if (log.isDebugEnabled()) {
            log.debug(this);
        }

        WizardState init = initDB();

        if (init != null) {
            // une erreur ou une annulation
            return init;
        }

        launchValidation();

        Map<Class<? extends IdDto>, ValidateResultForDtoType<? extends IdDto>> messages = ui.getModel().getValidateModel().getMessages();
        if (messages == null || messages.isEmpty()) {

            // pas de validation ou bien pas d'erreurs rencontrées

            sendMessage(t("observe.actions.validate.message.nothing.to.do"));
            sendMessage(t("observe.actions.validate.message.operation.done", new Date()));

            return WizardState.SUCCESSED;
        }

        sendMessage(
                t("observe.actions.validate.message.operation.needFix", new Date()));

        return WizardState.NEED_FIX;
    }

    protected WizardState initDB() throws Exception {


        // on recupere la source de données
        AdminUIModel model = ui.getModel();
        source = model.getConfigModel().getSafeLocalSource(false);

        DecoratorService decoratorService = getDecoratorService();

        dTrip = decoratorService.getDecoratorByType(TripSeineDto.class);

        dProgram = decoratorService.getDecoratorByType(ProgramDto.class);

        openSource(source);

        // recuperation des validateurs du modele

        ValidateModel validationModel = model.getValidateModel();
        Set<ValidatorDto> validators = validationModel.getValidators();

        sendMessage(t("observe.actions.validate.message.use.storage",
                      source.getLabel()));

        sendMessage(t("observe.actions.validate.message.prepare.validators"));

        if (!validators.isEmpty()) {

            // des validateurs ont été trouvés

            for (ValidatorDto v : validators) {
                String label = t(ObserveI18nDecoratorHelper.getTypeI18nKey(v.getType()));
                sendMessage(t("observe.actions.validate.message.detected", label));
            }
        } else {
            sendMessage(t("observe.actions.validate.message.no.validation.detected"));
        }

        return null;
    }

    public void launchValidation() throws Exception {

        AdminUIModel model = ui.getModel();
        // on vide les anciens messages
        ValidateModel stepModel = model.getValidateModel();
        stepModel.setMessages(null);

        SelectionTreeModel dataModel = model.getSelectDataModel().getSelectionDataModel();

        ObserveSwingDataSource dataSourceToValidate = model.getConfigModel().getLocalSource();

        int stepsCount = 1;
        if (!dataSourceToValidate.isOpen()) {
            stepsCount++;
        }
        if (dataModel.isLoadReferential()) {
            stepsCount++;
        }

        if (!dataSourceToValidate.isOpen()) {
            dataSourceToValidate.open();
        }
        if (dataModel.isUseData()) {
            stepsCount += dataModel.getSelectedData().size();
        }

        ProgressModel progressModel = stepModel.getProgressModel();
        progressModel.setStepsCount(stepsCount);

        ValidateService validateService = dataSourceToValidate.newValidateService();

        Map<Class<? extends IdDto>, ValidateResultForDtoType<? extends IdDto>> messages = new HashMap<>();

        ImmutableSet<NuitonValidatorScope> scopes = ImmutableSet.copyOf(stepModel.getScopes());
        String contextName = stepModel.getContextName();

        if (dataModel.isLoadReferential()) {

            // validation des referentiels selectionnes
            ValidateReferentialsRequest request = new ValidateReferentialsRequest();

            request.setReferentialTypes(dataModel.getSelectedReferentiel());
            request.setScopes(scopes);
            request.setValidationContext(contextName);

            progressModel.incrementsCurrentStep();

            ValidateReferentialsResult result = validateService.validateReferentials(request);

            messages.putAll(result.getResultByType());
        }

        if (dataModel.isUseData()) {

            for (DataReference<?> dataReference : dataModel.getSelectedData()) {

                String id = dataReference.getId();

                ValidateDataRequest request = new ValidateDataRequest();
                request.setDataIds(ImmutableSet.of(id));
                request.setScopes(scopes);
                request.setValidationContext(contextName);

                sendMessage(t("observe.actions.validate.message.validate.trip", getDecoratorService().getDataReferenceDecorator(dataReference.getType()).toString(dataReference)));
                progressModel.incrementsCurrentStep();
                ValidateDataResult result = validateService.validateData(request);

                messages.putAll(result.getResultByType());

            }

        }

        progressModel.incrementsCurrentStep();
        dataSourceToValidate.close();

        stepModel.setMessages(messages);

    }

    public void saveReport() {
        WizardState finalState = null;
        AdminUIModel model = ui.getModel();
        ValidateModel validationModel = model.getValidateModel();
        try {
            if (validationModel.isGenerateReport()) {

                sendMessage(
                        t("observe.actions.validate.message.save.report", validationModel.getReportFile()));

                generateReportFile(validationModel);
            } else {
                sendMessage(
                        t("observe.actions.validate.message.not.save.report"));
            }
            finalState = WizardState.SUCCESSED;
        } catch (Exception e) {
            validationModel.setError(e);
            finalState = WizardState.FAILED;
        } finally {
            model.setStepState(AdminStep.VALIDATE, finalState);
        }
    }

    //FIXME A remplacer par une template
    public void generateReportFile(ValidateModel validationModel) throws IOException {
        File reportFile = validationModel.getReportFile();
        if (log.isInfoEnabled()) {
            log.info("save report in " + reportFile);
        }

        DecoratorService service = getDecoratorService();
        StringBuilder builder = new StringBuilder();
        builder.append(LINE).append('\n');

        builder.append(t("observe.actions.validate.report.title", new Date())).append('\n');
        builder.append(t("observe.actions.validate.report.scopes", validationModel.getScopes())).append('\n');
        builder.append(t("observe.actions.validate.report.contextName", validationModel.getContextName())).append('\n');

        Map<Class<? extends IdDto>, ValidateResultForDtoType<? extends IdDto>> messages = validationModel.getMessages();

        builder.append(t("observe.actions.validate.report.entities.with.messages", messages.size())).append('\n');
        builder.append(LINE).append('\n').append('\n');

        for (ValidateResultForDtoType<?> validateResultForDtoType : messages.values()) {

            ImmutableSet<? extends ValidateResultForDto<?>> validateResultForHelper = validateResultForDtoType.getValidateResultForDto();
            for (ValidateResultForDto<?> validateResultForDto : validateResultForHelper) {
                AbstractReference<?> referenceDto = validateResultForDto.getDto();

                String refStr = service.getReferenceDecorator(referenceDto.getType()).toString(referenceDto);

                Set<ValidationMessage> refMessages = validateResultForDto.getMessages();

                EnumSet<NuitonValidatorScope> scopes = ValidateResultForDtoHelper.getScopes(validateResultForDto);

                builder.append(t("observe.actions.validate.report.entity", referenceDto.getId(), refStr, refMessages.size(), scopes)).append('\n');
                for (NuitonValidatorScope scope : scopes) {

                    List<ValidationMessage> messagesByScope = ValidateResultForDtoHelper.scopeMessageFilter(scope, validateResultForDto);

                    for (ValidationMessage message : messagesByScope) {

                        builder.append(message.getScope()).append(" - ").append(message.getMessage()).append('\n');

                    }

                    builder.append('\n');
                }

            }
            builder.append(LINE).append('\n').append('\n');
        }

        String content = builder.toString();
        if (log.isInfoEnabled()) {
            log.info(content);
        }
        FileUtils.write(reportFile, content, Charsets.UTF_8.name());
    }


    // ------------------------------------------------------------------------
    // -- ValidateConfgUI methods
    // ------------------------------------------------------------------------

    public void updateValidationScopes(JCheckBox checkBox) {
        NuitonValidatorScope scope = getValidatorScope(checkBox);
        ValidateModel validateModel = ui.getModel().getValidateModel();
        if (checkBox.isSelected()) {
            // ajout du scope
            validateModel.addScope(scope);
        } else {
            // supprime le scope
            validateModel.removeScope(scope);
        }
    }

    public String updateValidatorResumeLabel(@SuppressWarnings("unused") boolean valid) {

        ValidateModel validateModel = ui.getModel().getValidateModel();

        if (validateModel == null) {
            return null;
        }
        return t("observe.actions.validate.selected.validators", validateModel.getValidators().size());
    }

    public NuitonValidatorScope getValidatorScope(JCheckBox checkBox) {
        return (NuitonValidatorScope) checkBox.getClientProperty("value");
    }

    public boolean isScopeSelected(Set<NuitonValidatorScope> scopes, JCheckBox checkBox) {
        NuitonValidatorScope scope = getValidatorScope(checkBox);
        return scopes.contains(scope);
    }

    public void chooseValidationReportFile(ValidateConfigUI configUI) {
        File f = UIHelper.chooseDirectory(
                configUI,
                t("observe.actions.validate.title.choose.report.directory"),
                t("observe.actions.validate.choose.report.directory"),
                new File(configUI.validationReportDirectoryText.getText())
        );
        changeValidationReportDirectory(configUI, f);
    }

    public void changeValidationReportDirectory(ValidateConfigUI configUI, File f) {
        ui.getModel().getValidateModel().setReportFile(new File(f, configUI.validationReportFilenameText.getText()));
    }

    public void changeValidationReportFilename(ValidateConfigUI configUI, String filename) {
        ui.getModel().getValidateModel().setReportFile(new File(configUI.validationReportDirectoryText.getText(), filename));
    }

}
