package fr.ird.observe.application.swing.ui.content.table.impl.seine;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.ird.observe.application.swing.ui.content.table.ContentTableMeta;
import fr.ird.observe.application.swing.ui.content.table.ContentTableModel;
import fr.ird.observe.application.swing.ui.content.table.ContentTableUIModel;
import fr.ird.observe.application.swing.ui.content.table.ObserveContentTableUI;
import fr.ird.observe.services.dto.reference.ReferentialReference;
import fr.ird.observe.services.dto.referential.SizeMeasureTypeDto;
import fr.ird.observe.services.dto.seine.TargetLengthDto;
import fr.ird.observe.services.dto.seine.TargetLengthHelper;
import fr.ird.observe.services.dto.seine.TargetSampleDto;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Created on 9/26/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since XXX
 */
public class TargetSampleUIModel extends ContentTableUIModel<TargetSampleDto, TargetLengthDto> {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_DEFAULT_SIZE_MEASURE_TYPE = "defaultSizeMeasureType";

    private ReferentialReference<SizeMeasureTypeDto> defaultSizeMeasureType;

    public TargetSampleUIModel(TargetSampleUI ui) {

        super(TargetSampleDto.class,
              TargetLengthDto.class,
              new String[]{
                      TargetSampleDto.PROPERTY_TARGET_LENGTH,
                      TargetSampleDto.PROPERTY_COMMENT},
              new String[]{
                      TargetLengthDto.PROPERTY_SPECIES,
                      TargetLengthDto.PROPERTY_SEX,
                      TargetLengthDto.PROPERTY_LENGTH,
                      TargetLengthDto.PROPERTY_IS_LENGTH_COMPUTED,
                      TargetLengthDto.PROPERTY_WEIGHT,
                      TargetLengthDto.PROPERTY_IS_WEIGHT_COMPUTED,
                      TargetLengthDto.PROPERTY_COUNT,
                      TargetLengthDto.PROPERTY_SIZE_MEASURE_TYPE,
                      TargetLengthDto.PROPERTY_ACQUISITION_MODE,
                      TargetLengthDto.PROPERTY_TAG_NUMBER});

        List<ContentTableMeta<TargetLengthDto>> metas = Arrays.asList(
                ContentTableModel.newTableMeta(TargetLengthDto.class, TargetLengthDto.PROPERTY_SPECIES, true),
                ContentTableModel.newTableMeta(TargetLengthDto.class, TargetLengthDto.PROPERTY_SEX, true),
                ContentTableModel.newTableMeta(TargetLengthDto.class, TargetLengthDto.PROPERTY_SIZE_MEASURE_TYPE, true),
                ContentTableModel.newTableMeta(TargetLengthDto.class, TargetLengthDto.PROPERTY_LENGTH, true),
                ContentTableModel.newTableMeta(TargetLengthDto.class, TargetLengthDto.PROPERTY_WEIGHT, true),
                ContentTableModel.newTableMeta(TargetLengthDto.class, TargetLengthDto.PROPERTY_COUNT, false),
                ContentTableModel.newTableMeta(TargetLengthDto.class, TargetLengthDto.PROPERTY_TOTAL_WEIGHT, true));

        initModel(ui, metas);

    }

    @Override
    protected ContentTableModel<TargetSampleDto, TargetLengthDto> createTableModel(
            ObserveContentTableUI<TargetSampleDto, TargetLengthDto, ?> ui,
            List<ContentTableMeta<TargetLengthDto>> contentTableMetas) {
        return new ContentTableModel<TargetSampleDto, TargetLengthDto>(ui, contentTableMetas) {
            private static final long serialVersionUID = 1L;

            @Override
            protected Collection<TargetLengthDto> getChilds(TargetSampleDto bean) {
                return bean.getTargetLength();
            }

            @Override
            protected void load(TargetLengthDto source, TargetLengthDto target) {
                TargetLengthHelper.copyTargetLengthDto(source, target);
            }

            @Override
            protected void setChilds(TargetSampleDto parent, List<TargetLengthDto> childs) {
                parent.setTargetLength(Sets.newLinkedHashSet(childs));
            }
        };
    }

    public ReferentialReference<SizeMeasureTypeDto> getDefaultSizeMeasureType() {
        return defaultSizeMeasureType;
    }

    public void setDefaultSizeMeasureType(ReferentialReference<SizeMeasureTypeDto> defaultSizeMeasureType) {
        ReferentialReference<SizeMeasureTypeDto> oldValue = getDefaultSizeMeasureType();
        this.defaultSizeMeasureType = defaultSizeMeasureType;
        firePropertyChange(PROPERTY_DEFAULT_SIZE_MEASURE_TYPE, oldValue, defaultSizeMeasureType);
    }
}
