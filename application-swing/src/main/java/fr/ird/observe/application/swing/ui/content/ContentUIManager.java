/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.content;

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.ObserveSwingSessionHelper;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.NavigationTreeNodeSupport;
import fr.ird.observe.services.dto.AbstractObserveDto;
import org.nuiton.jaxx.runtime.JAXXContext;
import org.nuiton.jaxx.runtime.context.JAXXInitialContext;
import org.nuiton.jaxx.runtime.swing.CardLayout2;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JPanel;
import java.awt.Component;
import java.lang.reflect.Constructor;

/**
 * Manager des ecrans d'editions
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public class ContentUIManager {

    /** Logger */
    private static final Log log = LogFactory.getLog(ContentUIManager.class);

    public Class<? extends ContentUI<?, ?>> convertNodeToContentUI(NavigationTreeNodeSupport<?> node) {
        return node.getContentClass();
    }

    public <U extends ContentUI<?, ?>> U getContent(Class<U> uiClass) {

        CardLayout2 layout = getLayout();
        JPanel layoutContent = getLayoutContent();
        String constraints = uiClass.getName();

        if (!layout.contains(constraints)) {

            // pas trouvé
            return null;
        }

        U content = (U) layout.getComponent(layoutContent, constraints);

        if (log.isDebugEnabled()) {
            log.debug("Will use existing content [" + constraints + "] : " + content.getClass().getName());
        }
        return content;
    }

    public <U extends ContentUI<?, ?>> U createContent(Class<U> uiClass) {

        String constraints = uiClass.getName();

        U result;
        try {
            Constructor<U> constructor = uiClass.getConstructor(JAXXContext.class);
            if (log.isDebugEnabled()) {
                log.debug("create new content : " + uiClass);
            }
            result = constructor.newInstance(new JAXXInitialContext().add(getMainUI()));
        } catch (Exception e) {
            throw new IllegalStateException("Could not create content ui " + uiClass, e);
        }

        try {

            // ajout du content dans son parent
            getLayoutContent().add(result, constraints);
            if (log.isDebugEnabled()) {
                log.debug("Add new content [" + constraints + "] : " + result.getClass().getName());
            }

            getMainUI().getModel().attachFocusOnBody(result);
            getMainUI().getModel().attachFocusOnBodyDeep(result);

            return result;
        } catch (Exception e) {
            throw new IllegalStateException("Could not init content ui " + uiClass, e);
        }
    }

    public void openContent(ContentUI<?, ?> content) {

        String constraints = content.getClass().getName();

        if (log.isDebugEnabled()) {
            log.debug("Will open ui [" + constraints + "] : " + content.getClass());
        }

        // on ouvre l'ui
        try {
            content.open();

            ObserveSwingSessionHelper swingSessionHelper = ObserveSwingApplicationContext.get().getSwingSessionHelper();
            swingSessionHelper.addComponent(content, true);
            swingSessionHelper.save();

        } catch (Exception e) {
            UIHelper.handlingError(e);
        } finally {

            // on affiche l'ui quoi qu'il arrive ?
            getLayout().show(getLayoutContent(), constraints);
        }

    }

    public void close() {
        getLayout().reset(getLayoutContent());
    }

    public ContentUI<?, ?> getSelectedContentUI() {
        return getSelectedContentUI(getMainUI());
    }

    public boolean closeSelectedContentUI() {
        return closeSelectedContentUI(getMainUI());
    }

    /**
     * Essaye de fermer l'écran d'édition s'il existe.
     *
     * @param mainUI l'ui principale
     * @return {@code true} si le contenu a bien été fermé, {@code false} si on
     * ne peut pas fermer l'écran
     * @since 1.5
     */
    public boolean closeSelectedContentUI(ObserveMainUI mainUI) {
        ContentUI<?, ?> ui = getSelectedContentUI(mainUI);
        if (ui == null) {
            // no content ui
            return true;
        }
        boolean closed = false;
        try {
            closed = ui.close();
        } catch (Exception e) {
            UIHelper.handlingError(e);
        }
        return closed;
    }

    public void removeSelectedContentUI() {

        ContentUI<?, ?> selectedContentUI = getSelectedContentUI();
        if (selectedContentUI != null) {
            getLayout().removeLayoutComponent(selectedContentUI, selectedContentUI.getClass().getName());
            getLayoutContent().remove(selectedContentUI);
            selectedContentUI.destroy();
        }
    }

    public void restartEdit() {

        ContentUI<?, ?> selectedUI = getSelectedContentUI();
        if (selectedUI == null) {

            // pas d'écran selectionne
            return;
        }
        ContentUIModel<? extends AbstractObserveDto> model = selectedUI.getModel();
        if (!model.isEditable()) {

            // modele non editable
            return;
        }

        ContentMode contentMode = model.getMode();
        if (ContentMode.UPDATE != contentMode) {

            // ecran non en mode mis a jour
            return;
        }

        if (log.isDebugEnabled()) {
            log.debug("Will restart edit for " + selectedUI.getName());
        }
        selectedUI.restartEdit();

    }


    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        close();
    }

    private JPanel getLayoutContent() {
        return getMainUI().getContent();
    }

    private CardLayout2 getLayout() {
        return getMainUI().getContentLayout();
    }

    private ObserveMainUI getMainUI() {
        return ObserveSwingApplicationContext.get().getMainUI();
    }

    private ContentUI<?, ?> getSelectedContentUI(ObserveMainUI ui) {

        if (ui == null) {
            // no ui, so no modification
            return null;
        }

        ContentUI<?, ?> result = null;
        CardLayout2 layout = ui.getContentLayout();
        JPanel container = ui.getContent();
        Component currentContent = layout.getVisibleComponent(container);
        if (currentContent != null && currentContent instanceof ContentUI<?, ?>) {

            result = (ContentUI<?, ?>) currentContent;
        }
        return result;
    }

}
