package fr.ird.observe.application.swing.ui.content.ref;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.DataContext;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.decoration.ObserveI18nDecoratorHelper;
import fr.ird.observe.application.swing.ui.content.ContentMode;
import fr.ird.observe.application.swing.ui.content.ContentUIHandler;
import fr.ird.observe.application.swing.ui.tree.navigation.NavigationTree;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.NavigationTreeNodeSupport;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import org.nuiton.jaxx.runtime.spi.UIHandler;

import javax.swing.JButton;
import javax.swing.JPanel;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 9/28/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since XXX
 */
public class ReferenceHomeUIHandler extends ContentUIHandler<ProgramDto, ReferenceHomeUI> implements UIHandler<ReferenceHomeUI> {

    public ReferenceHomeUIHandler() {
        super(null, null);
    }

    @Override
    protected ContentMode getContentMode(DataContext dataContext) {
        return ContentMode.READ;
    }

    @Override
    protected boolean computeCanWrite(ObserveSwingDataSource source) {
        return source.canWriteReferential();
    }

    @Override
    public void afterInit(ReferenceHomeUI ui) {
        super.afterInit(ui);

        JPanel panel = ui.getBody();

        String nodeName = ui.getModel().getNodeName();
        NavigationTree navigation = ObserveSwingApplicationContext.get().getMainUI().getNavigation();
        NavigationTreeNodeSupport<?> referentialNode = navigation.getReferentialNode(nodeName);

        for (Class<? extends ReferentialDto> type : ui.getModel().getTypes()) {

            String text = t(ObserveI18nDecoratorHelper.getTypePluralI18nKey(type));
            JButton button = new JButton(text);
            button.addActionListener(e -> navigation.selectNode(referentialNode.findChildByClass(type)));
            panel.add(button);

        }

        getModel().setEnabled(true);
    }
}
