/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.admin.report;

import com.google.common.collect.Lists;
import fr.ird.observe.application.swing.ObserveSwingTechnicalException;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.admin.AdminStep;
import fr.ird.observe.application.swing.ui.admin.AdminTabUIHandler;
import fr.ird.observe.application.swing.ui.admin.config.ConfigUI;
import fr.ird.observe.services.dto.DataDto;
import fr.ird.observe.services.dto.constants.ObserveModelType;
import fr.ird.observe.services.dto.reference.DataReference;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.service.actions.report.DataMatrix;
import fr.ird.observe.services.service.actions.report.Report;
import fr.ird.observe.services.service.actions.report.ReportService;
import fr.ird.observe.services.service.actions.report.ReportVariable;
import org.nuiton.jaxx.runtime.context.JAXXInitialContext;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.widgets.select.BeanComboBox;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardState;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.decorator.Decorator;
import org.nuiton.decorator.JXPathDecorator;
import org.nuiton.util.TimeLog;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ItemEvent;
import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Le controleur des onglets.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public class ReportUIHandler extends AdminTabUIHandler<ReportUI> implements UIHandler<ReportUI> {

    public static final String VARIABLE_NAME = "variableName";

    /** Logger */
    private static final Log log = LogFactory.getLog(ReportUIHandler.class);

    private static final TimeLog timeLog = new TimeLog(ReportUIHandler.class, 0, 1000);

    private Runnable revalidateTabUI;

    @Override
    public void afterInit(ReportUI ui) {
        super.afterInit(ui);
        if (log.isDebugEnabled()) {
            log.debug(" specialized for [" + ui.getStep() + "] for main ui " + parentUI.getClass().getName() + "@" + System.identityHashCode(this.ui));
        }
        revalidateTabUI = ui::revalidate;
        UIHelper.setLayerUI(ui.getContent(), parentUI.getConfigBlockLayerUI());

        ReportModel stepModel = getStepModel();
        stepModel.addPropertyChangeListener(evt -> {

            if (ui.getModel().getModelState() == WizardState.CANCELED) {

                // action annulée, on ne declanche plus rien
                return;
            }
            String propertyName = evt.getPropertyName();
            Object newValue = evt.getNewValue();

            ReportModel source = (ReportModel) evt.getSource();

            if (ReportModel.REPORTS_PROPERTY_NAME.equals(propertyName)) {
                List<?> reports = (List<?>) newValue;
                onReportsChanged(ui, reports);
            } else if (ReportModel.SELECTED_REPORT_PROPERTY_NAME.equals(propertyName)) {
                Report report = (Report) newValue;
                onSelectedReportChanged(ui, source, report);
            } else if (ReportModel.VARIABLES_PROPERTY_NAME.equals(propertyName)) {
                Map<String, Object> variables = (Map<String, Object>) newValue;
                onVariablesChanges(source, variables);
            } else if (ReportModel.VALID_PROPERTY_NAME.equals(propertyName)) {
                final Boolean valid = (Boolean) newValue;
                SwingUtilities.invokeLater(() -> onValidChanged(source, valid != null && valid));


            }
        });

        ui.getReportSelector().setRenderer(new DefaultListCellRenderer() {
                                               private static final long serialVersionUID = 1L;

                                               @Override
                                               public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                                                   if (value == null) {

                                                       // on affiche une message de sélection de report
                                                       value = t("observe.message.select.report");
                                                   }
                                                   return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                                               }
                                           }

        );
        // ajout du renderer sur le tableau

        DefaultTableCellRenderer renderer = new DefaultTableCellRenderer() {
            private static final long serialVersionUID = 1L;
            Font defaultFont;

            Font headerFont;

            public Font getDefaultFont(JTable table) {
                if (defaultFont == null) {
                    defaultFont = table.getFont();
                }
                return defaultFont;
            }

            public Font getHeaderFont(JTable table) {
                if (headerFont == null) {
                    headerFont = getDefaultFont(table).deriveFont(Font.BOLD);
                }
                return headerFont;
            }

            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Font font;
                if (row == 0 || column == 0) {

                    // on est sur une bordure
                    font = getHeaderFont(table);
                } else {
                    font = getDefaultFont(table);
                }
                Component comp = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                comp.setFont(font);
                return comp;
            }
        };
        ui.getResultTable().setDefaultRenderer(Object.class, renderer);
        ui.getResultTable().setDefaultRenderer(String.class, renderer);

        // initialisation de l'ui de configuration
        if (log.isInfoEnabled()) {
            log.info("Init extra configuration for " + ui.getName());
        }

        ConfigUI configUI = (ConfigUI) parentUI.getStepUI(AdminStep.CONFIG);
        JAXXInitialContext tx = new JAXXInitialContext().add(configUI).add(this);

        ReportConfigUI extraConfig = new ReportConfigUI(tx);

        configUI.getExtraConfig().add(extraConfig);
    }

    public void setModelTypeFromEvent(ItemEvent event) {
        if (event.getStateChange() == ItemEvent.SELECTED) {
            getStepModel().setModelType((ObserveModelType) event.getItem());
        }
    }

    public void updateSelectedReportFromEvent(ItemEvent event) {

        if (event.getStateChange() == ItemEvent.SELECTED) {
            if (log.isInfoEnabled()) {
                log.info("Item selected!");
            }
            JComboBox source = (JComboBox) event.getSource();
            updateSelectedReport((Report) source.getSelectedItem());
        }
    }

    public void updateSelectedReport(Report report) {
        ui.getModel().setBusy(true);
        try {
            if (log.isInfoEnabled()) {
                log.info("New selected report : " + report);
            }
            getStepModel().setSelectedReport(report);

        } finally {
            ui.getModel().setBusy(false);
        }
    }

    public String updateSelectedReportDescrption(Report report) {
        if (report == null) {
            return t("observe.message.no.report.selected");
        }
        return t(report.getDescription());
    }

    public void updateVariable(JComboBox combo, Object value) {
        String variableName = (String) combo.getClientProperty(VARIABLE_NAME);
        if (variableName == null) {
            throw new IllegalStateException(
                    "No 'variableName' clientProperty on " + combo);
        }
        if (log.isInfoEnabled()) {
            log.info("Set variable [" + variableName + "] to value " + value);
        }
        getStepModel().addVariable(variableName, value);
    }

    public void chooseReportFile() {
        ReportModel model = ui.getModel().getReportModel();
        File f = UIHelper.chooseFile(
                ui,
                t("observe.title.choose.reportFile"),
                t("observe.action.choose.reportFile"),
                model.getReportFile(),
                "^.+\\.properties$",
                t("observe.action.choose.reportFile.description"));
        model.setReportFile(f);
    }

    public void copyReportToClipBoard(Report report,
                                      ResultTableModel model,
                                      boolean copyRowHeaders,
                                      boolean copyColumnHeaders) {
        if (report == null) {

            // pas de report sélectionné, rien à faire
            return;
        }

        if (log.isDebugEnabled()) {
            log.debug("Will copy result of report " + report.getName());
            log.debug("Result dimension : [" + model.getRowCount() + "," +
                              model.getColumnCount() + "]");
        }
        String content = model.getClipbordContent(copyRowHeaders, copyColumnHeaders);
        UIHelper.copyToClipBoard(content);
    }

    protected void onReportsChanged(ReportUI tabUI, List<?> newValue) {

        if (log.isDebugEnabled()) {
            log.debug("New reports : " + newValue);
        }

        // on ajoute toujours une premiere valeur null (pour ne rien selectionne)
        newValue.add(0, null);

        // on charge le nouveau modèle dans la liste déroulante
        UIHelper.fillComboBox(tabUI.getReportSelector(), newValue, null);
    }

    @SuppressWarnings({"unchecked"})
    protected void onSelectedReportChanged(ReportUI tabUI, ReportModel model, Report report) {

        if (log.isInfoEnabled()) {
            log.info("New selected report [" + report + "]");
        }

        ui.getModel().setBusy(true);

        try {
            // on regénère l'ui de configuration des variables
            JPanel variablesPanel = tabUI.getReportVariableSelectorPanel();
            variablesPanel.removeAll();

            // on utilise les variables uniquement si nécessaire
            boolean useVariables = report != null && report.isVariableRequired();
            tabUI.getReportVariableSelectorPane().setVisible(useVariables);

            // calcul des données et contruction du tableau
            model.getResultModel().clear();

            if (report != null) {

                try {

                    ObserveSwingDataSource dataSource = ui.getModel().getConfigModel().getSafeLocalSource(true);

                    ReportService reportService = dataSource.newReportService();

                    report = reportService.populateVariables(report, ui.getModel().getSelectDataModel().getSelectedTrip().getId());

                } catch (Exception e) {
                    throw new ObserveSwingTechnicalException("unable to populate report : " + report.getName(), e);
                }

                if (useVariables) {

                    // on construit les ui pour chaqsue variable
                    for (ReportVariable variable : report.getVariables()) {
                        String variableName = variable.getName();
                        //                String value = variables.get(variableName);
                        Set values = variable.getValues();
                        List<Object> universe = Lists.newArrayList(values);
                        BeanComboBox<Object> combo = new BeanComboBox<>();

                        combo.setShowReset(true);
                        variablesPanel.add(combo);
                        Decorator decorator;
                        if (ReferentialDto.class.isAssignableFrom(variable.getType()) && !universe.isEmpty()) {
                            decorator = getDecoratorService().getReferentialReferenceDecorator(variable.getType());
                        } else if (DataDto.class.isAssignableFrom(variable.getType()) && !universe.isEmpty()) {
                            decorator = getDecoratorService().getDataReferenceDecorator(variable.getType());
                        } else {
                            decorator = getDecoratorService().getDecoratorByType(variable.getType());
                        }
                        combo.init((JXPathDecorator<Object>) decorator, universe);
                        JComboBox jComboBox = combo.getCombobox();
                        jComboBox.putClientProperty(VARIABLE_NAME, variableName);
                        jComboBox.addItemListener(e -> {

                            JComboBox comboBox = (JComboBox) e.getSource();

                            if (e.getStateChange() == ItemEvent.DESELECTED) {

                                // ne rien faire de l'évènement de déselection
                                // sauf si le modèle devient vide

                                if (comboBox.getSelectedItem() == null) {
                                    updateVariable(comboBox, null);
                                }
                                return;
                            }

                            Object o = e.getItem();
                            updateVariable(comboBox, o);
                        });
                    }
                }

                // on revalide la disposition de l'onglet
                SwingUtilities.invokeLater(revalidateTabUI);

                Map<String, Object> variables = model.getVariables();
                updateValidState(report, variables);
            }
        } finally {

            ui.getModel().setBusy(false);

        }
    }

    protected void onVariablesChanges(ReportModel model, Map<String, Object> variables) {

        Report report = model.getSelectedReport();
        if (report != null) {

            updateValidState(report, variables);
        }
    }

    protected void onValidChanged(ReportModel model, boolean valid) {

        if (log.isInfoEnabled()) {
            log.info("valid state changed to " + valid);
        }

        if (!valid) {
            // calcul des données et contruction du tableau
            model.getResultModel().clear();
            return;
        }

        ui.getModel().setBusy(true);

        try {

            Report report = model.getSelectedReport();

            DataReference trip = ui.getModel().getSelectDataModel().getSelectedTrip();

            if (log.isDebugEnabled()) {
                log.debug("Build result for report [" + report.getName() + "] on " + trip);
            }

            Map<String, Object> variables = model.getVariables();

            for (ReportVariable variable : report.getVariables()) {
                Object value = variables.get(variable.getName());
                variable.setSelectedValue(value);
            }

            long startTime = TimeLog.getTime();

            ObserveSwingDataSource dataSource = ui.getModel().getConfigModel().getSafeLocalSource(true);
            if (!dataSource.isOpen()) {
                dataSource.open();
            }

            ReportService reportService = dataSource.newReportService();

            DataMatrix data = reportService.executeReport(report, trip.getId());

            timeLog.log(startTime, "execute", report.getName());

            if (log.isInfoEnabled()) {
                log.info("Result to dispaly:\n" + data.getClipbordContent(true, true));
            }

            // calcul des données et contruction du tableau
            model.getResultModel().populate(report, data);

            // mise a jour du clipboard automatique si requis
            if (model.isAutoCopyToClipboard()) {
                copyReportToClipBoard(report,
                                      model.getResultModel(),
                                      model.isCopyRowHeaders(),
                                      model.isCopyColumnHeaders());
            }

        } catch (Exception e) {

            UIHelper.handlingError("Could not obtain report data", e);

            model.getResultModel().clear();

        } finally {

            ui.getModel().setBusy(false);

        }
    }

    protected ReportModel getStepModel() {
        return ui.getModel().getReportModel();
    }

    protected void updateValidState(Report report, Map<String, Object> variables) {

        boolean canExecute = report != null && report.canExecute(variables);
        getStepModel().setValid(canExecute);
    }

    public void destroy() {
        ObserveSwingDataSource dataSource = ui.getModel().getConfigModel().getSafeLocalSource(false);

        if (dataSource.isOpen()) {
            dataSource.close();
        }
    }
}
