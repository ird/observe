/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.content.list;

import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.db.constants.DataContextType;
import fr.ird.observe.application.swing.ui.content.ContentMode;
import fr.ird.observe.application.swing.ui.content.ContentUIHandler;
import fr.ird.observe.application.swing.ui.tree.navigation.NavigationTree;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.NavigationTreeNodeSupport;
import fr.ird.observe.services.dto.DataDto;
import fr.ird.observe.services.dto.IdDto;
import fr.ird.observe.services.dto.reference.AbstractReference;
import fr.ird.observe.services.dto.reference.DataReference;
import org.nuiton.jaxx.widgets.select.BeanListHeader;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.decorator.Decorator;
import org.nuiton.decorator.DecoratorUtil;
import org.nuiton.decorator.JXPathDecorator;

import javax.swing.DefaultListCellRenderer;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.SwingUtilities;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.5
 */
public abstract class ContentListUIHandler<E extends IdDto, C extends DataDto, U extends ContentListUI<E, C, U>> extends ContentUIHandler<E, U> {

    /** Logger */
    private static final Log log = LogFactory.getLog(ContentListUIHandler.class);

    public ContentListUIHandler(DataContextType parentType, DataContextType type) {
        super(parentType, type);
    }

    @Override
    protected boolean computeCanWrite(ObserveSwingDataSource source) {
        return source.canWriteData();
    }

    @Override
    public final ContentListUIModel<E, C> getModel() {
        return (ContentListUIModel<E, C>) super.getModel();
    }

    @Override
    public void afterInit(U ui) {
        super.afterInit(ui);

        // on installe un renderer sur la liste pour afficher les couleurs + icones comme dans l'arbre
        ListCellRenderer renderer = ui.getList().getCellRenderer();
        NavigationTree treeHelper = getTreeHelper(ui);

        ui.getList().setCellRenderer(new EntityListCellRenderer(renderer, treeHelper));

        ContentListUIModel<E, C> model = ui.getModel();
        model.addPropertyChangeListener(ContentListUIModel.PROPERTY_SELECTED_DATAS, e -> {
            model.setCanClose(computeCanClose());
            model.setCanReopen(computeCanReopen());
            model.setCanCreate(computeCanCreate());
            model.setCanMove(computeCanMove());
            model.setCanGotoSelected(computeCanGotoSelected());
        });

        model.addPropertyChangeListener(ContentListUIModel.PROPERTY_DATA, e -> updateList((List) e.getNewValue()));
    }

    protected void updateList(List<DataReference<C>> data) {
        BeanListHeader<DataReference<C>> list = ui.getListHeader();
        if (CollectionUtils.isNotEmpty(data)) {
            if (log.isDebugEnabled()) {
                log.debug(list.getName() + " - " + data.size());
            }
            Decorator<DataReference<C>> decorator = list.getHandler().getDecorator();
            DecoratorUtil.sort((JXPathDecorator<DataReference<C>>) decorator, data, 0);
            list.setData(data);
        } else {
            list.setData(Collections.emptyList());
        }
    }

    @Override
    public final void openUI() {

        super.openUI();

        // init renderer
        EntityListCellRenderer renderer = (EntityListCellRenderer) getUi().getList().getCellRenderer();
        renderer.init();

        ContentListUIModel<E, C> model = getModel();

        String selectedId = getSelectedParentId();

        if (log.isInfoEnabled()) {
            log.info(prefix + "selectedId   = " + selectedId);
        }

        ContentMode mode = computeContentMode();

        if (log.isInfoEnabled()) {
            log.info(prefix + "content mode = " + mode);
        }

        // We want to force the bindings on "mode" to be triggered each time we open a list ui.
        //
        // The ui mode can already be set to the mode we're currently applying now.
        // As the state of the buttons is bound to that mode,
        // we need to force a mode change to ensure the buttons state will be refreshed.
        //
        // For example :
        // The button of the activities list of a route "Go to the open activity of another road" may need to disapear
        // because that road has just been deleted..
        //
        // As the ui state of the buttons is usually only bound to the mode of the model
        // and as the state of those buttons usually not only depends on that mode  (but also on attributes of the data context for instance, such as a route/activity/trip is open or not),
        // we need to force the buttons to recompute their state by forcing the mode's listeners to be triggered.
        model.setMode(null);
        model.setMode(mode);

        // il n'est pas nécessaire de charger le bean car seuls ses enfants nous sont utile dans cette ecran
        List<DataReference<C>> data = new ArrayList<>();

        NavigationTree treeHelper = getTreeHelper(ui);
        NavigationTreeNodeSupport selectedNode = treeHelper.getSelectedNode();

        // on charge si besoin les enfants du nœud
        selectedNode.populateChilds();

        Enumeration children = selectedNode.children();
        while (children.hasMoreElements()) {
            NavigationTreeNodeSupport o = (NavigationTreeNodeSupport) children.nextElement();
            data.add((DataReference<C>) o.getData());
        }

        model.setData(data);

        model.setCanClose(computeCanClose());
        model.setCanReopen(computeCanReopen());
        model.setCanCreate(computeCanCreate());
        model.setCanMove(computeCanMove());
        model.setCanGotoOpen(computeCanGotoOpen());
        model.setCanGotoSelected(computeCanGotoSelected());

        SwingUtilities.invokeLater(() -> {
            getUi().getListSelectionModel().clearSelection();
            if (!getModel().isEmpty()) {
                getUi().getListSelectionModel().setSelectionInterval(0, 0);
            }
        });

        // finalize openUI with specified code
        finalizeOpenUI();
    }

    protected boolean computeCanGotoSelected() {
        return ui.getModel().isOneSelectedData();
    }

    protected abstract boolean computeCanGotoOpen();

    protected boolean computeCanMove() {
        return ui.getModel().isOneOrMoreSelectedData();
    }

    protected boolean computeCanCreate() {
        return ui.getModel().isCreatingMode();
    }

    protected boolean computeCanReopen() {
        return ui.getModel().isOneSelectedData() && getModel().getMode() == ContentMode.CREATE;
    }

    protected abstract boolean computeCanClose();

    public abstract NavigationTreeNodeSupport<?> getOpenNode();

    @Override
    protected void updateToolbarActions() {
        super.updateToolbarActions();
        // on n'a pas les données nécessaires pour afficher les informations techniques
        getUi().getShowTechnicalInformations().setEnabled(false);
    }

    @Override
    public void createDataUI() {
        NavigationTree treeHelper = getTreeHelper(ui);
        NavigationTreeNodeSupport parentNode = treeHelper.getSelectedNode();
        treeHelper.addUnsavedNode(parentNode, getModel().getChildType());
    }

    /**
     * When a data has been selected in the list.
     *
     * @param event the mouse event fired
     */
    void onDataSelected(MouseEvent event) {
        if (event.getClickCount() > 1) {
            if (getModel().isOneSelectedData()) {
                gotoChild(getUi().getSelectedData());
            }
        }
    }

    /**
     * Pour effectuer un traitement supplémantaire à la fin de la méthode
     * {@link #openUI()}.
     */
    protected void finalizeOpenUI() {
        // rien par default
    }


    /**
     * Un renderer de liste d'entité qui reprendre la cosmétique de l'arbre
     * de navigation.
     *
     * @since 1.5
     */
    private static class EntityListCellRenderer extends DefaultListCellRenderer {

        private static final long serialVersionUID = 1L;

        private final transient ListCellRenderer delegate;

        private final transient NavigationTree treeHelper;

        private final ThreadLocal<NavigationTreeNodeSupport> containerNode = new ThreadLocal<>();

        EntityListCellRenderer(ListCellRenderer<?> delegate, NavigationTree treeHelper) {
            this.delegate = delegate;
            this.treeHelper = treeHelper;
        }

        public void init() {
            containerNode.set(treeHelper.getSelectedNode());
        }

        @Override
        public Component getListCellRendererComponent(JList<?> list,
                                                      Object value,
                                                      int index,
                                                      boolean isSelected,
                                                      boolean cellHasFocus) {

            // obtain the text from the delegate renderer
            JLabel comp = (JLabel) delegate.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);

            if (value == null || !(value instanceof AbstractReference) || containerNode.get() == null) {

                // rien de plus a faire
                return comp;
            }

            // recuperation du noeud correspondant dans l'arbre
            NavigationTreeNodeSupport node = treeHelper.getChild(containerNode.get(), ((AbstractReference) value).getId());

            if (node == null) {

                // noeud non trouve (cela ne devrait jamais arrive
                return comp;
            }

            Icon icon = node.getIcon("");
            comp.setIcon(icon);
            Color color = node.getColor();
            comp.setForeground(color);
            comp.setText(node.getText());
//            comp.setBackground(null);
            return comp;
        }
    }

}
