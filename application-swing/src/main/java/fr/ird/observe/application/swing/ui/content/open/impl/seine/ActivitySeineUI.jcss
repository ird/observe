/*
 * #%L
 * ObServe :: Swing
 * %%
 * Copyright (C) 2008 - 2010 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

#model {
  editable:true;
  modified:{validator.isChanged()};
  valid:{validator.isValid()};
}

#body {
  layout:{new BorderLayout()};
}

#extraActions {
  visible:{model.isUpdatingMode()};
}

#time {
  propertyTime:{ActivitySeineDto.PROPERTY_TIME};
  bean:{bean};
  label:{t("observe.common.heureobservation")};
  time:{bean.getTime()};
  _validatorLabel:{t("observe.common.heureobservation")};
}

#coordinatesLabel {
  text:"observe.storage.activityLongline.coordinate";
  labelFor:{coordinatesEditor};
}

#coordinatesEditor {
  propertyLatitude:{ActivitySeineDto.PROPERTY_LATITUDE};
  propertyLongitude:{ActivitySeineDto.PROPERTY_LONGITUDE};
  propertyQuadrant:{ActivitySeineDto.PROPERTY_QUADRANT};
}

#generalTab {
  title:{t("observe.storage.activitySeine.tab.general")};
  icon:{getHandler().getErrorIconIfFalse(model.isGeneralTabValid())};
}

#measurementsTab {
  title:{t("observe.storage.activitySeine.tab.measurements")};
  icon:{getHandler().getErrorIconIfFalse(model.isMeasurementsTabValid())};
}

#vesselActivityInformation {
  font-size:11;
  actionIcon:"information";
}

#vesselActivitySeineLabel {
  text:"observe.common.vesselActivitySeine";
  labelFor:{vesselActivitySeine};
}

#vesselActivitySeine {
  property:{ActivitySeineDto.PROPERTY_VESSEL_ACTIVITY_SEINE};
  selectedItem:{bean.getVesselActivitySeine()};
  enabled:{!model.isUpdatingMode()};
}

#surroundingActivityLabel {
  text:"observe.common.surroundingActivity";
  labelFor:{surroundingActivity};
}

#surroundingActivity {
  property:{ActivitySeineDto.PROPERTY_SURROUNDING_ACTIVITY};
  selectedItem:{bean.getSurroundingActivity()};
}

#previousFpaZoneLabel {
  text:"observe.common.previousFpaZone";
  labelFor:{previousFpaZone};
}

#previousFpaZone {
  property:{ActivitySeineDto.PROPERTY_PREVIOUS_FPA_ZONE};
  selectedItem:{bean.getPreviousFpaZone()};
  enabled:{model.isChangedZoneOperation()};
}

#currentFpaZoneLabel {
  text:"observe.common.currentFpaZone";
  labelFor:{currentFpaZone};
}

#currentFpaZone {
  property:{ActivitySeineDto.PROPERTY_CURRENT_FPA_ZONE};
  selectedItem:{bean.getCurrentFpaZone()};
  enabled:{!model.isChangedZoneOperation()};
}

#nextFpaZoneLabel {
  text:"observe.common.nextFpaZone";
  labelFor:{nextFpaZone};
}

#nextFpaZone {
  property:{ActivitySeineDto.PROPERTY_NEXT_FPA_ZONE};
  selectedItem:{bean.getNextFpaZone()};
  enabled:{model.isChangedZoneOperation()};
}

#vesselSpeedLabel {
  text:"observe.common.vesselSpeed";
  labelFor:{vesselSpeed};
}

#vesselSpeed {
  property:{ActivitySeineDto.PROPERTY_VESSEL_SPEED};
  numberValue:{bean.getVesselSpeed()};
}

#ersIdLabel{
  text:"observe.common.ersId";
  labelFor:{ersId};
}

#resetErsId{
  toolTipText:"observe.content.action.reset.ersId.tip";
  _resetPropertyName:{ActivitySeineDto.PROPERTY_ERS_ID};
}

#ersId {
  _propertyName:{ActivitySeineDto.PROPERTY_ERS_ID};
  text:{getStringValue(bean.getErsId())};
}

#seaSurfaceTemperatureLabel {
  text:"observe.common.seaSurfaceTemperature";
  labelFor:{seaSurfaceTemperature};
}

#seaSurfaceTemperature {
  property:{ActivitySeineDto.PROPERTY_SEA_SURFACE_TEMPERATURE};
  numberValue:{bean.getSeaSurfaceTemperature()};
}

#windLabel {
  text:"observe.common.wind";
  labelFor:{wind};
}

#wind {
  property:{ActivitySeineDto.PROPERTY_WIND};
  selectedItem:{bean.getWind()};
}

#detectionModeLabel {
  text:"observe.common.detectionMode";
  labelFor:{detectionMode};
}

#detectionMode {
  property:{ActivitySeineDto.PROPERTY_DETECTION_MODE};
  selectedItem:{bean.getDetectionMode()};
}

#reasonForNoFishingLabel {
  text:"observe.common.nonCoupSenne";
  labelFor:{reasonForNoFishing};
}

#reasonForNoFishing {
  property:{ActivitySeineDto.PROPERTY_REASON_FOR_NO_FISHING};
  selectedItem:{bean.getReasonForNoFishing()};
}


#observedSystemTab {
  title:{t("observe.storage.activitySeine.tab.observedSystem")};
  icon:{getHandler().getErrorIconIfFalse(model.isObservedSystemTabValid())};
}

#observedSystemPane {
  minimumSize:{new Dimension(10,150)};
}

#observedSystem {
  bean:{bean};
  property: {ActivitySeineDto.PROPERTY_OBSERVED_SYSTEM};
  selected:{(Collection)bean.getObservedSystem()};
  _validatorLabel: {t("observe.common.observedSystem")};
  showListLabel: true;
  universeLabel:{t("observe.storage.activityObservedSystem.availableObservedSystem")};
  selectedLabel:{t("observe.storage.activityObservedSystem.selectedObservedSystem")};
}

#observedSystemDistanceLabel {
  text:"observe.common.observedSystemDistance";
  labelFor:{observedSystemDistance};
}

#observedSystemDistance {
  bean:{bean};
  property: {ActivitySeineDto.PROPERTY_OBSERVED_SYSTEM_DISTANCE};
  numberValue:{bean.getObservedSystemDistance()};
}

#reopen {
  _toolTipText:{t("observe.content.action.reopen.activity.tip")};
}

#close {
  enabled:{!model.isModified() && (model.isHistoricalData() || model.isValid())};
  _toolTipText:{t("observe.action.close.activity.tip")};
}

#closeAndCreate {
  enabled:{!model.isModified() && (model.isHistoricalData() || model.isValid())};
  _text:{t("observe.content.action.closeAndCreate.activity")};
  _toolTipText:{t("observe.content.action.closeAndCreate.activity.tip")};
}

#delete {
  _toolTipText:{t("observe.action.delete.activity.tip")};
}

#addSet {
  enabled:{!model.isModified() && model.isValid()};
  visible:{bean.isSetOperation() && bean.getSetSeine() == null};
  _observeAction:{AddActivitySeineSetUIAction.ACTION_NAME};
}

#addDCP {
  enabled:{!model.isModified() && model.isValid()};
  visible:{bean.isDcpOperation()};
  _observeAction:{AddActivitySeineFloatingObjectUIAction.ACTION_NAME};
}
