package fr.ird.observe.application.swing.ui.tree.actions;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.ird.observe.application.swing.ui.tree.navigation.NavigationTree;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.NavigationTreeNodeSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created on 1/9/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.11
 */
public abstract class NodeChangeActionListener implements ActionListener {

    /** Logger. */
    private static final Log log = LogFactory.getLog(NodeChangeActionListener.class);

    private final String nodeId;

    private final String parentNodeId;

    private final NavigationTree tree;

    public NodeChangeActionListener(NavigationTree tree,
                                    String nodeId,
                                    String parentNodeId) {
        this.nodeId = nodeId;
        this.parentNodeId = parentNodeId;
        this.tree = tree;
    }

    @Override
    public void actionPerformed(ActionEvent event) {

        NavigationTreeNodeSupport node = tree.getSelectedNode();
        NavigationTreeNodeSupport oldParentNode = getParentNode(node);
        NavigationTreeNodeSupport grandParentNode = oldParentNode.getParent();
        NavigationTreeNodeSupport newParentNode = getNewParentNode(grandParentNode, parentNodeId);

        closeNode(node.getId());

        int position = moveNodeToParent(nodeId, parentNodeId, oldParentNode.getId());

        tree.selectNode(newParentNode);

        tree.removeNode(node);

        NavigationTreeNodeSupport newNode = tree.getChild(newParentNode, nodeId);

        if (newNode == null) {

            // create it
            if (log.isInfoEnabled()) {
                log.info("Insert node: ");
            }
            tree.insertNode(newParentNode, node, position);
            newNode = node;
        }

        tree.selectNode(newNode);

    }

    public NavigationTree getTree() {
        return tree;
    }

    protected abstract void closeNode(String nodeId);

    protected abstract NavigationTreeNodeSupport getParentNode(NavigationTreeNodeSupport node);

    protected abstract NavigationTreeNodeSupport getNewParentNode(NavigationTreeNodeSupport grandParentNode, String parentNodeId);

    protected abstract int moveNodeToParent(String nodeId, String parentNodeId, String oldParentNodeId);
}
