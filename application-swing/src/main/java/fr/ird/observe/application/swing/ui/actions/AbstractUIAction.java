/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.actions;

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.ui.ObserveKeyStrokes;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.content.ContentUI;
import org.nuiton.jaxx.runtime.JAXXObject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import java.awt.event.ActionEvent;

import static org.nuiton.i18n.I18n.t;

/**
 * Pour implanter les actions communes.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public abstract class AbstractUIAction extends AbstractAction {

    private static final long serialVersionUID = 1L;

    /** Logger */
    private static final Log log = LogFactory.getLog(AbstractUIAction.class);

    public static final String EDITOR = "editor";
    public static final String CLIENT_PROPERTY_UI = "ui";

    private final ObserveMainUI mainUI;

    public AbstractUIAction(ObserveMainUI mainUI, String actionCommandKey, String label, String shortDescription, String actionIcon, KeyStroke acceleratorKey) {
        super(t(label), UIHelper.getUIManagerActionIcon(actionIcon));
        putValue(ACTION_COMMAND_KEY, actionCommandKey);
        this.mainUI = mainUI;
        ObserveKeyStrokes.addKeyStore(this, acceleratorKey, label, shortDescription);
    }

    public KeyStroke getAcceleratorKey() {
        return (KeyStroke) getValue(ACCELERATOR_KEY);
    }

    public String getActionCommandKey() {
        return (String) getValue(ACTION_COMMAND_KEY);
    }

    public ObserveMainUI getMainUI() {
        return mainUI;
    }

    public void initAction(JComponent ui, AbstractButton editor) {
        editor.setAction(this);
        editor.putClientProperty(CLIENT_PROPERTY_UI, ui);
        //FIXME Attention on introduit un état dans l'action et si l'action est utilisée plusieurs fois dans un même écran
        //FIXME cela va introduire des effets de bord
        putValue(EDITOR, editor);

        KeyStroke keyStroke = getAcceleratorKey();
        if (keyStroke != null) {

            String actionCommandKey = getActionCommandKey();
            ui.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(keyStroke, actionCommandKey);
            ui.getActionMap().put(actionCommandKey, this);
        }
    }

    public void initForMainUi(AbstractButton editor) {
        editor.setAction(this);
        editor.putClientProperty("ui", mainUI);
        //FIXME Attention on introduit un état dans l'action et si l'action est utilisée plusieurs fois dans un même écran
        //FIXME cela va introduire des effets de bord
        putValue(EDITOR, editor);

        KeyStroke keyStroke = getAcceleratorKey();
        if (keyStroke != null) {

            String actionCommandKey = getActionCommandKey();
            mainUI.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(keyStroke, actionCommandKey);
            mainUI.getRootPane().getActionMap().put(actionCommandKey, this);
        }
    }

    public void updateAction(JAXXObject ui, AbstractButton editor) {

        ObserveKeyStrokes.addKeyStroke2(editor, getAcceleratorKey());

        String actionIcon = (String) editor.getClientProperty("actionIcon");
        if (actionIcon != null) {
            Icon icon = UIHelper.getUIManagerActionIcon(actionIcon);
            editor.setIcon(icon);
        }
    }


    protected boolean canExecuteAction() {
        JComponent editor = getEditor();
        if ((editor == null || (editor.isVisible() && editor.isEnabled()))) {
            return true;
        }
        if (log.isInfoEnabled()) {
            log.info("Disabled action: " + getActionCommandKey() + " :: " + this);
        }
        return false;
    }

    private JComponent getEditor() {
        return (JComponent) getValue(EDITOR);
    }

    protected ContentUI<?, ?> getContentUI(ActionEvent e) {
        JComponent c = (JComponent) e.getSource();
        ContentUI<?, ?> ui;

        //FIXME On devrait peut-être toujours se baser sur le contentui manager
        //FIXME et ne plus rien attacher aux editeurs ?
        if (c instanceof ContentUI) {
            ui = (ContentUI<?, ?>) c;
        } else if (c.getParent() instanceof ContentUI) {
            ui = (ContentUI<?, ?>) c.getParent();
        } else {
            ui = (ContentUI<?, ?>) c.getClientProperty("ui");
        }
        if (ui == null) {
            ui = ObserveSwingApplicationContext.get().getContentUIManager().getSelectedContentUI();
        }
        return ui;
    }

}
