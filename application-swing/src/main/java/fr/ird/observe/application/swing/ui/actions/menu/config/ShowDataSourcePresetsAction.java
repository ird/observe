package fr.ird.observe.application.swing.ui.actions.menu.config;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.ObserveUIMode;
import fr.ird.observe.application.swing.ui.actions.AbstractUIAction;
import fr.ird.observe.application.swing.ui.storage.presets.RemotePresetsUI;

import java.awt.event.ActionEvent;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 20/12/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class ShowDataSourcePresetsAction extends AbstractUIAction {

    public static final String ACTION_NAME = ShowDataSourcePresetsAction.class.getName();

    public ShowDataSourcePresetsAction(ObserveMainUI mainUI) {
        super(mainUI, ACTION_NAME, t("observe.action.connexions"), t("observe.action.connexions.tip"), "db-remote", null);
        putValue(MNEMONIC_KEY, (int) 'G');
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        getMainUI().getDataSourcePresets().setContentContainer(new RemotePresetsUI());
        getMainUI().getModel().setMode(ObserveUIMode.PRESETS);

    }
}
