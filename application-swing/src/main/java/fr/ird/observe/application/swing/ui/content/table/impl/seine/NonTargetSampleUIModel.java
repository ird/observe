package fr.ird.observe.application.swing.ui.content.table.impl.seine;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.ird.observe.application.swing.ui.content.table.ContentTableMeta;
import fr.ird.observe.application.swing.ui.content.table.ContentTableModel;
import fr.ird.observe.application.swing.ui.content.table.ContentTableUIModel;
import fr.ird.observe.application.swing.ui.content.table.ObserveContentTableUI;
import fr.ird.observe.services.dto.reference.ReferentialReference;
import fr.ird.observe.services.dto.referential.SizeMeasureTypeDto;
import fr.ird.observe.services.dto.seine.NonTargetLengthDto;
import fr.ird.observe.services.dto.seine.NonTargetLengthHelper;
import fr.ird.observe.services.dto.seine.NonTargetSampleDto;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Created on 9/26/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since XXX
 */
public class NonTargetSampleUIModel extends ContentTableUIModel<NonTargetSampleDto, NonTargetLengthDto> {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_DEFAULT_SIZE_MEASURE_TYPE = "defaultSizeMeasureType";

    private ReferentialReference<SizeMeasureTypeDto> defaultSizeMeasureType;

    public NonTargetSampleUIModel(NonTargetSampleUI ui) {

        super(NonTargetSampleDto.class,
              NonTargetLengthDto.class,
              new String[]{
                      NonTargetSampleDto.PROPERTY_NON_TARGET_LENGTH,
                      NonTargetSampleDto.PROPERTY_COMMENT},
              new String[]{NonTargetLengthDto.PROPERTY_SPECIES,
                      NonTargetLengthDto.PROPERTY_LENGTH,
                      NonTargetLengthDto.PROPERTY_IS_LENGTH_COMPUTED,
                      NonTargetLengthDto.PROPERTY_WEIGHT,
                      NonTargetLengthDto.PROPERTY_IS_WEIGHT_COMPUTED,
                      NonTargetLengthDto.PROPERTY_SEX,
                      NonTargetLengthDto.PROPERTY_COUNT,
                      NonTargetLengthDto.PROPERTY_SPECIES_FATE,
                      NonTargetLengthDto.PROPERTY_ACQUISITION_MODE,
                      NonTargetLengthDto.PROPERTY_PICTURES_REFERENCES
              });

        List<ContentTableMeta<NonTargetLengthDto>> metas = Arrays.asList(
                ContentTableModel.newTableMeta(NonTargetLengthDto.class, NonTargetLengthDto.PROPERTY_SPECIES, false),
                ContentTableModel.newTableMeta(NonTargetLengthDto.class, NonTargetLengthDto.PROPERTY_LENGTH, false),
                ContentTableModel.newTableMeta(NonTargetLengthDto.class, NonTargetLengthDto.PROPERTY_WEIGHT, false),
                ContentTableModel.newTableMeta(NonTargetLengthDto.class, NonTargetLengthDto.PROPERTY_COUNT, false),
                new ContentTableMeta<NonTargetLengthDto>(NonTargetLengthDto.class, NonTargetLengthDto.PROPERTY_SEX, false) {

                    private static final long serialVersionUID = 1L;

                    @Override
                    public boolean isNullValue(ContentTableModel<?, ?> m, NonTargetLengthDto bean, int row) {
                        return bean.getSex() != null;
                    }
                },
                ContentTableModel.newTableMeta(NonTargetLengthDto.class, NonTargetLengthDto.PROPERTY_PICTURES_REFERENCES, false));

        initModel(ui, metas);

    }

    @Override
    protected ContentTableModel<NonTargetSampleDto, NonTargetLengthDto> createTableModel(ObserveContentTableUI<NonTargetSampleDto, NonTargetLengthDto, ?> ui, List<ContentTableMeta<NonTargetLengthDto>> contentTableMetas) {
        return new ContentTableModel<NonTargetSampleDto, NonTargetLengthDto>(ui, contentTableMetas) {
            private static final long serialVersionUID = 1L;

            @Override
            protected Collection<NonTargetLengthDto> getChilds(NonTargetSampleDto bean) {
                return bean.getNonTargetLength();
            }

            @Override
            protected void load(NonTargetLengthDto source, NonTargetLengthDto target) {
                NonTargetLengthHelper.copyNonTargetLengthDto(source, target);
            }

            @Override
            protected void setChilds(NonTargetSampleDto parent, List<NonTargetLengthDto> childs) {
                parent.setNonTargetLength(Sets.newLinkedHashSet(childs));
            }
        };
    }

    public ReferentialReference<SizeMeasureTypeDto> getDefaultSizeMeasureType() {
        return defaultSizeMeasureType;
    }

    public void setDefaultSizeMeasureType(ReferentialReference<SizeMeasureTypeDto> defaultSizeMeasureType) {
        ReferentialReference<SizeMeasureTypeDto> oldValue = getDefaultSizeMeasureType();
        this.defaultSizeMeasureType = defaultSizeMeasureType;
        firePropertyChange(PROPERTY_DEFAULT_SIZE_MEASURE_TYPE, oldValue, defaultSizeMeasureType);
    }
}
