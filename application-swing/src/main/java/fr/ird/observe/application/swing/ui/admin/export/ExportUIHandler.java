/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.admin.export;

import com.google.common.base.Preconditions;
import com.google.common.collect.SetMultimap;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.decoration.DecoratorService;
import fr.ird.observe.application.swing.decoration.ObserveI18nDecoratorHelper;
import fr.ird.observe.application.swing.decoration.decorators.DataReferenceDecorator;
import fr.ird.observe.application.swing.decoration.decorators.ReferentialReferenceDecorator;
import fr.ird.observe.application.swing.ui.ObserveKeyStrokes;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.admin.AdminStep;
import fr.ird.observe.application.swing.ui.admin.AdminTabUIHandler;
import fr.ird.observe.application.swing.ui.admin.AdminUIModel;
import fr.ird.observe.application.swing.ui.admin.config.ConfigUI;
import fr.ird.observe.application.swing.ui.content.ref.usage.UsageForDisplayUI;
import fr.ird.observe.application.swing.ui.tree.selection.SelectionTreeModel;
import fr.ird.observe.application.swing.ui.util.ProgressModel;
import fr.ird.observe.services.dto.IdDto;
import fr.ird.observe.services.dto.longline.TripLonglineDto;
import fr.ird.observe.services.dto.reference.AbstractReference;
import fr.ird.observe.services.dto.reference.DataReference;
import fr.ird.observe.services.dto.reference.ReferenceMap;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import fr.ird.observe.services.service.data.ExportTripRequest;
import fr.ird.observe.services.service.data.ExportTripResult;
import fr.ird.observe.services.service.data.ImportTripRequest;
import fr.ird.observe.services.service.data.ImportTripResult;
import fr.ird.observe.services.service.data.TripManagementService;
import fr.ird.observe.services.service.referential.MissingReferentialResult;
import fr.ird.observe.services.service.referential.ReferentialService;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.runtime.swing.editor.MyDefaultCellEditor;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardState;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.decorator.Decorator;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Le controleur des onglets.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public class ExportUIHandler extends AdminTabUIHandler<ExportUI> implements UIHandler<ExportUI> {

    /** Logger */
    private static final Log log = LogFactory.getLog(ExportUIHandler.class);

    @Override
    public void afterInit(ExportUI ui) {
        super.afterInit(ui);

        if (log.isDebugEnabled()) {
            log.debug(" specialized for [" + ui.getStep() + "] for main ui " + parentUI.getClass().getName() + "@" + System.identityHashCode(this.ui));
        }

        ui.getPrepareAction().setText(t("observe.actions.synchro.prepare.operation", t(ui.getStep().getOperationLabel())));
        ui.getStartAction().setText(t("observe.actions.synchro.launch.operation", t(ui.getStep().getOperationLabel())));

        ObserveKeyStrokes.addKeyStrokeFromMnemonic(ui.getPrepareAction());
        ObserveKeyStrokes.addKeyStrokeFromMnemonic(ui.getStartAction());

        ConfigUI configabUI = (ConfigUI) parentUI.getStepUI(AdminStep.CONFIG);

        configabUI.getCentralSourceInfoLabel().setText(t("observe.action.config.export.required.write.data"));
        configabUI.getCentralSourceLabel().setText(t("observe.action.config.export.required.write.data"));

        // tableau de l'export de données

        final JTable table4 = ui.getTrips();
        table4.setRowHeight(24);

        UIHelper.fixTableColumnWidth(table4, 0, 20);
        UIHelper.fixTableColumnWidth(table4, 3, 20);

        DefaultTableCellRenderer renderer5 = new DefaultTableCellRenderer();

        UIHelper.setI18nTableHeaderRenderer(
                table4,
                n("observe.actions.exportData.table.selected"),
                n("observe.actions.exportData.table.selected.tip"),
                n("observe.actions.exportData.table.program.label"),
                n("observe.actions.exportData.table.program.label.tip"),
                n("observe.actions.exportData.table.trip.label"),
                n("observe.actions.exportData.table.trip.label.tip"),
                n("observe.actions.exportData.table.exist.label"),
                n("observe.actions.exportData.table.exist.label.tip"));

        UIHelper.setTableColumnRenderer(table4, 0, UIHelper.newBooleanTableCellRenderer(renderer5));
        UIHelper.setTableColumnRenderer(table4, 1, UIHelper.newReferentialReferenceDecorateTableCellRenderer(renderer5, ProgramDto.class));
        UIHelper.setTableColumnRenderer(table4, 2, UIHelper.newDecorateTableCellRenderer(renderer5, DataReference.class, DecoratorService.TRIP_CONTEXT));
        UIHelper.setTableColumnRenderer(table4, 3, UIHelper.newBooleanTableCellRenderer(renderer5));
        UIHelper.setTableColumnEditor(table4, 0, MyDefaultCellEditor.newBooleanEditor(false));

        // pour tout selectionner - deselectionner dans l'entete du tableau
        table4.getTableHeader().addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {

                int colIndex = table4.getTableHeader().columnAtPoint(e.getPoint());
                colIndex = table4.convertColumnIndexToModel(colIndex);
                if (colIndex == 0) {
                    TripToExportTableModel model = (TripToExportTableModel) table4.getModel();
                    boolean oldValue = model.isSelectAll();
                    // toggle selectAll
                    model.setSelectAll(!oldValue);
                }
            }
        });
        AdminUIModel model = ui.getModel();
        model.getExportModel().getProgressModel().installUI(ui.getProgressBar());
    }

    public void updateState(ExportUI tabUI, WizardState newState) {
        super.updateState(tabUI, newState);
        if (newState == WizardState.NEED_FIX) {
            // met a jour la liste des marées
            tabUI.tripsModel.init(tabUI.getStepModel().getData());
        }
    }

    public void doPrepareAction() {

        addAdminWorker(ui.getPrepareAction().getToolTipText(), this::doPrepareAction0);
    }

    public void doStartAction() {

        int[] rows = ui.getTripsModel().getSelected();
        ui.getModel().getExportModel().setExportDataSelectedIndex(rows);

        addAdminWorker(ui.getStartAction().getToolTipText(), this::doStartAction0);

    }

    private WizardState doPrepareAction0() throws Exception {

        DecoratorService decoratorProvider = getDecoratorService();
        AdminUIModel model = ui.getModel();
        ExportModel stepModel = model.getExportModel();

        ReferentialReferenceDecorator<ProgramDto> pDecorator = decoratorProvider.getReferentialReferenceDecorator(ProgramDto.class);
        DataReferenceDecorator<TripLonglineDto> tripLonglineDecorator = decoratorProvider.getDataReferenceDecorator(TripLonglineDto.class);
        DataReferenceDecorator<TripSeineDto> tripSeineDecorator = decoratorProvider.getDataReferenceDecorator(TripSeineDto.class);
        stepModel.setProgramDecorator(pDecorator);
        stepModel.setTripSeineDecorator(tripSeineDecorator);
        stepModel.setTripLonglineDecorator(tripLonglineDecorator);

        ObserveSwingDataSource centralSource = model.getConfigModel().getSafeCentralSource(false);

        stepModel.setCentralSource(centralSource);

        if (!centralSource.canWriteData()) {
            // l'utilisateur n'a pas le droit d'écrire sur la base distante
            sendMessage(t("observe.message.can.not.write.data"));
            return WizardState.FAILED;
        }

        ObserveSwingDataSource source = model.getConfigModel().getSafeLocalSource(false);

        stepModel.setSource(source);

        // ouverture de la base distante
        openSource(centralSource);

        // ouverture base source
        openSource(source);

        // il s'agit de la première passe : préparation des données

        sendMessage(t("observe.actions.exportData.message.prepare.data"));

        // récupération des couples (program, marees) sur la base temporaire
        SelectionTreeModel selectionModel = model.getSelectDataModel().getSelectionDataModel();
        stepModel.setData(selectionModel.getSelectedTripEntries());

        List<TripEntry> entries = stepModel.getData();

        if (entries.isEmpty()) {
            // pas de données possible à exporter
            sendMessage(t("observe.actions.exportData.message.not.possible"));
            sendMessage(t("observe.actions.operation.message.done", new Date()));
            return WizardState.CANCELED;
        }

        sendMessage(t("observe.actions.exportData.message.operation.needFix", new Date()));

        // on rend la main à l'ui pour que l'utilisateur selectionne les marees a exporter
        return WizardState.NEED_FIX;
    }


    private WizardState doStartAction0() throws Exception {

        AdminUIModel model = ui.getModel();
        ExportModel stepModel = model.getExportModel();

        boolean insertMissingReferentials = stepModel.isInsertMissingReferentials();

        // on filtre les marées sélectionnées pour export
        List<TripEntry> tripEntries = stepModel.getSelectedTrips();

        Preconditions.checkState(CollectionUtils.isNotEmpty(tripEntries));

        DecoratorService decoratorService = getDecoratorService();
        ReferentialReferenceDecorator<ProgramDto> programDecorator = decoratorService.getReferentialReferenceDecorator(ProgramDto.class);

        int stepCount = 2 + 2 * tripEntries.size();
        if (insertMissingReferentials) {
            stepCount++;
        }

        ProgressModel progressModel = stepModel.getProgressModel();
        progressModel.setStepsCount(stepCount);

        progressModel.incrementsCurrentStep();
        try (ObserveSwingDataSource localDataSource = openSource(stepModel.getSource())) {

            TripManagementService localTripManagementService = localDataSource.newTripManagementService();

            progressModel.incrementsCurrentStep();
            try (ObserveSwingDataSource centralDataSource = openSource(stepModel.getCentralSource())) {

                TripManagementService centralTripManagementService = centralDataSource.newTripManagementService();

                ReferentialService centraltReferentialService = centralDataSource.newReferentialService();

                // chargement du référentiel de la base centrale
                if (log.isInfoEnabled()) {
                    log.info("Chargement du réferentiel de la base centrale.");
                }
                SetMultimap<Class<? extends ReferentialDto>, String> centralSourceReferential =
                        centraltReferentialService.getReferentialIds();

                List<String> tripIds = tripEntries.stream().map(TripEntry::getTripId).collect(Collectors.toList());

                // génération du code sql pour les référentiels manquants
                if (log.isInfoEnabled()) {
                    log.info("Chargement de l'enveloppe de référentiels pour les " + tripIds.size() + " marée(s).");
                }
                MissingReferentialResult result =
                        localDataSource.newReferentialService().computeMissingReferentials(centralSourceReferential, tripIds.toArray(new String[tripIds.size()]));

                if (!insertMissingReferentials && result != null) {

                    // l'export n'est pas possible
                    UIHelper.displayWarning(t("observe.title.can.not.export.data"), t("observe.actions.exportData.error.missingReferentialsId"));
                    sendMessage(t("observe.actions.exportData.error.missingReferentialsId"));
                    return WizardState.CANCELED;
                }

                if (insertMissingReferentials && result != null) {

                    String message = t("observe.message.show.usage.for.missingReferentials");

                    ReferenceMap usages = localDataSource.getReferentialMap(result.getMissingIds());
                    UsageForDisplayUI usagesUI = UsageForDisplayUI.build(message, usages);

                    int reponse = UIHelper.askUser(null,
                                                   t("observe.title.can.not.export.data"),
                                                   usagesUI,
                                                   JOptionPane.WARNING_MESSAGE,
                                                   new Object[]{
                                                           t("observe.choice.confirm.insert"),
                                                           t("observe.choice.cancel")},
                                                   0);

                    if (reponse != 0) {

                        return WizardState.CANCELED;
                    }

                    // insertion des référentiels manquants dans la base centrale
                    if (log.isInfoEnabled()) {
                        log.info("Insertion dans la base centrale des référentiels manquants.");
                    }

                    for (Map.Entry<Class<? extends IdDto>, Set<? extends AbstractReference>> entry : usages.entrySet()) {
                        Class<? extends IdDto> key = entry.getKey();
                        Set<? extends AbstractReference> references = entry.getValue();
                        String type = t(ObserveI18nDecoratorHelper.getTypePluralI18nKey(key));
                        sendMessage(t("observe.actions.exportData.message.add.missing.referentials", type, references.size()));
                        Decorator decorator = decoratorService.getReferenceDecorator(key);
                        for (AbstractReference reference : references) {
                            sendMessage(t("observe.actions.exportData.message.add.missing.referential", decorator.toString(reference)));
                        }
                    }

                    progressModel.incrementsCurrentStep();
                    centraltReferentialService.insertMissingReferentials(result.getSqlCode());

                }

                for (TripEntry tripEntry : tripEntries) {

                    progressModel.incrementsCurrentStep();
                    ExportTripRequest exportTripRequest = new ExportTripRequest(false, tripEntry.getProgramId(), tripEntry.getTripId());
                    ExportTripResult exportTripResult = localTripManagementService.exportTrip(exportTripRequest);
                    logExportResult(n("observe.actions.exportData.message.result.export.trip"),
                                    exportTripResult,
                                    programDecorator,
                                    tripEntry.getProgram(),
                                    tripEntry.getTrip());

                    progressModel.incrementsCurrentStep();
                    ImportTripRequest importTripRequest = new ImportTripRequest(exportTripResult);
                    ImportTripResult importTripResult = centralTripManagementService.importTrip(importTripRequest);
                    logImportResult(n("observe.actions.exportData.message.result.import.trip"),
                                    n("observe.actions.exportData.message.result.delete.trip"),
                                    importTripResult,
                                    programDecorator,
                                    tripEntry.getProgram(),
                                    tripEntry.getTrip());

                }

            }
        }

        sendMessage(t("observe.actions.operation.message.done", new Date()));

        return WizardState.SUCCESSED;
    }
}
