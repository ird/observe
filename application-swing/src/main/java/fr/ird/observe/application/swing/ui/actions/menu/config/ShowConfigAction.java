package fr.ird.observe.application.swing.ui.actions.menu.config;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.configuration.ObserveSwingApplicationConfig;
import fr.ird.observe.application.swing.configuration.ObserveSwingApplicationConfigOption;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.decoration.decorators.ReferentialReferenceDecorator;
import fr.ird.observe.application.swing.ui.ObserveKeyStrokes;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.ObserveUICallback;
import fr.ird.observe.application.swing.ui.ObserveUIMode;
import fr.ird.observe.application.swing.ui.actions.AbstractUIAction;
import fr.ird.observe.services.dto.reference.ReferentialReference;
import fr.ird.observe.services.dto.referential.SpeciesListDto;
import org.nuiton.jaxx.widgets.select.BeanFilterableComboBox;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.renderer.DefaultTableRenderer;
import org.nuiton.config.ConfigOptionDef;
import org.nuiton.decorator.Decorator;
import org.nuiton.jaxx.widgets.config.ConfigCategoryUI;
import org.nuiton.jaxx.widgets.config.ConfigUI;
import org.nuiton.jaxx.widgets.config.ConfigUIHelper;
import org.nuiton.jaxx.widgets.config.model.ConfigUIModelBuilder;
import org.nuiton.jaxx.widgets.config.model.MainCallBackFinalizer;

import javax.swing.DefaultCellEditor;
import javax.swing.Icon;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTable;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.util.Collection;
import java.util.EventObject;
import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Created on 1/17/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.13
 */
public class ShowConfigAction extends AbstractUIAction {

    private static final long serialVersionUID = 3038774900992805790L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(ShowConfigAction.class);

    public static final String ACTION_NAME = ShowConfigAction.class.getSimpleName();

    public ShowConfigAction(ObserveMainUI ui) {
        super(ui, ACTION_NAME, t("observe.action.configuration"), t("observe.action.configuration.tip"), "config", null);
        putValue(MNEMONIC_KEY, (int) 'C');

    }

    @Override
    public void actionPerformed(ActionEvent event) {


        if (log.isInfoEnabled()) {
            log.info("ObServe opening configuration ui...");
        }
        ObserveSwingApplicationContext applicationContext = ObserveSwingApplicationContext.get();
        boolean canContinue = applicationContext.getContentUIManager().closeSelectedContentUI();
        if (canContinue) {

            ObserveSwingDataSource dataSource = applicationContext.getDataSourcesManager().getMainDataSource();

            ObserveMainUI mainUI = getMainUI();
            ObserveSwingApplicationConfig config = applicationContext.getConfig();

            ObserveConfigUIBuilder helper = buildUI(config, dataSource);
            helper.setCloseAction(this::quit);
            ConfigUI configUI = helper.buildUI(mainUI, "observe.config.category.application");

            configUI.getQuit().setMnemonic('Q');
            int tabCount = configUI.getCategories().getTabCount();
            for (int i = 0; i < tabCount; i++) {
                ConfigCategoryUI categoryUI = (ConfigCategoryUI) configUI.getCategories().getComponentAt(i);
                categoryUI.getSave().setMnemonic('E');
                categoryUI.getReset().setMnemonic('A');
                ObserveKeyStrokes.addKeyStrokeFromMnemonic(categoryUI);
            }
            ObserveKeyStrokes.addKeyStrokeFromMnemonic(configUI);
            JPanel jPanel = new JPanel();
            jPanel.setLayout(new BorderLayout());
            jPanel.add(configUI, BorderLayout.CENTER);
            getMainUI().getConfiguration().setContentContainer(jPanel);
            // si on ne met pas ça, à la fermeture la fenetre principale est fermée
            configUI.getHandler().setTopContainer(jPanel);
            configUI.getHandler().setConfigCallBackUICustomizer(callBackUI -> {
                callBackUI.getGo().setMnemonic('A');
                callBackUI.getGo().setText(t("observe.action.apply"));
                callBackUI.getGo().setToolTipText(t("observe.action.apply"));
                ObserveKeyStrokes.addKeyStrokeFromMnemonic(callBackUI.getGo());
            });

            mainUI.getModel().setMode(ObserveUIMode.CONFIGURATION);
        }

    }

    public void quit() {

        ObserveMainUI mainUI = ObserveSwingApplicationContext.get().getMainUI();
        mainUI.getConfiguration().setContentContainer(new JPanel());
        if (ObserveSwingApplicationContext.get().getDataSourcesManager().getMainDataSource() == null) {
            mainUI.getModel().setMode(ObserveUIMode.NO_DB);
        } else {
            mainUI.getModel().setMode(ObserveUIMode.DB);
        }

    }

    protected ObserveConfigUIBuilder buildUI(ObserveSwingApplicationConfig config, ObserveSwingDataSource dataSource) {

        SpeciesListTableCellEditor editor = null;
        SpeciesListsTableCellRenderer renderer = null;

        if (dataSource != null && dataSource.isOpen()) {

            Set<ReferentialReference<SpeciesListDto>> speciesLists = dataSource.getReferentialReferences(SpeciesListDto.class);

            Map<String, ReferentialReference<SpeciesListDto>> speciesListMap = ReferentialReference.splitById(speciesLists);

            ReferentialReferenceDecorator<SpeciesListDto> referenceDecorator = ObserveSwingApplicationContext.get().getDecoratorService().getReferentialReferenceDecorator(SpeciesListDto.class);

            editor = new SpeciesListTableCellEditor(speciesLists, speciesListMap, referenceDecorator);
            renderer = new SpeciesListsTableCellRenderer(speciesListMap, referenceDecorator);

        }

        ObserveConfigUIBuilder helper = new ObserveConfigUIBuilder(config, editor, renderer);

        for (ObserveUICallback callback : ObserveUICallback.values()) {
            helper.registerCallBack(callback);
        }

        helper.setFinalizer(new MainCallBackFinalizer(ObserveUICallback.application.name()));

        helper.setCloseAction(ObserveSwingApplicationContext.get().getContentUIManager()::restartEdit);

        addApplicatinOptions(helper);
        addUiOptions(helper);
        addDataOptions(helper, dataSource);
        addExpertOptions(helper);
        addTechnicalOptions(helper);

        return helper;

    }

    private void addApplicatinOptions(ObserveConfigUIBuilder helper) {

        helper.addCategory(n("observe.config.category.application"),
                           n("observe.config.category.application.description"),
                           ObserveUICallback.application.name());

        helper.addOption(ObserveSwingApplicationConfigOption.VALIDATION_SPEED_ENABLE);
        helper.addOption(ObserveSwingApplicationConfigOption.VALIDATION_SPEED_MAX_VALUE);
        helper.addOption(ObserveSwingApplicationConfigOption.VALIDATION_LENGTH_WEIGHT_ENABLE);

    }

    protected void addUiOptions(ObserveConfigUIBuilder helper) {

        helper.addCategory(n("observe.config.category.ui"),
                           n("observe.config.category.ui.description"),
                           ObserveUICallback.ui.name());

        helper.addOption(ObserveSwingApplicationConfigOption.SHOW_NUMBER_EDITOR_BUTTON);
        helper.addOption(ObserveSwingApplicationConfigOption.AUTO_POPUP_NUMBER_EDITOR);
        helper.addOption(ObserveSwingApplicationConfigOption.SHOW_DATE_TIME_EDITOR_SLIDER);
        helper.addOption(ObserveSwingApplicationConfigOption.SHOW_MNEMONIC);
        helper.addOption(ObserveSwingApplicationConfigOption.COORDINATE_FORMAT);

        helper.addOption(ObserveSwingApplicationConfigOption.FOCUS_BORDER_COLOR);
        helper.addOption(ObserveSwingApplicationConfigOption.NO_FOCUS_BORDER_COLOR);

        helper.addOption(ObserveSwingApplicationConfigOption.DEFAULT_DB_MODE);
        helper.addOption(ObserveSwingApplicationConfigOption.DEFAULT_CREATION_MODE);
        helper.addOption(ObserveSwingApplicationConfigOption.STORE_REMOTE_STORAGE);

        helper.addOption(ObserveSwingApplicationConfigOption.MAP_BACKGROUND_COLOR);
        ObserveSwingApplicationConfig.MAP_LAYERS.forEach(helper::addOption);
        helper.addOption(ObserveSwingApplicationConfigOption.MAP_STYLE_FILE);

    }

    protected void addDataOptions(ObserveConfigUIBuilder helper, ObserveSwingDataSource dataSource) {

        if (dataSource != null && dataSource.isOpen()) {

            helper.addCategory(n("observe.config.category.speciesList.seine"),
                               n("observe.config.category.speciesList.seine.description"),
                               ObserveUICallback.ui.name());

            helper.addSpeciesListOption(ObserveSwingApplicationConfigOption.SPECIES_LIST_SEINE_TARGET_CATCH_ID);
            helper.addSpeciesListOption(ObserveSwingApplicationConfigOption.SPECIES_LIST_SEINE_SCHOOL_ESTIMATE_ID);
            helper.addSpeciesListOption(ObserveSwingApplicationConfigOption.SPECIES_LIST_SEINE_OBJECT_SCHOOL_ESTIMATE_ID);
            helper.addSpeciesListOption(ObserveSwingApplicationConfigOption.SPECIES_LIST_SEINE_NON_TARGET_CATCH_ID);
            helper.addSpeciesListOption(ObserveSwingApplicationConfigOption.SPECIES_LIST_SEINE_OBJECT_OBSERVED_SPECIES_ID);

            helper.addCategory(n("observe.config.category.speciesList.longline"),
                               n("observe.config.category.speciesList.longline.description"),
                               ObserveUICallback.ui.name());

            helper.addSpeciesListOption(ObserveSwingApplicationConfigOption.SPECIES_LIST_LONGLINE_CATCH_ID);
            helper.addSpeciesListOption(ObserveSwingApplicationConfigOption.SPECIES_LIST_LONGLINE_ENCOUNTER_ID);


        }

        helper.addCategory(n("observe.config.category.observation"),
                           n("observe.config.category.observation.description"),
                           ObserveUICallback.ui.name());

        helper.addOption(ObserveSwingApplicationConfigOption.DETAILLED_ACTIVITIES_OBSERVATION);
        helper.addOption(ObserveSwingApplicationConfigOption.NON_TARGET_OBSERVATION);
        helper.addOption(ObserveSwingApplicationConfigOption.BAIT_OBSERVATION);
        helper.addOption(ObserveSwingApplicationConfigOption.MAMMALS_OBSERVATION);
        helper.addOption(ObserveSwingApplicationConfigOption.SAMPLES_OBSERVATION);
        helper.addOption(ObserveSwingApplicationConfigOption.OBJECTS_OBSERVATION);
        helper.addOption(ObserveSwingApplicationConfigOption.BIRDS_OBSERVATION);
        helper.addOption(ObserveSwingApplicationConfigOption.TARGET_DISCARDS_OBSERVATION);

    }

    protected void addExpertOptions(ObserveConfigUIBuilder helper) {

        helper.addCategory(n("observe.config.category.expert"),
                           n("observe.config.category.expert.description"),
                           ObserveUICallback.application.name());

        helper.addOption(ObserveSwingApplicationConfigOption.LOAD_LOCAL_STORAGE);
        helper.addOption(ObserveSwingApplicationConfigOption.BACKUP_USE);
        helper.addOption(ObserveSwingApplicationConfigOption.BACKUP_DELAY);
        helper.addOption(ObserveSwingApplicationConfigOption.BACKUP_AT_CLOSE);

        helper.addOption(ObserveSwingApplicationConfigOption.CHANGE_SYNCHRO_SRC);
        helper.addOption(ObserveSwingApplicationConfigOption.H2_CAN_MIGRATE);
        helper.addOption(ObserveSwingApplicationConfigOption.OBSTUNA_CAN_MIGRATE);
        helper.addOption(ObserveSwingApplicationConfigOption.H2_SERVER_PORT);

        helper.addOption(ObserveSwingApplicationConfigOption.SHOW_MIGRATION_PROGRESSION);
        helper.addOption(ObserveSwingApplicationConfigOption.SHOW_MIGRATION_SQL);
        helper.addOption(ObserveSwingApplicationConfigOption.SHOW_SQL);

    }

    protected void addTechnicalOptions(ObserveConfigUIBuilder helper) {

        helper.addCategory(n("observe.config.category.technical"),
                           n("observe.config.category.technical.description"));

        helper.addOption(ObserveSwingApplicationConfigOption.CONFIG_FILE);
        helper.addOption(ObserveSwingApplicationConfigOption.DATA_DIRECTORY);
        helper.addOption(ObserveSwingApplicationConfigOption.DB_DIRECTORY);
        helper.addOption(ObserveSwingApplicationConfigOption.BACKUP_DIRECTORY);
        helper.addOption(ObserveSwingApplicationConfigOption.IMPORT_DIRECTORY);
        helper.addOption(ObserveSwingApplicationConfigOption.TMP_DIRECTORY);
        helper.addOption(ObserveSwingApplicationConfigOption.VALIDATION_REPORT_DIRECTORY);
        helper.addOption(ObserveSwingApplicationConfigOption.RESOURCES_DIRECTORY);
        helper.addOption(ObserveSwingApplicationConfigOption.SWING_SESSION_FILE);

        helper.addOption(ObserveSwingApplicationConfigOption.H2_LOGIN);
        helper.addOption(ObserveSwingApplicationConfigOption.H2_PASSWORD);

    }

    protected class ObserveConfigUIBuilder extends ConfigUIHelper {

        private final SpeciesListTableCellEditor speciesListTableCellEditor;

        private final SpeciesListsTableCellRenderer speciesListsTableCellRenderer;

        protected ObserveConfigUIBuilder(ObserveSwingApplicationConfig config,
                                         SpeciesListTableCellEditor speciesListTableCellEditor,
                                         SpeciesListsTableCellRenderer speciesListsTableCellRenderer) {
            super(config, config.get(), config.get().getUserConfigFile());
            this.speciesListTableCellEditor = speciesListTableCellEditor;
            this.speciesListsTableCellRenderer = speciesListsTableCellRenderer;
        }

        @Override
        public ConfigUIModelBuilder addOption(ConfigOptionDef def) throws IllegalStateException, NullPointerException {
            return super.addOption(def).setOptionShortLabel(def.getDescription());
        }

        public ConfigUIModelBuilder registerCallBack(ObserveUICallback callback) {

            String name = callback.name();
            String description = callback.getLabel();
            Icon icon = callback.getIcon();
            registerCallBack(name, description, icon, callback);

            return modelBuilder;

        }

        protected ConfigUIModelBuilder addSpeciesListOption(ObserveSwingApplicationConfigOption option) {

            return addOption(option).setOptionEditor(speciesListTableCellEditor)
                                    .setOptionRenderer(speciesListsTableCellRenderer);

        }

    }

    protected class SpeciesListsTableCellRenderer extends DefaultTableRenderer {

        private static final long serialVersionUID = 1L;

        private final Map<String, ReferentialReference<SpeciesListDto>> entityMap;

        private final Decorator<ReferentialReference<SpeciesListDto>> decorator;

        public SpeciesListsTableCellRenderer(Map<String, ReferentialReference<SpeciesListDto>> entityMap, Decorator<ReferentialReference<SpeciesListDto>> decorator) {
            this.entityMap = entityMap;
            this.decorator = decorator;
        }

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value,
                                                       boolean isSelected, boolean hasFocus,
                                                       int row, int column) {

            String speciesListId = String.valueOf(value);
            ReferentialReference<SpeciesListDto> speciesList = entityMap.get(speciesListId);
            return super.getTableCellRendererComponent(table, decorator.toString(speciesList), isSelected, hasFocus, row, column);

        }
    }

    protected class SpeciesListTableCellEditor extends DefaultCellEditor {

        private static final long serialVersionUID = 1L;

        private final Map<String, ReferentialReference<SpeciesListDto>> entityMap;

        private final ReferentialReferenceDecorator<SpeciesListDto> decorator;

        protected SpeciesListTableCellEditor(Collection<ReferentialReference<SpeciesListDto>> entities,
                                             Map<String, ReferentialReference<SpeciesListDto>> entityMap,
                                             ReferentialReferenceDecorator<SpeciesListDto> decorator) {

            super(new JComboBox());

            this.entityMap = entityMap;
            this.decorator = decorator;

            BeanFilterableComboBox<ReferentialReference<SpeciesListDto>> component = new BeanFilterableComboBox<>();
            component.setI18nPrefix("observe.common.");
            component.setShowReset(true);
            component.setBeanType((Class) ReferentialReference.class);

            setClickCountToStart(1);

            editorComponent = component;
            delegate = new DefaultCellEditor.EditorDelegate() {

                private static final long serialVersionUID = 1L;

                @Override
                public void setValue(Object value) {
                    if (value != null && String.class.isInstance(value)) {

                        value = SpeciesListTableCellEditor.this.entityMap.get(String.valueOf(value));
                    }
                    component.setSelectedItem(value);
                }

                @Override
                public Object getCellEditorValue() {
                    String result = null;
                    Object selectedItem = component.getSelectedItem();
                    if (selectedItem != null && ReferentialReference.class.isInstance(selectedItem)) {
                        ReferentialReference reference = (ReferentialReference) selectedItem;
                        result = reference.getId();
                    }
                    return result;
                }

                @Override
                public boolean shouldSelectCell(EventObject anEvent) {
                    if (anEvent instanceof MouseEvent) {
                        MouseEvent e = (MouseEvent) anEvent;
                        return e.getID() != MouseEvent.MOUSE_DRAGGED;
                    }
                    return true;
                }

                @Override
                public boolean stopCellEditing() {
                    if (component.isEditable()) {
                        // Commit edited value.
                        component.getCombobox().actionPerformed(
                                new ActionEvent(SpeciesListTableCellEditor.this, 0, ""));
                    }
                    return super.stopCellEditing();
                }
            };

            component.init(this.decorator, Lists.newArrayList(entities));
        }

    }
}
