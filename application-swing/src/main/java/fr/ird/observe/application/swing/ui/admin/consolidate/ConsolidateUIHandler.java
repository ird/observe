/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.admin.consolidate;

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.ui.ObserveKeyStrokes;
import fr.ird.observe.application.swing.ui.admin.AdminTabUIHandler;
import fr.ird.observe.application.swing.ui.util.ProgressModel;
import fr.ird.observe.services.dto.reference.DataReference;
import fr.ird.observe.services.service.actions.consolidate.ConsolidateActivitySeineDataResult;
import fr.ird.observe.services.service.actions.consolidate.ConsolidateDataService;
import fr.ird.observe.services.service.actions.consolidate.ConsolidateTripSeineDataRequest;
import fr.ird.observe.services.service.actions.consolidate.ConsolidateTripSeineDataResult;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardState;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * Le controleur des onglets.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.5
 */
public class ConsolidateUIHandler extends AdminTabUIHandler<ConsolidateUI> implements UIHandler<ConsolidateUI> {

    /** Logger */
    private static final Log log = LogFactory.getLog(ConsolidateUIHandler.class);

    @Override
    public void afterInit(ConsolidateUI ui) {
        super.afterInit(ui);
        ui.getStartButton().setText(t("observe.actions.synchro.launch.operation", t(ui.getStep().getOperationLabel())));
        ObserveKeyStrokes.addKeyStrokeFromMnemonic(ui.getStartButton());

        getStepModel().getProgressModel().installUI(ui.getProgressBar());
    }

    public ConsolidateModel getStepModel() {
        return getModel().getConsolidateModel();
    }

    public void startAction() {
        addAdminWorker(ui.getStartButton().getToolTipText(), this::doAction);
    }

    public WizardState doAction() {
        if (log.isDebugEnabled()) {
            log.debug(this);
        }

        getStepModel().setSource(getModel().getConfigModel().getSafeLocalSource(false));

        Set<DataReference<?>> trips = getModel().getSelectDataModel().getSelectionDataModel().getSelectedData();
        ImmutableSet<String> tripIds = ImmutableSet.copyOf(trips.stream()
                                                                .filter(DataReference.newTripSeinePredicate())
                                                                .map(DataReference.ID_FUNCTION)
                                                                .collect(Collectors.toSet()));

        ProgressModel progressModel = getStepModel().getProgressModel();
        progressModel.setStepsCount(tripIds.size());

        try (ObserveSwingDataSource dataSource = getStepModel().getSource()) {

            ConsolidateDataService consolidateDataService = dataSource.newConsolidateDataService();

            ImmutableSet.Builder<ConsolidateTripSeineDataResult> resultsBuilder = ImmutableSet.builder();
            for (String tripId : tripIds) {

                progressModel.incrementsCurrentStep();
                ConsolidateTripSeineDataRequest request = new ConsolidateTripSeineDataRequest();
                request.setTripSeineId(tripId);
                request.setFailIfLenghtWeightParameterNotFound(false);
                ConsolidateTripSeineDataResult result = consolidateDataService.consolidateTripSeine(request);
                if (result != null) {

                    String tripSeineLabel = result.getTripSeineLabel();

                    sendMessage(t("observe.actions.consolidate.message.trip", tripSeineLabel));

                    for (ConsolidateActivitySeineDataResult activitySeineDataResult : result.getConsolidateActivitySeineDataResults()) {

                        sendMessage(t("observe.actions.consolidate.message.activity", activitySeineDataResult.getActivitySeineLabel()));

                    }
                    resultsBuilder.add(result);
                }
            }

            ImmutableSet<ConsolidateTripSeineDataResult> results = resultsBuilder.build();

            if (results.isEmpty()) {

                sendMessage(t("observe.actions.consolidate.message.noChanges"));

            } else {

                sendMessage(t("observe.actions.consolidate.message.save.changes", results.size()));
            }

            sendMessage(t("observe.actions.consolidate.message.operation.done", new Date()));

        }

        return WizardState.SUCCESSED;

    }
}
