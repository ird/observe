<!--
  #%L
  ObServe :: Application Swing
  %%
  Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

<fr.ird.observe.application.swing.ui.content.ContentUI
        superGenericType='FloatingObjectTransmittingBuoyDto, FloatingObjectTransmittingBuoyOperationUI'
        contentTitle='{n("observe.content.floatingObjectTransmittingBuoyOperation.title")}'>

  <import>
    fr.ird.observe.services.dto.seine.FloatingObjectTransmittingBuoyDto
    fr.ird.observe.services.dto.seine.TransmittingBuoyDto
    fr.ird.observe.common.constants.seine.Ownership
    fr.ird.observe.common.constants.seine.TypeTransmittingBuoyOperation
    fr.ird.observe.services.dto.referential.CountryDto
    fr.ird.observe.services.dto.reference.ReferentialReference
    fr.ird.observe.services.dto.referential.seine.TransmittingBuoyOperationDto
    fr.ird.observe.services.dto.referential.seine.TransmittingBuoyTypeDto
    fr.ird.observe.application.swing.ui.actions.content.ResetEditUIAction
    fr.ird.observe.application.swing.ui.actions.content.SaveEditUIAction
    fr.ird.observe.application.swing.ui.actions.global.ResetDataGlobalUIAction
    fr.ird.observe.application.swing.ui.actions.global.SaveDataGlobalUIAction
    fr.ird.observe.application.swing.ui.util.JComment

    org.nuiton.jaxx.widgets.select.BeanComboBox

    java.awt.Dimension

    static fr.ird.observe.application.swing.ui.UIHelper.getStringValue
    static org.nuiton.i18n.I18n.n
  </import>

  <!-- model -->
  <FloatingObjectTransmittingBuoyOperationUIModel id='model'/>

  <!-- edit bean -->
  <FloatingObjectTransmittingBuoyDto id='bean'/>

  <!-- transmittingBuoy1 edit bean -->
  <TransmittingBuoyDto id='transmittingBuoy1' initializer='new TransmittingBuoyDto()'/>

  <!-- transmittingBuoy2 edit bean -->
  <TransmittingBuoyDto id='transmittingBuoy2' initializer='new TransmittingBuoyDto()'/>

  <!-- validator -->
  <BeanValidator id='validator' errorTableModel='{getErrorTableModel()}' context='ui-update-objectOperation'
                 beanClass='fr.ird.observe.services.dto.seine.FloatingObjectTransmittingBuoyDto' autoField='true'>
  </BeanValidator>

  <BeanValidator id='validatorBalise1' beanClass='fr.ird.observe.services.dto.seine.TransmittingBuoyDto'
                 errorTableModel='{getErrorTableModel()}' parentValidator='{validator}' context='ui-update'>
    <field name="transmittingBuoyType" component="transmittingBuoyType1"/>
    <field name="transmittingBuoyOperation" component="transmittingBuoyOperation1"/>
    <field name="ownership" component="ownership1"/>
    <field name="code" component="code1"/>
    <field name="country" component="country1"/>
  </BeanValidator>

  <BeanValidator id='validatorBalise2' beanClass='fr.ird.observe.services.dto.seine.TransmittingBuoyDto'
                 errorTableModel='{getErrorTableModel()}' parentValidator='{validator}' context='ui-update'>
    <field name="transmittingBuoyType" component="transmittingBuoyType2"/>
    <field name="transmittingBuoyOperation" component="transmittingBuoyOperation2"/>
    <field name="ownership" component="ownership2"/>
    <field name="code" component="code2"/>
    <field name="country" component="country2"/>
  </BeanValidator>


  <!-- formulaire -->
  <JPanel id="body" layout='{new BorderLayout()}'>


    <Table fill='both' constraints="BorderLayout.NORTH">
      <row>
        <cell anchor='west'>
          <JLabel id='typeOperationLabel'/>
        </cell>
        <cell anchor='east' weightx="1" fill="both">
          <EnumEditor id='typeOperation' genericType='TypeTransmittingBuoyOperation'
                      constructorParams='TypeTransmittingBuoyOperation.class'
                      onItemStateChanged='if(event.getStateChange()==ItemEvent.SELECTED &amp;&amp; model.isEditing()) { getHandler().changeTypeOperation(typeOperation.getSelectedItem(), true); model.setModified(true); }'/>
        </cell>
      </row>
    </Table>

    <JPanel id="transmittingBuoys" constraints="BorderLayout.CENTER"/>

    <JComment id='comment' constraints="BorderLayout.SOUTH"/>

  </JPanel>

  <!-- actions -->
  <Table id="actions" fill='both' weightx='1' insets='0'>
    <row>
      <cell weightx="0.5">
        <JButton id='reset'/>
      </cell>
      <cell weightx="0.5">
        <JButton id='save'/>
      </cell>
    </row>
  </Table>

  <JPanel id="invisible">

    <JLabel id='noBaliseEditor'/>

    <Table id='transmittingBuoy1Editor' fill="horizontal">
      <row>
        <cell anchor='west'>
          <JLabel id='transmittingBuoyOperation1Label'/>
        </cell>
        <cell anchor='east' weightx="1" fill="both">
          <BeanComboBox id='transmittingBuoyOperation1' constructorParams='this'
                        genericType='ReferentialReference&lt;TransmittingBuoyOperationDto&gt;'
                        _entityClass='TransmittingBuoyOperationDto.class'/>
        </cell>
      </row>
      <row>
        <cell anchor='west'>
          <JLabel id='transmittingBuoyType1Label'/>
        </cell>
        <cell anchor='east' weightx="1" fill="both">
          <BeanComboBox id='transmittingBuoyType1' constructorParams='this'
                        genericType='ReferentialReference&lt;TransmittingBuoyTypeDto&gt;'
                        _entityClass='TransmittingBuoyTypeDto.class'/>
        </cell>
      </row>
      <row>
        <cell anchor='west'>
          <JLabel id='ownership1Label'/>
        </cell>
        <cell anchor='east' weightx="1" fill="both">
          <EnumEditor id='ownership1'
                      constructorParams='Ownership.class'
                      onItemStateChanged='transmittingBuoy1.setOwnership((Ownership) ownership1.getSelectedItem())'/>
        </cell>
      </row>
      <row>
        <cell anchor='west'>
          <JLabel id='country1Label'/>
        </cell>
        <cell anchor='east' weightx="1" fill="both">
          <BeanComboBox id='country1' constructorParams='this' genericType='ReferentialReference&lt;CountryDto&gt;'
                        _entityClass='CountryDto.class'/>
        </cell>
      </row>
      <row>
        <cell anchor='west'>
          <JLabel id='code1Label'/>
        </cell>
        <cell anchor='east' weightx="1" fill="both">
          <JTextField id='code1' onKeyReleased='transmittingBuoy1.setCode(code1.getText())'/>
        </cell>
      </row>
      <row>
        <cell anchor='west'>
          <JLabel id='brand1Label'/>
        </cell>
        <cell anchor='east' weightx="1" fill="both">
          <JTextField id='brand1' onKeyReleased='transmittingBuoy1.setBrand(brand1.getText())'/>
        </cell>
      </row>
    </Table>

    <Table id='transmittingBuoy2Editor' fill="horizontal">
      <row>
        <cell anchor='west'>
          <JLabel id='transmittingBuoyOperation2Label'/>
        </cell>
        <cell anchor='east' weightx="1" fill="both">
          <BeanComboBox id='transmittingBuoyOperation2' constructorParams='this'
                        genericType='ReferentialReference&lt;TransmittingBuoyOperationDto&gt;'
                        _entityClass='TransmittingBuoyOperationDto.class'/>
        </cell>
      </row>
      <row>
        <cell anchor='west'>
          <JLabel id='transmittingBuoyType2Label'/>
        </cell>
        <cell anchor='east' weightx="1" fill="both">
          <BeanComboBox id='transmittingBuoyType2' constructorParams='this'
                        genericType='ReferentialReference&lt;TransmittingBuoyTypeDto&gt;'
                        _entityClass='TransmittingBuoyTypeDto.class'/>
        </cell>
      </row>
      <row>
        <cell anchor='west'>
          <JLabel id='ownership2Label'/>
        </cell>
        <cell anchor='east' weightx="1" fill="both">
          <EnumEditor id='ownership2' constructorParams='Ownership.class'
                      onItemStateChanged='transmittingBuoy2.setOwnership((Ownership) ownership2.getSelectedItem())'/>
        </cell>
      </row>
      <row>
        <cell anchor='west'>
          <JLabel id='country2Label'/>
        </cell>
        <cell anchor='east' weightx="1" fill="both">
          <BeanComboBox id='country2' constructorParams='this' genericType='ReferentialReference&lt;CountryDto&gt;'
                        _entityClass='CountryDto.class'/>
        </cell>
      </row>
      <row>
        <cell anchor='west'>
          <JLabel id='code2Label'/>
        </cell>
        <cell anchor='east' weightx="1" fill="both">
          <JTextField id='code2' onKeyReleased='transmittingBuoy2.setCode(code2.getText())'/>
        </cell>
      </row>
      <row>
        <cell anchor='west'>
          <JLabel id='brand2Label'/>
        </cell>
        <cell anchor='east' weightx="1" fill="both">
          <JTextField id='brand2' onKeyReleased='transmittingBuoy2.setBrand(brand2.getText())'/>
        </cell>
      </row>
    </Table>

  </JPanel>
</fr.ird.observe.application.swing.ui.content.ContentUI>
