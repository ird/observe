package fr.ird.observe.application.swing.ui.actions.content;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ui.ObserveKeyStrokes;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.content.ContentUI;
import fr.ird.observe.application.swing.ui.tree.navigation.NavigationTree;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.NavigationTreeNodeSupport;
import fr.ird.observe.services.dto.longline.SetLonglineDto;

import static fr.ird.observe.application.swing.ui.content.ContentUIHandler.getTreeHelper;
import static org.nuiton.i18n.I18n.t;

/**
 * Created on 06/12/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class AddActivityLonglineSetUIAction extends AbstractContentUIAction {

    public static final String ACTION_NAME = AddActivityLonglineSetUIAction.class.getSimpleName();

    public AddActivityLonglineSetUIAction(ObserveMainUI mainUI) {
        super(mainUI, ACTION_NAME, t("observe.action.add.setLongline"), t("observe.action.add.setLongline.tip"), "add", ObserveKeyStrokes.KEY_STROKE_ADD_SET_LONGLINE);
    }

    @Override
    protected void actionPerformed(ContentUI<?, ?> contentUI) {

        NavigationTree treeHelper = getTreeHelper(contentUI);
        NavigationTreeNodeSupport parentNode = treeHelper.getSelectedNode();
        treeHelper.addUnsavedNode(parentNode, SetLonglineDto.class);

    }
}
