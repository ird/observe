package fr.ird.observe.application.swing.ui.actions.menu.navigation;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.db.DataContext;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.tree.navigation.NavigationTree;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.NavigationTreeNodeSupport;

import java.util.Optional;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 02/12/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class GotoOpenTripAction extends GotoActionSupport {

    public static final String ACTION_NAME = "GotoOpenTripAction";

    public GotoOpenTripAction(ObserveMainUI mainUI) {
        super(mainUI,
              ACTION_NAME,
              DataContext.PROPERTY_OPEN_TRIP,
              (int) 'M',
              t("observe.content.action.goto.open.trip.short"),
              t("observe.content.action.goto.open.trip.tip"),
              "navigation.fr.ird.observe.services.dto.TripDto");
    }

    @Override
    protected boolean computeIsOpen(DataContext dataContext) {
        return dataContext.isOpenTrip();
    }

    @Override
    protected Optional<NavigationTreeNodeSupport> getNodeToSelect(NavigationTree navigation, DataContext dataContext) {
        return Optional.of(navigation.getTripNode(dataContext.getOpenProgramId(), dataContext.getOpenTripId()));
    }

}
