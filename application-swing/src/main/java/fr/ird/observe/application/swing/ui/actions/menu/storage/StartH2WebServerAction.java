package fr.ird.observe.application.swing.ui.actions.menu.storage;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.actions.AbstractUIAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.h2.tools.Server;

import java.awt.event.ActionEvent;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 1/17/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.13
 */
public class StartH2WebServerAction extends AbstractUIAction {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(StartH2WebServerAction.class);

    public static final String ACTION_NAME = StartH2WebServerAction.class.getSimpleName();

    public StartH2WebServerAction(ObserveMainUI ui) {

        super(ui, ACTION_NAME, t("observe.action.start.h2.web.server"), t("observe.action.start.h2.web.server.tip"), "db-start-server", null);
        putValue(MNEMONIC_KEY, (int) 'W');

    }

    @Override
    public void actionPerformed(ActionEvent event) {

        ObserveMainUI ui = getMainUI();
        if (ui.getModel().isH2WebServer()) {

            if (log.isWarnEnabled()) {
                log.warn("Can not start h2 web server... (web server already found)");
            }

        } else {

            if (log.isInfoEnabled()) {
                log.info("Will start h2 web server ...");
            }

            try {

                Server server = Server.createWebServer("-webAllowOthers");
                server.start();
                Server.openBrowser("http://localhost:" + server.getPort());

                // On mémorise l'instance du server dans le contexte applicatif afin de pouvoir la récupérer plus tard,
                // par exemple lorsque l'on souhaitera arrêter le server.
                ObserveSwingApplicationContext.get().setH2WebServer(server);

                ui.getModel().setH2WebServer(true);

            } catch (Exception e) {

                UIHelper.handlingError("Could not start h2 web server ", e);
            }

        }

    }

}
