package fr.ird.observe.application.swing.ui.tree.navigation.nodes;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ui.content.ContentUI;
import fr.ird.observe.services.dto.reference.ReferentialReference;
import fr.ird.observe.services.dto.referential.ProgramDto;
import org.apache.commons.collections4.EnumerationUtils;

import java.util.Iterator;

/**
 * Created on 14/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class RootNavigationTreeNode extends StringNavigationTreeNodeSupport implements Iterable<ReferenceNavigationTreeNodeSupport<ProgramDto, ReferentialReference<ProgramDto>>> {

    public RootNavigationTreeNode() {
        super("<ROOT>", true);
        setLoaded(true);
    }

    @Override
    public String getId() {
        return null;
    }

    @Override
    public Class<? extends ContentUI<?, ?>> getContentClass() {
        return null;
    }

    @Override
    public Iterator<ReferenceNavigationTreeNodeSupport<ProgramDto, ReferentialReference<ProgramDto>>> iterator() {
        return (Iterator) EnumerationUtils.toList(children()).stream().filter(c -> c instanceof ReferenceNavigationTreeNodeSupport).iterator();
    }
}
