package fr.ird.observe.application.swing.ui.tree.navigation;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.NavigationTreeNodeSupport;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.RootNavigationTreeNode;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.WithChildsToReload;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.longline.ProgramLonglineNavigationTreeNode;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.referential.ReferentialsCommonNavigationTreeNode;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.referential.ReferentialsLonglineNavigationTreeNode;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.referential.ReferentialsSeineNavigationTreeNode;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.seine.ProgramSeineNavigationTreeNode;
import fr.ird.observe.services.dto.longline.TripLonglineDto;
import fr.ird.observe.services.dto.longline.TripLonglineHelper;
import fr.ird.observe.services.dto.reference.DataReference;
import fr.ird.observe.services.dto.reference.ReferentialReference;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.referential.ProgramHelper;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import fr.ird.observe.services.dto.seine.TripSeineHelper;
import fr.ird.observe.services.service.data.NavigationRequest;
import fr.ird.observe.services.service.data.NavigationResult;
import fr.ird.observe.services.service.data.NavigationService;

import javax.swing.tree.DefaultTreeModel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created on 14/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class NavigationTreeModel extends DefaultTreeModel {

    private boolean loadSeine = true;
    private boolean loadLongline = true;
    private boolean loadReferential = true;
    private boolean loadEmptyProgram = true;

    public NavigationTreeModel() {
        super(new RootNavigationTreeNode());
    }

    public NavigationTreeNodeSupport<?> findNode(NavigationTreeNodeSupport<?> node, Object... ids) {
        NavigationTreeNodeSupport<?> result = null;
        for (Object id : ids) {

            if (id instanceof String) {
                result = node.findChildById((String) id);
            } else if (id instanceof Class) {
                result = node.findChildByType((Class) id);

            }
            if (result == null) {

                // un des noeud n'a pas ete trouve, on sortReferential
                break;
            }
            node = result;
        }
        return result;
    }

    public NavigationTreeNodeSupport<?> findNodeByType(NavigationTreeNodeSupport<?> node, Class<?> nodeType) {
        return node.findChildByType(nodeType);
    }

    public void populate() {

        NavigationService navigationService = ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newNavigationService();

        NavigationRequest navigationRequest = toNavigationRequest();
        NavigationResult navigationResult = navigationService.getNavigation(navigationRequest);

        List<ReferentialReference<ProgramDto>> programs = new ArrayList<>(navigationResult.getPrograms());
        programs = ProgramHelper.sort(programs);

        RootNavigationTreeNode root = new RootNavigationTreeNode();

        Map<ReferentialReference<ProgramDto>, Collection<DataReference<TripLonglineDto>>> tripsLonglineByProgram = navigationResult.getTripsLonglineByProgram().asMap();
        Map<ReferentialReference<ProgramDto>, Collection<DataReference<TripSeineDto>>> tripsSeineByProgram = navigationResult.getTripsSeineByProgram().asMap();
        for (ReferentialReference<ProgramDto> program : programs) {

            if (ProgramHelper.isProgramLongline(program)) {

                List<DataReference<TripLonglineDto>> references = new ArrayList<>(tripsLonglineByProgram.getOrDefault(program, Collections.emptyList()));
                references = TripLonglineHelper.sort(references);

                ProgramLonglineNavigationTreeNode programNode = new ProgramLonglineNavigationTreeNode(program, references);
                root.add(programNode);
            } else if (ProgramHelper.isProgramSeine(program)) {

                List<DataReference<TripSeineDto>> references = new ArrayList<>(tripsSeineByProgram.getOrDefault(program, Collections.emptyList()));
                references = TripSeineHelper.sort(references);

                ProgramSeineNavigationTreeNode programNode = new ProgramSeineNavigationTreeNode(program, references);
                root.add(programNode);
            }

        }

        if (isLoadReferential()) {
            root.add(new ReferentialsCommonNavigationTreeNode());
            root.add(new ReferentialsLonglineNavigationTreeNode());
            root.add(new ReferentialsSeineNavigationTreeNode());
        }

        setRoot(root);
    }

    private NavigationRequest toNavigationRequest() {
        NavigationRequest navigationRequest = new NavigationRequest();
        navigationRequest.setLoadLongline(isLoadLongline());
        navigationRequest.setLoadSeine(isLoadSeine());
        navigationRequest.setLoadEmptyProgram(isLoadEmptyProgram());
        return navigationRequest;
    }

    @Override
    public RootNavigationTreeNode getRoot() {
        return (RootNavigationTreeNode) super.getRoot();
    }

    public boolean isLoadSeine() {
        return loadSeine;
    }

    public void setLoadSeine(boolean loadSeine) {
        this.loadSeine = loadSeine;
    }

    public boolean isLoadLongline() {
        return loadLongline;
    }

    public void setLoadLongline(boolean loadLongline) {
        this.loadLongline = loadLongline;
    }

    public boolean isLoadReferential() {
        return loadReferential;
    }

    public void setLoadReferential(boolean loadReferential) {
        this.loadReferential = loadReferential;
    }

    public boolean isLoadEmptyProgram() {
        return loadEmptyProgram;
    }

    public void setLoadEmptyProgram(boolean loadEmptyProgram) {
        this.loadEmptyProgram = loadEmptyProgram;
    }

    public <N extends NavigationTreeNodeSupport> void reload(N node) {
        node.reload();
        super.reload(node);
    }

    public <N extends NavigationTreeNodeSupport> void open(N node) {
        if (node instanceof WithChildsToReload) {
            boolean updated = node.populateChilds();
            if (updated) {
                reload(node);
            }
        }
    }

}
