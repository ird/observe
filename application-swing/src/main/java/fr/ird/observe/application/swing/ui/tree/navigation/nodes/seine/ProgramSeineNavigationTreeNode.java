package fr.ird.observe.application.swing.ui.tree.navigation.nodes.seine;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ui.content.list.impl.seine.TripSeinesUI;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.ReferenceNavigationTreeNodeSupport;
import fr.ird.observe.services.dto.reference.DataReference;
import fr.ird.observe.services.dto.reference.ReferentialReference;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import org.apache.commons.collections4.EnumerationUtils;

import java.util.Collection;
import java.util.Iterator;

/**
 * Created on 14/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class ProgramSeineNavigationTreeNode extends ReferenceNavigationTreeNodeSupport<ProgramDto, ReferentialReference<ProgramDto>> implements Iterable<TripSeineNavigationTreeNode> {

    public ProgramSeineNavigationTreeNode(ReferentialReference<ProgramDto> data, Collection<DataReference<TripSeineDto>> trips) {
        super(data, true);
        for (DataReference<TripSeineDto> trip : trips) {
            add(new TripSeineNavigationTreeNode(trip));
        }
    }

    @Override
    public void reload() {

    }

    @Override
    public String getText() {
        return super.getText() + " (" + getChildCount() + ")";
    }

    @Override
    public Class<TripSeinesUI> getContentClass() {
        return TripSeinesUI.class;
    }


    @Override
    public boolean isOpen() {
        // le programme est ouvert si l'une de ses marées est ouverte
        for (TripSeineNavigationTreeNode node : this) {
            if (node.isOpen()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator<TripSeineNavigationTreeNode> iterator() {
        return EnumerationUtils.toList(children()).iterator();
    }
}
