/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.admin;

import fr.ird.observe.application.swing.I18nEnumHelper;
import fr.ird.observe.application.swing.ObserveRunner;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.decoration.DecoratorService;
import fr.ird.observe.application.swing.decoration.decorators.ReferentialReferenceDecorator;
import fr.ird.observe.application.swing.ui.admin.resume.ShowResumeUI;
import fr.ird.observe.services.dto.reference.DataReference;
import fr.ird.observe.services.dto.reference.ReferentialReference;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.service.BabModelVersionException;
import fr.ird.observe.services.service.DatabaseConnexionNotAuthorizedException;
import fr.ird.observe.services.service.DatabaseNotFoundException;
import fr.ird.observe.services.service.data.DeleteTripResult;
import fr.ird.observe.services.service.data.ExportTripResult;
import fr.ird.observe.services.service.data.ImportTripResult;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardState;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.StringUtil;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.concurrent.Callable;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Le controleur des onglets.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public class AdminTabUIHandler<U extends AdminTabUI> {

    /** Logger */
    private static final Log log = LogFactory.getLog(AdminTabUIHandler.class);

    protected U ui;

    protected AdminUI parentUI;

    /** Service de decoration. */
    private DecoratorService decoratorService;

    public void beforeInit(U ui) {
        this.ui = ui;
        this.parentUI = ui.getContextValue(AdminUI.class, "parent");
    }

    public void afterInit(U ui) {
        AdminUIModel model = ui.getModel();
        ui.getModel().addPropertyChangeListener(AdminUIModel.STEP_PROPERTY_NAME, e -> ui.getProgress().setString(getProgressString(model.getStepIndex(model.getStep()), model.getSteps().size())));
        model.addPropertyChangeListener(AdminUIModel.STEPS_PROPERTY_NAME, e -> ui.getProgress().setString(getProgressString(model.getStepIndex(model.getStep()), model.getSteps().size())));

        AdminStep step = ui.getStep();

        //FIXME ? ui.addPropertyChangeListener(parentUI);

        if (log.isDebugEnabled()) {
            log.debug("common for [" + step + "] for main ui " + parentUI.getClass().getName() + "@" + System.identityHashCode(ui));
        }

        String stepLabel = I18nEnumHelper.getLabel(step);
        ui.getRUNNING_label().setText(t("observe.actions.operation.message.running", stepLabel));
        ui.SUCCESSED_label.setText(t("observe.actions.operation.message.successed", stepLabel));
        ui.NEED_FIX_label.setText(t("observe.actions.operation.message.needFix", stepLabel));
        ui.CANCELED_label.setText(t("observe.actions.operation.message.canceled", stepLabel));
        ui.FAILED_label.setText(t("observe.actions.operation.message.failed", stepLabel));
        ui.progression.setVisible(true);
        ui.progressionTop.setVisible(true);
    }

    public U getUi() {
        return ui;
    }

    public AdminUIModel getModel() {
        return ui.getModel();
    }

    public final DecoratorService getDecoratorService() {
        if (decoratorService == null) {
            decoratorService = ObserveSwingApplicationContext.get().getDecoratorService();
        }
        return decoratorService;
    }

    public void updateState(AdminTabUI ui, WizardState newState) {
        if (newState == null) {
            newState = WizardState.PENDING;
        }
        ui.getContentLayout().show(ui.content, newState.name());
        switch (newState) {
            case PENDING:
                ui.description.setText(t(ui.getStep().getDescription()));
                return;
            case RUNNING:
                ui.RUNNING_progressionPane.getViewport().setView(ui.progression);
                ui.RUNNING_progressionPane.setColumnHeaderView(ui.progressionTop);
                break;
            case SUCCESSED:
                ui.SUCCESSED_progressionPane.getViewport().setView(ui.progression);
                ui.SUCCESSED_progressionPane.setColumnHeaderView(ui.progressionTop);
                break;
            case FAILED:
                Exception e = ui.getStepModel().getError();
                StringWriter w = new StringWriter();
                e.printStackTrace(new PrintWriter(w));
                ui.addMessage(ui.getStep(), w.toString());
                ui.FAILED_progressionPane.getViewport().setView(ui.progression);
                ui.FAILED_progressionPane.setColumnHeaderView(ui.progressionTop);
                break;
            case NEED_FIX:
                break;
            case CANCELED:
                ui.CANCELED_progressionPane.getViewport().setView(ui.progression);
                ui.CANCELED_progressionPane.setColumnHeaderView(ui.progressionTop);
                break;
        }
    }

    public void sendMessage(String message) {

        AdminStep step = ui.getStep();
        ui.addMessage(step, message);
        ShowResumeUI resumeUI = (ShowResumeUI) parentUI.getStepUI(AdminStep.SHOW_RESUME);
        if (resumeUI != null) {
            resumeUI.addMessage(step, message);
        }
    }

    public String getProgressString(int currentStep, int nbStep) {
        String txt = "";
        AdminStep step = ui.getStep();
        if (step != null) {
            txt = n("observe.storage.step.label");
            txt = t(txt, currentStep + 1, nbStep, I18nEnumHelper.getLabel(step));
        }
        return txt;
    }

    protected ObserveSwingDataSource openSource(ObserveSwingDataSource service) throws DatabaseConnexionNotAuthorizedException, DatabaseNotFoundException, BabModelVersionException {
        if (!service.isOpen()) {
            service.open();
        }
        return service;
    }

    public void addAdminWorker(String label, Callable<WizardState> callable) {
        AdminActionWorker worker = AdminActionWorker.newWorker(this, label, callable);
        ObserveRunner.getActionExecutor().addAction(worker);
    }

    public void addMessage(AdminStep step, String text) {
        ui.getProgression().append(text + "\n");
    }

    protected void logExportResult(String i18nKey,
                                   ExportTripResult tripResult,
                                   ReferentialReferenceDecorator<ProgramDto> programDecorator,
                                   ReferentialReference<ProgramDto> program,
                                   DataReference trip) {

        String message = sendLogResultMessage(i18nKey, programDecorator, program, trip, tripResult.getTime());
        if (log.isInfoEnabled()) {
            log.info(message);
        }

    }


    protected void logImportResult(String importI18nKey,
                                   String deleteI18nKey,
                                   ImportTripResult tripResult,
                                   ReferentialReferenceDecorator<ProgramDto> programDecorator,
                                   ReferentialReference<ProgramDto> program,
                                   DataReference trip) {

        if (tripResult.isDeleted()) {

            String message = sendLogResultMessage(deleteI18nKey, programDecorator, program, trip, tripResult.getDeleteTime());
            if (log.isInfoEnabled()) {
                log.info(message);
            }

        }

        if (tripResult.isImported()) {

            String message = sendLogResultMessage(importI18nKey, programDecorator, program, trip, tripResult.getImportTime());
            if (log.isInfoEnabled()) {
                log.info(message);
            }

        }

    }

    protected void logDeleteResult(String deleteI18nKey,
                                   DeleteTripResult tripResult,
                                   ReferentialReferenceDecorator<ProgramDto> programDecorator,
                                   ReferentialReference<ProgramDto> program,
                                   DataReference trip) {

        String message = sendLogResultMessage(deleteI18nKey, programDecorator, program, trip, tripResult.getTime());
        if (log.isInfoEnabled()) {
            log.info(message);
        }

    }

    protected String sendLogResultMessage(String i18nKey,
                                          ReferentialReferenceDecorator<ProgramDto> programDecorator,
                                          ReferentialReference<ProgramDto> program,
                                          DataReference trip,
                                          long time) {

        DecoratorService decoratorService = getDecoratorService();
        String programStr = programDecorator.toString(program);
        String tripStr = decoratorService.getTripReferenceDecorator(trip).toString(trip);
        String timeStr = StringUtil.convertTime(time);
        String message = StringUtils.leftPad(timeStr, 20) + " - " + t(i18nKey, programStr, tripStr);
        sendMessage(message);
        return message;
    }

}
