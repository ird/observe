package fr.ird.observe.application.swing.ui.content.table.impl.seine;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.db.constants.DataContextType;
import fr.ird.observe.application.swing.ui.content.table.ContentTableUI;
import fr.ird.observe.application.swing.ui.content.table.ContentTableUIHandler;
import fr.ird.observe.services.dto.IdDto;
import fr.ird.observe.services.dto.reference.ReferentialReference;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.seine.TargetLengthDto;

import java.beans.PropertyChangeListener;

/**
 * Created on 31/10/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public abstract class AbstractSampleUIHandler<E extends IdDto, D extends IdDto, U extends ContentTableUI<E, D, U>> extends ContentTableUIHandler<E, D, U> {

    /**
     * Ecoute les modifications de la propriété {@link TargetLengthDto#getWeight()},
     * et repasser alors le flag {@link TargetLengthDto#isIsWeightComputed()} à
     * {@code false}.
     *
     * @since 3.0
     */
    protected final PropertyChangeListener weightChanged;

    /**
     * Ecoute les modifications de la propriété {@link TargetLengthDto#getLength()},
     * et repasser alors le flag {@link TargetLengthDto#isIsLengthComputed()} à
     * {@code false}.
     *
     * @since 3.0
     */
    protected final PropertyChangeListener lengthChanged;

    /**
     * Ecoute les modifications de la propriété {@link TargetLengthDto#getLength()},
     * et repasser alors le flag {@link TargetLengthDto#isIsLengthComputed()} à
     * {@code false}.
     *
     * @since 3.0
     */
    protected final PropertyChangeListener speciesChanged;

    protected AbstractSampleUIHandler() {
        super(DataContextType.SetSeine);
        weightChanged = evt -> onWeightChanged((Float) evt.getNewValue());
        lengthChanged = evt -> onLengthChanged((Float) evt.getNewValue());
        speciesChanged = evt -> onSpeciesChanged((ReferentialReference<SpeciesDto>) evt.getNewValue());
    }

    public abstract void resetIsWeightComputed();

    public abstract void resetIsLengthComputed();

    protected abstract void onSpeciesChanged(ReferentialReference<SpeciesDto> species);

    protected abstract void onWeightChanged(Float newValue);

    protected abstract void onLengthChanged(Float newValue);
}
