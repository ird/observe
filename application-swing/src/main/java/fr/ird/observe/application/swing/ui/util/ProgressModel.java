package fr.ird.observe.application.swing.ui.util;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.jdesktop.beans.AbstractBean;

import javax.swing.JProgressBar;

/**
 * Created on 17/12/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class ProgressModel extends AbstractBean {

    public static final String PROPERTY_CURRENT_STEP = "currentStep";
    public static final String PROPERTY_STEPS_COUNT = "stepsCount";
    private int currentStep;
    private int stepsCount;

    public void installUI(JProgressBar progressBar) {
        progressBar.setVisible(true);
        addPropertyChangeListener(PROPERTY_CURRENT_STEP, evt -> {
            int newValue = (int) evt.getNewValue();
            progressBar.setValue(newValue);
            progressBar.setString((int) (progressBar.getPercentComplete() * 100) + "%");

        });
        addPropertyChangeListener(PROPERTY_STEPS_COUNT, evt -> progressBar.setMaximum((Integer) evt.getNewValue()));
    }

    public int getCurrentStep() {
        return currentStep;
    }

    public void setCurrentStep(int currentStep) {
        int oldValue = this.currentStep;
        this.currentStep = currentStep;
        firePropertyChange(PROPERTY_CURRENT_STEP, oldValue, currentStep);
    }

    public void setStepsCount(int stepsCount) {
        int oldValue = this.stepsCount;
        this.stepsCount = stepsCount;
        firePropertyChange(PROPERTY_STEPS_COUNT, oldValue, stepsCount);
    }

    public int getStepsCount() {
        return stepsCount;
    }

    public void incrementsCurrentStep() {
        setCurrentStep(getCurrentStep() + 1);
    }
}
