package fr.ird.observe.application.swing.ui.tree.navigation.nodes.seine;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ui.content.table.impl.seine.ObjectObservedSpeciesUI;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.ClassNavigationTreeNode;
import fr.ird.observe.services.dto.seine.ObjectObservedSpeciesDto;

/**
 * Created on 04/12/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class ObjectObservedSpeciesNavigationTreeNode extends ClassNavigationTreeNode<ObjectObservedSpeciesDto> {

    public ObjectObservedSpeciesNavigationTreeNode() {
        super(ObjectObservedSpeciesDto.class);
    }

    @Override
    public Class<ObjectObservedSpeciesUI> getContentClass() {
        return ObjectObservedSpeciesUI.class;
    }

    @Override
    public boolean isLeaf() {
        return true;
    }
}
