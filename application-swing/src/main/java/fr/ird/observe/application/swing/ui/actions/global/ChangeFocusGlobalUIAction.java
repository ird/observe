package fr.ird.observe.application.swing.ui.actions.global;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ui.ObserveKeyStrokes;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.content.ContentUI;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import java.awt.FocusTraversalPolicy;
import java.awt.event.ActionEvent;

/**
 * Created on 11/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class ChangeFocusGlobalUIAction extends AbstractGlobalUIAction {

    /** Logger */
    private static final Log log = LogFactory.getLog(ChangeFocusGlobalUIAction.class);

    public static final String ACTION_NAME = "focusOnTreeGlobal";

    public ChangeFocusGlobalUIAction(ObserveMainUI mainUI) {
        super(mainUI, ACTION_NAME, ObserveKeyStrokes.KEY_STROKE_CHANGE_FOCUS);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        boolean navigation = getMainUI().getModel().isFocusOnNavigation();

        JComponent focusComponent;
        if (navigation) {
            if (log.isInfoEnabled()) {
                log.info("Focus to content");
            }
            getMainUI().getModel().setFocusOnNavigation(false);
            ContentUI<?, ?> contentUI = getContentUI(e);
            FocusTraversalPolicy focusTraversalPolicy = contentUI.getFocusTraversalPolicy();
            focusComponent = (JComponent) focusTraversalPolicy.getFirstComponent(contentUI);
//            if (focusComponent instanceof JList) {
//                JList jList = (JList) focusComponent;
//                if (jList.getModel().getSize() > 0) {
//                    jList.setSelectedIndex(0);
//                }
//                jList.requestFocusInWindow();
//            }

        } else {
            if (log.isInfoEnabled()) {
                log.info("Focus to navigation");
            }
            getMainUI().getModel().setFocusOnNavigation(true);
            focusComponent = getMainUI().getNavigation();
        }

        SwingUtilities.invokeLater(focusComponent::requestFocusInWindow);
    }
}
