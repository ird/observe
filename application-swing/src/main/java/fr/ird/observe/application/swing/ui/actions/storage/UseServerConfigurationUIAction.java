package fr.ird.observe.application.swing.ui.actions.storage;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.db.constants.ConnexionStatus;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.actions.AbstractUIAction;
import fr.ird.observe.application.swing.ui.storage.StorageUI;
import fr.ird.observe.application.swing.ui.storage.StorageUIModel;
import fr.ird.observe.application.swing.ui.storage.tabs.ConfigUI;
import fr.ird.observe.services.dto.presets.ServerDataSourceConfiguration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.AbstractButton;
import javax.swing.SwingUtilities;
import java.awt.event.ActionEvent;

/**
 * Created on 18/12/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class UseServerConfigurationUIAction extends AbstractUIAction {

    /** Logger */
    private static final Log log = LogFactory.getLog(UseServerConfigurationUIAction.class);

    public static final String ACTION_NAME = UseServerConfigurationUIAction.class.getName();

    public UseServerConfigurationUIAction(ObserveMainUI mainUI) {
        super(mainUI, ACTION_NAME, null, null, "db-server", null);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        AbstractButton value = (AbstractButton) getValue(EDITOR);
        ConfigUI configUI = (ConfigUI) value.getClientProperty(CLIENT_PROPERTY_UI);
        ServerDataSourceConfiguration configuration = (ServerDataSourceConfiguration) value.getClientProperty("configuration");
        if (log.isInfoEnabled()) {
            log.info("Use server configuration: " + configuration.getName());
        }

        StorageUIModel model = configUI.getModel();

        model.setRemoteUrl(configuration.getUrl());
        model.setRemoteLogin(configuration.getLogin());
        model.setRemotePassword(configuration.getPassword().toCharArray());
        model.setServerDatabase(configuration.getDatabaseName());

        StorageUI parentContainer = configUI.getParentContainer(StorageUI.class);
        configUI.getTestRemoteAction().requestFocusInWindow();

        SwingUtilities.invokeLater(() -> {
            configUI.getTestRemoteAction().doClick();
            if (ConnexionStatus.SUCCESS == configUI.getModel().getConnexionStatus()) {
                parentContainer.getNextAction().requestFocusInWindow();
            }
        });

    }
}
