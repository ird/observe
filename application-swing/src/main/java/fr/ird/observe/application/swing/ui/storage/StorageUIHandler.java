/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.storage;

import com.google.common.base.Preconditions;
import fr.ird.observe.application.swing.I18nEnumHelper;
import fr.ird.observe.application.swing.ObserveActionExecutor;
import fr.ird.observe.application.swing.ObserveRunner;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.ObserveSwingApplicationDataSourcesManager;
import fr.ird.observe.application.swing.ObserveTextGenerator;
import fr.ird.observe.application.swing.configuration.ObserveSwingApplicationConfig;
import fr.ird.observe.application.swing.configuration.constants.CreationMode;
import fr.ird.observe.application.swing.configuration.constants.DbMode;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.db.constants.ConnexionStatus;
import fr.ird.observe.application.swing.decoration.DecoratorService;
import fr.ird.observe.application.swing.ui.ObserveKeyStrokes;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.storage.tabs.RolesTableModel;
import fr.ird.observe.application.swing.ui.storage.tabs.SecurityModel;
import fr.ird.observe.application.swing.ui.storage.tabs.SelectDataUI;
import fr.ird.observe.application.swing.ui.storage.tabs.StorageTabUI;
import fr.ird.observe.application.swing.ui.tree.selection.SelectionTreeModel;
import fr.ird.observe.services.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.services.configuration.ObserveDataSourceInformation;
import fr.ird.observe.services.configuration.rest.ObserveDataSourceConfigurationRest;
import fr.ird.observe.services.configuration.topia.ObserveDataSourceConfigurationTopiaH2;
import fr.ird.observe.services.configuration.topia.ObserveDataSourceConfigurationTopiaPG;
import fr.ird.observe.services.dto.ObserveDbUserDto;
import fr.ird.observe.services.dto.source.DataSourceCreateConfigurationDto;
import fr.ird.observe.services.service.sql.AddSqlScriptProducerRequest;
import fr.ird.observe.services.service.sql.SqlScriptProducerService;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.runtime.swing.wizard.WizardUILancher;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import java.awt.Component;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Objects;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Le handler des actions sur base.
 *
 * On retrouve ici les méthodes pour importer, exporter, faire des backup,
 * synchroniser les bases.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class StorageUIHandler implements UIHandler<StorageUI> {

    /** Logger */
    private static final Log log = LogFactory.getLog(StorageUIHandler.class);

    private StorageUI ui;

    public void start() {

        // on demarre le modele pour le type de base connu
        ui.getModel().start(ui.getModel().getDbMode());

        ui.setSize(800, 600);

        // centrage sur la frame parent
        UIHelper.center(ui.getContextValue(Window.class, "parent"), ui);

        SwingUtilities.invokeLater(() -> {
            JComponent focusOwner = getFocusComponent(ui.getModel().getStep());
            if (focusOwner != null) {
                focusOwner.grabFocus();
            }
        });

        // affichage ui
        ui.setVisible(true);
    }

    public JComponent getFocusComponent(StorageStep newStep) {

        DbMode dbMode = ui.getModel().getDbMode();
        JComponent focusOwner;
        switch (newStep) {

            case CHOOSE_DB_MODE:
                switch (dbMode) {

                    case USE_LOCAL:
                        focusOwner = ui.getCHOOSE_DB_MODE().getUseLocalMode();
                        if (!focusOwner.isEnabled()) {
                            focusOwner = ui.getCHOOSE_DB_MODE().getUseRemoteMode();
                        }
                        break;
                    case CREATE_LOCAL:
                        focusOwner = ui.getCHOOSE_DB_MODE().getCreateLocalMode();
                        break;
                    case USE_REMOTE:
                        focusOwner = ui.getCHOOSE_DB_MODE().getUseRemoteMode();
                        break;
                    case USE_SERVER:
                        focusOwner = ui.getCHOOSE_DB_MODE().getUseServerMode();
                        break;
                    default:
                        focusOwner = null; // ne peut pas arriver
                }

                break;
            case CONFIG:
                switch (dbMode) {

                    case USE_LOCAL:
                        focusOwner = null; // on ne peut pas arriver sur cet ecran
                        break;
                    case CREATE_LOCAL:
                        switch (ui.getModel().getCreationMode()) {
                            case IMPORT_EXTERNAL_DUMP:
                                focusOwner = ui.getCONFIG().getDumpFile();
                                break;
                            case IMPORT_REMOTE_STORAGE:
                                focusOwner = ui.getCONFIG().getRemoteUrl();
                                break;
                            case IMPORT_SERVER_STORAGE:
                                focusOwner = ui.getCONFIG().getRemoteUrl();
                                break;
                            default:
                                focusOwner = null;
                        }
                        break;
                    case USE_REMOTE:
                        focusOwner = ui.getCONFIG().getRemoteUrl();
                        break;
                    case USE_SERVER:
                        focusOwner = ui.getCONFIG().getRemoteUrl();
                        break;
                    default:
                        focusOwner = null; // ne peut pas arriver
                }

                break;
            case CONFIG_REFERENTIEL:
                focusOwner = ui.getCONFIG_REFERENTIEL().getConfigureCentralSource();
                break;
            case CONFIG_DATA:
                focusOwner = ui.getCONFIG_DATA().getConfigureCentralSource();
                break;
            case BACKUP:
                focusOwner = ui.getBACKUP().getDirectoryText();
                break;
            case SELECT_DATA:
                focusOwner = ui.getSELECT_DATA().getSelectTree();
                break;
            case ROLES:
                focusOwner = ui.getROLES().getRoles();
                break;
            case CONFIRM:
                focusOwner = ui.getApplyAction();
                break;
            default:
                focusOwner = null;
        }
        return focusOwner;
    }

    public void onStepChanged(StorageStep oldStep, StorageStep newStep) {
        if (newStep == null) {
            return;
        }
        if (log.isDebugEnabled()) {
            log.debug("new step : " + newStep);
        }
        StorageUIModel model = ui.getModel();

        boolean mustRecompute = oldStep == null || oldStep.ordinal() < newStep.ordinal();

        if (StorageStep.CONFIG == newStep) {

            model.updateEditConfigValues();

            if (mustRecompute) {

                // on redemande un test de connexion a chaque fois qu'on rentre dans l'onglet
                model.setConnexionStatus(ConnexionStatus.UNTESTED);
            }

        }

        if (StorageStep.CONFIG_REFERENTIEL == newStep && mustRecompute) {

            // mise à jour de l'univers des étapes (ajout/suppression étape confg_data et select_data possible)
            ui.getModel().updateUniverse();

        }

        if (StorageStep.CONFIG_DATA == newStep && mustRecompute) {

            // mise à jour de l'univers des étapes (ajout/suppression étape select_data possible)
            ui.getModel().updateUniverse();

        }

        if (StorageStep.SELECT_DATA == newStep && mustRecompute) {

            // récupération des données possibles à importer
            initSelectData(ui);
        }

        if (StorageStep.ROLES == newStep && mustRecompute) {

            // récupération des rôles de la base cible
            updateSecurity(model, ui.getROLES().getRolesModel());

        }

        if (StorageStep.CONFIRM == newStep) {

            // generation du rapport
            String text = computeReport(oldStep);
            ui.CONFIRM.getResume().setText(text);

        }


        JComponent focusComponent = getFocusComponent(newStep);
        if (focusComponent != null) {
            focusComponent.requestFocusInWindow();
        }
    }

    public void launchApply() {

        ui.getModel().setAlreadyApplied(true);

        Runnable action = WizardUILancher.APPLY_DEF.getContextValue(ui);

        if (action == null) {
            final StorageUILauncher launcher = ui.getContextValue(StorageUILauncher.class);

            action = () -> {
                try {
                    launcher.doAction(ui);
                } finally {
                    launcher.doClose(ui, false);
                }
            };
        }
        ObserveMainUI mainUI = ObserveSwingApplicationContext.get().getMainUI();
        if (mainUI == null) {
            if (log.isWarnEnabled()) {
                log.warn("Launch standalone apply action " + action);
            }
            action.run();
        } else {
            ObserveActionExecutor executor = ObserveRunner.getActionExecutor();

            executor.addAction(t("observe.action.storage.applyAction"), action);
        }
    }

    public void launchCancel() {

        Runnable action = WizardUILancher.CANCEL_DEF.getContextValue(ui);

        ObserveMainUI mainUI = ObserveSwingApplicationContext.get().getMainUI();
        if (mainUI == null) {
            if (log.isWarnEnabled()) {
                log.warn("Launch standalone cancel action " + action);
            }
            if (action != null) {
                action.run();
            }
        } else {
            ObserveActionExecutor executor = ObserveRunner.getActionExecutor();
            executor.addAction(t("observe.storage.action.cancel"), action);
        }
    }

    /**
     * Utiliser le storage defini dans le modèle donné.
     *
     * @param model le model++ du storage a creer ou utiliser
     */
    public void doChangeStorage(StorageUIModel model) {
        ObserveSwingApplicationContext observeContext = ObserveSwingApplicationContext.get();

        ObserveSwingApplicationConfig config = observeContext.getConfig();

        // faut-il detruire la base locale ?
        boolean destroyLocalBase =
                config.isLocalStorageExist() &&
                        model.getDbMode() == DbMode.CREATE_LOCAL;

        if (log.isDebugEnabled()) {
            log.debug(">>> should destroy local db ? " + destroyLocalBase);
        }


        ObserveSwingApplicationDataSourcesManager dataSourcesManager = observeContext.getDataSourcesManager();
        ObserveSwingDataSource currentDataSource = dataSourcesManager.getMainDataSource();

        ObserveSwingDataSource localDataSource = null;

        if (currentDataSource != null && currentDataSource.isLocal()) {
            localDataSource = currentDataSource;
        }

        boolean localDbIsSane = true;
        if (destroyLocalBase || model.isDoBackup()) {
            if (localDataSource == null) {
                ObserveDataSourceConfigurationTopiaH2 localConfiguration = dataSourcesManager.newH2DataSourceConfiguration(config, t("observe.storage.label.local"));

                // la base ne doit pas etre mise a jour dans ce cas
                localConfiguration.setCanMigrate(false);

                // on charge un storage sur la base locale
                localDataSource = ObserveSwingApplicationContext.get().getDataSourcesManager().newDataSource(localConfiguration);
            }

            Objects.requireNonNull(localDataSource);

            // Let's check if the datasource is opened or not :
            // we could have close the datasource through the ui
            // If the datasource is closed, we try to open it
            if (!localDataSource.isOpen()) {
                try {

                    localDataSource.open();

                } catch (Exception e) {
                    // on a pas reussi à ouvrir la base locale
                    // cela ne doit pas empécher de continuer
                    // il faut juste supprimer physiquement le repertoire
                    // de la base
                    if (log.isErrorEnabled()) {
                        log.error(t("observe.error.storage.could.not.load.local.db", e.getMessage()), e);
                    }

                    // on conserve l'état
                    localDbIsSane = false;

                    // pour la suite on fait comme si il n'y a pas de local storage
                    localDataSource = null;
                }
            }
        }

        if (model.isDoBackup()) {
            if (!localDbIsSane) {

                // la base locale n'est pas saine, on doit arrêter l'operation
                // de changement de base sous peine de perdre la base.
                Exception e = new Exception(t("observe.error.storage.could.not.backup.unsane.local.db"));
                UIHelper.handlingError(e);
                return;
            }

            Objects.requireNonNull(localDataSource);

            // effectue la backup de la base locale existante
            File f = model.getBackupFile();
            if (log.isDebugEnabled()) {
                log.debug(">>> do backup with " + localDataSource + " in " + f);
            }
            try {
                SqlScriptProducerService dumpProducerService = localDataSource.newSqlScriptProducerService();
                backupLocalDatabase(dumpProducerService, f);
            } catch (Exception e) {
                UIHelper.handlingError(e);
                return;
            }
        }

        if (destroyLocalBase) {
            if (log.isDebugEnabled()) {
                log.debug(">>> destroy local db " + localDataSource);
            }
            if (!localDbIsSane) {
                // la base locale n'est pas saine, on va supprimer directement
                // le dossier de la base
                File localDBDirectory = config.getLocalDBDirectory();
                if (log.isInfoEnabled()) {
                    log.info(">>> destroy local db directory " + localDBDirectory);
                }
                try {
                    FileUtils.deleteDirectory(localDBDirectory);
                } catch (IOException e) {
                    UIHelper.handlingError(e);
                }
            } else {
                try {
                    localDataSource.destroy();
                } catch (Exception e) {
                    UIHelper.handlingError(e);
                    return;
                }
            }
        }

        // suppression du storage precedent
        if (currentDataSource != null && currentDataSource.isOpen()) {
            if (log.isDebugEnabled()) {
                log.debug(">>> close main storage " + currentDataSource);
            }
            // on doit fermer le storage en cours d'utilisation
            currentDataSource.close();
        }

        // suppression du storage local
        if (localDataSource != null
                && localDataSource.isOpen() // Si la base a été détruite précédemment, elle n'est plus ouverte
                && localDataSource != currentDataSource) {
            // ce cas peut arriver lorsque l'on fait juste une backup
            // sans vouloir supprimer la base locale
            if (log.isDebugEnabled()) {
                log.debug(">>> close local storage " + localDataSource);
            }
            // on doit fermer le storage local ouvert
            localDataSource.close();
        }

        if (log.isDebugEnabled()) {
            log.debug("Will create new storage...");
        }

        // preparation du nouveau storage

        try {

            currentDataSource = ObserveSwingApplicationContext.get().getDataSourcesManager().newDataSourceFromModel(model);

            // si on utilise la base local on lance une migration de la base si necessaire
            if (DbMode.USE_LOCAL.equals((model.getDbMode()))) {

                ObserveDataSourceInformation dataSourceInformation = currentDataSource.checkCanConnect();

                currentDataSource.migrateData(dataSourceInformation, config.getModelVersion());

            }

            dataSourcesManager.setMainDataSource(currentDataSource);
            observeContext.getDataSourcesManager().prepareMainStorage(currentDataSource, true);

            if (model.getDbMode() == DbMode.CREATE_LOCAL) {

                DataSourceCreateConfigurationDto creationConfigurationDto = model.getCreationConfigurationDto();

                try {

                    currentDataSource.create(creationConfigurationDto);

                } catch (Exception e) {
                    //si il y a une erreur lor de la création de la base locla

                    // on suprimer la base
                    File localDBDirectory = config.getLocalDBDirectory();
                    if (log.isInfoEnabled()) {
                        log.info(">>> destroy local db directory " + localDBDirectory);
                    }
                    try {
                        FileUtils.deleteDirectory(localDBDirectory);
                    } catch (IOException e2) {
                        UIHelper.handlingError(e2);
                    }

                    config.setLocalStorageExist(false);
                    dataSourcesManager.setMainDataSource(null);

                    throw e;
                }
            } else {
                // ouverture du nouveau storage
                currentDataSource.open();
            }

            if (DbMode.CREATE_LOCAL.equals(model.getDbMode())
                    && (CreationMode.IMPORT_REMOTE_STORAGE.equals(model.getCreationMode()) || CreationMode.IMPORT_SERVER_STORAGE.equals(model.getCreationMode()))
                    && config.isLocalStorageExist()) {
                // si on a creer la base locale a partir d'un import d'une base
                // distante, on sauvegarde la base locale comme dump initial
                // il s'agit d'un dump du référentiel
                File f = config.getInitialDbDump();
                if (f.exists()) {
                    // on supprime le dump sql de la base embarquée
                    if (!f.delete()) {
                        throw new IllegalStateException("could not delete " + f);
                    }
                }
                if (log.isInfoEnabled()) {
                    log.info(">>> create initial dump with " + currentDataSource + " in " + f);
                }
                try {
                    SqlScriptProducerService dumpProducerService = observeContext.getMainDataSourceServicesProvider().newSqlScriptProducerService();
                    backupLocalDatabase(dumpProducerService, f);
                    config.setInitialDumpExist(true);
                } catch (Exception e) {
                    UIHelper.handlingError(e);
                }
            }

            if (log.isInfoEnabled()) {
                log.info(">>> main storage opened " + currentDataSource.getLabel());
            }

        } catch (Exception ex) {
            UIHelper.handlingError(ex);
            throw new RuntimeException(ex);
        } finally {
            ObserveRunner.cleanMemory();
        }
    }

    /**
     * Effectue une sauvegarde de la base locale vers le fichier choisi.
     *
     * @param dumpProducerService le service de dump
     * @param dst                 le fichier de sauvegarde
     */
    public void backupLocalDatabase(SqlScriptProducerService dumpProducerService, File dst) {
        if (dst == null) {
            throw new IllegalArgumentException(
                    "file where to backup can not be null");
        }
        if (log.isDebugEnabled()) {
            log.debug(dst);
        }

        AddSqlScriptProducerRequest request = AddSqlScriptProducerRequest.forH2().addSchema().addReferential().addAllData();
        byte[] dataDump = dumpProducerService.produceAddSqlScript(request).getSqlCode();

        try (FileOutputStream outputStream = new FileOutputStream(dst)) {

            outputStream.write(dataDump);
        } catch (Exception e) {
            UIHelper.handlingError(e);
        }
    }

    public void destroy() {
        ui.getModel().destroy();
        if (log.isDebugEnabled()) {
            log.debug("destroy ui " + ui.getName());
        }
        UIHelper.TabbedPaneIterator<Component> itr = UIHelper.newTabbedPaneIterator(ui.getTabs());
        for (; itr.hasNext(); ) {
            StorageTabUI tab = (StorageTabUI) itr.next();
            if (log.isDebugEnabled()) {
                log.debug("destroy ui " + tab.getName());
            }
            tab.destroy();
        }
        UIHelper.destroy(ui);
    }

    @Override
    public void beforeInit(StorageUI ui) {
        this.ui = ui;

        // verification du context parent
        UIHelper.checkJAXXContextEntry(ui, UIHelper.newContextEntryDef(StorageUIModel.class));
        UIHelper.checkJAXXContextEntry(ui, UIHelper.newContextEntryDef("apply", Runnable.class));
        UIHelper.checkJAXXContextEntry(ui, UIHelper.newContextEntryDef("cancel", Runnable.class));

    }

    @Override
    public void afterInit(StorageUI storageUI) {

        InputMap inputMap = ui.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        ActionMap actionMap = ui.getRootPane().getActionMap();

        inputMap.put(ObserveKeyStrokes.KEY_STROKE_ESCAPE, "cancel");
        actionMap.put("cancel", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ui.doActionPerformed__on__cancelAction(e);
            }
        });
        ObserveKeyStrokes.addKeyStroke(ui.getCancelAction(), ObserveKeyStrokes.KEY_STROKE_ESCAPE);

        ObserveKeyStrokes.addKeyStrokeFromMnemonic(ui);

        StorageUIModel model = ui.getModel();

        // on écoute les changements d'étapes
        model.addPropertyChangeListener(StorageUIModel.STEP_PROPERTY_NAME, evt -> {
            StorageUIModel model1 = (StorageUIModel) evt.getSource();
            StorageStep oldStep = (StorageStep) evt.getOldValue();
            StorageStep newStep = (StorageStep) evt.getNewValue();
            if (log.isDebugEnabled()) {
                log.debug("step has changed <old:" + oldStep + ", new:" + newStep + ">");
            }
            int oldStepIndex = oldStep == null ? -1 : model1.getStepIndex(oldStep);
            int newStepIndex = model1.getStepIndex(newStep);
            JTabbedPane tabs = ui.getTabs();
            if (oldStepIndex + 1 == newStepIndex) {

                // creation d'un nouvel onglet
                StorageTabUI c = (StorageTabUI) ui.getObjectById(newStep.name());
                String title = I18nEnumHelper.getLabel(newStep);
                String tip = I18nEnumHelper.getDescription(newStep);

                if (log.isDebugEnabled()) {
                    log.debug("Create tab " + title + " ui = " + c);
                }
                tabs.addTab(title, null, c, tip);
                tabs.setMnemonicAt(newStepIndex, title.charAt(0));

                // selection du nouvel onglet
                int index = tabs.indexOfComponent(c);
                if (index > -1) {
                    tabs.setSelectedIndex(index);
                }
                ui.onStepChanged(oldStep, newStep);

            } else if (oldStepIndex > newStepIndex) {

                // il s'agit d'un retour en arrière
                // on supprime tous les onglets obsoletes
                int index = newStepIndex + 1;
                while (tabs.getTabCount() > index) {
                    if (log.isDebugEnabled()) {
                        log.debug("remove tab : " + index);
                    }
                    tabs.remove(index);
                }

                ui.onStepChanged(oldStep, newStep);
            } else {
                throw new IllegalStateException("can not go from " + oldStep + " to " + newStep);
            }
        });

        // recuperation de la source de données en cours d'utilisation
        ObserveDataSourceConfiguration dataSourceConfiguration = ui.getContextValue(ObserveDataSourceConfiguration.class);

        // chargement du modèle
        model.init(ui, dataSourceConfiguration);
    }

    protected void initSelectData(StorageUI ui) {

        StorageUIModel model = ui.getModel();

        boolean closeSource = true;
        try {

            // Creation de la data source de lecture des données à sélectionner
            ObserveSwingDataSource importDataSource;

            if (ObstunaAdminAction.CREATE == model.getAdminAction()) {
                importDataSource = model.toImportDataSourceConfig();
            } else {
                importDataSource = ObserveSwingApplicationContext.get().getDataSourcesManager().getMainDataSource();

                closeSource = false;
            }

            Preconditions.checkState(importDataSource != null, "Can't select data on a null dataSource");

            try {

                if (closeSource) {

                    importDataSource.open();
                    model.checkImportDbVersion(importDataSource);

                }

                SelectDataUI selectDataUI = (SelectDataUI) ui.getStepUI(StorageStep.SELECT_DATA);
                SelectionTreeModel treeModel = selectDataUI.getSelectTree().getTreeModel();

                treeModel.setLoadLongline(true);
                treeModel.setLoadSeine(true);
                treeModel.setUseOpenData(true);
                treeModel.setLoadReferential(false);
                treeModel.setShowEmptyTrips(false);

                treeModel.populate(importDataSource);

                boolean selectAll = model.isSelectAll();

                if (selectAll) {
                    treeModel.selectAllTrips();
                    SwingUtilities.invokeLater(selectDataUI.getSelectTree()::expandAll);
                }

                // positionnement du model de selection de données dans le model du wizard
                model.setSelectDataModel(treeModel);

                if (selectDataUI.getSelectTree().getRowCount() > 0) {
                    selectDataUI.getSelectTree().setSelectionRow(0);
                }
                SwingUtilities.invokeLater(selectDataUI.getSelectTree()::requestFocusInWindow);

            } finally {
                if (closeSource) {
                    importDataSource.close();
                }
            }

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error(e, e);
            }
        }
    }

    protected void updateSecurity(StorageUIModel model, RolesTableModel roleModel) {

        SecurityModel security = model.getSecurityModel();

        ObserveSwingDataSource dataSource = null;

        switch (model.getDbMode()) {
            case USE_REMOTE:
                ObserveDataSourceConfigurationTopiaPG pgConfig = model.getPgConfig();
                dataSource = ObserveSwingApplicationContext.get().getDataSourcesManager().newDataSource(pgConfig);
                break;
            case USE_SERVER:
                ObserveDataSourceConfigurationRest restConfig = model.getRestConfig();
                dataSource = ObserveSwingApplicationContext.get().getDataSourcesManager().newDataSource(restConfig);
                break;
        }

        if (dataSource != null) {
            try {

                Set<ObserveDbUserDto> users = dataSource.getUsers();

                if (log.isInfoEnabled()) {
                    log.info("Db roles : " + users);
                }

                security.init(users);
                roleModel.init(security);

            } catch (Exception e) {
                throw new RuntimeException("Could not obtain db roles", e);
            } finally {
                if (dataSource.isOpen()) {
                    dataSource.close();
                }
            }
        }

    }

    protected String computeReport(StorageStep step) {
        if (log.isDebugEnabled()) {
            log.debug("Build report from step " + step);
        }
        StorageUIModel model = ui.getModel();
        ObserveSwingApplicationContext applicationContext = ObserveSwingApplicationContext.get();
        ObserveTextGenerator textGenerator = applicationContext.getTextGenerator();
        return textGenerator.getDataSourceConnectionReport(model);
    }

    protected DecoratorService getDecoratorService() {
        return ObserveSwingApplicationContext.get().getDecoratorService();
    }

}
