package fr.ird.observe.application.swing.ui.admin.config;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.ui.admin.AdminActionModel;
import fr.ird.observe.application.swing.ui.admin.AdminStep;
import fr.ird.observe.application.swing.ui.admin.AdminUIModel;
import fr.ird.observe.application.swing.ui.admin.report.ReportModel;
import fr.ird.observe.application.swing.ui.tree.selection.SelectionTreeModel;
import fr.ird.observe.services.dto.IdHelper;
import fr.ird.observe.services.dto.constants.ObserveModelType;
import fr.ird.observe.services.dto.longline.TripLonglineDto;
import fr.ird.observe.services.dto.reference.DataReference;
import fr.ird.observe.services.dto.reference.DataReferenceList;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import fr.ird.observe.services.service.data.longline.TripLonglineService;
import fr.ird.observe.services.service.data.seine.TripSeineService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created on 28/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class SelectDataModel extends AdminActionModel {

    /** Logger */
    private static final Log log = LogFactory.getLog(SelectDataModel.class);

    protected SelectionTreeModel selectionDataModel;

    public SelectDataModel() {
        super(AdminStep.SELECT_DATA);
    }

    public SelectionTreeModel getSelectionDataModel() {
        return selectionDataModel;
    }

    public void setSelectionDataModel(SelectionTreeModel selectionDataModel) {
        this.selectionDataModel = selectionDataModel;
    }


    public void initSelectionModel(AdminUIModel uiModel) {

        SelectionTreeModel selectionDataModel = getSelectionDataModel();

        ObserveSwingDataSource dataSource = uiModel.getConfigModel().getSafeLocalSource(true);

        if (dataSource.isLocal()) {
            selectionDataModel.setShowEmptyTrips(false);
        }

        boolean selectAllTrips = true;
        if (uiModel.containsOperation(AdminStep.REPORT)) {

            // on remplit le modèle de sélection uniquement avec le bon type de marées
            ReportModel reportModel = uiModel.getReportModel();
            ObserveModelType reportType = reportModel.getModelType();
            boolean loadSeineTrip = ObserveModelType.PS == reportType;

            selectionDataModel.setLoadLongline(!loadSeineTrip);
            selectionDataModel.setLoadSeine(loadSeineTrip);

            selectAllTrips = false;

        }

        // on remplit le modèle de sélection

        selectionDataModel.populate(dataSource);

        if (uiModel.containsOperation(AdminStep.EXPORT_DATA)) {

            // on doit aussi calculer les ids des marées distantes
            ObserveSwingDataSource safeCentralSource = uiModel.getConfigModel().getSafeCentralSource(true);

            List<DataReference<?>> existingTrip = new ArrayList<>();

            TripSeineService tripSeineService = safeCentralSource.newTripSeineService();
            DataReferenceList<TripSeineDto> tripSeineSet = tripSeineService.getAllTripSeine();
            existingTrip.addAll(tripSeineSet.getReferences());

            TripLonglineService tripLonglineService = safeCentralSource.newTripLonglineService();
            DataReferenceList<TripLonglineDto> tripLonglineSet = tripLonglineService.getAllTripLongline();
            existingTrip.addAll(tripLonglineSet.getReferences());

            selectionDataModel.setExistingTrips(existingTrip);


        }

        if (dataSource.isLocal() && selectAllTrips) {

            // on sélectionne toutes les marées
            selectionDataModel.selectAllTrips();
        }

    }

    public DataReference getSelectedTrip() {
        Set<DataReference<?>> data = getSelectionDataModel().getSelectedData();
        if (data.isEmpty()) {

            // pas de Trip selectionne
            return null;
        }
        DataReference dto = data.iterator().next();
        if (IdHelper.isTrip(dto)) {
            return dto;
        }

        // la donnée n'est pas une marée
        return null;
    }

    public void start(AdminUIModel uiModel) {

        if (!uiModel.needSelect()) {

            // pas d'opération avec une sélection de données, rien a faire ici
            return;
        }

        SelectionTreeModel selectionModel = getSelectionDataModel();

        if (uiModel.containsOperation(AdminStep.EXPORT_DATA)) {

            selectionModel.setLoadLongline(true);
            selectionModel.setLoadSeine(true);
            selectionModel.setLoadReferential(false);
            selectionModel.setUseOpenData(false);
        }

        if (uiModel.containsOperation(AdminStep.REPORT)) {

            selectionModel.setUseOpenData(true);
            selectionModel.setLoadLongline(true);
            selectionModel.setLoadSeine(true);
            selectionModel.setLoadReferential(false);
        }

        if (uiModel.containsOperation(AdminStep.VALIDATE)) {

            selectionModel.setUseOpenData(true);
            selectionModel.setLoadLongline(true);
            selectionModel.setLoadSeine(true);
            selectionModel.setLoadReferential(true);
        }

        if (uiModel.containsOperation(AdminStep.CONSOLIDATE)) {

            selectionModel.setUseOpenData(true);
            selectionModel.setLoadLongline(true);
            selectionModel.setLoadSeine(true);
            selectionModel.setLoadReferential(false);
        }

        selectionModel.addPropertyChangeListener(SelectionTreeModel.SELECTED_COUNT, evt -> {
            SelectionTreeModel source = (SelectionTreeModel) evt.getSource();
            if (log.isDebugEnabled()) {
                log.debug(String.format("selection data model [%s] changed on %s, new value = %s",
                                        source, evt.getPropertyName(), evt.getNewValue()));
            }
            uiModel.validate();
            if (log.isDebugEnabled()) {
                log.debug("nb selected export datas = " + source.getSelectedCount());
            }
        });
    }

    public boolean validate(AdminUIModel uiModel) {

        boolean validate = true;

        if (uiModel.containsOperation(AdminStep.VALIDATE)) {

            // la config doit etre ok
            validate = uiModel.validate(AdminStep.CONFIG);
            if (validate) {

                // il faut au moins une donnee de selectionnee
                boolean empty = selectionDataModel.isSelectionEmpty();
                validate = !empty;
            }
        }
        if (uiModel.containsOperation(AdminStep.EXPORT_DATA)) {

            // la config doit etre ok
            validate &= uiModel.validate(AdminStep.CONFIG);
            if (validate) {

                // il faut au moins une donnee de selectionnee
                boolean empty = selectionDataModel.isSelectionEmpty();
                validate = !empty;
            }
        }
        if (uiModel.containsOperation(AdminStep.CONSOLIDATE)) {

            // la config doit etre ok
            validate &= uiModel.validate(AdminStep.CONFIG);
            if (validate) {

                // il faut au moins une donnee de selectionnee
                boolean empty = selectionDataModel.isSelectionEmpty();
                validate = !empty;
            }
        }
        if (uiModel.containsOperation(AdminStep.REPORT)) {

            // la config doit etre ok
            validate &= uiModel.validate(AdminStep.CONFIG);
            if (validate) {

                // il faut exactement une Trip de selectionnee
                int selectedCount = selectionDataModel.getSelectedCount();
                validate = selectedCount == 1;
            }
        }

        return validate;
    }
}
