package fr.ird.observe.application.swing.ui.content.table.impl.longline;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.db.constants.DataContextType;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.content.table.ContentTableUIHandler;
import fr.ird.observe.services.dto.longline.BranchlinesCompositionDto;
import fr.ird.observe.services.dto.longline.SetLonglineGlobalCompositionDto;
import fr.ird.observe.services.dto.referential.longline.HookSizeDto;
import fr.ird.observe.services.dto.referential.longline.HookTypeDto;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import static org.nuiton.i18n.I18n.n;

/**
 * Created on 12/6/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.9
 */
public class BranchlinesCompositionUIHandler extends ContentTableUIHandler<SetLonglineGlobalCompositionDto, BranchlinesCompositionDto, BranchlinesCompositionUI> implements UIHandler<BranchlinesCompositionUI> {

    /** Logger */
    private static final Log log = LogFactory.getLog(BranchlinesCompositionUIHandler.class);

    public BranchlinesCompositionUIHandler() {
        super(DataContextType.SetLongline);
    }

    @Override
    protected void installFocusTraversalPolicy() {
        // rien a installer
    }

    @Override
    protected void onSelectedRowChanged(int editingRow, BranchlinesCompositionDto bean, boolean create) {

        if (getTableModel().isEditable()) {
            if (log.isDebugEnabled()) {
                log.debug("Row has changed to " + editingRow);
            }
            UIHelper.askFocus(getUi().getTopType());
        }

    }

    @Override
    protected void initTableUI(DefaultTableCellRenderer renderer) {

        JTable table = getUi().getTable();

        UIHelper.setI18nTableHeaderRenderer(
                table,
                n("observe.content.branchlinesComposition.table.topType"),
                n("observe.content.branchlinesComposition.table.topType.tip"),
                n("observe.content.branchlinesComposition.table.tracelineType"),
                n("observe.content.branchlinesComposition.table.tracelineType.tip"),
                n("observe.content.branchlinesComposition.table.length"),
                n("observe.content.branchlinesComposition.table.length.tip"),
                n("observe.content.branchlinesComposition.table.tracelineLength"),
                n("observe.content.branchlinesComposition.table.tracelineLength.tip"),
                n("observe.content.branchlinesComposition.table.proportion"),
                n("observe.content.branchlinesComposition.table.proportion.tip"));

        UIHelper.setTableColumnRenderer(table, 0, UIHelper.newReferentialReferenceDecorateTableCellRenderer(renderer, HookTypeDto.class));
        UIHelper.setTableColumnRenderer(table, 1, UIHelper.newReferentialReferenceDecorateTableCellRenderer(renderer, HookSizeDto.class));
        UIHelper.setTableColumnRenderer(table, 2, UIHelper.newEmptyNumberTableCellRenderer(renderer));
        UIHelper.setTableColumnRenderer(table, 3, UIHelper.newEmptyNumberTableCellRenderer(renderer));
        UIHelper.setTableColumnRenderer(table, 4, UIHelper.newEmptyNumberTableCellRenderer(renderer));

        // when model change in table, let's recompute the proportion sum
        table.getModel().addTableModelListener(e -> {
            int proportionSum = getBean().getBranchlinesCompositionProportionSum();
            getBean().setBranchlinesCompositionProportionSum(proportionSum);
        });

    }

    @Override
    protected String getEditBeanIdToLoad() {
        return getDataContext().getSelectedSetId();
    }

    @Override
    public void afterSave(boolean refresh) {
        super.afterSave(refresh);
    }

    @Override
    protected void doPersist(SetLonglineGlobalCompositionDto bean) {
        // fait par le doSave de LonglineGlobalCompositionUIHandler
    }

    @Override
    protected void loadEditBean(String beanId) {
        // fait par le loadEditBean de LonglineGlobalCompositionUIHandler
    }

}
