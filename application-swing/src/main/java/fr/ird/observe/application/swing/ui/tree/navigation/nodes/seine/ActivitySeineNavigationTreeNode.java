package fr.ird.observe.application.swing.ui.tree.navigation.nodes.seine;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ui.content.open.impl.seine.ActivitySeineUI;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.ReferenceNavigationTreeNodeSupport;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.WithChildsToReload;
import fr.ird.observe.services.dto.reference.DataReference;
import fr.ird.observe.services.dto.reference.DataReferenceSet;
import fr.ird.observe.services.dto.seine.ActivitySeineDto;
import fr.ird.observe.services.dto.seine.ActivitySeineHelper;
import fr.ird.observe.services.dto.seine.FloatingObjectDto;
import fr.ird.observe.services.dto.seine.SetSeineDto;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 14/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class ActivitySeineNavigationTreeNode extends ReferenceNavigationTreeNodeSupport<ActivitySeineDto, DataReference<ActivitySeineDto>> implements WithChildsToReload {

    public ActivitySeineNavigationTreeNode(DataReference<ActivitySeineDto> data) {
        super(data, true);
//        if (isPersisted()) {
//            DataReference<SetSeineDto> set = ActivitySeineHelper.getSetSeine(data);
//            if (set != null) {
//                add(new SetSeineNavigationTreeNode(set));
//            }
//        }
    }

    @Override
    public void reload() {
        DataReference<ActivitySeineDto> data = getMainDataSourceServicesProvider().newActivitySeineService().loadReferenceToRead(getId());
        setData(data);
    }

    @Override
    public String getText() {
        return isPersisted() ? super.getText() : t("observe.type.activitySeine.unsaved");
    }

    @Override
    public boolean isLeaf() {
        return !isPersisted();
    }

    @Override
    public Class<ActivitySeineUI> getContentClass() {
        return ActivitySeineUI.class;
    }

    @Override
    public void reloadChilds() {
        DataReference<SetSeineDto> set = ActivitySeineHelper.getSetSeine(getData());
        if (set != null) {
            add(new SetSeineNavigationTreeNode(set));
        }
        DataReferenceSet<FloatingObjectDto> referenceSet = getMainDataSourceServicesProvider().newFloatingObjectService().getFloatingObjectByActivitySeine(getId());
        for (DataReference<FloatingObjectDto> reference : referenceSet.getReferences()) {
            add(new FloatingObjectSeineNavigationTreeNode(reference));
        }
    }
}
