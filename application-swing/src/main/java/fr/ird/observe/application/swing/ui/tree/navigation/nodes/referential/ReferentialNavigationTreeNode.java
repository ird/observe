package fr.ird.observe.application.swing.ui.tree.navigation.nodes.referential;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingTechnicalException;
import fr.ird.observe.application.swing.decoration.ObserveI18nDecoratorHelper;
import fr.ird.observe.application.swing.ui.content.ContentUI;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.ClassNavigationTreeNode;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import org.apache.commons.lang3.StringUtils;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 16/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class ReferentialNavigationTreeNode<D extends ReferentialDto> extends ClassNavigationTreeNode<D> {

    public ReferentialNavigationTreeNode(Class<D> data) {
        super(data, true);
    }

    @Override
    public Class<? extends ContentUI<?, ?>> getContentClass() {
        String packageName = getData().getPackage().getName();
        String packagePrefix;
        if (packageName.endsWith("longline")) {
            packagePrefix = "longline.";
        } else if (packageName.endsWith("seine")) {
            packagePrefix = "seine.";

        } else {
            packagePrefix = "";
        }

        String className = String.format("fr.ird.observe.application.swing.ui.content.ref.impl.%s%sUI", packagePrefix, StringUtils.removeEnd(getData().getSimpleName(), "Dto"));
        try {
            return (Class) Class.forName(className);
        } catch (ClassNotFoundException e) {
            throw new ObserveSwingTechnicalException("Can't get ui class for: " + className, e);
        }
    }

    @Override
    public String getText() {
        return t(ObserveI18nDecoratorHelper.getTypePluralI18nKey(getDataType()));
    }

    @Override
    public String getIconPath() {
        return "navigation.sub.referentiel";
    }

    @Override
    public boolean isLeaf() {
        return true;
    }

    @Override
    public boolean isOpen() {
        return true;
    }
}
