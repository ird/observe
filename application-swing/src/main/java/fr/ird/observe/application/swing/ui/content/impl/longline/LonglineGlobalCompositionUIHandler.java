package fr.ird.observe.application.swing.ui.content.impl.longline;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.DataContext;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.db.constants.DataContextType;
import fr.ird.observe.application.swing.ui.content.ContentMode;
import fr.ird.observe.application.swing.ui.content.ContentUIHandler;
import fr.ird.observe.application.swing.ui.content.table.impl.longline.BaitsCompositionUI;
import fr.ird.observe.application.swing.ui.content.table.impl.longline.BaitsCompositionUIModel;
import fr.ird.observe.application.swing.ui.content.table.impl.longline.BranchlinesCompositionUI;
import fr.ird.observe.application.swing.ui.content.table.impl.longline.BranchlinesCompositionUIModel;
import fr.ird.observe.application.swing.ui.content.table.impl.longline.FloatlinesCompositionUI;
import fr.ird.observe.application.swing.ui.content.table.impl.longline.FloatlinesCompositionUIModel;
import fr.ird.observe.application.swing.ui.content.table.impl.longline.HooksCompositionUI;
import fr.ird.observe.application.swing.ui.content.table.impl.longline.HooksCompositionUIModel;
import fr.ird.observe.services.dto.form.Form;
import fr.ird.observe.services.dto.longline.ActivityLonglineDto;
import fr.ird.observe.services.dto.longline.SetLonglineGlobalCompositionDto;
import fr.ird.observe.services.dto.longline.SetLonglineGlobalCompositionHelper;
import fr.ird.observe.services.dto.result.SaveResultDto;
import fr.ird.observe.services.service.data.longline.SetLonglineGlobalCompositionService;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.validator.NuitonValidatorScope;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 12/5/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.8
 */
public class LonglineGlobalCompositionUIHandler extends ContentUIHandler<SetLonglineGlobalCompositionDto, LonglineGlobalCompositionUI> implements UIHandler<LonglineGlobalCompositionUI> {

    /** Logger */
    private static final Log log = LogFactory.getLog(LonglineGlobalCompositionUIHandler.class);

    public LonglineGlobalCompositionUIHandler() {
        super(DataContextType.ActivityLongline, DataContextType.SetLongline);
    }

    @Override
    protected void installFocusTraversalPolicy() {
        // rien a installer
    }

    @Override
    protected boolean computeCanWrite(ObserveSwingDataSource source) {
        return source.canWriteData();
    }

    @Override
    protected ContentMode getContentMode(DataContext dataContext) {

        String activityId = dataContext.getSelectedActivityLonglineId();

        if (getOpenDataManager().isOpenActivityLongline(activityId)) {

            // l'activité est ouverte, mode édition
            return ContentMode.UPDATE;
        }

        // l'activité n'est pas ouverte, mode lecture
        addMessage(getUi(),
                   NuitonValidatorScope.INFO,
                   getTypeI18nKey(ActivityLonglineDto.class),
                   t("observe.storage.activityLongline.message.not.open"));
        return ContentMode.READ;
    }

    @Override
    public void afterInit(LonglineGlobalCompositionUI ui) {
        super.afterInit(ui);
        {
            // init floatlinesComposition tab
            FloatlinesCompositionUI compositionUI = ui.getFloatlinesCompositionUI();
            ui.getFloatlinesCompositionPanel().remove(compositionUI);
            ui.getFloatlinesCompositionPanel().add(compositionUI.getBody());
        }

        {
            // init branchlinesComposition tab
            BranchlinesCompositionUI compositionUI = ui.getBranchlinesCompositionUI();
            ui.getBranchlinesCompositionPanel().remove(compositionUI);
            ui.getBranchlinesCompositionPanel().add(compositionUI.getBody());
        }

        {
            // init hooksComposition tab
            HooksCompositionUI compositionUI = ui.getHooksCompositionUI();
            ui.getHooksCompositionPanel().remove(compositionUI);
            ui.getHooksCompositionPanel().add(compositionUI.getBody());
        }

        {
            // init baitsComposition tab
            BaitsCompositionUI compositionUI = ui.getBaitsCompositionUI();
            ui.getBaitsCompositionPanel().remove(compositionUI);
            ui.getBaitsCompositionPanel().add(compositionUI.getBody());
        }
    }

    @Override
    public void openUI() {
        super.openUI();

        String activityId = getSelectedParentId();
        String setId = getSelectedId();

        if (log.isInfoEnabled()) {
            log.info("activityId = " + activityId);
            log.info("setId      = " + setId);
        }
        ContentMode mode = computeContentMode();

        // update mode
        Form<SetLonglineGlobalCompositionDto> form = getSetLonglineGlobalCompositionService().loadForm(setId);

        loadReferentialReferenceSetsInModel(form);

        getModel().setForm(form);
        SetLonglineGlobalCompositionHelper.copySetLonglineGlobalCompositionDto(form.getObject(), getBean());

        // utilisation du mode requis
        setContentMode(mode);

        if (mode != ContentMode.READ) {
            getUi().startEdit(null);
        }

        {
            // open floatlinesComposition tab
            FloatlinesCompositionUIModel floatlinesCompositionModel = getUi().getFloatlinesCompositionModel();
            floatlinesCompositionModel.setReferentialReferenceSets(getModel().getReferentialReferenceSets());
            floatlinesCompositionModel.setForm(form);
            SetLonglineGlobalCompositionHelper.copySetLonglineGlobalCompositionDto(form.getObject(), floatlinesCompositionModel.getBean());

            FloatlinesCompositionUI compositionUI = getUi().getFloatlinesCompositionUI();
            compositionUI.open();

        }

        {
            // open branchlinesComposition tab
            BranchlinesCompositionUIModel branchlinesCompositionModel = getUi().getBranchlinesCompositionModel();
            branchlinesCompositionModel.setReferentialReferenceSets(getModel().getReferentialReferenceSets());
            branchlinesCompositionModel.setForm(form);
            SetLonglineGlobalCompositionHelper.copySetLonglineGlobalCompositionDto(form.getObject(), branchlinesCompositionModel.getBean());

            BranchlinesCompositionUI compositionUI = getUi().getBranchlinesCompositionUI();
            compositionUI.open();

        }

        {
            // open hooksComposition tab
            HooksCompositionUIModel hooksCompositionModel = getUi().getHooksCompositionModel();
            hooksCompositionModel.setReferentialReferenceSets(getModel().getReferentialReferenceSets());
            hooksCompositionModel.setForm(form);
            SetLonglineGlobalCompositionHelper.copySetLonglineGlobalCompositionDto(form.getObject(), hooksCompositionModel.getBean());

            HooksCompositionUI compositionUI = getUi().getHooksCompositionUI();
            compositionUI.open();

        }

        {
            // open baitsComposition tab
            BaitsCompositionUIModel baitsCompositionModel = getUi().getBaitsCompositionModel();
            baitsCompositionModel.setReferentialReferenceSets(getModel().getReferentialReferenceSets());
            baitsCompositionModel.setForm(form);
            SetLonglineGlobalCompositionHelper.copySetLonglineGlobalCompositionDto(form.getObject(), baitsCompositionModel.getBean());

            BaitsCompositionUI compositionUI = getUi().getBaitsCompositionUI();
            compositionUI.open();

        }

    }

    @Override
    public void startEditUI(String... binding) {
        LonglineGlobalCompositionUIModel model = (LonglineGlobalCompositionUIModel) getModel();
        addInfoMessage(t("observe.content.longlineGlobalComposition.message.updating"));

        super.startEditUI(LonglineGlobalCompositionUI.BINDING_COMMENT_MODEL);
        model.setModified(false);
    }

    @Override
    protected boolean doSave(SetLonglineGlobalCompositionDto bean) {

        bean.setFloatlinesComposition(getUi().getFloatlinesCompositionModel().getBean().getFloatlinesComposition());
        bean.setBranchlinesComposition(getUi().getBranchlinesCompositionModel().getBean().getBranchlinesComposition());
        bean.setHooksComposition(getUi().getHooksCompositionModel().getBean().getHooksComposition());
        bean.setBaitsComposition(getUi().getBaitsCompositionModel().getBean().getBaitsComposition());

        SaveResultDto saveResult = getSetLonglineGlobalCompositionService().save(bean);
        saveResult.toDto(bean);

        return true;
    }

    @Override
    protected void afterSave(boolean refresh) {
        super.afterSave(refresh);

        getUi().getFloatlinesCompositionUI().getHandler().afterSave(refresh);
        getUi().getBranchlinesCompositionUI().getHandler().afterSave(refresh);
        getUi().getHooksCompositionUI().getHandler().afterSave(refresh);
        getUi().getBaitsCompositionUI().getHandler().afterSave(refresh);
    }

    protected SetLonglineGlobalCompositionService getSetLonglineGlobalCompositionService() {

        return ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newSetLonglineGlobalCompositionService();
    }

}
