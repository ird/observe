package fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.decoration.decorators.ReferentialReferenceDecorator;
import fr.ird.observe.services.dto.reference.ReferentialReference;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.widgets.select.BeanComboBox;

import java.awt.Dimension;
import java.beans.PropertyChangeListener;
import java.util.List;

/**
 * Created on 11/08/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ReferentialReplaceUIHandler<R extends ReferentialDto> implements UIHandler<ReferentialReplaceUI<R>> {

    public static final String CONTEXT_NAME = "replaceUI";

    private ReferentialReplaceUI<R> ui;
    protected final PropertyChangeListener listenData;

    public ReferentialReplaceUIHandler() {
        this.listenData = evt -> ui.setReplaceReference((ReferentialReference) evt.getNewValue());
    }

    @Override
    public void beforeInit(ReferentialReplaceUI<R> ui) {
        this.ui = ui;
    }

    @Override
    public void afterInit(ReferentialReplaceUI<R> ui) {
        BeanComboBox<ReferentialReference<R>> beanComboBox = ui.getList();

        beanComboBox.setI18nPrefix("observe.common.");
        beanComboBox.setMinimumSize(new Dimension(0, 24));
        beanComboBox.setBeanType((Class) ReferentialReference.class);
        List<ReferentialReference<R>> references = ui.getContextValue(List.class, CONTEXT_NAME);
        ReferentialReferenceDecorator<R> decorator = ui.getContextValue(ReferentialReferenceDecorator.class, CONTEXT_NAME);
        beanComboBox.init(decorator, references);
    }
}
