<!--
  #%L
  ObServe :: Application Swing
  %%
  Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->
<fr.ird.observe.application.swing.ui.content.ref.ContentI18nReferenceUI
        superGenericType='SettingShapeDto, SettingShapeUI'>

  <style source="../ReferenceEntity.jcss"/>

  <import>
    fr.ird.observe.services.dto.referential.longline.SettingShapeDto

    static fr.ird.observe.application.swing.ui.UIHelper.getStringValue
    fr.ird.observe.common.constants.ReferenceStatus
  </import>

  <!-- validator -->
  <BeanValidator id='validator' autoField='true'
                 beanClass='fr.ird.observe.services.dto.referential.longline.SettingShapeDto'
                 context='ui-create' errorTableModel='{getErrorTableModel()}'/>

  <!-- model -->
  <SettingShapeUIModel id='model'/>

  <!-- edit bean -->
  <SettingShapeDto id='bean'/>

  <Table id='editTable'>

    <!-- uri -->
    <row>
      <cell anchor="west">
        <JLabel id='uriLabel'/>
      </cell>
      <cell anchor='east' weightx="1" fill="both">
        <JTextField id='uri'/>
      </cell>
    </row>

    <!-- code / status -->
    <row>
      <cell anchor="west">
        <JLabel id='codeStatusLabel'/>
      </cell>
      <cell anchor='east' weightx="1" fill="both">
        <JPanel id='codeStatusPanel' layout='{new BorderLayout()}'>
          <JTextField id='code' constraints='BorderLayout.WEST'/>
          <EnumEditor id='status' constructorParams='ReferenceStatus.class' genericType='ReferenceStatus'
                      onItemStateChanged='getBean().setStatus((ReferenceStatus)status.getSelectedItem())'
                      constraints='BorderLayout.CENTER'/>
        </JPanel>
      </cell>
    </row>

    <!-- needComment -->
    <row>
      <cell anchor='east' weightx="1" fill="both" columns="2">
        <JCheckBox id='needComment' onItemStateChanged='getBean().setNeedComment(needComment.isSelected())'/>
      </cell>
    </row>
  </Table>

  <Table id='editI18nTable'/>

</fr.ird.observe.application.swing.ui.content.ref.ContentI18nReferenceUI>
