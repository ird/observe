package fr.ird.observe.application.swing.ui.storage.tabs;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ui.UIHelper;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.runtime.swing.editor.MyDefaultCellEditor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 27/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since X
 */
public class RolesUIHandler extends StorageTabUIHandler<RolesUI> implements UIHandler<RolesUI> {

    /** Logger */
    private static final Log log = LogFactory.getLog(RolesUIHandler.class);

    @Override
    public void afterInit(RolesUI ui) {
        if (ui.getStep() != null) {
            ui.setDescriptionText(t(ui.getStep().getDescription()));
        }
        ui.getSecurityModel().addPropertyChangeListener(evt -> {
            if (log.isDebugEnabled()) {
                log.debug("Security model changed [" + evt.getPropertyName() + "] <" + evt.getOldValue() + " : " + evt.getNewValue() + ">");
            }
            ui.getModel().validate();
        });

        JTable table = ui.getRoles();
        table.setRowHeight(24);

        UIHelper.fixTableColumnWidth(table, 1, 100);
        UIHelper.fixTableColumnWidth(table, 2, 100);
        UIHelper.fixTableColumnWidth(table, 3, 100);
        UIHelper.fixTableColumnWidth(table, 4, 100);

        DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();

        UIHelper.setI18nTableHeaderRenderer(
                table,
                RolesTableModel.COLUMN_NAMES[0],
                RolesTableModel.COLUMN_NAME_TIPS[0],
                RolesTableModel.COLUMN_NAMES[1],
                RolesTableModel.COLUMN_NAME_TIPS[1],
                RolesTableModel.COLUMN_NAMES[2],
                RolesTableModel.COLUMN_NAME_TIPS[2],
                RolesTableModel.COLUMN_NAMES[3],
                RolesTableModel.COLUMN_NAME_TIPS[3],
                RolesTableModel.COLUMN_NAMES[4],
                RolesTableModel.COLUMN_NAME_TIPS[4]
        );

        UIHelper.setTableColumnRenderer(table, 0, UIHelper.newStringTableCellRenderer(renderer, 50, true));
        UIHelper.setTableColumnRenderer(table, 1, UIHelper.newBooleanTableCellRenderer(renderer));
        UIHelper.setTableColumnRenderer(table, 2, UIHelper.newBooleanTableCellRenderer(renderer));
        UIHelper.setTableColumnRenderer(table, 3, UIHelper.newBooleanTableCellRenderer(renderer));
        UIHelper.setTableColumnRenderer(table, 4, UIHelper.newBooleanTableCellRenderer(renderer));

        UIHelper.setTableColumnEditor(table, 1, MyDefaultCellEditor.newBooleanEditor());
        UIHelper.setTableColumnEditor(table, 2, MyDefaultCellEditor.newBooleanEditor());
        UIHelper.setTableColumnEditor(table, 3, MyDefaultCellEditor.newBooleanEditor());
        UIHelper.setTableColumnEditor(table, 4, MyDefaultCellEditor.newBooleanEditor());

    }

}
