/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.admin;

import fr.ird.observe.application.swing.configuration.constants.DbMode;
import fr.ird.observe.application.swing.ui.admin.config.ConfigModel;
import fr.ird.observe.application.swing.ui.admin.config.SelectDataModel;
import fr.ird.observe.application.swing.ui.admin.consolidate.ConsolidateModel;
import fr.ird.observe.application.swing.ui.admin.export.ExportModel;
import fr.ird.observe.application.swing.ui.admin.report.ReportModel;
import fr.ird.observe.application.swing.ui.admin.resume.ShowResumeModel;
import fr.ird.observe.application.swing.ui.admin.save.SaveLocalModel;
import fr.ird.observe.application.swing.ui.admin.synchronize.data.DataSynchroModel;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.legacy.SynchronizeModel;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ng.ReferentialSynchroModel;
import fr.ird.observe.application.swing.ui.admin.validate.ValidateModel;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardExtModel;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardState;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Le modele de l'ui pour effectuer des opérations de synchro et validation.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class AdminUIModel extends WizardExtModel<AdminStep> {

    //    public static final String SELECTION_MODEL_CHANGED_PROPERTY_NAME = "selectionModelChanged";
    public static final String SELECTED_TRIP_PROPERTY_NAME = "selectedTrip";

    /** Logger */
    private static final Log log = LogFactory.getLog(AdminUIModel.class);

    public static final PropertyChangeListener LOG_PROPERTY_CHANGE_LISTENER = new LogPropertyChanged();

    protected final ConfigModel configModel = new ConfigModel();

    protected final SelectDataModel selectDataModel = new SelectDataModel();

    protected final ShowResumeModel showResumeModel = new ShowResumeModel();

    /** la liste des modes disponibles en entrée */
    protected final EnumSet<DbMode> availableIncomingModes;

    private final PropertyChangeListener listenStepChanged;

    @Override
    public void firePropertyChange(String propertyName, Object newValue) {
        super.firePropertyChange(propertyName, newValue);
    }

    public AdminUIModel() {
        super(AdminStep.class);

        availableIncomingModes = EnumSet.noneOf(DbMode.class);

        if (log.isDebugEnabled()) {
            log.debug("model [" + this + "] is instanciate.");
        }

        listenStepChanged = evt -> {

            if (isWasStarted()) {

                // on ne propage plus rien (il n'y a plus de configuration possible...)
                if (log.isDebugEnabled()) {
                    log.debug("Stop propagation, was started... " + evt.getPropertyName());
                }
                return;
            }

            AdminStep oldStep = (AdminStep) evt.getOldValue();
            AdminStep newStep = (AdminStep) evt.getNewValue();

            if (AdminStep.CONFIG == newStep) {

                // on repasse sur l'écran de configuration

                if (containsOperation(AdminStep.REFERENTIAL_SYNCHRONIZE) || containsOperation(AdminStep.DATA_SYNCHRONIZE)) {

                    getConfigModel().setLocalSourceLabel(t("observe.storage.config.left.storage"));
                    getConfigModel().setCentralSourceLabel(t("observe.storage.config.right.storage"));

                }

                return;
            }

            if (oldStep != null && oldStep != AdminStep.CONFIG) {

                // on fait rien si on ne vient pas de la configuration
                return;
            }

            // on était sur l'écran de configuration

            // mise à jour des modèles de sélection si on arrive sur une étape
            // qui le requière

            boolean needSelect = needSelect();

            if (!needSelect) {

                // pas besoin d'agir sur le model de sélection de données
                return;
            }

            if (newStep == AdminStep.SELECT_DATA) {

                getSelectDataModel().initSelectionModel(this);

            }

        };

    }

    public ValidateModel getValidateModel() {
        return (ValidateModel) getStepModel(AdminStep.VALIDATE);
    }

    public SynchronizeModel getSynchronizeReferentielModel() {
        return (SynchronizeModel) getStepModel(AdminStep.SYNCHRONIZE);
    }

    public ConfigModel getConfigModel() {
        return configModel;
    }

    public ShowResumeModel getShowResumeModel() {
        return showResumeModel;
    }

    public SelectDataModel getSelectDataModel() {
        return selectDataModel;
    }

    public ExportModel getExportModel() {
        return (ExportModel) getStepModel(AdminStep.EXPORT_DATA);
    }

    public ReportModel getReportModel() {
        return (ReportModel) getStepModel(AdminStep.REPORT);
    }

    public ConsolidateModel getConsolidateModel() {
        return (ConsolidateModel) getStepModel(AdminStep.CONSOLIDATE);
    }

    public SaveLocalModel getSaveLocalModel() {
        return (SaveLocalModel) getStepModel(AdminStep.SAVE_LOCAL);
    }

    public DataSynchroModel getDataSynchroModel() {
        return (DataSynchroModel) getStepModel(AdminStep.DATA_SYNCHRONIZE);
    }

    public ReferentialSynchroModel getReferentialSynchroModel() {
        return (ReferentialSynchroModel) getStepModel(AdminStep.REFERENTIAL_SYNCHRONIZE);
    }

    @Override
    public AdminActionModel getStepModel(AdminStep operation) {
        return (AdminActionModel) super.getStepModel(operation);
    }

    public boolean needSelect() {
        if (WizardState.CANCELED == getModelState()) {
            return false;
        }
        Set<AdminStep> operations = getOperations();
        for (AdminStep operation : operations) {
            if (operation.isNeedSelect()) {
                return true;
            }
        }
        return false;
    }

    public boolean isValidStep() {
        return validStep;
    }

    /**
     * Pour savoir si on a besoin d'une base source pour l'opération.
     *
     * Actuellement, seul l'opération d'import acces n'a pas besoin d'une telle
     * base.
     *
     * @return {@code true} si au moins une des opérations a besoin d'une source
     * d'entrée, {@code false} autrement.
     * @since 2.0
     */
    public boolean isNeedIncomingDataSource() {
        for (AdminStep op : getOperations()) {
            if (op.hasIncomingModes()) {

                // l'operation requiere une base en entree
                return true;
            }
        }

        // aucune operation ne requiere pas de base en entree
        return false;
    }

    /**
     * Pour savoir si on a besoin d'une base de référence pour l'opération.
     *
     * @return {@code true} si au moins une des opérations a besoin d'une source
     * de référence, {@code false} autrement.
     * @since 2.0
     */
    public boolean isNeedReferentielDataSource() {
        for (AdminStep op : getOperations()) {
            if (op.isNeedReferentiel()) {

                // l'operation requiere une base de référence
                return true;
            }
        }

        // aucune operation ne requiere pas de base de référence
        return false;
    }

    /**
     * Construit l'ensemble des modes disponibles pour la base en entrée.
     *
     * @return l'ensemble des modes disponibles.
     */
    public EnumSet<DbMode> getIncomingDataSourceMode() {

        EnumSet<DbMode> result = EnumSet.noneOf(DbMode.class);
        getOperations().stream().filter(AdminStep::hasIncomingModes).forEach(op -> result.addAll(Arrays.asList(op.getIncomingModes())));
        return result;
    }

    public void start(AdminUI ui) {

        availableIncomingModes.clear();

        // demarrage du modèle : on fixe ici une fois pour toute les liste
        // des onglets visibles
        start();

        // preparation de l'ui (création des onglets visibles)
        ui.blockOperations();

        if (log.isInfoEnabled()) {
            log.info("enables steps = " + steps);
            log.info("enables operations = " + operations);
        }

        getConfigModel().start(ui);

        getSelectDataModel().start(this);

        if (containsOperation(AdminStep.SAVE_LOCAL)) {
            getSaveLocalModel().start(this);
        }
        if (containsOperation(AdminStep.VALIDATE)) {
            getValidateModel().start(this);
        }
        if (containsOperation(AdminStep.REPORT)) {
            getReportModel().start(this);
        }
        if (containsOperation(AdminStep.CONSOLIDATE)) {
            getConsolidateModel().start(this);
        }
        if (containsOperation(AdminStep.DATA_SYNCHRONIZE)) {
            getDataSynchroModel().start(ui);
        }
        if (containsOperation(AdminStep.REFERENTIAL_SYNCHRONIZE)) {
            getReferentialSynchroModel().start(ui);
        }

        removePropertyChangeListener(STEP_PROPERTY_NAME, listenStepChanged);
        addPropertyChangeListener(STEP_PROPERTY_NAME, listenStepChanged);

        if (log.isInfoEnabled()) {
            log.info("End of start...");
        }

        // on revalide le modèle (tout est prêt)
        validate();
    }

    @Override
    public void destroy() {
        getConfigModel().destroy();
        super.destroy();
    }

    @Override
    public void cancel() {
        super.cancel();
        // on affiche l'onglet de resume si requis
        AdminStep newStep = getSteps().get(getSteps().size() - 1);
        if (log.isDebugEnabled()) {
            log.debug("Operation canceled, will go to final step " + newStep);
        }
        gotoStep(newStep);
    }

    @Override
    public AdminUIModel addOperation(AdminStep step) {
        if (AdminStep.EXPORT_DATA == step) {

            // pour exporter les données utilisateurs
            // l'opération de synchronisation de référentiel est nécessaire
            getOperations().add(AdminStep.SYNCHRONIZE);

            // l'opération de calcul des données est aussi obligatoire
            getOperations().add(AdminStep.CONSOLIDATE);
        }

        if (AdminStep.REPORT == step) {

            // l'opération de calcul des données est obligatoire
            getOperations().add(AdminStep.CONSOLIDATE);
        }
        return (AdminUIModel) super.addOperation(step);
    }

    @Override
    public void removeOperation(AdminStep step) {
        if (AdminStep.SYNCHRONIZE == step) {

            // pour exporter les données utilisateurs
            // l'opération de synchronisation de référentiel est nécessaire
            getOperations().remove(AdminStep.EXPORT_DATA);
        }

        if (AdminStep.CONSOLIDATE == step) {

            // pour exporter les données utilisateurs ou effectuer des reports
            // l'opération de calcul des données est nécessaire
            getOperations().remove(AdminStep.EXPORT_DATA);
            getOperations().remove(AdminStep.REPORT);
        }

        super.removeOperation(step);
    }

    @Override
    public boolean validate(AdminStep step) {

        boolean validate = super.validate(step) && !getOperations().isEmpty();
        if (!validate) {
            if (log.isDebugEnabled()) {
                log.debug("not valid from generic states...");
            }
            return false;
        }

        switch (step) {
            case CONFIG:
                return getConfigModel().validate(this);
            case SELECT_DATA:
                return getSelectDataModel().validate(this);
            case VALIDATE:
                return getValidateModel().validate(this);
            case EXPORT_DATA:
                return getExportModel().validate(this);
            case CONSOLIDATE:
                return getConsolidateModel().validate(this);
            case REPORT:
                return getReportModel().validate(this);
            case SYNCHRONIZE:
                return getSynchronizeReferentielModel().validate(this, step);
            case SAVE_LOCAL:
                return getSaveLocalModel().validate(this, step);
            case SHOW_RESUME:
                return getShowResumeModel().validate(this);
        }
        return true;
    }

    @Override
    protected AdminStep[] updateStepUniverse() {

        List<AdminStep> universe = new ArrayList<>();

        // toujours l'onglet de configuration des opérations
        universe.add(AdminStep.CONFIG);

        // pour savoir si on doit ajouter l'onglet de résumé (cas par défaut)
        boolean needResume = true;
        if (!operations.isEmpty()) {

            if (needSelect()) {

                // ajout de l'onglet de selection des donnees
                universe.add(AdminStep.SELECT_DATA);
            }

            if (containsOperation(AdminStep.DATA_SYNCHRONIZE)) {

                universe.add(AdminStep.DATA_SYNCHRONIZE);
            }

            if (containsOperation(AdminStep.REFERENTIAL_SYNCHRONIZE)) {

                universe.add(AdminStep.REFERENTIAL_SYNCHRONIZE);
            }

            if (containsOperation(AdminStep.SYNCHRONIZE)) {

                // ajout de l'onglet de resolution des entites obsoletes
                universe.add(AdminStep.SYNCHRONIZE);
            }

            if (containsOperation(AdminStep.VALIDATE)) {

                // ajout de l'onglet de validation
                universe.add(AdminStep.VALIDATE);
            }

            if (containsOperation(AdminStep.CONSOLIDATE)) {

                // ajout de l'onglet de consolidation
                universe.add(AdminStep.CONSOLIDATE);
            }

            if (containsOperation(AdminStep.REPORT)) {

                universe.add(AdminStep.REPORT);

                // pas de page de resume
                needResume = false;
            }

            updateSaveLocalOperation();

            if (containsOperation(AdminStep.SAVE_LOCAL)) {

                // ajout de l'onglet de sauvegarde de la base locale
                universe.add(AdminStep.SAVE_LOCAL);
            }

            if (containsOperation(AdminStep.EXPORT_DATA)) {

                // ajout de l'onglet de sauvegarde de la base distante
                universe.add(AdminStep.EXPORT_DATA);
            }

            if (needResume) {

                // ajout d'un onglet de resume
                universe.add(AdminStep.SHOW_RESUME);
            }
        }
        return universe.toArray(new AdminStep[universe.size()]);
    }

    protected void updateSaveLocalOperation() {
        boolean shouldAdd = false;
        for (AdminStep s : operations) {
            if (s.isNeedSave()) {
                shouldAdd = true;
            }
        }
        if (shouldAdd) {

            // ajout de l'operations
            operations.add(AdminStep.SAVE_LOCAL);
        } else {

            // on doit supprimer l'operations
            operations.remove(AdminStep.SAVE_LOCAL);
        }
    }

    public <E> EnumSet<DbMode> getAvailableIncomingModes() {
        return availableIncomingModes;
    }

    public static class LogPropertyChanged implements PropertyChangeListener {
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            String name = evt.getPropertyName();
            Object oldValue = evt.getOldValue();
            Object newValue = evt.getNewValue();
            if (log.isDebugEnabled()) {
                log.debug(evt.getSource() + " - Property [" + name + "] has changed from  " + oldValue + " to " + newValue);
            }

        }
    }

}
