package fr.ird.observe.application.swing.ui.tree.navigation.nodes;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.decoration.ObserveI18nDecoratorHelper;
import fr.ird.observe.services.dto.IdDto;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 14/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public abstract class ClassNavigationTreeNode<D extends IdDto> extends NavigationTreeNodeSupport<Class<D>> {

    protected ClassNavigationTreeNode(Class<D> data) {
        this(data, false);
    }

    protected ClassNavigationTreeNode(Class<D> data, boolean allowChildren) {
        super(data, allowChildren);
    }

    @Override
    public Class<D> getDataType() {
        return getData();
    }

    @Override
    public void reload() {
    }

    @Override
    public String getId() {
        return getParent().getId();
    }

    @Override
    public String getText() {
        return t(ObserveI18nDecoratorHelper.getTypeI18nKey(getDataType()));
    }

    @Override
    public String getIconPath() {
        return "navigation." + getParent().getDataType().getName() + ".sub";
    }

    @Override
    public boolean isOpen() {
        // ouvert si le parent est ouvert
        return getParent().isOpen();
    }
}
