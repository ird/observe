package fr.ird.observe.application.swing.ui.storage.tabs;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.I18nEnumHelper;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.ObserveTextGenerator;
import fr.ird.observe.application.swing.configuration.constants.CreationMode;
import fr.ird.observe.application.swing.ui.storage.StorageStep;
import fr.ird.observe.application.swing.ui.storage.StorageUIModel;
import org.nuiton.jaxx.runtime.spi.UIHandler;

import javax.swing.JPanel;
import javax.swing.JRadioButton;
import java.awt.Component;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.Date;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 27/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since X
 */
public class ChooseDbModeUIHandler extends StorageTabUIHandler<ChooseDbModeUI> implements UIHandler<ChooseDbModeUI> {

    @Override
    public void afterInit(ChooseDbModeUI ui) {
        PropertyChangeListener listener = evt -> {
            StorageUIModel model = (StorageUIModel) evt.getSource();

            ObserveSwingApplicationContext applicationContext = ObserveSwingApplicationContext.get();
            ObserveTextGenerator textGenerator = applicationContext.getTextGenerator();
            String txt = textGenerator.getLoadDataSourceResume(model);
            ui.getResume().setText(txt);
        };
        StorageUIModel model = ui.getModel();
        model.addPropertyChangeListener(StorageUIModel.DB_MODE_PROPERTY_NAME, listener);
        model.addPropertyChangeListener(StorageUIModel.CREATION_MODE_PROPERTY_NAME, listener);
        ui.setDescriptionText(t(StorageStep.CHOOSE_DB_MODE.getDescription()));
    }

    protected String updateInternalDumpModeLabel(@SuppressWarnings("unused") boolean dumpExist) {
        File f = ObserveSwingApplicationContext.get().getConfig().getInitialDbDump();
        String text;
        if (f.exists()) {
            text = t("observe.storage.internalDump.last.modified", new Date(f.lastModified()));
        } else {
            text = t("observe.storage.internalDump.not.exist");
        }
        return I18nEnumHelper.getLabel(CreationMode.IMPORT_INTERNAL_DUMP) + text;
    }

    public boolean updateCreationModeLayout(boolean visible, JRadioButton button) {
        JPanel panel = ui.getUseCreateMode();
        if (visible) {
            for (Component c : panel.getComponents()) {
                if (button.equals(c)) {
                    // button already in
                    return true;
                }
            }

            panel.add(button);
        } else {
            for (Component c : panel.getComponents()) {
                if (button.equals(c)) {
                    // button still in
                    panel.remove(c);
                    return false;
                }
            }
        }
        return visible;
    }
}
