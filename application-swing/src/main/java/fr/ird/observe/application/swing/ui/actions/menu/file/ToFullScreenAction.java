package fr.ird.observe.application.swing.ui.actions.menu.file;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.configuration.ObserveSwingApplicationConfig;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.actions.AbstractUIAction;

import java.awt.event.ActionEvent;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 27/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class ToFullScreenAction extends AbstractUIAction {

    private static final long serialVersionUID = 3038774900992805790L;

    public static final String ACTION_NAME = ToFullScreenAction.class.getSimpleName();

    public ToFullScreenAction(ObserveMainUI ui) {

        super(ui, ACTION_NAME, t("observe.action.toFullScreen"), t("observe.action.toFullScreen.tip"), "full-screen", null);
        putValue(MNEMONIC_KEY, (int) 'S');

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        ObserveSwingApplicationConfig config = getMainUI().getConfig();
        config.setFullScreen(true);

        getMainUI().getInitializer().reloadUI(ObserveSwingApplicationContext.get(), config);

    }
}
