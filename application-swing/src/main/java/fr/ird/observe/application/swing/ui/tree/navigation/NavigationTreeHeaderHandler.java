package fr.ird.observe.application.swing.ui.tree.navigation;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.ui.ObserveKeyStrokes;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.NavigationTreeNodeSupport;
import fr.ird.observe.services.dto.longline.TripLonglineDto;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.search.Searchable;
import org.jdesktop.swingx.search.TreeSearchable;

import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import java.awt.Color;
import java.awt.event.ActionEvent;

/**
 * @author Samuel Maisonneuve- maisonneuve@codelutin.com
 */
public class NavigationTreeHeaderHandler implements UIHandler<NavigationTreeHeader> {

    /** Logger */
    private static final Log log = LogFactory.getLog(NavigationTreeHeaderHandler.class);

    private NavigationTreeHeader ui;

    public NavigationTreeHeader getUi() {
        return ui;
    }

    private NavigationTree getTree() {
        return getUi().getTree();
    }

    @Override
    public void beforeInit(NavigationTreeHeader ui) {
        this.ui = ui;
    }

    @Override
    public void afterInit(NavigationTreeHeader ui) {

        ui.getShowSeine().setIcon(UIHelper.getUIManagerIcon("navigation." + TripSeineDto.class.getName() + "-16"));
        ui.getShowLongline().setIcon(UIHelper.getUIManagerIcon("navigation." + TripLonglineDto.class.getName() + "-16"));
        ui.getShowReferential().setIcon(UIHelper.getUIManagerIcon("navigation.referentiel-16"));
        ui.getShowEmptyProgram().setIcon(UIHelper.getUIManagerIcon("navigation." + ProgramDto.class.getName() + "-16"));

        NavigationTree tree = ui.getTree();
        TreeSearchable searchable = new TreeSearchable(tree);
        tree.setSearchable(searchable);
        searchable.setMatchHighlighter(new ColorHighlighter(Color.YELLOW, Color.BLACK));

        ObserveKeyStrokes.addKeyStroke(ui.getShowSeine(), ObserveKeyStrokes.KEY_STROKE_SHOW_SEINE);
        ObserveKeyStrokes.addKeyStroke(ui.getShowLongline(), ObserveKeyStrokes.KEY_STROKE_SHOW_LONGLINE);
        ObserveKeyStrokes.addKeyStroke(ui.getShowReferential(), ObserveKeyStrokes.KEY_STROKE_SHOW_REFERENTIAL);
        ObserveKeyStrokes.addKeyStroke(ui.getShowEmptyProgram(), ObserveKeyStrokes.KEY_STROKE_SHOW_EMPTY_PROGRAM);

        InputMap inputMap = ui.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        ActionMap actionMap = ui.getActionMap();
        inputMap.put(ObserveKeyStrokes.KEY_STROKE_SHOW_SEINE, "showSeine");
        actionMap.put("showSeine", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ui.getShowSeine().doClick();
            }
        });
        inputMap.put(ObserveKeyStrokes.KEY_STROKE_SHOW_LONGLINE, "showLongline");
        actionMap.put("showLongline", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ui.getShowLongline().doClick();
            }
        });
        inputMap.put(ObserveKeyStrokes.KEY_STROKE_SHOW_REFERENTIAL, "showReferential");
        actionMap.put("showReferential", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ui.getShowReferential().doClick();
            }
        });
        inputMap.put(ObserveKeyStrokes.KEY_STROKE_SHOW_EMPTY_PROGRAM, "showEmptyProgram");
        actionMap.put("showEmptyProgram", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ui.getShowEmptyProgram().doClick();
            }
        });
    }

    public void search(String term) {

        Searchable searchable = getTree().getSearchable();
        searchable.search(term);

    }

    public void tryToUpdateNavigationTree() {

        boolean canContinue = ObserveSwingApplicationContext.get().getContentUIManager().closeSelectedContentUI();

        if (!canContinue) {
            return;
        }

        NavigationTree tree = getTree();

        NavigationTreeNodeSupport<?> selectedNode = tree.getSelectedNode();
        if (log.isInfoEnabled()) {
            log.info("Selected node: " + selectedNode);
        }
        tree.clearSelection();

        boolean showSeine = ui.getShowSeine().isSelected();
        boolean showLongline = ui.getShowLongline().isSelected();
        boolean showReferential = ui.getShowReferential().isSelected();
        boolean showEmptyProgram = ui.getShowEmptyProgram().isSelected();

        NavigationTreeModel treeModel = tree.getTreeModel();
        treeModel.setLoadSeine(showSeine);
        treeModel.setLoadLongline(showLongline);
        treeModel.setLoadReferential(showReferential);
        treeModel.setLoadEmptyProgram(showEmptyProgram);

        treeModel.populate();

        //FIXME repositionner le nœud sélectionné
        if (!treeModel.getRoot().isLeaf()) {
            tree.setSelectionRow(0);
        }
    }

    public void collapseAll() {

        NavigationTree tree = getTree();
        tree.clearSelection();
        tree.collapseAll();
        tree.setSelectionRow(0);

    }

    public void expandAll() {

        NavigationTree tree = getTree();
        tree.expandAll();

    }

}
