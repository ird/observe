<!--
  #%L
  ObServe :: Application Swing
  %%
  Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

<fr.ird.observe.application.swing.ui.content.open.ContentOpenableUI superGenericType='ActivitySeineDto, ActivitySeineUI'
                                                                    contentTitle='{n("observe.storage.activitySeine.title")}'>

  <style source="../../../Common.jcss"/>

  <import>
    fr.ird.observe.services.dto.seine.ActivitySeineDto
    fr.ird.observe.services.dto.seine.SetSeineDto
    fr.ird.observe.services.dto.seine.FloatingObjectDto
    fr.ird.observe.services.dto.reference.ReferentialReference
    fr.ird.observe.services.dto.referential.FpaZoneDto
    fr.ird.observe.services.dto.referential.seine.ObservedSystemDto
    fr.ird.observe.services.dto.referential.seine.VesselActivitySeineDto
    fr.ird.observe.services.dto.referential.seine.SurroundingActivityDto
    fr.ird.observe.services.dto.referential.seine.ReasonForNoFishingDto
    fr.ird.observe.services.dto.referential.seine.DetectionModeDto
    fr.ird.observe.services.dto.referential.seine.WindDto

    fr.ird.observe.application.swing.ui.actions.content.DeleteDataUIAction
    fr.ird.observe.application.swing.ui.actions.content.AddActivitySeineSetUIAction
    fr.ird.observe.application.swing.ui.actions.content.AddActivitySeineFloatingObjectUIAction
    fr.ird.observe.application.swing.ui.util.JComment

    org.nuiton.jaxx.widgets.number.NumberEditor
    org.nuiton.jaxx.widgets.select.BeanComboBox

    org.nuiton.jaxx.widgets.datetime.TimeEditor
    org.nuiton.jaxx.widgets.gis.absolute.CoordinatesEditor
    org.nuiton.jaxx.widgets.select.FilterableDoubleList

    java.awt.Dimension
    java.util.Collection

    static fr.ird.observe.application.swing.ui.UIHelper.getStringValue
    static org.nuiton.i18n.I18n.n
  </import>

  <!-- model -->
  <ActivitySeineUIModel id='model'/>

  <!-- edit bean -->
  <ActivitySeineDto id='bean'/>

  <!-- validator -->
  <BeanValidator id='validator' autoField='true' beanClass='fr.ird.observe.services.dto.seine.ActivitySeineDto'
                 errorTableModel='{getErrorTableModel()}' context='ui-create'>

    <field name='longitude' component='coordinatesEditor'/>
    <field name='latitude' component='coordinatesEditor'/>
    <field name='quadrant' component='coordinatesEditor'/>
    <field name='floatingObjectEmpty' component='addDCP'/>

  </BeanValidator>

  <!-- formulaire -->
  <JPanel id="body">
    <Table insets="0" fill="both" constraints='BorderLayout.CENTER'>
      <row>
        <cell anchor="north" weightx="1">
          <JTabbedPane id='mainTabbedPane'>
            <tab id='generalTab'>
              <Table fill="both">
                <!-- heure observation  -->
                <row>
                  <cell columns="2" weightx="1">
                    <TimeEditor id='time' constructorParams='this'/>
                  </cell>
                </row>

                <!--  position -->
                <row>
                  <cell anchor='west'>
                    <JLabel id='coordinatesLabel'/>
                  </cell>
                  <cell anchor='east' fill="both">
                    <CoordinatesEditor id='coordinatesEditor' constructorParams='this'/>
                  </cell>
                </row>

                <!-- activity vessel -->
                <row>
                  <cell anchor='west' columns="2">
                    <JLabel id='vesselActivityInformation'/>
                  </cell>
                </row>
                <row>
                  <cell anchor='west'>
                    <JLabel id='vesselActivitySeineLabel'/>
                  </cell>
                  <cell anchor='east' weightx="1">
                    <BeanComboBox id='vesselActivitySeine'
                                  genericType='ReferentialReference&lt;VesselActivitySeineDto&gt;'
                                  _entityClass='VesselActivitySeineDto.class' constructorParams='this'/>
                  </cell>
                </row>

                <!--  activity environnante -->
                <row>
                  <cell anchor='west'>
                    <JLabel id='surroundingActivityLabel'/>
                  </cell>
                  <cell anchor='east' weightx="1">
                    <BeanComboBox id='surroundingActivity' constructorParams='this'
                                  genericType='ReferentialReference&lt;SurroundingActivityDto&gt;'
                                  _entityClass='SurroundingActivityDto.class'/>
                  </cell>
                </row>

                <!--  previousFpaZone -->
                <row>
                  <cell anchor='west'>
                    <JLabel id='previousFpaZoneLabel'/>
                  </cell>
                  <cell anchor='east' weightx="1">
                    <BeanComboBox id='previousFpaZone' constructorParams='this'
                                  genericType='ReferentialReference&lt;FpaZoneDto&gt;' _entityClass='FpaZoneDto.class'/>
                  </cell>
                </row>

                <!--  currentFpaZone -->
                <row>
                  <cell anchor='west'>
                    <JLabel id='currentFpaZoneLabel'/>
                  </cell>
                  <cell anchor='east' weightx="1">
                    <BeanComboBox id='currentFpaZone' constructorParams='this'
                                  genericType='ReferentialReference&lt;FpaZoneDto&gt;' _entityClass='FpaZoneDto.class'/>
                  </cell>
                </row>

                <!--  nextFpaZone -->
                <row>
                  <cell anchor='west'>
                    <JLabel id='nextFpaZoneLabel'/>
                  </cell>
                  <cell anchor='east' weightx="1">
                    <BeanComboBox id='nextFpaZone' constructorParams='this'
                                  genericType='ReferentialReference&lt;FpaZoneDto&gt;' _entityClass='FpaZoneDto.class'/>
                  </cell>
                </row>

                <!-- id ers  -->
                <row>

                  <cell anchor='west'>
                    <JLabel id='ersIdLabel'/>
                  </cell>
                  <cell anchor='east' weightx="1" fill="both">
                    <JPanel layout='{new BorderLayout()}'>
                      <JToolBar id='ersIdToolbar' constraints='BorderLayout.WEST'>
                        <JButton id='resetErsId' constraints='BorderLayout.WEST' styleClass='resetButton'/>
                      </JToolBar>
                      <JTextField id='ersId' constraints='BorderLayout.CENTER'/>
                    </JPanel>
                  </cell>
                </row>
              </Table>
            </tab>

            <tab id='measurementsTab'>
              <Table fill="both">
                <!-- vitesse vessel -->
                <row>
                  <cell anchor='west'>
                    <JLabel id='vesselSpeedLabel'/>
                  </cell>
                  <cell anchor='east' weightx="1" fill="both">
                    <NumberEditor id='vesselSpeed' constructorParams='this' styleClass="float2"/>
                  </cell>
                </row>

                <!-- température de surface -->
                <row>
                  <cell anchor='west'>
                    <JLabel id='seaSurfaceTemperatureLabel'/>
                  </cell>
                  <cell anchor='east' fill="both">
                    <NumberEditor id='seaSurfaceTemperature' constructorParams='this' styleClass="float2"/>
                  </cell>
                </row>

                <!--  vent beaufort -->
                <row>
                  <cell anchor='west'>
                    <JLabel id='windLabel'/>
                  </cell>
                  <cell anchor='east'>
                    <BeanComboBox id='wind' genericType='ReferentialReference&lt;WindDto&gt;'
                                  _entityClass='WindDto.class' constructorParams='this'/>
                  </cell>
                </row>

                <!--  mode de détection -->
                <row>
                  <cell anchor='west'>
                    <JLabel id='detectionModeLabel'/>
                  </cell>
                  <cell anchor='east'>
                    <BeanComboBox id='detectionMode' genericType='ReferentialReference&lt;DetectionModeDto&gt;'
                                  _entityClass='DetectionModeDto.class' constructorParams='this'/>
                  </cell>
                </row>

                <!-- cause non coups senne  -->
                <row>
                  <cell anchor='west'>
                    <JLabel id='reasonForNoFishingLabel'/>
                  </cell>
                  <cell anchor='east'>
                    <BeanComboBox id='reasonForNoFishing'
                                  genericType='ReferentialReference&lt;ReasonForNoFishingDto&gt;'
                                  _entityClass='ReasonForNoFishingDto.class' constructorParams='this'/>
                  </cell>
                </row>
                <row>
                  <cell columns="2" weighty="1">
                    <JLabel/>
                  </cell>
                </row>
              </Table>
            </tab>

            <tab id="observedSystemTab">
              <Table fill="both">
                <!-- systeme observe -->
                <row>
                  <cell columns="2" fill="both" weighty="0.7">
                    <JScrollPane id='observedSystemPane' onFocusGained='observedSystem.requestFocus()'>
                      <FilterableDoubleList id='observedSystem' _entityClass='ObservedSystemDto.class'
                                            genericType='ReferentialReference&lt;ObservedSystemDto&gt;'/>
                    </JScrollPane>
                  </cell>
                </row>
                <!-- distance au système observe -->
                <row>
                  <cell anchor='west'>
                    <JLabel id='observedSystemDistanceLabel'/>
                  </cell>
                  <cell weightx="1" fill="both">
                    <NumberEditor id='observedSystemDistance' constructorParams='this' styleClass="float2"/>
                  </cell>
                </row>
              </Table>
            </tab>

          </JTabbedPane>
        </cell>
      </row>

      <!--  comment -->
      <row>
        <cell columns='2' weighty="1">
          <JComment id="comment"/>
        </cell>
      </row>
    </Table>
  </JPanel>

  <!-- surcharge des actions (pour appliquer la css specifique -->
  <JButton id='delete'/>
  <JButton id='reopen'/>
  <JButton id='close'/>
  <JButton id='closeAndCreate'/>
  <JButton id='actionDown'/>

  <Table id='extraActions' fill="both" weightx="1" insets='2'>
    <row>
      <cell weightx="0.5" fill="both">
        <JButton id='addSet'/>
      </cell>
      <cell weightx="0.5" fill="both">
        <JButton id='addDCP'/>
      </cell>
    </row>
  </Table>

</fr.ird.observe.application.swing.ui.content.open.ContentOpenableUI>
