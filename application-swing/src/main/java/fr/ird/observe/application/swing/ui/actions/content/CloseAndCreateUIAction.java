/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.actions.content;

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.DataContext;
import fr.ird.observe.application.swing.ui.ObserveKeyStrokes;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.content.ContentUI;
import fr.ird.observe.application.swing.ui.content.list.impl.longline.ActivityLonglinesUI;
import fr.ird.observe.application.swing.ui.content.list.impl.seine.ActivitySeinesUI;
import fr.ird.observe.application.swing.ui.content.open.ContentOpenableUI;
import fr.ird.observe.application.swing.ui.content.open.impl.longline.ActivityLonglineUI;
import fr.ird.observe.application.swing.ui.content.open.impl.seine.ActivitySeineUI;
import fr.ird.observe.application.swing.ui.tree.navigation.NavigationTree;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.NavigationTreeNodeSupport;

import javax.swing.tree.TreePath;

import static org.nuiton.i18n.I18n.n;

/**
 * Action pour fermer l'objet sous-jacent à l'écran et en créer un nouveau.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.5
 */
public class CloseAndCreateUIAction extends AbstractContentUIAction {

    private static final long serialVersionUID = 1L;

    public static final String ACTION_NAME = "closeAndCreate";

    public CloseAndCreateUIAction(ObserveMainUI mainUI) {
        super(mainUI,
              ACTION_NAME,
              n("observe.content.action.closeAndCreate"),
              n("observe.content.action.closeAndCreate.tip"),
              "add",
              ObserveKeyStrokes.KEY_STROKE_CLOSE_AND_CREATE
        );
    }

    @Override
    protected void actionPerformed(ContentUI<?, ?> ui) {

        if (ui instanceof ContentOpenableUI) {
            ((ContentOpenableUI<?, ?>) ui).closeAndCreateData();
            return;
        }

        DataContext dataContext = getMainUI().getDataContext();
        if (ui instanceof ActivitySeinesUI) {

            // cas particulier pour l'écran des activités

            if (!dataContext.isOpenActivitySeine()) {

                ui.createData();
                return;
            }

            NavigationTree tree = getMainUI().getNavigation();

            boolean wasCollapsed = isOpenActivityNodeCollapsed(tree, dataContext);

            // selection du noeud de l'activité ouverte
            getMainUI().getMenuNavigationActivity().doClick();

            NavigationTreeNodeSupport selectedNode = tree.getSelectedNode();
            selectedNode = tree.getActivitySeineNode(selectedNode, dataContext.getOpenActivitySeineId());

            tree.selectNode(selectedNode);

            // on conserve le path de l'activité
            TreePath path = tree.getSelectionPath();

            // recuperation de l'écran associé
            ActivitySeineUI selectedUI = (ActivitySeineUI)
                    ObserveSwingApplicationContext.get().getContentUIManager().getSelectedContentUI();

            // fermeture de l'activité et création d'une nouvelle
            selectedUI.closeAndCreateData();

            if (wasCollapsed) {

                // on ferme le noeud de l'activity (qui a ete ouvert
                // lors de la selection de celle-ci)
                tree.collapsePath(path);
            }

            return;
        }

        if (ui instanceof ActivityLonglinesUI) {

            // cas particulier pour l'écran des activités

            if (!dataContext.isOpenActivityLongline()) {

                ui.createData();
                return;
            }

            NavigationTree tree = getMainUI().getNavigation();

            boolean wasCollapsed = isOpenActivityNodeCollapsed(tree, dataContext);

            // selection du noeud de l'activity ouverte
            NavigationTreeNodeSupport selectedNode = tree.getSelectedNode();
            selectedNode = tree.getActivityLonglineNode(selectedNode, dataContext.getOpenActivitySeineId());
            tree.selectNode(selectedNode);

            // on conserve le path de l'activité
            TreePath path = tree.getSelectionPath();

            // recuperation de l'écran associé
            ActivityLonglineUI selectedUI = (ActivityLonglineUI)
                    ObserveSwingApplicationContext.get().getContentUIManager().getSelectedContentUI();

            // fermeture de l'activité et création d'une nouvelle
            selectedUI.closeAndCreateData();

            if (wasCollapsed) {

                // on ferme le noeud de l'activity (qui a ete ouvert
                // lors de la selection de celle-ci)
                tree.collapsePath(path);
            }

            return;
        }

        throw new IllegalStateException("Can not come here!");
    }

    protected boolean isOpenActivityNodeCollapsed(NavigationTree tree, DataContext dataContext) {

        // on regarde si le noeud de l'activité ouverte est collapsé

        if (dataContext.isOpenActivity()) {

            NavigationTreeNodeSupport node = tree.getSelectedNode();
            TreePath path = tree.getSelectionPath();

            String id = dataContext.getOpenActivityId();
            NavigationTreeNodeSupport node1 = tree.getChild(node, id);

            if (node1 != null) {
                TreePath activityPath = path.pathByAddingChild(node1);
                return tree.isCollapsed(activityPath);
            }
        }

        return false;
    }

}
