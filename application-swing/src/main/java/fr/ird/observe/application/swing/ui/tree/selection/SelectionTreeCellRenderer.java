package fr.ird.observe.application.swing.ui.tree.selection;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ui.tree.selection.nodes.SelectionTreeNodeSupport;
import fr.ird.observe.application.swing.ui.tree.selection.nodes.TripLonglineSelectionTreeNode;
import fr.ird.observe.application.swing.ui.tree.selection.nodes.TripSeineSelectionTreeNode;
import org.jdesktop.swingx.tree.DefaultXTreeCellRenderer;

import javax.swing.Icon;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JTree;
import java.awt.BorderLayout;
import java.awt.Component;

/**
 * Created on 14/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class SelectionTreeCellRenderer extends DefaultXTreeCellRenderer {

    private final JPanel panel;
    private final JCheckBox selected;

    public SelectionTreeCellRenderer() {
        selected = new JCheckBox();
        panel = new JPanel(new BorderLayout(2, 2));
        panel.setOpaque(false);
        panel.add(selected, BorderLayout.WEST);
        panel.add(this, BorderLayout.CENTER);
    }

    public Icon getIcon(SelectionTreeNodeSupport node) {
        return node.getIcon("");
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        SelectionTreeNodeSupport node = (SelectionTreeNodeSupport) value;
        Icon icon = getIcon(node);
        super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
        setIcon(icon);
        if (node instanceof TripSeineSelectionTreeNode) {
            setToolTipText(((TripSeineSelectionTreeNode) node).getToolTipText());
        } else if (node instanceof TripLonglineSelectionTreeNode) {
            setToolTipText(((TripLonglineSelectionTreeNode) node).getToolTipText());
        }
        selected.setSelected(node.isSelected());
        return panel;
    }

}
