package fr.ird.observe.application.swing.ui.content;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import fr.ird.observe.application.swing.ObserveSwingTechnicalException;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.actions.AbstractUIAction;
import fr.ird.observe.application.swing.ui.actions.global.AbstractGlobalUIAction;
import org.apache.commons.beanutils.ConstructorUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.reflections.Reflections;

import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.Set;

/**
 * Created on 10/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class ObserveActionMap extends ActionMap {

    /** Logger */
    private static final Log log = LogFactory.getLog(ObserveActionMap.class);

    private final ObserveMainUI ui;
    private final ImmutableMap<String, Class<? extends AbstractUIAction>> mapping;
    private final ImmutableMap<String, ? extends AbstractGlobalUIAction> globalActions;

    public ObserveActionMap(ObserveMainUI ui) {
        this.ui = ui;
        Set<Class<? extends AbstractUIAction>> actionTypes = new Reflections("fr.ird.observe.application.swing.ui.actions").getSubTypesOf(AbstractUIAction.class);

        InputMap inputMap = ui.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);

        ImmutableMap.Builder<String, Class<? extends AbstractUIAction>> mappingBuilder = ImmutableMap.builder();
        ImmutableMap.Builder<String, AbstractGlobalUIAction> globalActionsBuilder = ImmutableMap.builder();
        for (Class<? extends AbstractUIAction> actionType : actionTypes) {

            if (Modifier.isAbstract(actionType.getModifiers())) {
                continue;
            }

            String actionId;
            try {
                actionId = (String) actionType.getDeclaredField("ACTION_NAME").get(null);
            } catch (IllegalAccessException | NoSuchFieldException e) {
                throw new ObserveSwingTechnicalException("Can't find ACTION_NAME field on action: " + actionType.getName());
            }

            if (AbstractGlobalUIAction.class.isAssignableFrom(actionType)) {

                // action globale
                Class<? extends AbstractGlobalUIAction> globalActionType = (Class) actionType;
                if (log.isInfoEnabled()) {
                    log.info("Register global action: " + actionId + " - type: " + globalActionType.getName());
                }
                AbstractGlobalUIAction globalUIAction = newAction(actionId, globalActionType);
                globalActionsBuilder.put(actionId, globalUIAction);
                inputMap.put(globalUIAction.getAcceleratorKey(), actionId);

            } else {

                // action simple
                if (log.isInfoEnabled()) {
                    log.info("Register action: " + actionId + " - type: " + actionType.getName());
                }
                mappingBuilder.put(actionId, actionType);

            }

        }

        mapping = mappingBuilder.build();
        globalActions = globalActionsBuilder.build();

    }

    @Override
    public Action get(Object key) {

        AbstractGlobalUIAction globalUIAction = globalActions.get(key.toString());
        if (globalUIAction != null) {
            return globalUIAction;
        }

        Class<? extends AbstractUIAction> actionType = mapping.get(key.toString());
        if (actionType == null) {

            return super.get(key);
        }

        return newAction((String) key, actionType);

    }

    public ImmutableMap<String, ? extends AbstractGlobalUIAction> getGlobalActions() {
        return globalActions;
    }

    private <A extends AbstractUIAction> A newAction(String key, Class<A> actionType) {

        try {
            if (log.isDebugEnabled()) {
                log.debug("Create action: " + key + ", type: " + actionType.getName());
            }
            return ConstructorUtils.invokeConstructor(actionType, ui);
        } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
            throw new ObserveSwingTechnicalException("Hum, can't creation action for type: " + actionType.getName(), e);
        }

    }

}
