/*
 * #%L
 * ObServe :: Swing
 * %%
 * Copyright (C) 2008 - 2010 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

#PENDING_content {
  layout:{new BorderLayout()};
}

#NEED_FIX_content {
  layout:{new BorderLayout()};
}

#startAction {
  actionIcon:"wizard-start";
  text:"observe.actions.synchro.referential.launch.operation";
  mnemonic:D;
}

#contentSplitPane {
  orientation: {JSplitPane.VERTICAL_SPLIT};
  resizeWeight: 0.8;
}

#applyAction {
  action: {new ApplyAction(this)};
}

#leftTree {
  rootVisible:false;
  largeModel:true;
  minimumSize:{UIHelper.newMinDimension()};
  font-size:11;
  toggleClickCount:100;
  cellRenderer:{new ReferentialSynchronizeTreeCellRenderer()};
}

#leftTreePane {
  verticalScrollBarPolicy:{JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED};
}

#rightTree {
  rootVisible:false;
  largeModel:true;
  minimumSize:{UIHelper.newMinDimension()};
  font-size:11;
  toggleClickCount:100;
  cellRenderer:{new ReferentialSynchronizeTreeCellRenderer()};
}

#rightTreePane {
  verticalScrollBarPolicy:{JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED};
}

#copyLeft {
  action: {new RegisterCopyTaskAction(this, true)};
  visible:{stepModel.getSynchronizeMode().isRightWrite()};
}

#copyRight {
  action: {new RegisterCopyTaskAction(this, false)};
  visible:{stepModel.getSynchronizeMode().isLeftWrite()};
}

#revertLeft {
    action: {new RegisterRevertTaskAction(this, true)};
    visible:{stepModel.getSynchronizeMode().isLeftWrite()};
}

#revertRight {
  action: {new RegisterRevertTaskAction(this, false)};
  visible:{stepModel.getSynchronizeMode().isRightWrite()};
}

#skipLeft {
    action: {new RegisterSkipTaskAction(this, true)};
}

#skipRight {
    action: {new RegisterSkipTaskAction(this, false)};
}

#deleteLeft {
    action: {new RegisterDeleteTaskAction(this, true)};
    visible:{stepModel.getSynchronizeMode().isLeftWrite()};
}

#deleteRight {
    action: {new RegisterDeleteTaskAction(this, false)};
    visible:{stepModel.getSynchronizeMode().isRightWrite()};
}

#desactivateLeft {
    action: {new RegisterDesactivateTaskAction(this, true, false)};
    visible:{stepModel.getSynchronizeMode().isLeftWrite()};
}

#desactivateRight {
    action: {new RegisterDesactivateTaskAction(this, false, false)};
    visible:{stepModel.getSynchronizeMode().isRightWrite()};
}

#desactivateWithReplaceLeft {
    action: {new RegisterDesactivateTaskAction(this, true, true)};
    visible:{stepModel.getSynchronizeMode().isLeftWrite()};
}

#desactivateWithReplaceRight {
    action: {new RegisterDesactivateTaskAction(this, false, true)};
    visible:{stepModel.getSynchronizeMode().isRightWrite()};
}

#actionsToConsumePane {
  border:{new TitledBorder(t("observe.actions.synchro.data.actionsToPerform"))};
  verticalScrollBarPolicy:{JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED};
}

#actionsToConsume {
  selectionMode:{ListSelectionModel.SINGLE_SELECTION};
  cellRenderer:{new ReferentialSynchronizeTaskListCellRenderer()};
  model:{stepModel.getTasks()};
}