package fr.ird.observe.application.swing.ui.actions.menu.navigation;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.DataContext;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.actions.AbstractUIAction;
import fr.ird.observe.application.swing.ui.tree.navigation.NavigationTree;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.NavigationTreeNodeSupport;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.AbstractButton;
import java.awt.event.ActionEvent;
import java.util.Optional;

/**
 * Created on 02/12/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public abstract class GotoActionSupport extends AbstractUIAction {

    /** Logger */
    private static final Log log = LogFactory.getLog(GotoActionSupport.class);

    protected abstract boolean computeIsOpen(DataContext dataContext);

    protected abstract Optional<NavigationTreeNodeSupport> getNodeToSelect(NavigationTree navigation, DataContext dataContext);

    public GotoActionSupport(ObserveMainUI mainUI,
                             String actionName,
                             String openPropertyName,
                             int mnemonic,
                             String label,
                             String tip,
                             String icon) {

        super(mainUI, actionName, label, tip, null, null);

        putValue(SMALL_ICON, UIHelper.getUIManagerIcon(icon));
        putValue(MNEMONIC_KEY, mnemonic);

        if (openPropertyName != null) {
            DataContext dataContext = ObserveSwingApplicationContext.get().getDataContext();
            dataContext.addPropertyChangeListener(openPropertyName, e -> setEnabled(BooleanUtils.isTrue((Boolean) e.getNewValue())));
        }
    }

    @Override
    public void initForMainUi(AbstractButton editor) {
        super.initForMainUi(editor);

        DataContext dataContext = ObserveSwingApplicationContext.get().getDataContext();
        boolean open = computeIsOpen(dataContext);
        if (log.isInfoEnabled()) {
            log.info("Action [" + getValue(ACTION_COMMAND_KEY) + "] - open? " + open);
        }
        setEnabled(open);

    }

    @Override
    public final void actionPerformed(ActionEvent e) {

        DataContext dataContext = ObserveSwingApplicationContext.get().getDataContext();
        boolean open = computeIsOpen(dataContext);
        if (!open) {
            return;
        }

        NavigationTree navigation = getMainUI().getNavigation();

        getNodeToSelect(navigation, dataContext).ifPresent(navigation::selectNode);

    }

}
