package fr.ird.observe.application.swing.ui.actions.content;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.actions.AbstractUIAction;
import fr.ird.observe.application.swing.ui.content.ContentUI;

import javax.swing.KeyStroke;
import java.awt.event.ActionEvent;

/**
 * Created on 10/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public abstract class AbstractContentUIAction extends AbstractUIAction {


    protected ActionEvent e;

    public AbstractContentUIAction(ObserveMainUI mainUI, String actionCommandKey, String label, String shortDescription, String actionIcon, KeyStroke keyStroke) {
        super(mainUI, actionCommandKey, label, shortDescription, actionIcon, keyStroke);
    }

    protected abstract void actionPerformed(ContentUI<?, ?> contentUI);

    @Override
    public final void actionPerformed(ActionEvent e) {
        if (!canExecuteAction()) {
            return;
        }
        this.e = e;

        ContentUI<?, ?> contentUI = getContentUI(e);
        actionPerformed(contentUI);
    }
}
