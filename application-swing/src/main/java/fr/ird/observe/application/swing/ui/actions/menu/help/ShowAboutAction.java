package fr.ird.observe.application.swing.ui.actions.menu.help;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveResourceManager;
import fr.ird.observe.application.swing.ObserveRunner;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.ObserveTextGenerator;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.actions.AbstractUIAction;
import org.nuiton.jaxx.widgets.about.AboutUI;
import org.nuiton.jaxx.widgets.about.AboutUIBuilder;
import org.nuiton.util.Resource;

import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.File;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 1/17/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.13
 */
public class ShowAboutAction extends AbstractUIAction {

    private static final long serialVersionUID = 1L;
    public static final String ACTION_NAME = ShowAboutAction.class.getSimpleName();

    public ShowAboutAction(ObserveMainUI ui) {

        super(ui, ACTION_NAME, t("observe.action.about"), t("observe.action.about.tip"), "about", null);
        putValue(MNEMONIC_KEY, KeyEvent.VK_P);

    }

    @Override
    public void actionPerformed(ActionEvent event) {

        ObserveMainUI ui = getMainUI();

        File i18nFile = new File(ui.getConfig().getI18nDirectory(), ObserveResourceManager.OBSERVE_I18N_ARCHIVE.substring(1));
        String name = ObserveRunner.getRunner().getRunnerName();

        ObserveTextGenerator textGenerator = ObserveSwingApplicationContext.get().getTextGenerator();
        String aboutContent = textGenerator.getAbout(ui.getConfig());
        String translateContent = textGenerator.getTranslate(i18nFile);

        AboutUI about = AboutUIBuilder.builder(ui)
                                      .setIconPath("/icons/logo.png")
                                      .setTitle(t("observe.title.about"))
                                      .setBottomText(ui.getConfig().getCopyrightText())
                                      .addAboutTab(aboutContent, true)
                                      .addDefaultLicenseTab(name, false)
                                      .addDefaultThirdPartyTab(name, false)
                                      .addDefaultChangelogTab(name, false)
                                      .addTab(t("observe.about.translate.title"), translateContent, true)
                                      .build();

        // tweak top panel
        JPanel topPanel = about.getTopPanel();
        topPanel.removeAll();
        topPanel.setLayout(new BorderLayout());
        topPanel.add(new JLabel(Resource.getIcon("/icons/logo-small.png")), BorderLayout.WEST);
        topPanel.add(new JLabel(Resource.getIcon("/icons/logo_ird.png")), BorderLayout.EAST);

        about.display();

    }
}
