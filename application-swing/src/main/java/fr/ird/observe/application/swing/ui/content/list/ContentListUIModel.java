/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.content.list;

import fr.ird.observe.application.swing.ui.content.ContentUIModel;
import fr.ird.observe.services.dto.DataDto;
import fr.ird.observe.services.dto.IdDto;
import fr.ird.observe.services.dto.reference.DataReference;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.List;

/**
 * Le modèle pour un écran d'édition avec des fils.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since .14
 */
public abstract class ContentListUIModel<E extends IdDto, C extends DataDto> extends ContentUIModel<E> {

    private static final long serialVersionUID = 1L;

    /** Logger */
    private static final Log log = LogFactory.getLog(ContentListUIModel.class);

    public static final String PROPERTY_DATA = "data";
    public static final String PROPERTY_SELECTED_DATAS = "selectedDatas";
    public static final String PROPERTY_ONE_SELECTED_DATA = "oneSelectedData";
    public static final String PROPERTY_ONE_OR_MORE_SELECTED_DATA = "oneOrMoreSelectedData";

    public static final String PROPERTY_EMPTY = "empty";
    public static final String PROPERTY_CAN_REOPEN = "canReopen";
    public static final String PROPERTY_CAN_GOTO_SELECTED = "canGotoSelected";
    public static final String PROPERTYCAN_GOTO_OPEN = "canGotoOpen";
    public static final String PROPERTY_CAN_CLOSE = "canClose";
    public static final String PROPERTY_CAN_CREATE = "canCreate";

    public static final String PROPERTY_CAN_MOVE = "canMove";
    public static final String PROPERTY_LAYOUT_NAME = "layoutName";

    /** type des entites */
    protected final Class<C> childType;

    /** liste des entites */
    protected List<DataReference<C>> data;

    /** entités sélectionnées dans la liste */
    protected List<DataReference<C>> selectedDatas;

    private boolean canReopen;
    private boolean canGotoSelected;
    private boolean canGotoOpen;
    private boolean canClose;
    private boolean canCreate;
    private boolean canMove;

    private String layoutName;

    public ContentListUIModel(Class<E> beanType, Class<C> childType) {
        super(beanType);
        this.childType = childType;
    }

    @Override
    public void destroy() {
        super.destroy();
        dataReferenceSetsByPropertyName = null;
        selectedDatas = null;
    }

    public Class<C> getChildType() {
        return childType;
    }

    public List<DataReference<C>> getData() {
        return data;
    }

    public void setData(List<DataReference<C>> data) {
        boolean wasEmpty = isEmpty();
        this.data = data;
        // on force toujours la propagation de la liste
        firePropertyChange(PROPERTY_DATA, null, data);
        firePropertyChange(PROPERTY_EMPTY, wasEmpty, isEmpty());
        setSelectedDatas(null);
        setLayoutName(isEmpty() ? "empty" : "list");
    }

    public List<DataReference<C>> getSelectedDatas() {
        return selectedDatas;
    }

    public void setSelectedDatas(List<DataReference<C>> selectedDatas) {
        List<DataReference<C>> old = getSelectedDatas();
        this.selectedDatas = selectedDatas;
        if (log.isDebugEnabled()) {
            log.debug("New selected datas : " + selectedDatas);
        }
        firePropertyChange(PROPERTY_SELECTED_DATAS, old, selectedDatas);
        firePropertyChange(PROPERTY_ONE_SELECTED_DATA, isOneSelectedData());
        firePropertyChange(PROPERTY_ONE_OR_MORE_SELECTED_DATA, isOneOrMoreSelectedData());
    }

    public boolean isOneSelectedData() {
        return selectedDatas != null && selectedDatas.size() == 1;
    }

    public boolean isOneOrMoreSelectedData() {
        return CollectionUtils.isNotEmpty(selectedDatas);
    }

    public boolean isCanReopen() {
        return canReopen;
    }

    public void setCanReopen(boolean canReopen) {
        this.canReopen = canReopen;
        firePropertyChange(PROPERTY_CAN_REOPEN, canReopen);
    }

    public boolean isEmpty() {
        return data == null || data.isEmpty();
    }


    public boolean isCanGotoSelected() {
        return canGotoSelected;
    }

    public void setCanGotoSelected(boolean canGotoSelected) {
        this.canGotoSelected = canGotoSelected;
        firePropertyChange(PROPERTY_CAN_GOTO_SELECTED, canGotoSelected);
    }

    public boolean isCanGotoOpen() {
        return canGotoOpen;
    }

    public void setCanGotoOpen(boolean canGotoOpen) {
        this.canGotoOpen = canGotoOpen;
        firePropertyChange(PROPERTYCAN_GOTO_OPEN, canGotoOpen);
    }

    public boolean isCanClose() {
        return canClose;
    }

    public void setCanClose(boolean canClose) {
        this.canClose = canClose;
        firePropertyChange(PROPERTY_CAN_CLOSE, canClose);
    }

    public boolean isCanCreate() {
        return canCreate;
    }

    public void setCanCreate(boolean canCreate) {
        this.canCreate = canCreate;
        firePropertyChange(PROPERTY_CAN_CREATE, canCreate);
    }

    public boolean isCanMove() {
        return canMove;
    }

    public void setCanMove(boolean canMove) {
        this.canMove = canMove;
        firePropertyChange(PROPERTY_CAN_MOVE, canMove);
    }

    public String getLayoutName() {
        return layoutName;
    }

    public void setLayoutName(String layoutName) {
        this.layoutName = layoutName;
        firePropertyChange(PROPERTY_LAYOUT_NAME, layoutName);
    }
}
