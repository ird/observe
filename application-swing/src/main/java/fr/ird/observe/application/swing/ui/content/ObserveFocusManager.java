package fr.ird.observe.application.swing.ui.content;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import fr.ird.observe.application.swing.ui.content.impl.longline.LonglineDetailCompositionUI;
import fr.ird.observe.application.swing.ui.content.impl.longline.SetLonglineUI;
import fr.ird.observe.application.swing.ui.content.impl.seine.FloatingObjectTransmittingBuoyOperationUI;
import fr.ird.observe.application.swing.ui.content.impl.seine.FloatingObjectUI;
import fr.ird.observe.application.swing.ui.content.impl.seine.SetSeineUI;
import fr.ird.observe.application.swing.ui.content.list.ContentListUI;
import fr.ird.observe.application.swing.ui.content.list.impl.longline.ActivityLonglinesUI;
import fr.ird.observe.application.swing.ui.content.list.impl.longline.TripLonglinesUI;
import fr.ird.observe.application.swing.ui.content.list.impl.seine.ActivitySeinesUI;
import fr.ird.observe.application.swing.ui.content.list.impl.seine.RoutesUI;
import fr.ird.observe.application.swing.ui.content.list.impl.seine.TripSeinesUI;
import fr.ird.observe.application.swing.ui.content.open.ContentOpenableUI;
import fr.ird.observe.application.swing.ui.content.open.impl.longline.ActivityLonglineUI;
import fr.ird.observe.application.swing.ui.content.open.impl.longline.TripLonglineUI;
import fr.ird.observe.application.swing.ui.content.open.impl.seine.ActivitySeineUI;
import fr.ird.observe.application.swing.ui.content.open.impl.seine.RouteUI;
import fr.ird.observe.application.swing.ui.content.open.impl.seine.TripSeineUI;
import fr.ird.observe.application.swing.ui.content.ref.ContentReferenceUI;
import fr.ird.observe.application.swing.ui.content.ref.ReferenceHomeCommonUI;
import fr.ird.observe.application.swing.ui.content.ref.ReferenceHomeLonglineUI;
import fr.ird.observe.application.swing.ui.content.ref.ReferenceHomeSeineUI;
import fr.ird.observe.application.swing.ui.content.ref.impl.GearUI;
import fr.ird.observe.application.swing.ui.content.ref.impl.LengthLengthParameterUI;
import fr.ird.observe.application.swing.ui.content.ref.impl.LengthWeightParameterUI;
import fr.ird.observe.application.swing.ui.content.ref.impl.ProgramUI;
import fr.ird.observe.application.swing.ui.content.ref.impl.SpeciesListUI;
import fr.ird.observe.application.swing.ui.content.ref.impl.SpeciesUI;
import fr.ird.observe.application.swing.ui.content.ref.impl.VesselUI;
import fr.ird.observe.application.swing.ui.content.ref.impl.seine.WeightCategoryUI;
import fr.ird.observe.application.swing.ui.content.table.ContentTableUI;
import fr.ird.observe.application.swing.ui.content.table.impl.longline.CatchLonglineUI;
import fr.ird.observe.application.swing.ui.content.table.impl.longline.EncounterUI;
import fr.ird.observe.application.swing.ui.content.table.impl.longline.GearUseFeaturesLonglineUI;
import fr.ird.observe.application.swing.ui.content.table.impl.longline.SensorUsedUI;
import fr.ird.observe.application.swing.ui.content.table.impl.longline.TdrUI;
import fr.ird.observe.application.swing.ui.content.table.impl.seine.GearUseFeaturesSeineUI;
import fr.ird.observe.application.swing.ui.content.table.impl.seine.NonTargetCatchUI;
import fr.ird.observe.application.swing.ui.content.table.impl.seine.NonTargetSampleUI;
import fr.ird.observe.application.swing.ui.content.table.impl.seine.ObjectObservedSpeciesUI;
import fr.ird.observe.application.swing.ui.content.table.impl.seine.ObjectSchoolEstimateUI;
import fr.ird.observe.application.swing.ui.content.table.impl.seine.SchoolEstimateUI;
import fr.ird.observe.application.swing.ui.content.table.impl.seine.TargetCatchUI;
import fr.ird.observe.application.swing.ui.content.table.impl.seine.TargetDiscardCatchUI;
import fr.ird.observe.application.swing.ui.content.table.impl.seine.TargetSampleCaptureUI;
import fr.ird.observe.application.swing.ui.content.table.impl.seine.TargetSampleRejeteUI;
import fr.ird.observe.application.swing.ui.content.table.impl.seine.TargetSampleUI;
import org.nuiton.jaxx.widgets.number.NumberEditor;
import org.nuiton.jaxx.widgets.select.BeanComboBox;
import org.jdesktop.swingx.JXDatePicker;
import org.nuiton.jaxx.widgets.select.FilterableDoubleList;

import javax.swing.JComponent;
import javax.swing.JSpinner;
import javax.swing.LayoutFocusTraversalPolicy;
import java.awt.Component;
import java.awt.Container;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;

/**
 * Created on 09/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class ObserveFocusManager {

    private final ImmutableMap<Class<? extends ContentUI>, ObserveLayoutFocusTraversalPolicy> policies;

    public <U extends ContentUI> void installFocusTraversalPolicy(U ui) {
        Objects.requireNonNull(ui);
        Class<? extends ContentUI> uiClass = ui.getClass();
        ObserveLayoutFocusTraversalPolicy policy = policies.get(uiClass);
        if (policy == null && ui instanceof ContentReferenceUI) {
            uiClass = ContentReferenceUI.class;
            policy = policies.get(uiClass);
        }

        Objects.requireNonNull(policy, "Could not focus traversal policy for ui: " + ui.getClass().getName());
        ui.setFocusCycleRoot(true);
        policy.setUiSupplier(() -> ui);
        ui.setFocusTraversalPolicy(policy);
    }

    public ObserveFocusManager() {

        ImmutableMap.Builder<Class<? extends ContentUI>, ObserveLayoutFocusTraversalPolicy> builder = ImmutableMap.builder();

        builder.put(LonglineDetailCompositionUI.class, new ObserveLayoutFocusTraversalPolicy<LonglineDetailCompositionUI>() {

            @Override
            protected Component getFirstComponentForEdit(Container aContainer) {
                LonglineDetailCompositionUI ui = getUi();
                int selectedIndex = ui.getMainTabbedPane().getSelectedIndex();
                switch (selectedIndex) {
                    case 0:
                        return ui.getCanGenerate();
                    case 1:
                        return ui.getDeleteAll();
                    case 2:
                        int subSelectedIndex = ui.getBranchlineDetailUI().getSubTabbedPane().getSelectedIndex();
                        switch (subSelectedIndex) {
                            case 0:
                                return ui.getBranchlineDetailUI().getTopType();
                            case 1:
                                return ui.getBranchlineDetailUI().getHookType();
                        }
                        break;
                }
                return super.getFirstComponent(aContainer);
            }

            @Override
            protected Component getLastComponentForEdit(Container aContainer) {
                LonglineDetailCompositionUI ui = getUi();
                if (ui.getSave().isEnabled()) {
                    return ui.getSave();
                }
                if (ui.getReset().isEnabled()) {
                    return ui.getReset();
                }
                int selectedIndex = ui.getMainTabbedPane().getSelectedIndex();
                switch (selectedIndex) {
                    case 0:
                        return ui.getCanGenerate();
                    case 1:
                        return ui.getDeleteAll();
                    case 2:
                        return ui.getBranchlineDetailUI().getComment();
                }
                return super.getLastComponent(aContainer);
            }

        });

        builder.put(SetLonglineUI.class, new ObserveLayoutFocusTraversalPolicy<SetLonglineUI>() {

            @Override
            protected Component getFirstComponentForEdit(Container aContainer) {
                SetLonglineUI ui = getUi();
                int selectedIndex = ui.getMainTabbedPane().getSelectedIndex();
                switch (selectedIndex) {
                    case 0:
                        return ui.getHomeId();
                    case 1:
                        return ui.getSettingShape();
                    case 2:
                        return ui.getHaulingDirectionSameAsSetting();
                }
                return super.getFirstComponent(aContainer);
            }

            @Override
            protected Component getLastComponentForEdit(Container aContainer) {
                return getUi().getDelete();
            }

        });
        builder.put(FloatingObjectTransmittingBuoyOperationUI.class, new ObserveLayoutFocusTraversalPolicy<FloatingObjectTransmittingBuoyOperationUI>() {

            @Override
            protected Component getFirstComponentForEdit(Container aContainer) {
                return getUi().getTypeOperation();
            }

            @Override
            protected Component getLastComponentForEdit(Container aContainer) {
                FloatingObjectTransmittingBuoyOperationUI ui = getUi();
                if (ui.getSave().isEnabled()) {
                    return ui.getSave();
                }
                if (ui.getReset().isEnabled()) {
                    return ui.getReset();
                }
                return ui.getComment();
            }

        });

        builder.put(SetSeineUI.class, new ObserveLayoutFocusTraversalPolicy<SetSeineUI>() {

            @Override
            protected Component getFirstComponentForEdit(Container aContainer) {
                SetSeineUI ui = getUi();
                int selectedIndex = ui.getMainTabbedPane().getSelectedIndex();
                switch (selectedIndex) {
                    case 0:
                        JComponent editor = ui.getStartTime().getHourEditor().getEditor();
                        return ((JSpinner.DateEditor) editor).getTextField();
                    case 1:
                        return ui.getSchoolThickness();
                }
                return super.getFirstComponent(aContainer);
            }

            @Override
            protected Component getLastComponentForEdit(Container aContainer) {
                SetSeineUI ui = getUi();
                if (ui.getDelete().isVisible() && ui.getDelete().isEnabled()) {
                    return ui.getDelete();
                }
                if (ui.getSave().isEnabled()) {
                    return ui.getSave();
                }
                return ui.getReset();
            }

        });

        builder.put(FloatingObjectUI.class, new ObserveLayoutFocusTraversalPolicy<FloatingObjectUI>() {

            @Override
            protected Component getFirstComponentForEdit(Container aContainer) {
                return getUi().getObjectOperation();
            }

            @Override
            protected Component getLastComponentForEdit(Container aContainer) {
                FloatingObjectUI ui = getUi();
                if (ui.getDelete().isVisible() && ui.getDelete().isEnabled()) {
                    return ui.getDelete();
                }
                if (ui.getSave().isEnabled()) {
                    return ui.getSave();
                }
                return ui.getReset();
            }

        });

        builder.put(TripSeinesUI.class, new ContentListUIObserveLayoutFocusTraversalPolicy());
        builder.put(RoutesUI.class, new ContentListUIObserveLayoutFocusTraversalPolicy());
        builder.put(ActivitySeinesUI.class, new ContentListUIObserveLayoutFocusTraversalPolicy());
        builder.put(TripLonglinesUI.class, new ContentListUIObserveLayoutFocusTraversalPolicy());
        builder.put(ActivityLonglinesUI.class, new ContentListUIObserveLayoutFocusTraversalPolicy());

        builder.put(TripLonglineUI.class, new ObserveLayoutFocusTraversalPolicy<TripLonglineUI>() {

            @Override
            protected Component getFirstComponentForEdit(Container aContainer) {
                TripLonglineUI ui = getUi();
                if (ui.getModel().isReadingMode()) {
                    if (ui.getReopen().isVisible()) {
                        return ui.getReopen();
                    }
                    if (ui.getActionUp().isEnabled()) {
                        return ui.getActionUp();
                    }
                    return ui.getActionDown();
                }
                return ui.getTripType();
            }

            @Override
            protected Component getLastComponentForEdit(Container aContainer) {
                TripLonglineUI ui = getUi();
                if (ui.getCloseAndCreate().isVisible() && ui.getCloseAndCreate().isEnabled()) {
                    return ui.getCloseAndCreate();
                }
                if (ui.getDelete().isVisible() && ui.getDelete().isEnabled()) {
                    return ui.getDelete();
                }
                if (ui.getModel().isReadingMode()) {
                    if (ui.getReopen().isVisible()) {
                        return ui.getReopen();
                    }
                    if (ui.getActionDown().isEnabled()) {
                        return ui.getActionDown();
                    }
                    return ui.getActionUp();
                }
                return ui.getActionUp();
            }

        });
        builder.put(ActivityLonglineUI.class, new ObserveLayoutFocusTraversalPolicy<ActivityLonglineUI>() {

            @Override
            protected Component getFirstComponentForEdit(Container aContainer) {
                return getUi().getTimeStamp().getDayDateEditor();
            }

            @Override
            protected Component getLastComponentForEdit(Container aContainer) {
                ActivityLonglineUI ui = getUi();
                if (ui.getCloseAndCreate().isEnabled()) {
                    return ui.getCloseAndCreate();
                }
                return ui.getDelete();
            }

        });
        builder.put(ActivitySeineUI.class, new ObserveLayoutFocusTraversalPolicy<ActivitySeineUI>() {

            @Override
            protected Component getFirstComponentForEdit(Container aContainer) {
                ActivitySeineUI ui = getUi();
                int selectedIndex = ui.getMainTabbedPane().getSelectedIndex();
                switch (selectedIndex) {
                    case 0:
                        JComponent editor = ui.getTime().getHourEditor().getEditor();
                        return ((JSpinner.DateEditor) editor).getTextField();
                    case 1:
                        return ui.getVesselSpeed();
                    case 2:
                        return ui.getObservedSystem().getUniverseList();
                }
                return super.getFirstComponent(aContainer);
            }

            @Override
            protected Component getLastComponentForEdit(Container aContainer) {
                ActivitySeineUI ui = getUi();
                if (ui.getModel().isCreatingMode()) {
                    if (ui.getSave().isEnabled()) {
                        return ui.getSave();
                    }
                    return ui.getReset();
                } else
                    return ui.getCloseAndCreate();
            }

        });

        builder.put(TripSeineUI.class, new ObserveLayoutFocusTraversalPolicy<TripSeineUI>() {

            @Override
            protected Component getFirstComponentForEdit(Container aContainer) {
                return getUi().getCaptain();
            }

            @Override
            protected Component getLastComponentForEdit(Container aContainer) {
                TripSeineUI ui = getUi();
                if (!ui.getCloseAndCreate().isEnabled()) {
                    return ui.getDelete();
                }
                return ui.getCloseAndCreate();
            }

        });

        builder.put(RouteUI.class, new ObserveLayoutFocusTraversalPolicy<RouteUI>() {
            @Override
            protected Component getFirstComponentForEdit(Container aContainer) {
                return getUi().getDate();
            }

            @Override
            protected Component getLastComponentForEdit(Container aContainer) {
                RouteUI ui = getUi();
                if (ui.getCloseAndCreate().isEnabled()) {
                    return ui.getCloseAndCreate();
                }
                return ui.getDelete();
            }

        });
        builder.put(TdrUI.class, new ObserveLayoutFocusTraversalPolicy<TdrUI>() {

            @Override
            protected Component getFirstComponentForEdit(Container aContainer) {
                TdrUI ui = getUi();
                int selectedIndex = ui.getMainTabbedPane().getSelectedIndex();
                switch (selectedIndex) {
                    case 0:
                        return ui.getHomeId();
                    case 1:
                        return ui.getSection();
                    case 2:
                        return ui.getEnableTimestamp();
                    case 3:
                        return ui.getFishingStartDepth();
                    case 4:
                        return ui.getSpecies().getUniverseList();
                }
                return super.getFirstComponent(aContainer);
            }

            @Override
            protected Component getLastComponentForEdit(Container aContainer) {
                TdrUI ui = getUi();
                if (ui.getSave().isEnabled()) {
                    return ui.getSave();
                }
                if (ui.getReset().isEnabled()) {
                    return ui.getReset();
                }
                if (ui.getDeleteEntry().isEnabled()) {
                    return ui.getDeleteEntry();
                }
                if (ui.getSaveEntry().isEnabled()) {
                    return ui.getSaveEntry();
                }
                if (ui.getResetEntry().isEnabled()) {
                    return ui.getResetEntry();
                }

                return super.getLastComponent(aContainer);
            }


        });
        builder.put(SensorUsedUI.class, new ObserveLayoutFocusTraversalPolicy<SensorUsedUI>() {

            @Override
            protected Component getFirstComponentForEdit(Container aContainer) {
                return getUi().getSensorType();
            }

            @Override
            protected Component getLastComponentForEdit(Container aContainer) {
                SensorUsedUI ui = getUi();
                if (ui.getSave().isEnabled()) {
                    return ui.getSave();
                }
                if (ui.getReset().isEnabled()) {
                    return ui.getReset();
                }
                return ui.getComment();
            }

        });
        builder.put(GearUseFeaturesLonglineUI.class, new ObserveLayoutFocusTraversalPolicy<GearUseFeaturesLonglineUI>() {
            @Override
            protected Component getFirstComponentForEdit(Container aContainer) {
                GearUseFeaturesLonglineUI ui = getUi();
                int selectedIndex = ui.getMainTabbedPane().getSelectedIndex();
                switch (selectedIndex) {
                    case 0:
                        return ui.getGear();
                    case 1:
                        return ui.getTable();
                }
                return null;
            }

            @Override
            protected Component getLastComponentForEdit(Container aContainer) {
                return getTableDefaultLastComponent(getUi(), null);
            }

        });
        builder.put(EncounterUI.class, new ObserveLayoutFocusTraversalPolicy<EncounterUI>() {

            @Override
            protected Component getFirstComponentForEdit(Container aContainer) {
                return getUi().getEncounterType();
            }

            @Override
            protected Component getLastComponentForEdit(Container aContainer) {
                EncounterUI ui = getUi();
                if (ui.getSave().isEnabled()) {
                    return ui.getSave();
                }
                if (ui.getReset().isEnabled()) {
                    return ui.getReset();
                }
                return ui.getComment();
            }

        });
        builder.put(CatchLonglineUI.class, new ObserveLayoutFocusTraversalPolicy<CatchLonglineUI>() {

            @Override
            protected Component getFirstComponentForEdit(Container aContainer) {
                CatchLonglineUI ui = getUi();
                int selectedIndex = ui.getMainTabbedPane().getSelectedIndex();
                switch (selectedIndex) {
                    case 0:
                        if (ui.getTableModel().isCreate()) {
                            return ui.getAcquisitionModeIndividual();
                        } else {
                            return ui.getSection();
                        }
                    case 1:
                        return ui.getDepredated();
                    case 2:
                        return ui.getStomacFullness();
                    case 3:
                        return ui.getSizeMeasuresTable();
                    case 4:
                        return ui.getWeightMeasuresTable();
                    case 5:
                        return ui.getDepthRecorder();
                }
                return null;
            }

            @Override
            protected Component getLastComponentForEdit(Container aContainer) {
                return getTableDefaultLastComponent(getUi(), null);
            }

        });
        builder.put(TargetSampleCaptureUI.class, new ObserveLayoutFocusTraversalPolicy<TargetSampleCaptureUI>() {

            @Override
            protected Component getFirstComponentForEdit(Container aContainer) {
                TargetSampleCaptureUI ui = getUi();
                if (ui.getAcquisitionModeEffectif().isEnabled()) {
                    return ui.getAcquisitionModeEffectif();
                }
                return ui.getSizeMeasureType();
            }

            @Override
            protected Component getLastComponentForEdit(Container aContainer) {
                TargetSampleUI ui = getUi();
                return getTableDefaultLastComponent(ui, ui.getComment());
            }

        });
        builder.put(TargetSampleRejeteUI.class, new ObserveLayoutFocusTraversalPolicy<TargetSampleRejeteUI>() {

            @Override
            protected Component getFirstComponentForEdit(Container aContainer) {
                TargetSampleRejeteUI ui = getUi();
                if (ui.getAcquisitionModeEffectif().isEnabled()) {
                    return ui.getAcquisitionModeEffectif();
                }
                return ui.getSizeMeasureType();
            }

            @Override
            protected Component getLastComponentForEdit(Container aContainer) {
                TargetSampleUI ui = getUi();
                return getTableDefaultLastComponent(ui, ui.getComment());
            }

        });
        builder.put(TargetDiscardCatchUI.class, new ObserveLayoutFocusTraversalPolicy<TargetDiscardCatchUI>() {

            @Override
            protected Component getFirstComponentForEdit(Container aContainer) {
                TargetDiscardCatchUI ui = getUi();
                if (ui.getSpecies().isEnabled()) {
                    return ui.getSpecies();
                }
                return ui.getCatchWeight();
            }

            @Override
            protected Component getLastComponentForEdit(Container aContainer) {
                TargetDiscardCatchUI ui = getUi();
                return getTableDefaultLastComponent(ui, ui.getTargetDiscardCatchCompositionEstimatedByObserver());
            }

        });
        builder.put(TargetCatchUI.class, new ObserveLayoutFocusTraversalPolicy<TargetCatchUI>() {

            @Override
            protected Component getFirstComponentForEdit(Container aContainer) {
                TargetCatchUI ui = getUi();
                if (ui.getSpecies().isEnabled()) {
                    return ui.getSpecies();
                }
                return ui.getCatchWeight();
            }

            @Override
            protected Component getLastComponentForEdit(Container aContainer) {
                TargetCatchUI ui = getUi();
                return getTableDefaultLastComponent(ui, ui.getTargetCatchCompositionEstimatedByObserver());
            }

        });
        builder.put(SchoolEstimateUI.class, new ObserveLayoutFocusTraversalPolicy<SchoolEstimateUI>() {

            @Override
            protected Component getFirstComponentForEdit(Container aContainer) {
                SchoolEstimateUI ui = getUi();
                if (ui.getSpecies().isEnabled()) {
                    return ui.getSpecies();
                }
                return ui.getTotalWeight();
            }

            @Override
            protected Component getLastComponentForEdit(Container aContainer) {
                SchoolEstimateUI ui = getUi();
                return getTableDefaultLastComponent(ui, ui.getComment());
            }

        });
        builder.put(ObjectSchoolEstimateUI.class, new ObserveLayoutFocusTraversalPolicy<ObjectSchoolEstimateUI>() {

            @Override
            protected Component getFirstComponentForEdit(Container aContainer) {
                return getUi().getSpecies();
            }

            @Override
            protected Component getLastComponentForEdit(Container aContainer) {
                ObjectSchoolEstimateUI ui = getUi();
                return getTableDefaultLastComponent(ui, ui.getComment());
            }

        });
        builder.put(ObjectObservedSpeciesUI.class, new ObserveLayoutFocusTraversalPolicy<ObjectObservedSpeciesUI>() {

            @Override
            protected Component getFirstComponentForEdit(Container aContainer) {
                return getUi().getSpecies();
            }

            @Override
            protected Component getLastComponentForEdit(Container aContainer) {
                ObjectObservedSpeciesUI ui = getUi();
                return getTableDefaultLastComponent(ui, ui.getComment());
            }

        });
        builder.put(NonTargetSampleUI.class, new ObserveLayoutFocusTraversalPolicy<NonTargetSampleUI>() {

            @Override
            protected Component getFirstComponentForEdit(Container aContainer) {
                NonTargetSampleUI ui = getUi();
                if (ui.getAcquisitionModeEffectif().isEnabled()) {
                    return ui.getAcquisitionModeEffectif();
                }
                return ui.getSizeMeasureType();
            }

            @Override
            protected Component getLastComponentForEdit(Container aContainer) {
                NonTargetSampleUI ui = getUi();
                return getTableDefaultLastComponent(ui, ui.getComment());
            }

        });
        builder.put(NonTargetCatchUI.class, new ObserveLayoutFocusTraversalPolicy<NonTargetCatchUI>() {

            @Override
            protected Component getFirstComponentForEdit(Container aContainer) {
                return getUi().getSpecies();
            }

            @Override
            protected Component getLastComponentForEdit(Container aContainer) {
                return getTableDefaultLastComponent(getUi(), null);
            }

        });
        builder.put(GearUseFeaturesSeineUI.class, new ObserveLayoutFocusTraversalPolicy<GearUseFeaturesSeineUI>() {

            @Override
            protected Component getFirstComponentForEdit(Container aContainer) {
                GearUseFeaturesSeineUI ui = getUi();
                int selectedIndex = ui.getMainTabbedPane().getSelectedIndex();
                switch (selectedIndex) {
                    case 0:
                        return ui.getGear();
                    case 1:
                        return ui.getTable();
                }
                return super.getFirstComponent(aContainer);
            }

            @Override
            protected Component getLastComponentForEdit(Container aContainer) {
                return getTableDefaultLastComponent(getUi(), null);
            }

        });
        builder.put(ContentReferenceUI.class, new ObserveLayoutFocusTraversalPolicy<ContentReferenceUI>() {

            @Override
            protected Component getFirstComponentForEdit(Container aContainer) {
                return getUi().getUri();
            }

            @Override
            protected Component getLastComponentForEdit(Container aContainer) {
                return getUi().getBackToList();
            }

        });

        builder.put(ReferenceHomeCommonUI.class, new ObserveLayoutFocusTraversalPolicy<ReferenceHomeCommonUI>() {
            @Override
            protected Component getFirstComponentForEdit(Container aContainer) {
                return null;
            }

            @Override
            protected Component getLastComponentForEdit(Container aContainer) {
                return null;
            }

        });
        builder.put(ReferenceHomeLonglineUI.class, new ObserveLayoutFocusTraversalPolicy<ReferenceHomeLonglineUI>() {
            @Override
            protected Component getFirstComponentForEdit(Container aContainer) {
                return null;
            }

            @Override
            protected Component getLastComponentForEdit(Container aContainer) {
                return null;
            }

        });
        builder.put(ReferenceHomeSeineUI.class, new ObserveLayoutFocusTraversalPolicy<ReferenceHomeSeineUI>() {
            @Override
            protected Component getFirstComponentForEdit(Container aContainer) {
                return null;
            }

            @Override
            protected Component getLastComponentForEdit(Container aContainer) {
                return null;
            }

        });

        builder.put(VesselUI.class, new ObserveLayoutFocusTraversalPolicy<VesselUI>() {

            @Override
            protected Component getFirstComponentForEdit(Container aContainer) {
                VesselUI ui = getUi();
                int selectedIndex = ui.getMainTabbedPane().getSelectedIndex();
                switch (selectedIndex) {
                    case 0:
                        return ui.getKeelCode();
                    case 1:
                        return ui.getLength();
                }
                return ui.getUri();
            }

            @Override
            protected Component getLastComponentForEdit(Container aContainer) {
                return getUi().getBackToList();
            }

        });

        builder.put(SpeciesUI.class, new ObserveLayoutFocusTraversalPolicy<SpeciesUI>() {

            @Override
            protected Component getFirstComponentForEdit(Container aContainer) {
                SpeciesUI ui = getUi();
                int selectedIndex = ui.getMainTabbedPane().getSelectedIndex();
                switch (selectedIndex) {
                    case 0:
                        return ui.getUri();
                    case 1:
                        return ui.getSizeMeasureType();
                }
                return ui.getUri();
            }

            @Override
            protected Component getLastComponentForEdit(Container aContainer) {
                return getUi().getBackToList();
            }

        });

        builder.put(SpeciesListUI.class, new ObserveLayoutFocusTraversalPolicy<SpeciesListUI>() {

            @Override
            protected Component getFirstComponentForEdit(Container aContainer) {
                SpeciesListUI ui = getUi();
                int selectedIndex = ui.getMainTabbedPane().getSelectedIndex();
                switch (selectedIndex) {
                    case 0:
                        return ui.getUri();
                    case 1:
                        return ui.getSpecies().getUniverseList();
                }
                return ui.getUri();
            }

            @Override
            protected Component getLastComponentForEdit(Container aContainer) {
                return getUi().getBackToList();
            }

        });

        builder.put(ProgramUI.class, new ObserveLayoutFocusTraversalPolicy<ProgramUI>() {

            @Override
            protected Component getFirstComponentForEdit(Container aContainer) {
                ProgramUI ui = getUi();
                int selectedIndex = ui.getMainTabbedPane().getSelectedIndex();
                switch (selectedIndex) {
                    case 0:
                        return ui.getUri();
                    case 1:
                        return ui.getNonTargetObservation();
                }
                return ui.getUri();
            }

            @Override
            protected Component getLastComponentForEdit(Container aContainer) {
                return getUi().getBackToList();
            }

        });

        builder.put(LengthWeightParameterUI.class, new ObserveLayoutFocusTraversalPolicy<LengthWeightParameterUI>() {

            @Override
            protected Component getFirstComponentForEdit(Container aContainer) {
                LengthWeightParameterUI ui = getUi();
                int selectedIndex = ui.getMainTabbedPane().getSelectedIndex();
                switch (selectedIndex) {
                    case 0:
                        if (ui.getModel().isCreatingMode()) {
                            return ui.getSex();
                        } else {

                            return ui.getStartDate();
                        }
                    case 1:
                        return ui.getCoefficients();
                }
                return ui.getUri();
            }

            @Override
            protected Component getLastComponentForEdit(Container aContainer) {
                return getUi().getBackToList();
            }

        });

        builder.put(LengthLengthParameterUI.class, new ObserveLayoutFocusTraversalPolicy<LengthLengthParameterUI>() {

            @Override
            protected Component getFirstComponentForEdit(Container aContainer) {
                LengthLengthParameterUI ui = getUi();
                int selectedIndex = ui.getMainTabbedPane().getSelectedIndex();
                switch (selectedIndex) {
                    case 0:
                        if (ui.getModel().isCreatingMode()) {
                            return ui.getInputSizeMeasureType();
                        } else {

                            return ui.getUri();
                        }
                    case 1:
                        return ui.getCoefficients();
                }
                return ui.getUri();
            }

            @Override
            protected Component getLastComponentForEdit(Container aContainer) {
                return getUi().getBackToList();
            }

        });

        builder.put(GearUI.class, new ObserveLayoutFocusTraversalPolicy<GearUI>() {

            @Override
            protected Component getFirstComponentForEdit(Container aContainer) {
                GearUI ui = getUi();
                int selectedIndex = ui.getMainTabbedPane().getSelectedIndex();
                switch (selectedIndex) {
                    case 0:
                        return ui.getUri();
                    case 1:
                        return ui.getGearCaracteristic().getUniverseList();
                }
                return ui.getUri();
            }

            @Override
            protected Component getLastComponentForEdit(Container aContainer) {
                return getUi().getBackToList();
            }
        });


        builder.put(WeightCategoryUI.class, new ObserveLayoutFocusTraversalPolicy<WeightCategoryUI>() {

            @Override
            protected Component getFirstComponentForEdit(Container aContainer) {
                return getUi().getSpecies();
            }

            @Override
            protected Component getLastComponentForEdit(Container aContainer) {
                return getUi().getBackToList();
            }

            @Override
            protected Component getFirstComponentForRead(Container aContainer) {
                return getUi().getBackToList();
            }

            @Override
            protected Component getLastComponentForRead(Container aContainer) {
                return getUi().getBackToList();
            }

        });

        policies = builder.build();

    }

    public static abstract class ObserveLayoutFocusTraversalPolicy<U extends ContentUI> extends LayoutFocusTraversalPolicy {

        private Supplier<U> uiSupplier;

        public void setUiSupplier(Supplier<U> uiSupplier) {
            this.uiSupplier = uiSupplier;
        }

        public U getUi() {
            return uiSupplier.get();
        }

        protected abstract Component getFirstComponentForEdit(Container aContainer);

        protected abstract Component getLastComponentForEdit(Container aContainer);

        protected Component getFirstComponentForRead(Container aContainer) {
            if (getUi() instanceof ContentOpenableUI) {
                ContentOpenableUI ui = (ContentOpenableUI) getUi();
                if (ui.getReopen().isEnabled() && ui.getReopen().isVisible()) {
                    return ui.getReopen();
                }
            } else if (getUi() instanceof ContentReferenceUI) {
                ContentReferenceUI ui = (ContentReferenceUI) getUi();
                return ui.getList();
            }
            if (getUi().getActionUp().isEnabled()) {
                return getUi().getActionUp();
            }
            return getUi().getActionDown();
        }

        protected Component getLastComponentForRead(Container aContainer) {
            if (getUi() instanceof ContentOpenableUI) {
                ContentOpenableUI ui = (ContentOpenableUI) getUi();
                if (ui.getReopen().isEnabled() && ui.getReopen().isVisible()) {
                    return ui.getReopen();
                }
            } else if (getUi() instanceof ContentReferenceUI) {
                ContentReferenceUI ui = (ContentReferenceUI) getUi();
                return ui.getBackToList();

            }
            if (getUi().getActionDown().isEnabled()) {
                return getUi().getActionDown();
            }
            return getUi().getActionUp();
        }

        @Override
        public final Component getComponentAfter(Container aContainer, Component aComponent) {
            Component lastComponent = getLastComponent(aContainer);
            lastComponent = getRealFocusComponent(lastComponent);
            if (aComponent.equals(lastComponent)) {
                return getFirstComponent(aContainer);
            }
            return super.getComponentAfter(aContainer, aComponent);
        }

        @Override
        public final Component getComponentBefore(Container aContainer, Component aComponent) {

            Component firstComponent = getFirstComponent(aContainer);
            firstComponent = getRealFocusComponent(firstComponent);
            if (aComponent.equals(firstComponent)) {
                return getLastComponent(aContainer);
            }
            return super.getComponentBefore(aContainer, aComponent);
        }

        protected Component getRealFocusComponent(Component firstComponent) {
            if (firstComponent instanceof BeanComboBox) {
                BeanComboBox component = (BeanComboBox) firstComponent;
                firstComponent = component.getCombobox().getEditor().getEditorComponent();
            } else if (firstComponent instanceof JSpinner) {
                JSpinner component = (JSpinner) firstComponent;
                firstComponent = component.getEditor();
            } else if (firstComponent instanceof NumberEditor) {
                NumberEditor component = (NumberEditor) firstComponent;
                firstComponent = component.getTextField();
            } else if (firstComponent instanceof JXDatePicker) {
                JXDatePicker component = (JXDatePicker) firstComponent;
                firstComponent = component.getEditor();
            } else if (firstComponent instanceof FilterableDoubleList) {
                FilterableDoubleList component = (FilterableDoubleList) firstComponent;
                firstComponent = component.getUniverseList();
            }
            return firstComponent;
        }

        @Override
        public final Component getFirstComponent(Container aContainer) {
            if (getUi().getModel().isReadingMode()) {
                return getFirstComponentForRead(aContainer);
            }
            return getFirstComponentForEdit(aContainer);
        }

        @Override
        public final Component getLastComponent(Container aContainer) {
            if (getUi().getModel().isReadingMode()) {
                return getLastComponentForRead(aContainer);
            }
            return getLastComponentForEdit(aContainer);
        }

        protected <UU extends ContentTableUI> Component getTableDefaultLastComponent(UU ui, Component optionalComponent) {
            if (ui.getSave().isEnabled()) {
                return ui.getSave();
            }
            if (ui.getReset().isEnabled()) {
                return ui.getReset();
            }
            if (optionalComponent != null) {
                return optionalComponent;
            }
            if (ui.getDeleteEntry().isVisible() && ui.getDeleteEntry().isEnabled()) {
                return ui.getDeleteEntry();
            }
            if (ui.getSaveEntry().isVisible() && ui.getSaveEntry().isEnabled()) {
                return ui.getSaveEntry();
            }
            if (ui.getResetEntry().isVisible() && ui.getResetEntry().isEnabled()) {
                return ui.getResetEntry();
            }
            return ui.getResetEntry();
        }

    }


    private static class ContentListUIObserveLayoutFocusTraversalPolicy extends ObserveLayoutFocusTraversalPolicy<ContentListUI> {

        private List<JComponent> actions;

        @Override
        protected Component getFirstComponentForEdit(Container aContainer) {
            return getUi().getList();
        }

        @Override
        protected Component getLastComponentForEdit(Container aContainer) {
            Optional<JComponent> optional = getActions().stream().filter(a -> a.isEnabled() && a.isVisible()).findFirst();
            return optional.isPresent() ? optional.get() : null;
        }

        @Override
        protected Component getFirstComponentForRead(Container aContainer) {
            return getUi().getList();
        }

        @Override
        protected Component getLastComponentForRead(Container aContainer) {
            Optional<JComponent> optional = getActions().stream().filter(a -> a.isEnabled() && a.isVisible()).findFirst();
            return optional.isPresent() ? optional.get() : null;
        }

        public List<JComponent> getActions() {
            if (actions == null) {

                actions = new ArrayList<>();
                actions.add(getUi().getGotoSelected());
                actions.add(getUi().getReopen());
                actions.add(getUi().getClose());
                actions.add(getUi().getGotoOpen());
                actions.add(getUi().getCreate());
                actions.add(getUi().getMove());

            }
            List<JComponent> result = new ArrayList<>(actions);
            Collections.reverse(result);
            return result;
        }

    }
}
