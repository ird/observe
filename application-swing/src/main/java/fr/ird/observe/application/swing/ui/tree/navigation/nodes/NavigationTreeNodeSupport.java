package fr.ird.observe.application.swing.ui.tree.navigation.nodes;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.ui.ObserveKeyStrokes;
import fr.ird.observe.application.swing.ui.actions.content.SelectNodeUIAction;
import fr.ird.observe.application.swing.ui.content.ContentUI;
import fr.ird.observe.application.swing.ui.tree.ObserveTreeNodeSupport;
import fr.ird.observe.services.ObserveServicesProvider;
import org.nuiton.jaxx.runtime.JAXXObject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.AbstractAction;
import javax.swing.JMenuItem;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.util.Enumeration;

/**
 * Created on 14/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public abstract class NavigationTreeNodeSupport<O> extends ObserveTreeNodeSupport<O> {

    /** Logger */
    private static final Log log = LogFactory.getLog(NavigationTreeNodeSupport.class);

    private boolean loaded;

    public abstract Class<?> getDataType();

    public abstract void reload();

    public abstract Class<? extends ContentUI<?, ?>> getContentClass();

    public void updateNode() {
        loaded = false;
        reload();
        if (withChildsToLoad()) {
            populateChilds();
        }
        loaded = true;
    }

    protected boolean withChildsToLoad() {
        return WithChildsToReload.class.isAssignableFrom(getClass());
    }

    public boolean isPersisted() {
        return getId() != null;
    }

    @Override
    public RootNavigationTreeNode getRoot() {
        return (RootNavigationTreeNode) super.getRoot();
    }

    @Override
    public NavigationTreeNodeSupport<?> getParent() {
        return (NavigationTreeNodeSupport<?>) super.getParent();
    }

    protected NavigationTreeNodeSupport(O data, boolean allowChildren) {
        super(data, allowChildren);
    }

    //FIXME Should reload then node ?
    public void setData(O data) {
        setUserObject(data);
    }

    public final boolean populateChilds() {
        if (isPersisted() && getAllowsChildren() && !isLoaded()) {
            if (isPersisted() && withChildsToLoad()) {
                removeAllChildren();
                ((WithChildsToReload) this).reloadChilds();
            }
            loaded = true;
            return true;
        }
        return false;
    }

    public boolean isLoaded() {
        return loaded;
    }

    public void setLoaded(boolean loaded) {
        this.loaded = loaded;
    }

    protected ObserveServicesProvider getMainDataSourceServicesProvider() {
        return ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider();
    }

    public NavigationTreeNodeSupport findChildById(String id) {
        if (id == null) {

            // id null ? donc rien a faire
            return null;
        }
        if (id.equals(getId())) {

            // on a trouve le bon noeud
            return this;
        }

        if (!isLoaded()) {

            // il faut charger les fils du noeud pour effectuer la recherche
            populateChilds();
        }

        if (isLeaf()) {

            // au final le noeud est une feuille, donc ne convient pas
            return null;
        }
        Enumeration<NavigationTreeNodeSupport> children = children();
        while (children.hasMoreElements()) {
            NavigationTreeNodeSupport node = children.nextElement();
            if (id.equals(node.getId()) || (node.getUserObject() instanceof String && id.equals(node.getUserObject()))) {
                return node;
            }
        }

        return null;
    }

    public NavigationTreeNodeSupport findChildByType(Class<?> childType) {

        if (childType.isAssignableFrom(getClass())) {

            // on a trouve le bon noeud
            return this;
        }

        if (!isLoaded()) {

            // il faut charger les fils du noeud pour effectuer la recherche
            populateChilds();
        }

        if (isLeaf()) {

            // au final le noeud est une feuille, donc ne convient pas
            return null;
        }
        Enumeration<NavigationTreeNodeSupport> children = children();
        while (children.hasMoreElements()) {
            NavigationTreeNodeSupport node = children.nextElement();
            if (childType.isAssignableFrom(node.getClass())) {
                return node;
            }
        }

        return null;
    }

    public NavigationTreeNodeSupport findChildByClass(Class<?> childType) {

        if (!isLoaded()) {

            // il faut charger les fils du noeud pour effectuer la recherche
            populateChilds();
        }

        if (isLeaf()) {

            // au final le noeud est une feuille, donc ne convient pas
            return null;
        }
        Enumeration<NavigationTreeNodeSupport> children = children();
        while (children.hasMoreElements()) {
            NavigationTreeNodeSupport node = children.nextElement();
            if (childType.isAssignableFrom((Class) node.getUserObject())) {
                return node;
            }
        }

        return null;
    }

    @Override
    public boolean isLeaf() {
        return isLoaded() && super.isLeaf();
    }

    public boolean isOpen() {
        return false;
    }

    @Override
    public Color getColor() {
        return isOpen() ? Color.BLACK : Color.GRAY;
    }

    public JMenuItem toMenuItem(SelectNodeUIAction action, JAXXObject ui) {

        JMenuItem mi = new JMenuItem();
        AbstractAction a = new AbstractAction() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (log.isInfoEnabled()) {
                    log.info("Select action from menuItem: " + e.getSource());
                }
                action.actionPerformed(mi);
            }
        };
        mi.setAction(a);
        mi.setText(getText().trim());
        ObserveKeyStrokes.addKeyStroke(mi, ObserveKeyStrokes.KEY_STROKE_PRESSED_ENTER);
        mi.setIcon(getIcon("-16"));
        mi.setForeground(getColor());
        mi.setBackground(Color.WHITE);
        mi.putClientProperty("node", this);
        mi.putClientProperty("ui", ui);
        return mi;
    }

}
