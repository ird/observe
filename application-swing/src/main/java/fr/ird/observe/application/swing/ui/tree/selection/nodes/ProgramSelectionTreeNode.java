package fr.ird.observe.application.swing.ui.tree.selection.nodes;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.services.dto.longline.TripLonglineDto;
import fr.ird.observe.services.dto.reference.DataReference;
import fr.ird.observe.services.dto.reference.ReferentialReference;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import org.apache.commons.collections4.EnumerationUtils;

import java.util.Collection;
import java.util.Iterator;

/**
 * Created on 18/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class ProgramSelectionTreeNode extends ReferenceSelectionTreeNodeSupport<ProgramDto, ReferentialReference<ProgramDto>> implements Iterable<TripSelectionTreeNodeSupport<?>> {

    public static ProgramSelectionTreeNode ofSeine(ReferentialReference<ProgramDto> data, Collection<DataReference<TripSeineDto>> tripSeines) {
        ProgramSelectionTreeNode result = new ProgramSelectionTreeNode(data);
        for (DataReference<TripSeineDto> tripSeine : tripSeines) {
            result.add(new TripSeineSelectionTreeNode(tripSeine));
        }
        return result;
    }

    public static ProgramSelectionTreeNode ofLongline(ReferentialReference<ProgramDto> data, Collection<DataReference<TripLonglineDto>> tripLonglines) {
        ProgramSelectionTreeNode result = new ProgramSelectionTreeNode(data);
        for (DataReference<TripLonglineDto> tripLongline : tripLonglines) {
            result.add(new TripLonglineSelectionTreeNode(tripLongline));
        }
        return result;
    }

    public ProgramSelectionTreeNode(ReferentialReference<ProgramDto> data) {
        super(data, true);
    }

    @Override
    public boolean isOpen() {
        return ObserveSwingApplicationContext.get().getOpenDataManager().isOpenProgram(toString());
    }

    @Override
    public Iterator<TripSelectionTreeNodeSupport<?>> iterator() {
        return (Iterator) EnumerationUtils.toList(super.children()).iterator();
    }

    @Override
    public String getText() {
        return super.getText() + " (" + getChildCount() + ")";
    }

    @Override
    public boolean isSelected() {
        for (ReferenceSelectionTreeNodeSupport<?, ?> nodeSupport : this) {
            if (!nodeSupport.isSelected()) {
                return false;
            }
        }
        return !isLeaf();
    }

    @Override
    public void setSelected(boolean selected) {
        for (ReferenceSelectionTreeNodeSupport<?, ?> node : this) {
            node.setSelected(selected);
        }
    }

    public ImmutableList<DataReference<?>> getSelected() {
        ImmutableList.Builder<DataReference<?>> builder = ImmutableList.builder();
        for (TripSelectionTreeNodeSupport<?> nodeSupport : this) {
            if (nodeSupport.isSelected()) {
                builder.add(nodeSupport.getData());
            }
        }
        return builder.build();
    }

}
