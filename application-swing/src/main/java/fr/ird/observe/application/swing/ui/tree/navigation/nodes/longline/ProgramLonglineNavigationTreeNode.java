package fr.ird.observe.application.swing.ui.tree.navigation.nodes.longline;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ui.content.list.impl.longline.TripLonglinesUI;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.ReferenceNavigationTreeNodeSupport;
import fr.ird.observe.services.dto.longline.TripLonglineDto;
import fr.ird.observe.services.dto.reference.DataReference;
import fr.ird.observe.services.dto.reference.ReferentialReference;
import fr.ird.observe.services.dto.referential.ProgramDto;
import org.apache.commons.collections4.EnumerationUtils;

import java.util.Collection;
import java.util.Iterator;

/**
 * Created on 14/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class ProgramLonglineNavigationTreeNode extends ReferenceNavigationTreeNodeSupport<ProgramDto, ReferentialReference<ProgramDto>> implements Iterable<TripLonglineNavigationTreeNode> {

    public ProgramLonglineNavigationTreeNode(ReferentialReference<ProgramDto> data, Collection<DataReference<TripLonglineDto>> trips) {
        super(data, true);
        for (DataReference<TripLonglineDto> trip : trips) {
            add(new TripLonglineNavigationTreeNode(trip));
        }
        setLoaded(true);
    }

    @Override
    public boolean isOpen() {
        // le programme est ouvert si l'une de ses marées est ouverte
        for (TripLonglineNavigationTreeNode node : this) {
            if (node.isOpen()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void reload() {
        ReferentialReference<ProgramDto> data = getMainDataSourceServicesProvider().newReferentialService().loadReference(ProgramDto.class, getId());
        setData(data);
    }

    @Override
    public String getText() {
        return super.getText() + " (" + getChildCount() + ")";
    }

    @Override
    public Class<TripLonglinesUI> getContentClass() {
        return TripLonglinesUI.class;
    }

    @Override
    public Iterator<TripLonglineNavigationTreeNode> iterator() {
        return EnumerationUtils.toList(children()).iterator();
    }
}
