<!--
  #%L
  ObServe :: Application Swing
  %%
  Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

<fr.ird.observe.application.swing.ui.content.table.ContentTableUI
        superGenericType='FloatingObjectObservedSpeciesDto, ObjectObservedSpeciesDto, ObjectObservedSpeciesUI'
        contentTitle='{n("observe.common.objectObservedSpecies")}'
        saveNewEntryText='{n("observe.content.action.create.objectObservedSpecies")}'
        saveNewEntryTip='{n("observe.content.action.create.objectObservedSpecies.tip")}'>

  <style source="../../CommonTable.jcss"/>

  <import>
    fr.ird.observe.services.dto.seine.FloatingObjectObservedSpeciesDto
    fr.ird.observe.services.dto.seine.ObjectObservedSpeciesDto
    fr.ird.observe.services.dto.reference.ReferentialReference
    fr.ird.observe.services.dto.referential.SpeciesDto
    fr.ird.observe.services.dto.referential.seine.SpeciesStatusDto
    fr.ird.observe.application.swing.ui.content.table.*
    fr.ird.observe.application.swing.ui.util.JComment

    org.nuiton.jaxx.widgets.number.NumberEditor
    org.nuiton.jaxx.widgets.select.BeanComboBox

    java.awt.Dimension

    static fr.ird.observe.application.swing.ui.UIHelper.getStringValue
    static org.nuiton.i18n.I18n.n
  </import>

  <!-- model -->
  <ObjectObservedSpeciesUIModel id='model' constructorParams='this'/>

  <!-- edit bean -->
  <FloatingObjectObservedSpeciesDto id='bean'/>

  <!-- table edit bean -->
  <ObjectObservedSpeciesDto id='tableEditBean'/>

  <!-- table model -->
  <ContentTableModel id='tableModel'/>

  <!-- le validateur de l'écran -->
  <BeanValidator id='validator' autoField='true'
                 beanClass='fr.ird.observe.services.dto.seine.FloatingObjectObservedSpeciesDto'
                 errorTableModel='{getErrorTableModel()}' context='ui-update'/>

  <!-- le validateur d'une entrée de tableau -->
  <BeanValidator id='validatorTable' autoField='true' errorTableModel='{getErrorTableModel()}' context='ui-update'
                 beanClass='fr.ird.observe.services.dto.seine.ObjectObservedSpeciesDto'/>

  <Table id='editorPanel' fill='both' insets='1'>

    <!-- species faune -->
    <row>
      <cell>
        <JLabel id='speciesLabel'/>
      </cell>
      <cell weightx='1' anchor='east'>
        <BeanComboBox id='species' constructorParams='this' genericType='ReferentialReference&lt;SpeciesDto&gt;'
                      _entityClass='SpeciesDto.class'/>
      </cell>
    </row>

    <!-- statut species -->
    <row>
      <cell>
        <JLabel id='speciesStatusLabel'/>
      </cell>
      <cell weightx='1' anchor='east'>
        <BeanComboBox id='speciesStatus' constructorParams='this'
                      genericType='ReferentialReference&lt;SpeciesStatusDto&gt;' _entityClass='SpeciesStatusDto.class'/>
      </cell>
    </row>

    <!-- count -->
    <row>
      <cell>
        <JLabel id='countLabel'/>
      </cell>
      <cell weightx='1' anchor='east'>
        <NumberEditor id='count' constructorParams='this' styleClass="int6"/>
      </cell>
    </row>
  </Table>

  <Table id='extraZone' fill='both' weightx='1' insets='0'>
    <row>
      <cell weighty='1'>
        <JComment id="comment"/>
      </cell>
    </row>
  </Table>

</fr.ird.observe.application.swing.ui.content.table.ContentTableUI>
