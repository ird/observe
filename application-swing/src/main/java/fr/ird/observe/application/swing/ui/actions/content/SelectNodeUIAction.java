/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.actions.content;

import fr.ird.observe.application.swing.ui.ObserveKeyStrokes;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.content.ContentUI;
import fr.ird.observe.application.swing.ui.content.list.ContentListUI;
import fr.ird.observe.application.swing.ui.tree.navigation.NavigationTree;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.NavigationTreeNodeSupport;
import fr.ird.observe.services.dto.reference.DataReference;

import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import java.util.Objects;

import static fr.ird.observe.application.swing.ui.content.ContentUIHandler.getTreeHelper;
import static org.nuiton.i18n.I18n.n;

/**
 * Action pour sélectionner un noeud (attaché à l'éditeur) dans l'arbre de
 * navigation.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public class SelectNodeUIAction extends AbstractContentUIAction {

    private static final long serialVersionUID = 1L;

    public static final String ACTION_NAME = "selectNode";

    public static final String NODE = "node";

    public SelectNodeUIAction(ObserveMainUI mainUI) {
        super(mainUI,
              ACTION_NAME,
              n("observe.action.selectNode"),
              n("observe.action.selectNode.tip"),
              "go-jump",
              ObserveKeyStrokes.KEY_STROKE_ALT_ENTER);
    }

    @Override
    public void actionPerformed(ContentUI<?, ?> contentUI) {

        if (contentUI instanceof ContentListUI<?, ?, ?>) {
            ContentListUI<?, ?, ?> contentListUI = (ContentListUI<?, ?, ?>) contentUI;

            boolean oneSelectedData = contentListUI.getModel().isOneSelectedData();
            if (!oneSelectedData) {
                return;
            }

            DataReference<?> dataReference = contentListUI.getModel().getSelectedDatas().get(0);
            Objects.requireNonNull(dataReference);

            NavigationTree tree = getTreeHelper(contentListUI);

            NavigationTreeNodeSupport selectedNode = tree.getSelectedNode();
            NavigationTreeNodeSupport nodeToSelect = tree.getChild(selectedNode, dataReference.getId());

            SwingUtilities.invokeLater(() -> tree.selectNode(nodeToSelect));
            return;
        }

        actionPerformed((JComponent) e.getSource());

    }

    public void actionPerformed(JComponent source) {

        NavigationTreeNodeSupport node = (NavigationTreeNodeSupport) source.getClientProperty(NODE);
        Objects.requireNonNull(node);

        SwingUtilities.invokeLater(() -> getMainUI().getNavigation().selectNode(node));
    }
}
