/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.actions.content;

import fr.ird.observe.application.swing.ui.ObserveKeyStrokes;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.content.ContentUI;
import fr.ird.observe.application.swing.ui.content.list.ContentListUI;
import fr.ird.observe.application.swing.ui.tree.navigation.NavigationTree;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.NavigationTreeNodeSupport;

import static fr.ird.observe.application.swing.ui.content.ContentUIHandler.getTreeHelper;

/**
 * Action pour sélectionner un noeud (attaché à l'éditeur) dans l'arbre de
 * navigation.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public class SelectOpenNodeUIAction extends AbstractContentUIAction {

    public static final String ACTION_NAME = "selectOpen";

    private static final long serialVersionUID = 1L;

    public SelectOpenNodeUIAction(ObserveMainUI mainUI) {
        super(mainUI, ACTION_NAME, "<NONE>", "<NONE>", "go-down", ObserveKeyStrokes.KEY_STROKE_SHIFT_ALT_ENTER);
    }

    @Override
    protected void actionPerformed(ContentUI<?, ?> contentUI) {

        if (!(contentUI instanceof ContentListUI)) {
            return;
        }

        ContentListUI<?, ?, ?> contentListUI = (ContentListUI<?, ?, ?>) contentUI;

        NavigationTreeNodeSupport<?> node = contentListUI.getOpenNode();
        NavigationTree tree = getTreeHelper(contentListUI);
        tree.selectNode(node);

    }

}
