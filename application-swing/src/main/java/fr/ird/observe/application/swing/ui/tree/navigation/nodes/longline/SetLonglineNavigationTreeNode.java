package fr.ird.observe.application.swing.ui.tree.navigation.nodes.longline;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ui.content.impl.longline.SetLonglineUI;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.ReferenceNavigationTreeNodeSupport;
import fr.ird.observe.services.dto.longline.SetLonglineDto;
import fr.ird.observe.services.dto.reference.DataReference;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 14/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class SetLonglineNavigationTreeNode extends ReferenceNavigationTreeNodeSupport<SetLonglineDto, DataReference<SetLonglineDto>> {

    public SetLonglineNavigationTreeNode(DataReference<SetLonglineDto> data) {
        super(data, true);
        if (isPersisted()) {
            add(new SetGlobalCompositionLonglineNavigationTreeNode());
            add(new SetDetailCompositionLonglineNavigationTreeNode());
            add(new CatchLonglineNavigationTreeNode());
            add(new TdrLonglineNavigationTreeNode());
        }
    }

    @Override
    public boolean isLeaf() {
        return !isPersisted();
    }

    @Override
    public void reload() {
        DataReference<SetLonglineDto> data = getMainDataSourceServicesProvider().newSetLonglineService().loadReferenceToRead(getId());
        setData(data);
    }

    @Override
    public String getText() {
        return isPersisted() ? super.getText() : t("observe.type.setLongline.unsaved");
    }

    @Override
    public boolean isOpen() {
        return getParent().isOpen();
    }

    @Override
    public Class<SetLonglineUI> getContentClass() {
        return SetLonglineUI.class;
    }

}
