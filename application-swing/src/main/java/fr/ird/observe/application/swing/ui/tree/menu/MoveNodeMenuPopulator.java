package fr.ird.observe.application.swing.ui.tree.menu;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ui.tree.actions.NodeChangeActionListener;
import fr.ird.observe.application.swing.ui.tree.navigation.NavigationTree;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.NavigationTreeNodeSupport;
import fr.ird.observe.application.swing.ui.util.DecoratedNodeEntity;

import java.util.List;

/**
 * Objets pour créer les items du menu déplacer dans le menu contextuel de l'arbre
 *
 * @author Kevin Morin (Code Lutin)
 * @since 5.0
 */
public abstract class MoveNodeMenuPopulator {

    /**
     * Crée une action pour déplacer le noeud quand on sélectionne le menu du nouveau parent
     *
     * @param tree     l'arbre de navigation
     * @param id       l'identifiant du nœud à deplacer
     * @param parentId l'identifiant du nœud parent
     * @return l'action de déplacement
     */
    public abstract NodeChangeActionListener createChangeActionListener(NavigationTree tree, String id, String parentId);

    /**
     * Récupère les parents dans lesquels on peut déplacer le noeud sélectionné
     *
     * @param node le noeud sélectionné
     * @return une liste contenant les ids et les libellés des noeuds
     */
    public abstract List<DecoratedNodeEntity> getPossibleParentNodes(NavigationTreeNodeSupport node);

}
