package fr.ird.observe.application.swing.ui.storage.presets;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.presets.RemoteDataSourceConfiguration;
import fr.ird.observe.services.dto.presets.ServerDataSourceConfiguration;
import org.jdesktop.beans.AbstractBean;

import java.util.List;

/**
 * Created on 20/12/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class RemotePresetsUIModel extends AbstractBean {

    private List<RemoteDataSourceConfiguration> remoteDataSourceConfigurations;
    private RemoteDataSourceConfiguration remoteDataSourceConfiguration;
    private ServerDataSourceConfiguration serverDataSourceConfiguration;
    private List<ServerDataSourceConfiguration> serverDataSourceConfigurations;

    private boolean remoteModified;
    private boolean serverModified;

    public List<RemoteDataSourceConfiguration> getRemoteDataSourceConfigurations() {
        return remoteDataSourceConfigurations;
    }

    public void setRemoteDataSourceConfigurations(List<RemoteDataSourceConfiguration> remoteDataSourceConfigurations) {
        this.remoteDataSourceConfigurations = remoteDataSourceConfigurations;
        firePropertyChange("remoteDataSourceConfigurations", null, remoteDataSourceConfigurations);
    }

    public List<ServerDataSourceConfiguration> getServerDataSourceConfigurations() {
        return serverDataSourceConfigurations;
    }

    public void setServerDataSourceConfigurations(List<ServerDataSourceConfiguration> serverDataSourceConfigurations) {
        this.serverDataSourceConfigurations = serverDataSourceConfigurations;
        firePropertyChange("serverDataSourceConfigurations", null, serverDataSourceConfigurations);
    }

    public RemoteDataSourceConfiguration getRemoteDataSourceConfiguration() {
        return remoteDataSourceConfiguration;
    }

    public void setRemoteDataSourceConfiguration(RemoteDataSourceConfiguration remoteDataSourceConfiguration) {
        this.remoteDataSourceConfiguration = remoteDataSourceConfiguration;
        firePropertyChange("remoteDataSourceConfiguration", null, remoteDataSourceConfiguration);
    }

    public ServerDataSourceConfiguration getServerDataSourceConfiguration() {
        return serverDataSourceConfiguration;
    }

    public void setServerDataSourceConfiguration(ServerDataSourceConfiguration serverDataSourceConfiguration) {
        this.serverDataSourceConfiguration = serverDataSourceConfiguration;
        firePropertyChange("serverDataSourceConfiguration", null, serverDataSourceConfiguration);
    }

    public boolean isRemoteModified() {
        return remoteModified;
    }

    public void setRemoteModified(boolean remoteModified) {
        this.remoteModified = remoteModified;
        firePropertyChange("remoteModified", null, remoteModified);
    }

    public boolean isServerModified() {
        return serverModified;
    }

    public void setServerModified(boolean serverModified) {
        this.serverModified = serverModified;
        firePropertyChange("serverModified", null, serverModified);
    }
}
