package fr.ird.observe.application.swing.ui.content.list.impl.longline;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.DataContext;
import fr.ird.observe.application.swing.db.constants.DataContextType;
import fr.ird.observe.application.swing.ui.content.ContentMode;
import fr.ird.observe.application.swing.ui.content.list.ContentListUIHandler;
import fr.ird.observe.application.swing.ui.tree.navigation.NavigationTree;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.NavigationTreeNodeSupport;
import fr.ird.observe.services.dto.longline.ActivityLonglineDto;
import fr.ird.observe.services.dto.longline.TripLonglineDto;
import fr.ird.observe.services.dto.reference.DataReference;
import org.nuiton.jaxx.runtime.spi.UIHandler;

import static org.nuiton.i18n.I18n.n;

/**
 * Created on 8/28/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.7
 */
public class ActivityLonglinesUIHandler extends ContentListUIHandler<TripLonglineDto, ActivityLonglineDto, ActivityLonglinesUI> implements UIHandler<ActivityLonglinesUI> {

    public ActivityLonglinesUIHandler() {
        super(DataContextType.TripLongline, DataContextType.ActivityLongline);
    }

    @Override
    protected boolean computeCanGotoOpen() {
        // il faut que l'activité ouverte soit dans cette marée
        boolean result = ObserveSwingApplicationContext.get().getDataContext().isOpenActivityLongline();
        if (result) {

            String id = ObserveSwingApplicationContext.get().getDataContext().getOpenActivityLonglineId();
            result = false;
            for (DataReference<ActivityLonglineDto> reference : ui.getModel().getData()) {
                if (id.equals(reference.getId())) {
                    result = true;
                    break;
                }
            }
        }
        return result;
    }

    @Override
    protected boolean computeCanClose() {
        return computeCanGotoOpen();
    }

    @Override
    public NavigationTreeNodeSupport<?> getOpenNode() {
        DataContext dataContext = ObserveSwingApplicationContext.get().getDataContext();
        NavigationTree treeHelper = getTreeHelper(ui);
        return treeHelper.getActivityLonglineNode(dataContext.getOpenProgramId(), dataContext.getOpenTripLonglineId(), dataContext.getOpenActivityLonglineId());
    }

    @Override
    protected ContentMode getContentMode(DataContext dataContext) {

        String openTripLonglineId = dataContext.getOpenTripId();

        if (openTripLonglineId == null) {

            // pas de marée ouverte, donc on ne peut pas ouvrir une activité
            addInfoMessage(n("observe.content.tripLongline.message.no.active.found"));
            return ContentMode.READ;
        }

        //
        // il existe une marée ouverte
        //

        boolean openActivity = dataContext.isOpenActivity();

        if (dataContext.isSelectedOpen(TripLonglineDto.class)) {

            //
            // la marée courante est ouverte
            //

            // l'activité courante est ouverte
            if (openActivity) {

                // il existe une activite d'ouverte dans la maree courante
                addInfoMessage(n("observe.storage.activityLongline.message.active.found"));
                return ContentMode.UPDATE;
            }

            // pas d'activité ouverte, on peut en ouvrir une
            addInfoMessage(n("observe.storage.activityLongline.message.no.active.found"));
            return ContentMode.CREATE;
        }

        //
        // la marée ouverte n'est pas la marée courante
        //

        if (openActivity) {

            // il existe une activité ouverte dans la marée ouverte
            addInfoMessage(n("observe.storage.activityLongline.message.active.found.for.other.trip"));
        } else {

            // il n'existe pas d'activité ouverte dans la marée ouverte
            addInfoMessage(n("observe.storage.activityLongline.message.no.active.found.for.other.trip"));
        }

        return ContentMode.READ;

    }

}
