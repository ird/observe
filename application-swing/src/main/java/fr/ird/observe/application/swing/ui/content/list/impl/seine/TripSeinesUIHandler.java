/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.content.list.impl.seine;

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.DataContext;
import fr.ird.observe.application.swing.db.constants.DataContextType;
import fr.ird.observe.application.swing.ui.content.ContentMode;
import fr.ird.observe.application.swing.ui.content.list.ContentListUIHandler;
import fr.ird.observe.application.swing.ui.tree.navigation.NavigationTree;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.NavigationTreeNodeSupport;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.seine.ProgramSeineNavigationTreeNode;
import fr.ird.observe.services.dto.reference.DataReference;
import fr.ird.observe.services.dto.reference.ReferentialReference;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import org.nuiton.jaxx.runtime.spi.UIHandler;

import static org.nuiton.i18n.I18n.n;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class TripSeinesUIHandler extends ContentListUIHandler<ProgramDto, TripSeineDto, TripSeinesUI> implements UIHandler<TripSeinesUI> {

    public TripSeinesUIHandler() {
        super(DataContextType.Program, DataContextType.TripSeine);
    }

    @Override
    protected ContentMode getContentMode(DataContext dataContext) {

        String openProgramId = dataContext.getOpenProgramId();

        if (openProgramId == null) {

            // pas de program ouvert (donc pas de maree ouverte)
            // on peut reouvrir une maree
            addInfoMessage(n("observe.content.tripSeine.message.no.active.found"));
            return ContentMode.CREATE;
        }

        //
        // il existe un maree ouverte
        //

        if (dataContext.isSelectedOpen(ProgramDto.class)) {

            // le program courant a une maree ouverte
            addInfoMessage(n("observe.content.tripSeine.message.active.found"));
            return ContentMode.UPDATE;
        }

        //
        // la marée ouverte est dans un autre program
        //

        addInfoMessage(n("observe.content.tripSeine.message.active.found.for.other.program"));
        return ContentMode.READ;
    }

    @Override
    protected void finalizeOpenUI() {
        NavigationTree treeHelper = getTreeHelper(ui);
        ProgramSeineNavigationTreeNode selectedNode = (ProgramSeineNavigationTreeNode) treeHelper.getSelectedNode();
        ReferentialReference<ProgramDto> data = selectedNode.getData();
        String title = getDecoratorService().getReferentialReferenceDecorator(ProgramDto.class).toString(data);
        getUi().setContentTitle(title);
    }

    protected boolean computeCanReopen() {
        return ui.getModel().isOneSelectedData() && !ObserveSwingApplicationContext.get().getDataContext().isOpenTrip();
    }

    @Override
    protected boolean computeCanGotoOpen() {
        // il faut que la marée ouverte soit dans ce programme
        boolean result = ObserveSwingApplicationContext.get().getDataContext().isOpenTripSeine();
        if (result) {

            String id = ObserveSwingApplicationContext.get().getDataContext().getOpenTripSeineId();
            result = false;
            for (DataReference<TripSeineDto> reference : ui.getModel().getData()) {
                if (id.equals(reference.getId())) {
                    result = true;
                    break;
                }
            }
        }
        return result;
    }

    @Override
    protected boolean computeCanClose() {
        // il faut que la marée ouverte soit dans ce programme et qu'il n'y a pas de route ouverte
        return computeCanGotoOpen() && !ObserveSwingApplicationContext.get().getDataContext().isOpenRoute();
    }

    @Override
    public NavigationTreeNodeSupport<?> getOpenNode() {
        DataContext dataContext = ObserveSwingApplicationContext.get().getDataContext();
        NavigationTree treeHelper = getTreeHelper(ui);
        return treeHelper.getTripNode(dataContext.getOpenProgramId(), dataContext.getOpenTripSeineId());
    }

}
