package fr.ird.observe.application.swing.ui.tree.actions;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.ird.observe.application.swing.ObserveOpenDataManager;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.ui.tree.navigation.NavigationTree;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.NavigationTreeNodeSupport;
import fr.ird.observe.services.ObserveServicesProvider;
import fr.ird.observe.services.dto.IdHelper;
import fr.ird.observe.services.service.data.longline.TripLonglineService;
import fr.ird.observe.services.service.data.seine.TripSeineService;

/**
 * Created on 1/9/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.11
 */
public class ChangeTripProgramActionListener extends NodeChangeActionListener {

    public ChangeTripProgramActionListener(NavigationTree tree, String tripId, String programId) {
        super(tree, tripId, programId);
    }

    @Override
    protected void closeNode(String tripId) {
        // Don't do anything : trip should stay open when being transferred
    }

    @Override
    protected NavigationTreeNodeSupport getParentNode(NavigationTreeNodeSupport node) {
        return node.getParent();
    }

    @Override
    protected NavigationTreeNodeSupport getNewParentNode(NavigationTreeNodeSupport grandParentNode, String parentNodeId) {
        return getTree().getChild(grandParentNode, parentNodeId);
    }

    @Override
    protected int moveNodeToParent(String nodeId, String parentNodeId, String oldParentNodeId) {
        int position;

        ObserveSwingApplicationContext applicationContext = ObserveSwingApplicationContext.get();
        ObserveServicesProvider servicesProvider = applicationContext.getMainDataSourceServicesProvider();
        if (IdHelper.isTripLonglineId(nodeId)) {
            TripLonglineService service = servicesProvider.newTripLonglineService();
            position = service.moveTripLonglineToProgram(nodeId, parentNodeId);

        } else {
            TripSeineService service = servicesProvider.newTripSeineService();
            position = service.moveTripSeineToProgram(nodeId, parentNodeId);
        }

        // Close old program and open new program
        ObserveOpenDataManager openDataManager = applicationContext.getOpenDataManager();
        if (openDataManager.isOpen(nodeId)) {
            openDataManager.closeProgram(oldParentNodeId);
            openDataManager.openProgram(parentNodeId);
        }

        return position;
    }
}
