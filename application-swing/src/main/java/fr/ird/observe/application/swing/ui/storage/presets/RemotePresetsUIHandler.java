package fr.ird.observe.application.swing.ui.storage.presets;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.configuration.ObserveSwingApplicationConfig;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.ui.ObserveKeyStrokes;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.ObserveUIMode;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.services.configuration.ObserveDataSourceInformation;
import fr.ird.observe.services.configuration.rest.ObserveDataSourceConfigurationRest;
import fr.ird.observe.services.configuration.topia.ObserveDataSourceConfigurationTopiaPG;
import fr.ird.observe.services.dto.presets.RemoteDataSourceConfiguration;
import fr.ird.observe.services.dto.presets.ServerDataSourceConfiguration;
import fr.ird.observe.services.security.BadObserveWebUserPasswordException;
import fr.ird.observe.services.security.UnknownObserveWebUserException;
import fr.ird.observe.services.security.UnknownObserveWebUserForDatabaseException;
import fr.ird.observe.services.security.UserLoginNotFoundException;
import fr.ird.observe.services.security.UserPasswordNotFoundException;
import fr.ird.observe.services.service.PingService;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.widgets.select.BeanComboBox;
import org.nuiton.decorator.Decorator;
import org.nuiton.decorator.JXPathDecorator;
import org.nuiton.version.Version;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import java.awt.BorderLayout;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 20/12/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class RemotePresetsUIHandler implements UIHandler<RemotePresetsUI> {

    private RemotePresetsUI ui;

    @Override
    public void beforeInit(RemotePresetsUI ui) {
        this.ui = ui;
        RemotePresetsUIModel model = new RemotePresetsUIModel();
        ObserveSwingApplicationConfig config = ObserveSwingApplicationContext.get().getConfig();
        List<ServerDataSourceConfiguration> serverDataSourceConfigurations = new ArrayList<>(Arrays.asList(config.getServerDataSourceConfigurations()));
        model.setServerDataSourceConfigurations(serverDataSourceConfigurations);
        List<RemoteDataSourceConfiguration> remoteDataSourceConfigurations = new ArrayList<>(Arrays.asList(config.getRemoteDataSourceConfigurations()));
        model.setRemoteDataSourceConfigurations(remoteDataSourceConfigurations);
        ui.setContextValue(model);
    }

    @Override
    public void afterInit(RemotePresetsUI ui) {

        Decorator<RemoteDataSourceConfiguration> remoteDecorator = ObserveSwingApplicationContext.get().getDecoratorService().getDecoratorByType(RemoteDataSourceConfiguration.class);
        ui.getRemoteConfigurations().init((JXPathDecorator<RemoteDataSourceConfiguration>) remoteDecorator, ui.getModel().getRemoteDataSourceConfigurations());

        Decorator<ServerDataSourceConfiguration> serverDecorator = ObserveSwingApplicationContext.get().getDecoratorService().getDecoratorByType(ServerDataSourceConfiguration.class);
        ui.getServerConfigurations().init((JXPathDecorator<ServerDataSourceConfiguration>) serverDecorator, ui.getModel().getServerDataSourceConfigurations());

        ui.getModel().addPropertyChangeListener("remoteDataSourceConfigurations", evt -> ui.getRemoteConfigurations().setData((List<RemoteDataSourceConfiguration>) evt.getNewValue()));
        ui.getModel().addPropertyChangeListener("serverDataSourceConfigurations", evt -> ui.getServerConfigurations().setData((List<ServerDataSourceConfiguration>) evt.getNewValue()));

        ui.getServerConfigurations().addPropertyChangeListener(BeanComboBox.PROPERTY_SELECTED_ITEM, evt -> {

            ServerDataSourceConfiguration oldValue = (ServerDataSourceConfiguration) evt.getOldValue();
            ui.getModel().setServerDataSourceConfiguration((ServerDataSourceConfiguration) evt.getNewValue());

            if (ui.getModel().getServerDataSourceConfiguration() == null) {
                ui.getServerPanel().remove(ui.getServerForm());
                ui.getServerPanel().add(ui.getNoServerSelected(), BorderLayout.CENTER);
            } else {
                if (oldValue == null) {
                    ui.getServerForm().setVisible(true);
                    ui.getServerPanel().remove(ui.getNoServerSelected());
                    ui.getServerPanel().add(ui.getServerForm(), BorderLayout.CENTER);
                }
                ui.getModel().setServerModified(false);
            }
            SwingUtilities.invokeLater(ui::repaint);
        });
        ui.getRemoteConfigurations().addPropertyChangeListener(BeanComboBox.PROPERTY_SELECTED_ITEM, evt -> {

            RemoteDataSourceConfiguration oldValue = (RemoteDataSourceConfiguration) evt.getOldValue();
            ui.getModel().setRemoteDataSourceConfiguration((RemoteDataSourceConfiguration) evt.getNewValue());

            if (ui.getModel().getRemoteDataSourceConfiguration() == null) {
                ui.getRemotePanel().remove(ui.getRemoteForm());
                ui.getRemotePanel().add(ui.getNoRemoteSelected(), BorderLayout.CENTER);
            } else {
                if (oldValue == null) {
                    ui.getRemoteForm().setVisible(true);
                    ui.getRemotePanel().remove(ui.getNoRemoteSelected());
                    ui.getRemotePanel().add(ui.getRemoteForm(), BorderLayout.CENTER);
                }
                ui.getModel().setRemoteModified(false);
            }
            SwingUtilities.invokeLater(ui::repaint);
        });

        ObserveKeyStrokes.addKeyStrokeFromMnemonic(ui);
    }

    public void testRemote() {

        ObserveDataSourceConfigurationTopiaPG config = new ObserveDataSourceConfigurationTopiaPG();
        config.setJdbcUrl(ui.getRemoteUrl().getText().trim());
        config.setUsername(ui.getRemoteLogin().getText().trim());
        config.setPassword(ui.getRemotePassword().getText().trim().toCharArray());
        config.setUseSsl(ui.getRemoteUseSsl().isSelected());

        String connexionStatusError = null;

        Version modelVersion = ObserveSwingApplicationContext.get().getConfig().getModelVersion();
        ObserveSwingDataSource dataSource = ObserveSwingApplicationContext.get().getDataSourcesManager().newDataSource(config);
        try {

            ObserveDataSourceInformation dataSourceInformation = dataSource.checkCanConnect();

            Version versionDataSource = dataSourceInformation.getVersion();

            // en mise a jour de la base on ne test pas la version
            if (!modelVersion.equals(versionDataSource)) {

                connexionStatusError = t("observe.storage.error.dbVersionMismatch", versionDataSource, modelVersion);
            }

        } catch (UnknownObserveWebUserException e) {
            connexionStatusError = t("observe.storage.error.rest.user.unknown", e.getUserLogin());
        } catch (BadObserveWebUserPasswordException e) {
            connexionStatusError = t("observe.storage.error.rest.password.bad", e.getUserLogin());
        } catch (UnknownObserveWebUserForDatabaseException e) {
            connexionStatusError = t("observe.storage.error.rest.database.unknownForUser", e.getDatabaseName(), e.getRole());
        } catch (UserLoginNotFoundException e) {
            connexionStatusError = t("observe.storage.error.rest.user.required");
        } catch (UserPasswordNotFoundException e) {
            connexionStatusError = t("observe.storage.error.rest.pasword.required");
        } catch (Exception e) {
            connexionStatusError = e.getMessage();
            if (connexionStatusError == null || connexionStatusError.isEmpty()) {
                connexionStatusError = e.getClass().getName();
            }
        } finally {
            if (dataSource.isOpen()) {
                dataSource.close();
            }
        }

        if (connexionStatusError == null) {
            UIHelper.displayInfo("Test de connexion", "Le connexion a été établie avec succès.");
        } else {
            UIHelper.displayWarning("Test de connexion", "Le test de connexion a échoué :\n" + connexionStatusError);
        }
    }

    public void testServer() {

        String connexionStatusError = null;
        ObserveDataSourceConfigurationRest config = new ObserveDataSourceConfigurationRest();
        String url = ui.getServerUrl().getText().trim();
        try {
            config.setServerUrl(new URL(url));
        } catch (MalformedURLException e) {
            connexionStatusError = t("observe.storage.error.badUrl", url);
        }
        config.setLogin(ui.getServerLogin().getText().trim());
        config.setPassword(ui.getServerPassword().getText().trim().toCharArray());
        String databaseName = ui.getServerDataBase().getText().trim();
        config.setOptionalDatabaseName(databaseName.isEmpty() ? null : databaseName);

        Version modelVersion = ObserveSwingApplicationContext.get().getConfig().getModelVersion();
        ObserveSwingDataSource dataSource = ObserveSwingApplicationContext.get().getDataSourcesManager().newDataSource(config);
        try {

            PingService pingService = dataSource.newPingService();

            Version modelServerVersion = pingService.ping();

            if (!modelVersion.equals(modelServerVersion)) {

                connexionStatusError = t("observe.storage.error.serverVersionMismatch", modelServerVersion, modelVersion);

            }

            if (connexionStatusError == null) {

                ObserveDataSourceInformation dataSourceInformation = dataSource.checkCanConnect();

                Version versionDataSource = dataSourceInformation.getVersion();

                // en mise a jour de la base on ne test pas la version
                if (!modelVersion.equals(versionDataSource)) {

                    connexionStatusError = t("observe.storage.error.dbVersionMismatch", versionDataSource, modelVersion);
                }
            }

        } catch (UnknownObserveWebUserException e) {
            connexionStatusError = t("observe.storage.error.rest.user.unknown", e.getUserLogin());
        } catch (BadObserveWebUserPasswordException e) {
            connexionStatusError = t("observe.storage.error.rest.password.bad", e.getUserLogin());
        } catch (UnknownObserveWebUserForDatabaseException e) {
            connexionStatusError = t("observe.storage.error.rest.database.unknownForUser", e.getDatabaseName(), e.getRole());
        } catch (UserLoginNotFoundException e) {
            connexionStatusError = t("observe.storage.error.rest.user.required");
        } catch (UserPasswordNotFoundException e) {
            connexionStatusError = t("observe.storage.error.rest.pasword.required");
        } catch (Exception e) {
            connexionStatusError = e.getMessage();
            if (connexionStatusError == null || connexionStatusError.isEmpty()) {
                connexionStatusError = e.getClass().getName();
            }
        } finally {
            if (dataSource.isOpen()) {
                dataSource.close();
            }
        }

        if (connexionStatusError == null) {
            UIHelper.displayInfo("Test de connexion", "Le connexion a été établie avec succès.");
        } else {
            UIHelper.displayWarning("Test de connexion", "Le test de connexion a échoué :\n" + connexionStatusError);
        }
    }

    public void saveRemote() {

        RemoteDataSourceConfiguration remoteDataSourceConfiguration = ui.getModel().getRemoteDataSourceConfiguration();

        remoteDataSourceConfiguration.setName(ui.getRemoteName().getText().trim());
        remoteDataSourceConfiguration.setUrl(ui.getRemoteUrl().getText().trim());
        remoteDataSourceConfiguration.setLogin(ui.getRemoteLogin().getText().trim());
        remoteDataSourceConfiguration.setPassword(ui.getRemotePassword().getText().trim());
        remoteDataSourceConfiguration.setUseSsl(ui.getRemoteUseSsl().isSelected());

        ObserveSwingApplicationContext.get().getConfig().updateRemoteDataSourceConfiguration(remoteDataSourceConfiguration);

        ui.getModel().setRemoteDataSourceConfiguration(null);
        ui.getModel().setRemoteDataSourceConfiguration(remoteDataSourceConfiguration);
        ui.getModel().setRemoteModified(false);

    }

    public void deleteRemote() {

        int response = UIHelper.askUser(t("observe.storage.presets.delete.title"),
                                        t("observe.storage.presets.delete.message"),
                                        JOptionPane.QUESTION_MESSAGE,
                                        new Object[]{t("observe.action.delete"), t("observe.action.cancel")},
                                        0);
        boolean delete = response == 0;

        if (delete) {
            RemoteDataSourceConfiguration remoteDataSourceConfiguration = ui.getModel().getRemoteDataSourceConfiguration();
            List<RemoteDataSourceConfiguration> remoteDataSourceConfigurations = new ArrayList<>(ui.getModel().getRemoteDataSourceConfigurations());
            remoteDataSourceConfigurations.remove(remoteDataSourceConfiguration);
            ui.getModel().setRemoteDataSourceConfigurations(remoteDataSourceConfigurations);
            ObserveSwingApplicationContext.get().getConfig().removeRemoteDataSourceConfiguration(remoteDataSourceConfiguration);
            ui.getModel().setRemoteDataSourceConfiguration(null);
        }
    }

    public void saveServer() {

        ServerDataSourceConfiguration serverDataSourceConfiguration = ui.getModel().getServerDataSourceConfiguration();

        serverDataSourceConfiguration.setName(ui.getServerName().getText().trim());
        serverDataSourceConfiguration.setUrl(ui.getServerUrl().getText().trim());
        serverDataSourceConfiguration.setLogin(ui.getServerLogin().getText().trim());
        serverDataSourceConfiguration.setPassword(ui.getServerPassword().getText().trim());
        String databaseName = ui.getServerDataBase().getText().trim();
        serverDataSourceConfiguration.setDatabaseName(databaseName.isEmpty() ? null : databaseName);

        ObserveSwingApplicationContext.get().getConfig().updateServerDataSourceConfiguration(serverDataSourceConfiguration);

        ui.getModel().setServerDataSourceConfiguration(null);
        ui.getModel().setServerDataSourceConfiguration(serverDataSourceConfiguration);
        ui.getModel().setServerModified(false);

    }

    public void deleteServer() {

        int response = UIHelper.askUser(t("observe.storage.presets.delete.title"),
                                        t("observe.storage.presets.delete.message"),
                                        JOptionPane.QUESTION_MESSAGE,
                                        new Object[]{t("observe.action.delete"), t("observe.action.cancel")},
                                        0
        );
        boolean delete = response == 0;

        if (delete) {
            ServerDataSourceConfiguration serverDataSourceConfiguration = ui.getModel().getServerDataSourceConfiguration();
            List<ServerDataSourceConfiguration> serverDataSourceConfigurations = new ArrayList<>(ui.getModel().getServerDataSourceConfigurations());
            serverDataSourceConfigurations.remove(serverDataSourceConfiguration);
            ui.getModel().setServerDataSourceConfigurations(serverDataSourceConfigurations);
            ObserveSwingApplicationContext.get().getConfig().removeServerDataSourceConfiguration(serverDataSourceConfiguration);
            ui.getModel().setServerDataSourceConfiguration(null);
        }
    }

    public void quit() {

        ObserveMainUI mainUI = ObserveSwingApplicationContext.get().getMainUI();
        mainUI.getDataSourcePresets().setContentContainer(new JPanel());
        if (ObserveSwingApplicationContext.get().getDataSourcesManager().getMainDataSource() == null) {
            mainUI.getModel().setMode(ObserveUIMode.NO_DB);
        } else {
            mainUI.getModel().setMode(ObserveUIMode.DB);
        }

    }
}
