package fr.ird.observe.application.swing.ui.tree.navigation.nodes.seine;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.decoration.ObserveI18nDecoratorHelper;
import fr.ird.observe.application.swing.ui.content.list.impl.seine.RoutesUI;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.ClassNavigationTreeNode;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.WithChildsToReload;
import fr.ird.observe.services.dto.reference.DataReference;
import fr.ird.observe.services.dto.reference.DataReferenceSet;
import fr.ird.observe.services.dto.seine.RouteDto;
import fr.ird.observe.services.service.data.seine.RouteService;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 14/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class RoutesSeineNavigationTreeNode extends ClassNavigationTreeNode<RouteDto> implements WithChildsToReload {

    public RoutesSeineNavigationTreeNode() {
        super(RouteDto.class, true);
    }

    @Override
    public String getId() {
        return getParent().getId();
    }

    @Override
    public Class<RoutesUI> getContentClass() {
        return RoutesUI.class;
    }

    @Override
    public String getIconPath() {
        return "navigation." + getData().getName();
    }

    @Override
    public String getText() {
        return t(ObserveI18nDecoratorHelper.getTypePluralI18nKey(getData()));
    }

    @Override
    public void reloadChilds() {
        RouteService routeService = ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newRouteService();
        DataReferenceSet<RouteDto> referenceSet = routeService.getRouteByTripSeine(getId());
        for (DataReference<RouteDto> reference : referenceSet.getReferences()) {
            add(new RouteSeineNavigationTreeNode(reference));
        }
    }
}
