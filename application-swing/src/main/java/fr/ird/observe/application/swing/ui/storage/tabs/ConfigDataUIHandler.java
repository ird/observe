package fr.ird.observe.application.swing.ui.storage.tabs;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.configuration.constants.CreationMode;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.storage.StorageStep;
import fr.ird.observe.application.swing.ui.storage.StorageUILauncher;
import fr.ird.observe.application.swing.ui.storage.StorageUIModel;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JComponent;
import java.awt.Window;
import java.beans.PropertyChangeListener;
import java.io.File;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 27/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since X
 */
public class ConfigDataUIHandler extends StorageTabUIHandler<ConfigDataUI> implements UIHandler<ConfigDataUI> {

    /** Logger */
    private static final Log log = LogFactory.getLog(ConfigDataUIHandler.class);

    @Override
    public void afterInit(ConfigDataUI ui) {
        if (ui.getStep() != null) {
            ui.setDescriptionText(t(ui.getStep().getDescription()));

            PropertyChangeListener listener = evt -> {
                String propertyName = evt.getPropertyName();
                if (StorageUIModel.DATA_IMPORT_MODE_PROPERTY_NAME.equals(propertyName)) {
                    CreationMode id = (CreationMode) evt.getNewValue();

                    if (id != null) {
                        refreshConfig(id.name());

                    }

                }
            };
            ui.getModel().addPropertyChangeListener(listener);
            ui.getModel().setDataImportMode(CreationMode.EMPTY);
        }
    }

    public void refreshConfig(String configId) {
        JComponent c = (JComponent) ui.getObjectById(configId);
        if (c != null) {
            if (log.isDebugEnabled()) {
                log.debug(configId);
            }
            ui.configLayout.show(ui.configContent, configId);
        }
    }

    public void chooseDumpFile() {
        File f = UIHelper.chooseFile(ui,
                                     t("observe.title.choose.db.dump"),
                                     t("observe.action.choose.db.dump"),
                                     ui.getModel().getDumpFile(),
                                     "^.+\\.sql\\.gz$",
                                     t("observe.action.choose.db.dump.description"));
        ui.getCentralSourceModel().setDumpFile(f);
        ui.getModel().validate();
    }

    public void obtainRemoteConnexion() {
        StorageUIModel sourceModel = ui.getCentralSourceModel();
        StorageUILauncher.obtainRemoteConnexion(ui.getDelegateContext(), ui.getParentContainer(Window.class), sourceModel);

        sourceModel.validate(StorageStep.CONFIG);

        ui.getModel().validate();
    }


    public void obtainServerConnexion() {
        StorageUIModel sourceModel = ui.getCentralSourceModel();
        StorageUILauncher.obtainServerConnexion(ui.getDelegateContext(), ui.getParentContainer(Window.class), sourceModel);

        sourceModel.validate(StorageStep.CONFIG);

        ui.getModel().validate();
    }

}
