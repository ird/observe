package fr.ird.observe.application.swing.ui.tree.selection.nodes;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.services.dto.longline.TripLonglineDto;
import fr.ird.observe.services.dto.longline.TripLonglineHelper;
import fr.ird.observe.services.dto.reference.DataReference;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 18/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class TripLonglineSelectionTreeNode extends TripSelectionTreeNodeSupport<TripLonglineDto> {

    public TripLonglineSelectionTreeNode(DataReference<TripLonglineDto> data) {
        super(data, false);
    }

    @Override
    public boolean isOpen() {
        return ObserveSwingApplicationContext.get().getOpenDataManager().isOpenTripLongline(getData().getId());
    }

    @Override
    public String getText() {
        String s = super.getText() + " (" + TripLonglineHelper.getActivityCount(getData()) + ")";
        if (isExist()) {
            return t("observe.common.exist.on.remote", s);
        }
        return s;
    }

    public String getToolTipText() {
        if (isExist()) {
            return t("observe.message.warning.will.be.delete", getText());
        } else {
            return getText();
        }
    }

}
