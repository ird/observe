/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.storage.tabs;

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.ObserveTextGenerator;
import fr.ird.observe.application.swing.ui.storage.StorageUIModel;
import fr.ird.observe.services.configuration.ObserveDataSourceInformation;
import org.nuiton.jaxx.runtime.JAXXObject;

import javax.swing.JLabel;

import static org.nuiton.i18n.I18n.t;

/**
 * Le controleur comun à tous les onglets.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public class StorageTabUIHandler<U extends JAXXObject> {

    protected U ui;

    public String updateStorageLabel(StorageUIModel service, boolean serviceValid, JLabel label, boolean remote) {
        String text;
        if (serviceValid && remote == service.isRemote()) {

            // on recupere le label du service
            text = service.getLabel();
        } else {

            // aucun service configuré
            text = t((String) label.getClientProperty("no"));
        }
        return text;
    }

    protected String updateDataSourcePolicy(StorageUIModel sourceModel, boolean valid, boolean remote) {
        String text = null;
        if (valid && remote == sourceModel.isRemote()) {
            ObserveDataSourceInformation dataSourceInformation = sourceModel.getDataSourceInformation();

            if (dataSourceInformation != null) {

                ObserveSwingApplicationContext applicationContext = ObserveSwingApplicationContext.get();
                ObserveTextGenerator textGenerator = applicationContext.getTextGenerator();
                text = textGenerator.getDataSourcePolicy(dataSourceInformation);
            }

        } else {

            text = t("observe.common.storage.not.valid");

        }

        return text;
    }

    public final void beforeInit(U ui) {
        this.ui = ui;
    }

}
