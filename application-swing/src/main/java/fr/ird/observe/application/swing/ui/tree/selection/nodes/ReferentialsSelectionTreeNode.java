package fr.ird.observe.application.swing.ui.tree.selection.nodes;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import org.apache.commons.collections4.EnumerationUtils;

import java.util.Iterator;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 18/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class ReferentialsSelectionTreeNode extends SelectionTreeNodeSupport<String> implements Iterable<ReferentialSelectionTreeNode<?>> {

    public static ReferentialsSelectionTreeNode of(String name, ImmutableSet<Class<? extends ReferentialDto>> types) {
        ReferentialsSelectionTreeNode result = new ReferentialsSelectionTreeNode(t(name));
        for (Class<? extends ReferentialDto> aClass : types) {
            ReferentialSelectionTreeNode child = new ReferentialSelectionTreeNode<>(aClass);
            result.add(child);
        }

        return result;
    }

    @Override
    public void setSelected(boolean selected) {
        for (ReferentialSelectionTreeNode<?> node : this) {
            node.setSelected(selected);
        }
    }

    public ReferentialsSelectionTreeNode(String name) {
        super(name, true);
    }

    @Override
    public String getText() {
        return t(getData());
    }

    @Override
    public String getIconPath() {
        return "navigation.referentiel";
    }

    @Override
    public boolean isOpen() {
        return true;
    }

    @Override
    public String getId() {
        return null;
    }

    @Override
    public Iterator<ReferentialSelectionTreeNode<?>> iterator() {
        return (Iterator) EnumerationUtils.toList(children()).iterator();
    }

    @Override
    public boolean isSelected() {
        for (ReferentialSelectionTreeNode<?> nodeSupport : this) {
            if (!nodeSupport.isSelected()) {
                return false;
            }
        }
        return true;
    }

    public ImmutableList<Class<? extends ReferentialDto>> getSelected() {
        ImmutableList.Builder<Class<? extends ReferentialDto>> builder = ImmutableList.builder();
        for (ReferentialSelectionTreeNode<?> nodeSupport : this) {
            if (nodeSupport.isSelected()) {
                builder.add(nodeSupport.getData());
            }
        }
        return builder.build();
    }

}
