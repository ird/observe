package fr.ird.observe.application.swing.ui.admin.config;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.configuration.constants.DbMode;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.ui.admin.AdminActionModel;
import fr.ird.observe.application.swing.ui.admin.AdminStep;
import fr.ird.observe.application.swing.ui.admin.AdminUI;
import fr.ird.observe.application.swing.ui.admin.AdminUIModel;
import fr.ird.observe.application.swing.ui.admin.synchronize.referential.ReferentialSynchronizeMode;
import fr.ird.observe.application.swing.ui.storage.StorageUIModel;
import fr.ird.observe.application.swing.ui.tree.selection.SelectionTreeModel;
import fr.ird.observe.services.configuration.ObserveDataSourceConfiguration;
import fr.ird.observe.services.configuration.ObserveDataSourceInformation;
import fr.ird.observe.services.service.BabModelVersionException;
import fr.ird.observe.services.service.DatabaseConnexionNotAuthorizedException;
import fr.ird.observe.services.service.DatabaseNotFoundException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.EnumSet;
import java.util.Objects;

import static fr.ird.observe.application.swing.configuration.constants.DbMode.USE_LOCAL;
import static fr.ird.observe.application.swing.configuration.constants.DbMode.USE_REMOTE;
import static fr.ird.observe.application.swing.configuration.constants.DbMode.USE_SERVER;
import static org.nuiton.i18n.I18n.t;

/**
 * Created on 28/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class ConfigModel extends AdminActionModel {

    /** Logger */
    private static final Log log = LogFactory.getLog(ConfigModel.class);
    public static final String PROPERTY_LOCAL_SOURCE_LABEL = "localSourceLabel";
    public static final String PROPERTY_CENTRAL_SOURCE_LABEL = "centralSourceLabel";

    /** la source de données en cours d'utilisation par l'application */
    protected ObserveSwingDataSource previousSource;
    /** la source de données sur laquel on veut travailler */
    protected ObserveSwingDataSource localSource;
    /** la source de données sur laquel on veut travailler */
    protected ObserveDataSourceInformation localSourceInformation;
    /** la source de données dite central (contenant le référentiel valide) */
    protected ObserveSwingDataSource centralSource;
    /** la configuration de la base source */
    protected final StorageUIModel localSourceModel;
    /** la configuration de la base central */
    protected final StorageUIModel centralSourceModel;
    private String localSourceLabel = t("observe.storage.config.source.storage");
    private String centralSourceLabel = t("observe.storage.config.referentiel.storage");

    public ConfigModel() {
        super(AdminStep.CONFIG);
        localSourceModel = new StorageUIModel() {

            @Override
            public String getLabel() {
                String txt = getLocalSourceLabel();
                String params;

                if (getDbMode() == USE_SERVER) {
                    params = t("observe.storage.server.db") + " " + getRemoteUrl();
                } else if (getDbMode() == USE_REMOTE) {
                    params = t("observe.storage.remote.db") + " " + getRemoteUrl();
                } else {
                    params = t("observe.storage.locale.db") + " " + getH2Config().getDirectory().getAbsolutePath();
                }
                txt = t(txt, params);
                return txt;
            }

            @Override
            public void validate() {
                super.validate();

                // on declanche la revalidation du modèle
                firePropertyChange(VALID_PROPERTY_NAME, isValid());
            }

        };

        centralSourceModel = new StorageUIModel() {

            @Override
            public String getLabel() {
                String txt = getCentralSourceLabel();
                String params;

                if (getDbMode() == USE_SERVER) {
                    params = t("observe.storage.server.db") + " " + getRemoteUrl();
                } else if (getDbMode() == USE_REMOTE) {
                    params = t("observe.storage.remote.db") + " " + getRemoteUrl();
                } else {
                    params = t("observe.storage.locale.db") + " " + getH2Config().getDirectory().getAbsolutePath();
                }
                txt = t(txt, params);
                return txt;
            }

            @Override
            public void validate() {
                super.validate();

                // on declanche la revalidation du modèle
                firePropertyChange(VALID_PROPERTY_NAME, isValid());
            }
        };

    }

    public static void doOpenSource(ObserveSwingDataSource source) {

        if (source != null && !source.isOpen()) {
            try {
                source.open();
            } catch (DatabaseConnexionNotAuthorizedException | DatabaseNotFoundException | BabModelVersionException e) {
                throw new IllegalStateException("Could not open " + source, e);
            }
        }

    }

    public static void doCloseSource(ObserveSwingDataSource source) {

        if (source != null && source.isOpen()) {
            source.close();
        }

    }


    public String getLocalSourceLabel() {
        return localSourceLabel;
    }

    public void setLocalSourceLabel(String localSourceLabel) {
        Object oldValue = getLocalSourceLabel();
        this.localSourceLabel = localSourceLabel;
        firePropertyChange(PROPERTY_LOCAL_SOURCE_LABEL, oldValue, localSourceLabel);
    }

    public String getCentralSourceLabel() {
        return centralSourceLabel;
    }

    public void setCentralSourceLabel(String centralSourceLabel) {
        Object oldValue = getCentralSourceLabel();
        this.centralSourceLabel = centralSourceLabel;
        firePropertyChange(PROPERTY_CENTRAL_SOURCE_LABEL, oldValue, centralSourceLabel);
    }


    public boolean validate(AdminUIModel uiModel) {

        boolean validate = true;

        StorageUIModel centralSourceModel = getCentralSourceModel();
        StorageUIModel localSourceModel = getLocalSourceModel();

        if (uiModel.isNeedIncomingDataSource()) {
            if (!localSourceModel.isValid()) {
                if (log.isDebugEnabled()) {
                    log.debug("incoming service is not valid");
                }
                validate = false;
            }
        }
        if (uiModel.isNeedReferentielDataSource()) {
            if (!centralSourceModel.isValid()) {
                if (log.isDebugEnabled()) {
                    log.debug("referentiel service is not valid");
                }
                validate = false;
            }
        }

        if (!validate) {
            return false;
        }

        if (uiModel.containsOperation(AdminStep.VALIDATE)) {

            // il faut avoir selectionner un context
            // il faut avoir au moins selectionner un scope
            if (StringUtils.isEmpty(uiModel.getValidateModel().getContextName())) {
                if (log.isDebugEnabled()) {
                    log.debug("no validation context name");
                }
                return false;
            }
            if (uiModel.getValidateModel().getScopes().isEmpty()) {
                if (log.isDebugEnabled()) {
                    log.debug("no validation scopes");
                }
                return false;
            }
            if (uiModel.getValidateModel().getModelMode() == null) {
                if (log.isDebugEnabled()) {
                    log.debug("no validation model mode");
                }
                return false;
            }
            if (uiModel.getValidateModel().isGenerateReport()) {

                // le fichier de rapport ne doit pas exister
                File file = uiModel.getValidateModel().getReportFile();

                if (file.exists()) {
                    if (log.isDebugEnabled()) {
                        log.debug("report file already exists");
                    }
                    return false;
                }

                // le repertoire du rapport doit exister
                File parentFile = file.getParentFile();
                if (parentFile == null || !parentFile.exists()) {
                    return false;
                }
            }
            if (uiModel.getValidateModel().getValidators().isEmpty()) {
                if (log.isDebugEnabled()) {
                    log.debug("no validators detected");
                }
                return false;
            }

            // la base precedente doit etre ouverte
            ObserveDataSourceInformation dataSourceInformation = getLocalSourceInformation();

            // si le test de connection a echoue
            if (dataSourceInformation == null) {
                if (log.isDebugEnabled()) {
                    log.debug("can not connect to data source ");
                }
                return false;
            }


            // pour valider une base il faut les droits
            SelectionTreeModel selectionModel = uiModel.getSelectDataModel().getSelectionDataModel();
            if (selectionModel.isUseData()) {

                // il faut les droits en Lecture sur les donnes
                if (!(dataSourceInformation.canReadData())) {
                    if (log.isDebugEnabled()) {
                        log.debug("can not read data");
                    }
                    return false;
                }
            }
            if (selectionModel.isLoadReferential()) {

                // il faut les droits en L sur le referentiel
                if (!(dataSourceInformation.canReadReferential())) {
                    if (log.isDebugEnabled()) {
                        log.debug("can not read referentiel");
                    }
                    return false;
                }
            }
        }

        if (uiModel.containsOperation(AdminStep.DATA_SYNCHRONIZE)) {

            // les deux bases (source et referentiel) doivent etre different
            validate = validateNotSameDataSources();

            boolean atLeastOneWrite = false;

            ObserveDataSourceInformation leftDataSourceInformation = getLocalSourceInformation();
            if (!(leftDataSourceInformation.canReadData())) {
                if (log.isDebugEnabled()) {
                    log.debug("can not read data on left data source");
                }
                return false;
            }
            if (leftDataSourceInformation.canWriteData()) {
                atLeastOneWrite = true;
            }

            if (centralSourceModel.getDataSourceInformation() != null) {

                ObserveDataSourceInformation rightDataSourceInformation = centralSourceModel.getDataSourceInformation();
                if (!(rightDataSourceInformation.canReadData())) {
                    if (log.isDebugEnabled()) {
                        log.debug("can not read data on right data source");
                    }
                    return false;
                }
                if (rightDataSourceInformation.canWriteData()) {
                    atLeastOneWrite = true;
                }

            }

            if (!atLeastOneWrite) {
                if (log.isDebugEnabled()) {
                    log.debug("can not write data on any side");
                }
                return false;
            }

        }

        if (validate && uiModel.containsOperation(AdminStep.REFERENTIAL_SYNCHRONIZE)) {

            // les deux bases (source et referentiel) doivent etre different
            validate = validateNotSameDataSources();

            ObserveDataSourceInformation leftDataSourceInformation = getLocalSourceInformation();
            if (leftDataSourceInformation == null) {
                return false;
            }
            if (!(leftDataSourceInformation.canReadReferential())) {
                if (log.isDebugEnabled()) {
                    log.debug("can not read and write referential on left data source");
                }
                return false;
            }

            ObserveDataSourceInformation rightDataSourceInformation = centralSourceModel.getDataSourceInformation();
            if (rightDataSourceInformation == null) {
                return false;
            }

            if (!(rightDataSourceInformation.canReadReferential())) {
                if (log.isDebugEnabled()) {
                    log.debug("can not read and write referential on right data source");
                }
                return false;
            }

            ReferentialSynchronizeMode synchronizeMode = uiModel.getReferentialSynchroModel().getSynchronizeMode();
            if (synchronizeMode == null) {
                return false;
            }

            if (synchronizeMode.isLeftWrite()) {

                validate = localSourceModel.isLocal() || leftDataSourceInformation.canWriteReferential();

            }

            if (synchronizeMode.isRightWrite()) {

                validate = centralSourceModel.isLocal() || rightDataSourceInformation.canWriteReferential();

            }

        }

        if (uiModel.containsOperation(AdminStep.SYNCHRONIZE)) {

            if (!localSourceModel.isValid()) {
                return false;
            }

            ObserveDataSourceInformation leftDataSourceInformation = getLocalSourceInformation();
            if (leftDataSourceInformation == null) {
                return false;
            }

            if (!centralSourceModel.isValid()) {
                return false;
            }

            ObserveDataSourceInformation rightDataSourceInformation = centralSourceModel.getDataSourceInformation();
            if (rightDataSourceInformation == null) {
                return false;
            }

            if (!(rightDataSourceInformation.canReadReferential())) {
                if (log.isDebugEnabled()) {
                    log.debug("can not read referential on central source");
                }
                return false;
            }

            // les deux bases (source et referentiel) doivent etre different
            validate = validateNotSameDataSources();

        }
        if (uiModel.containsOperation(AdminStep.EXPORT_DATA)) {

            // il faut une base locale et une connexion distante
            // avec des droits d'écriture de données
            validate &= centralSourceModel.getDataSourceInformation() != null
                    && centralSourceModel.getDataSourceInformation().canWriteData();

        }

        if (uiModel.containsOperation(AdminStep.REPORT)) {

            // il faut le fichier di'mport existe
            File reportFile = uiModel.getReportModel().getReportFile();
            validate &= reportFile != null && reportFile.exists();
        }

        return validate;
    }

    public static final PropertyChangeListener LOG_PROPERTY_CHANGE_LISTENER = new AdminUIModel.LogPropertyChanged();

    protected void startCentralSourceModel(AdminUI ui) {

        AdminUIModel uiModel = ui.getModel();
        if (!uiModel.isNeedReferentielDataSource()) {

            // pas besoin de la base distante
            return;
        }

        // par default, on tente d'utiliser la base distance
        centralSourceModel.init(ui, null);
        centralSourceModel.setCanCreateLocalService(false);
        boolean canUseLocalSource = uiModel.getOperations().contains(AdminStep.DATA_SYNCHRONIZE)
                || uiModel.getOperations().contains(AdminStep.SYNCHRONIZE)
                || uiModel.getOperations().contains(AdminStep.REFERENTIAL_SYNCHRONIZE);
        centralSourceModel.setCanUseLocalService(canUseLocalSource);
        centralSourceModel.setCanUseRemoteService(true);
        centralSourceModel.setCanUseServerService(true);
        centralSourceModel.start(USE_REMOTE);

        if (log.isDebugEnabled()) {
            centralSourceModel.removePropertyChangeListener(LOG_PROPERTY_CHANGE_LISTENER);
            centralSourceModel.addPropertyChangeListener(LOG_PROPERTY_CHANGE_LISTENER);
        }

        // on teste si la connexion distante existe
        if (centralSourceModel.isValid()) {
            centralSourceModel.testRemote();
        }
    }

    protected void startIncomingSourceModel(AdminUI ui) {

        AdminUIModel uiModel = ui.getModel();

        if (!uiModel.isNeedIncomingDataSource()) {

            // pas besoin de la base locale
            return;
        }

        ObserveSwingDataSource previousSource = getPreviousSource();

        ObserveDataSourceConfiguration previousSourceConfig = null;
        ObserveDataSourceInformation previousSourceInfo = null;

        EnumSet<DbMode> authorizedModes = uiModel.getIncomingDataSourceMode();
        EnumSet<DbMode> modes = EnumSet.noneOf(DbMode.class);

        if (authorizedModes.contains(USE_LOCAL)) {

            // ce mode est disponible uniquement si une base locale existe
            if (ObserveSwingApplicationContext.get().getConfig().isLocalStorageExist()) {
                modes.add(USE_LOCAL);
            }
        }

        if (authorizedModes.contains(USE_REMOTE)) {

            modes.add(USE_REMOTE);
        }

        if (authorizedModes.contains(USE_SERVER)) {

            modes.add(USE_SERVER);
        }

        if (authorizedModes.contains(DbMode.CREATE_LOCAL)) {

            modes.add(DbMode.CREATE_LOCAL);
        }

        if (previousSource != null) {

            try {
                previousSourceConfig = previousSource.getConfiguration().clone();
                previousSourceInfo = previousSource.getInformation();
            } catch (CloneNotSupportedException e) {
                if (log.isErrorEnabled()) {
                    log.error("con not clone previous data configuration", e);
                }
            }

            if (previousSource.isRemote()) {

                if (!modes.contains(USE_REMOTE)) {

                    // pas autorise a utiliser cette source en entree
                    previousSource = null;
                }
            }

            if (previousSource != null && previousSource.isServer()) {

                if (!modes.contains(USE_SERVER)) {

                    // pas autorise a utiliser cette source en entree
                    previousSource = null;
                }
            }

            if (previousSource != null && previousSource.isLocal()) {

                if (!modes.contains(USE_LOCAL)) {

                    // pas autorise a utiliser cette source en entree
                    previousSource = null;
                }
            }
        }

        uiModel.getAvailableIncomingModes().addAll(modes);

        localSourceModel.setCanCreateLocalService(modes.contains(DbMode.CREATE_LOCAL));
        localSourceModel.setCanUseLocalService(modes.contains(USE_LOCAL));
        localSourceModel.setCanUseRemoteService(modes.contains(USE_REMOTE));
        localSourceModel.setCanUseServerService(modes.contains(USE_SERVER));

        if (previousSource == null) {

            // on intialize le modèle de la source locale à partir du service entrant (s'il existe)
            localSourceModel.init(ui, null);

        } else {

            // on intialize le modèle de la source locale à partir du service entrant (s'il existe)
            localSourceModel.initFromPreviousConfig(previousSourceConfig, previousSourceInfo);
        }

        DbMode dbMode = localSourceModel.getDbMode();
        localSourceModel.start(dbMode);

        if (log.isDebugEnabled()) {
            localSourceModel.removePropertyChangeListener(LOG_PROPERTY_CHANGE_LISTENER);
            localSourceModel.addPropertyChangeListener(LOG_PROPERTY_CHANGE_LISTENER);
        }
    }

    public boolean validateNotSameDataSources() {
        boolean validate = true;
        DbMode dbMode = localSourceModel.getDbMode();
        if (dbMode != null && dbMode == centralSourceModel.getDbMode()) {
            switch (dbMode) {
                case USE_REMOTE:
                    validate = !Objects.equals(localSourceModel.getPgConfig().getJdbcUrl(), centralSourceModel.getPgConfig().getJdbcUrl());
                    break;
                case USE_SERVER:
                    validate = !Objects.equals(localSourceModel.getRestConfig().getServerUrl(), centralSourceModel.getRestConfig().getServerUrl())
                            || !Objects.equals(localSourceModel.getRestConfig().getOptionalDatabaseName(), centralSourceModel.getRestConfig().getOptionalDatabaseName());

                    break;
                default:
                    validate = false;
            }
        }
        return validate;
    }


    public void start(AdminUI ui) {

        // on positionne la source courante de l'application comme service
        // entrant (on se sert de la configuration de ce service s'il existe
        // pour la source locale).
        ObserveSwingDataSource previousSource = ObserveSwingApplicationContext.get().getDataSourcesManager().getMainDataSource();
        setPreviousSource(previousSource);

        // avant le demarrage du wizard, on ferme toujours la source
        // en cours d'utilisation

        if (previousSource != null && previousSource.isOpen()) {

            if (log.isDebugEnabled()) {
                log.debug("Close previous source " + previousSource.getLabel());
            }
            doCloseSource(previousSource);
        }

        startIncomingSourceModel(ui);

        startCentralSourceModel(ui);

    }

    @Override
    public void destroy() {
        super.destroy();
        localSourceModel.destroy();
        centralSourceModel.destroy();
    }

    public ObserveSwingDataSource getPreviousSource() {
        return previousSource;
    }

    public void setPreviousSource(ObserveSwingDataSource previousSource) {
        this.previousSource = previousSource;
    }

    public ObserveSwingDataSource getLocalSource() {
        return localSource;
    }

    public ObserveDataSourceInformation getLocalSourceInformation() {
        if (localSourceInformation == null) {

            ObserveSwingDataSource localSource = getSafeLocalSource(false);

            try {

                localSourceInformation = localSource.checkCanConnect();

            } catch (Exception e) {
                //FIXME ! il faut faire quelque chose dans ce cas précis, au moins avertir l'utilisateur
                if (log.isErrorEnabled()) {
                    log.error("unable to find local source information", e);
                }
            }

        }

        return localSourceInformation;
    }

    public ObserveSwingDataSource getCentralSource() {
        return centralSource;
    }

    public ObserveSwingDataSource getSafeLocalSource(boolean open) {
        if (localSource == null || open && !localSource.isOpen()) {

            localSource = ObserveSwingApplicationContext.get().getDataSourcesManager().newDataSourceFromModel(getLocalSourceModel());
        }
        if (open) {

            doOpenSource(localSource);
        }
        return localSource;
    }

    public ObserveSwingDataSource getSafeCentralSource(boolean open) {
        if (centralSource == null || open && !centralSource.isOpen()) {

            centralSource = ObserveSwingApplicationContext.get().getDataSourcesManager().newDataSourceFromModel(getCentralSourceModel());
        }
        if (open) {
            doOpenSource(centralSource);
        }
        return centralSource;
    }

    public StorageUIModel getLocalSourceModel() {
        return localSourceModel;
    }

    public StorageUIModel getCentralSourceModel() {
        return centralSourceModel;
    }


    public void removeLocalSource() {
        if (localSource != null) {
            doCloseSource(localSource);
            localSource = null;
            localSourceInformation = null;
        }
    }

    public void removeCentralSource() {
        if (centralSource != null) {
            doCloseSource(centralSource);
            centralSource = null;
        }
    }

}
