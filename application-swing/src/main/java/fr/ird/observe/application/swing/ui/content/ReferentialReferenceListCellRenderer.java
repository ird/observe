package fr.ird.observe.application.swing.ui.content;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.services.dto.reference.ReferentialReference;
import fr.ird.observe.services.dto.referential.ReferentialDto;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import java.awt.Color;
import java.awt.Component;

import static org.nuiton.i18n.I18n.t;

/**
 * Un renderer de liste d'entites d'un referentiel dans le quel on veut
 * differencier les entites qui sont desactivees.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.2
 */
public class ReferentialReferenceListCellRenderer<D extends ReferentialDto> implements ListCellRenderer<ReferentialReference<D>> {

    /** la couleur normal pour les entites non desactivees */
    protected Color normalColor;

    /** la couleur a utiliser pour les entites desactivees */
    protected Color disableColor = Color.LIGHT_GRAY;

    protected ListCellRenderer<? super ReferentialReference<D>> delegate;

    public ReferentialReferenceListCellRenderer(ListCellRenderer<? super ReferentialReference<D>> delegate) {
        this.delegate = delegate;
    }

    @Override
    public Component getListCellRendererComponent(JList<? extends ReferentialReference<D>> list, ReferentialReference<D> value, int index, boolean isSelected, boolean cellHasFocus) {
        JComponent comp;
        comp = (JComponent) delegate.getListCellRendererComponent(
                list,
                value,
                index,
                isSelected,
                cellHasFocus);
        if (normalColor == null) {
            // premiere fois, on intialise la couleur dite normale
            normalColor = comp.getForeground();
        }

        String tip = null;

        // par defaut, on utilise la couleur normale
        Color col = normalColor;
        if (value != null) {

            boolean enabled = value.isEnabled();

            if (!enabled) {
                // l'entite est desactivee
                // on la grise pour bien la differencier
                col = disableColor;
                tip = t("observe.common.obsolete.entity", ((JLabel) comp).getText());
            }
        }
        comp.setForeground(col);
        comp.setToolTipText(tip);
        return comp;
    }

}
