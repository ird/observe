package fr.ird.observe.application.swing.ui.admin.synchronize.data;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.tree.selection.SelectionTree;
import fr.ird.observe.application.swing.ui.tree.selection.SelectionTreeCellRenderer;
import fr.ird.observe.application.swing.ui.tree.selection.SelectionTreeModel;
import fr.ird.observe.application.swing.ui.tree.selection.nodes.ProgramSelectionTreeNode;
import fr.ird.observe.application.swing.ui.tree.selection.nodes.ReferenceSelectionTreeNodeSupport;
import fr.ird.observe.application.swing.ui.tree.selection.nodes.RootSelectionTreeNode;
import fr.ird.observe.application.swing.ui.tree.selection.nodes.SelectionTreeNodeSupport;

import javax.swing.Icon;

/**
 * Created by tchemit on 04/10/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class DataSynchroSelectionTreeCellRenderer extends SelectionTreeCellRenderer {

    private final Icon newTripIcon = UIHelper.createActionIcon("synchroAdd");
    private final Icon existingTripIcon = UIHelper.createActionIcon("synchroUpdate");

    private final SelectionTree oppositeTree;
    private ImmutableSet<String> oppositeIds;

    public DataSynchroSelectionTreeCellRenderer(SelectionTree oppositeTree) {
        this.oppositeTree = oppositeTree;
    }

    @Override
    public Icon getIcon(SelectionTreeNodeSupport node) {
        if (getOppositeIds().contains(node.getId())) {
            return existingTripIcon;
        } else {
            return newTripIcon;
        }
    }

    private ImmutableSet<String> getOppositeIds() {
        if (oppositeIds == null) {
            ImmutableSet.Builder<String> builder = ImmutableSet.builder();
            SelectionTreeModel model = oppositeTree.getTreeModel();
            RootSelectionTreeNode rootNode = model.getRoot();
            for (ProgramSelectionTreeNode programNode : rootNode) {
                for (ReferenceSelectionTreeNodeSupport<?, ?> tripNode : programNode) {
                    builder.add(tripNode.getData().getId());
                }
            }
            oppositeIds = builder.build();
        }
        return oppositeIds;
    }
}
