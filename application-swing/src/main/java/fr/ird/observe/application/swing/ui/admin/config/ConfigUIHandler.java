/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.admin.config;

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.ObserveTextGenerator;
import fr.ird.observe.application.swing.ui.ObserveKeyStrokes;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.admin.AdminStep;
import fr.ird.observe.application.swing.ui.admin.AdminTabUIHandler;
import fr.ird.observe.application.swing.ui.admin.AdminUIModel;
import fr.ird.observe.application.swing.ui.storage.StorageStep;
import fr.ird.observe.application.swing.ui.storage.StorageUILauncher;
import fr.ird.observe.application.swing.ui.storage.StorageUIModel;
import fr.ird.observe.services.configuration.ObserveDataSourceInformation;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.AbstractAction;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Le controleur des onglets.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public class ConfigUIHandler extends AdminTabUIHandler<ConfigUI> implements UIHandler<ConfigUI> {

    /** Logger */
    private static final Log log = LogFactory.getLog(ConfigUIHandler.class);

    @Override
    public void afterInit(ConfigUI ui) {

        ui.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(ObserveKeyStrokes.KEY_STROKE_CONFIGURE_LOCAL_SOURCE, "obtainLocalSource");
        ui.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(ObserveKeyStrokes.KEY_STROKE_CONFIGURE_REMOTE_SOURCE, "obtainRemoteSource");
        ui.getActionMap().put("obtainLocalSource", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ui.doActionPerformed__on__configureLocalSource(e);
            }
        });
        ui.getActionMap().put("obtainRemoteSource", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ui.doActionPerformed__on__configureCentralSource(e);
            }
        });

        ObserveKeyStrokes.addKeyStroke(ui.getConfigureLocalSource(), ObserveKeyStrokes.KEY_STROKE_CONFIGURE_LOCAL_SOURCE);
        ObserveKeyStrokes.addKeyStroke(ui.getConfigureCentralSource(), ObserveKeyStrokes.KEY_STROKE_CONFIGURE_REMOTE_SOURCE);
        super.afterInit(ui);
        UIHelper.setLayerUI(ui.getOperations(), parentUI.getOperationBlockLayerUI());
        UIHelper.setLayerUI(ui.getConfig(), parentUI.getConfigBlockLayerUI());
        UIHelper.setLayerUI(ui.getContent(), null);

    }

    public void updateOperationState(JCheckBox checkBox) {
        AdminStep op = getOperation(checkBox);
        if (log.isDebugEnabled()) {
            log.debug("Operation " + op + " selection has changed : selected:" + checkBox.isSelected());
        }
        if (checkBox.isSelected()) {
            // ajout de l'operation
            ui.getModel().addOperation(op);
        } else {
            // supprime cette opération
            ui.getModel().removeOperation(op);
        }
    }

    public String updateStorageLabel(StorageUIModel service, boolean serviceValid, JLabel label) {
        String text;
        if (serviceValid) {

            // on recupere le label du service
            text = service.getLabel();
        } else {

            // aucun service configuré
            text = t((String) label.getClientProperty("no"));
        }
        return text;
    }

    public AdminStep getOperation(JCheckBox checkBox) {
        return AdminStep.valueOf(checkBox.getName());
    }

    public boolean isOperationSelected(Set<AdminStep> operations, JCheckBox checkBox) {
        AdminStep scope = getOperation(checkBox);
        return operations.contains(scope);
    }

    public void obtainIncomingConnexion() {
        StorageUIModel sourceModel = ui.getLocalSourceModel();
        StorageUILauncher.obtainConnexion(ui, ui.getParentContainer(Window.class), sourceModel);
        if (log.isDebugEnabled()) {
            log.debug("After modifiy source model isValid : " + sourceModel.isValid() + " / " + sourceModel.isValidStep());
        }
        AdminUIModel model = ui.getModel();
        model.getConfigModel().removeLocalSource();
        model.getConfigModel().getLocalSourceModel().validate();
        model.validate();
    }

    public void obtainRemoteConnexion() {
        if (log.isInfoEnabled()) {
            log.info("start obtain remote connexion");
        }
        StorageUIModel sourceModel = ui.getCentralSourceModel();
        StorageUILauncher.obtainConnexion(ui, ui.getParentContainer(Window.class), sourceModel);
        AdminUIModel model = ui.getModel();
        model.getConfigModel().removeCentralSource();
        model.getConfigModel().getCentralSourceModel().validate(StorageStep.CONFIG);
        model.validate();
    }

    protected String updateDataSourcePolicy(StorageUIModel sourceModel, boolean valid) {
        String text = null;
        if (valid) {
            ObserveDataSourceInformation dataSourceInformation = sourceModel.getDataSourceInformation();

            if (dataSourceInformation != null && dataSourceInformation.getVersion() != null) {

                ObserveSwingApplicationContext applicationContext = ObserveSwingApplicationContext.get();
                ObserveTextGenerator textGenerator = applicationContext.getTextGenerator();
                text = textGenerator.getDataSourcePolicy(dataSourceInformation);
            }

        } else {

            text = t("observe.common.storage.not.valid");

        }

        return text;
    }

}
