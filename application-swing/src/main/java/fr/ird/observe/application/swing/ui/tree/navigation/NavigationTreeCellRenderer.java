package fr.ird.observe.application.swing.ui.tree.navigation;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ui.tree.navigation.nodes.NavigationTreeNodeSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.renderer.StringValue;

import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;
import java.awt.Color;
import java.awt.Component;

/**
 * Created on 14/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class NavigationTreeCellRenderer extends DefaultTreeCellRenderer implements StringValue {

    /** Logger */
    private static final Log log = LogFactory.getLog(NavigationTreeCellRenderer.class);

//    @Override
//    public Color getBackgroundSelectionColor() {
//        return null;
//    }
//
//    @Override
//    public Color getBackgroundNonSelectionColor() {
//        // Fixes  http://forge.codelutin.com/issues/830 for jdk 7
//        return Color.WHITE;
//    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {

        if (!(tree.getModel() instanceof NavigationTreeModel)) {
            Component rendererComponent;
            rendererComponent = super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
            return rendererComponent;
        }

        // get the icon to set for the node
        NavigationTreeNodeSupport node = (NavigationTreeNodeSupport) value;

        Icon icon = node.getIcon("");

        if (!sel) {

            Color color = node.getColor();
            if (log.isTraceEnabled()) {
                log.trace("===" + color + " for node " + node.getDataType() + " - " + node.getId());
            }
            setTextNonSelectionColor(color);
        }

        String text = node.getText();
        if (log.isTraceEnabled()) {
            log.trace("===" + text + " for node " + node.getDataType() + " - " + node.getId());
        }
        JLabel comp = (JLabel) super.getTreeCellRendererComponent(tree, text, sel, expanded, leaf, row, hasFocus);
        comp.setToolTipText(text);
        comp.setIcon(icon);
        return comp;
    }

    @Override
    public String getString(Object value) {
        NavigationTreeNodeSupport node = (NavigationTreeNodeSupport) value;
        return node.getText();
    }
}
