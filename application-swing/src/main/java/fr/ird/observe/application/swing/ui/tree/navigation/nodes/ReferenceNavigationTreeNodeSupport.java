package fr.ird.observe.application.swing.ui.tree.navigation.nodes;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.services.dto.IdDto;
import fr.ird.observe.services.dto.reference.AbstractReference;
import org.nuiton.decorator.Decorator;

import java.awt.Color;

/**
 * Created on 14/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public abstract class ReferenceNavigationTreeNodeSupport<D extends IdDto, O extends AbstractReference<D>> extends NavigationTreeNodeSupport<O> {

    @Override
    public String getId() {
        return getData().getId();
    }

    protected ReferenceNavigationTreeNodeSupport(O data, boolean allowChildren) {
        super(data, allowChildren && data.isPersisted());
    }

    @Override
    public Color getColor() {
        return isPersisted() ? super.getColor() : Color.RED;
    }

    @Override
    public Class<D> getDataType() {
        return getData().getType();
    }

    public boolean isOpen() {
        return !isPersisted() || ObserveSwingApplicationContext.get().getOpenDataManager().isOpen(getId());
    }

    @Override
    public String getText() {
        Decorator<D> decorator = ObserveSwingApplicationContext.get().getDecoratorService().getReferenceDecorator(getData().getType());
        return decorator.toString(getData());
    }

    @Override
    public String getIconPath() {
        return "navigation." + getData().getType().getName();
    }

}
