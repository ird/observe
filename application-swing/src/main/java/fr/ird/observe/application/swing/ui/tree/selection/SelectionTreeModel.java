package fr.ird.observe.application.swing.ui.tree.selection;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.ui.admin.export.TripEntry;
import fr.ird.observe.application.swing.ui.tree.selection.nodes.ProgramSelectionTreeNode;
import fr.ird.observe.application.swing.ui.tree.selection.nodes.ReferenceSelectionTreeNodeSupport;
import fr.ird.observe.application.swing.ui.tree.selection.nodes.ReferentialsSelectionTreeNode;
import fr.ird.observe.application.swing.ui.tree.selection.nodes.RootSelectionTreeNode;
import fr.ird.observe.application.swing.ui.tree.selection.nodes.SelectionTreeNodeSupport;
import fr.ird.observe.application.swing.ui.tree.selection.nodes.TripSelectionTreeNodeSupport;
import fr.ird.observe.services.dto.longline.TripLonglineDto;
import fr.ird.observe.services.dto.reference.DataReference;
import fr.ird.observe.services.dto.reference.ReferentialReference;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.dto.referential.ProgramHelper;
import fr.ird.observe.services.dto.referential.ReferentialDto;
import fr.ird.observe.services.dto.referential.ReferentialHelper;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import fr.ird.observe.services.service.data.NavigationRequest;
import fr.ird.observe.services.service.data.NavigationResult;
import fr.ird.observe.services.service.data.NavigationService;

import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import static org.nuiton.i18n.I18n.n;

/**
 * Created on 14/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class SelectionTreeModel extends DefaultTreeModel {

    public static final String SELECTED_COUNT = "selectedCount";
    public static final String SELECTION_EMPTY = "selectionEmpty";
    private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
    private boolean loadSeine = true;
    private boolean loadLongline = true;
    private boolean loadReferential = true;
    private boolean showEmptyTrips = true;
    private int selectedCount;
    private boolean useOpenData;

    public SelectionTreeModel() {
        super(new RootSelectionTreeNode());
    }

    public void setValueAt(SelectionTreeNodeSupport node, boolean value) {
        node.setSelected(value);
        if (node.isLeaf()) {
            nodeChanged(node.getParent());
            nodeChanged(node);
        } else {
            nodeChanged(node);
            Enumeration<? extends TreeNode> children = node.children();
            while (children.hasMoreElements()) {
                nodeChanged(children.nextElement());
            }
        }
        recomputeSelectedCount();
    }

    private void recomputeSelectedCount() {
        int result = getSelectedData().size();
        setSelectedCount(result);
    }

    public int getSelectedCount() {
        return selectedCount;
    }

    public void setSelectedCount(int selectedCount) {
        int oldValue = getSelectedCount();
        boolean oldSelectionempty = isSelectionEmpty();
        this.selectedCount = selectedCount;
        pcs.firePropertyChange(SELECTED_COUNT, oldValue, selectedCount);
        pcs.firePropertyChange(SELECTION_EMPTY, oldSelectionempty, isSelectionEmpty());
    }

    public boolean isSelectionEmpty() {
        return selectedCount == 0;
    }

    @Override
    public RootSelectionTreeNode getRoot() {
        return (RootSelectionTreeNode) super.getRoot();
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }

    public void addPropertyChangeListener(String name, PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(name, listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(String name, PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(name, listener);
    }

    public boolean isLoadSeine() {
        return loadSeine;
    }

    public void setLoadSeine(boolean loadSeine) {
        this.loadSeine = loadSeine;
    }

    public boolean isLoadLongline() {
        return loadLongline;
    }

    public void setLoadLongline(boolean loadLongline) {
        this.loadLongline = loadLongline;
    }

    public boolean isLoadReferential() {
        return loadReferential;
    }

    public void setLoadReferential(boolean loadReferential) {
        this.loadReferential = loadReferential;
    }

    public boolean isShowEmptyTrips() {
        return showEmptyTrips;
    }

    public void setShowEmptyTrips(boolean showEmptyTrips) {
        this.showEmptyTrips = showEmptyTrips;
    }

    public Set<DataReference<?>> getSelectedData() {
        ImmutableSet.Builder<DataReference<?>> builder = ImmutableSet.builder();
        for (ProgramSelectionTreeNode programNode : getRoot()) {
            builder.addAll(programNode.getSelected());
        }
        return builder.build();
    }

    public boolean isUseData() {
        return isLoadLongline() || isLoadSeine();
    }

    public void populate(ObserveSwingDataSource dataSource) {

        NavigationService navigationService = dataSource.newNavigationService();

        NavigationRequest navigationRequest = toNavigationRequest();
        NavigationResult navigationResult = navigationService.getNavigation(navigationRequest);

        List<ReferentialReference<ProgramDto>> programs = new ArrayList<>(navigationResult.getPrograms());

        programs = ProgramHelper.sort(programs);

        RootSelectionTreeNode root = new RootSelectionTreeNode();

        Map<ReferentialReference<ProgramDto>, Collection<DataReference<TripLonglineDto>>> tripsLonglineByProgram = navigationResult.getTripsLonglineByProgram().asMap();
        Map<ReferentialReference<ProgramDto>, Collection<DataReference<TripSeineDto>>> tripsSeineByProgram = navigationResult.getTripsSeineByProgram().asMap();
        for (ReferentialReference<ProgramDto> program : programs) {

            if (ProgramHelper.isProgramLongline(program)) {

                Collection<DataReference<TripLonglineDto>> references = tripsLonglineByProgram.getOrDefault(program, Collections.emptyList());
                ProgramSelectionTreeNode programNode = ProgramSelectionTreeNode.ofLongline(program, references);
                root.add(programNode);

            } else if (ProgramHelper.isProgramSeine(program)) {

                Collection<DataReference<TripSeineDto>> references = tripsSeineByProgram.getOrDefault(program, Collections.emptyList());
                ProgramSelectionTreeNode programNode = ProgramSelectionTreeNode.ofSeine(program, references);
                root.add(programNode);

            }

        }

        if (isLoadReferential()) {
            root.add(ReferentialsSelectionTreeNode.of(n("observe.type.reference.common"), ReferentialHelper.REFERENCE_COMMON_DTOS));
            root.add(ReferentialsSelectionTreeNode.of(n("observe.type.reference.longline"), ReferentialHelper.REFERENCE_LONGLINE_DTOS));
            root.add(ReferentialsSelectionTreeNode.of(n("observe.type.reference.seine"), ReferentialHelper.REFERENCE_SEINE_DTOS));
        }

        setRoot(root);

    }

    public ImmutableSet<Class<? extends ReferentialDto>> getSelectedReferentiel() {
        ImmutableSet.Builder<Class<? extends ReferentialDto>> builder = ImmutableSet.builder();
        for (ReferentialsSelectionTreeNode programNode : getRoot().getReferentialsNodes()) {
            ImmutableList<Class<? extends ReferentialDto>> selected = programNode.getSelected();
            if (!selected.isEmpty()) {
                builder.addAll(selected);
            }
        }
        return builder.build();
    }

    public Map<ReferentialReference<ProgramDto>, List<DataReference<?>>> getSelectedDataByProgram() {
        ImmutableMap.Builder<ReferentialReference<ProgramDto>, List<DataReference<?>>> result = ImmutableMap.builder();
        for (ProgramSelectionTreeNode programNode : getRoot()) {
            ImmutableList<DataReference<?>> trips = programNode.getSelected();
            if (!trips.isEmpty()) {
                result.put(programNode.getData(), trips);
            }
        }
        return result.build();
    }

    public void removeProgram(String programId) {
        RootSelectionTreeNode rootNode = getRoot();
        SelectionTreeNodeSupport result = rootNode.findById(programId);
        Objects.requireNonNull(result, "Could not find program node with id: " + programId);
        removeNodeFromParent(result);
    }

    public void removeTrip(ReferentialReference<ProgramDto> program, DataReference trip) {

        RootSelectionTreeNode rootNode = getRoot();
        SelectionTreeNodeSupport programNode = rootNode.findById(program.getId());
        Objects.requireNonNull(programNode, "Could not find program node with id: " + program);
        SelectionTreeNodeSupport tripNode = programNode.findById(trip.getId());
        Objects.requireNonNull(tripNode, "Could not find program node with id: " + trip);
        removeNodeFromParent(tripNode);

        if (programNode.isLeaf()) {
            removeProgram(program.getId());
        }
    }

    public boolean containsData(ReferentialReference<ProgramDto> program, DataReference trip) {
        ReferenceSelectionTreeNodeSupport programNode = getRoot().findById(program.getId());
        return programNode != null && programNode.findById(trip.getId()) != null;
    }

    public void removeAllSelectedData() {
        for (ProgramSelectionTreeNode programNode : getRoot()) {

            if (programNode.isSelected()) {
                removeProgram(programNode.getId());
            } else {
                for (TripSelectionTreeNodeSupport<?> tripNode : programNode) {
                    if (tripNode.isSelected()) {
                        removeTrip(programNode.getData(), tripNode.getData());
                    }
                }
            }

        }
        recomputeSelectedCount();
    }

    public void selectAllTrips() {
        for (ProgramSelectionTreeNode programNode : getRoot()) {
            programNode.setSelected(true);
        }
        recomputeSelectedCount();
    }

    public void unselectAllTrips() {
        for (ProgramSelectionTreeNode programNode : getRoot()) {
            programNode.setSelected(false);
        }
        recomputeSelectedCount();
    }

    public void setExistingTrips(List<DataReference<?>> existingTrips) {
        for (ProgramSelectionTreeNode programNode : getRoot()) {
            for (TripSelectionTreeNodeSupport<?> tripNode : programNode) {
                DataReference<?> data = tripNode.getData();
                tripNode.setExist(existingTrips.contains(data));
            }
        }
    }

    public boolean isUseOpenData() {
        return useOpenData;
    }

    public void setUseOpenData(boolean useOpenData) {
        this.useOpenData = useOpenData;
    }

    public boolean isNotEmpty() {
        return !isEmpty();
    }

    public ImmutableList<TripEntry> getSelectedTripEntries() {

        ImmutableList.Builder<TripEntry> tripEntries = ImmutableList.builder();
        for (ProgramSelectionTreeNode programNode : getRoot()) {

            ReferentialReference<ProgramDto> program = programNode.getData();

            for (TripSelectionTreeNodeSupport<?> tripNode : programNode) {
                if (tripNode.isSelected()) {
                    DataReference<?> trip = tripNode.getData();
                    boolean exists = tripNode.isExist();
                    TripEntry tripEntry = new TripEntry(program, trip, exists);
                    tripEntries.add(tripEntry);
                }
            }

        }
        return tripEntries.build();
    }

    public boolean isEmpty() {
        return getRoot().isLeaf();
    }

    public boolean isDataFull() {
        for (ProgramSelectionTreeNode programNode : getRoot()) {
            if (!programNode.isSelected()) {
                return false;
            }
        }
        return !getRoot().isLeaf();
    }

    // Ne pas supprimer utiliser dans les templates
    @SuppressWarnings("unused")
    public ImmutableList<ReferentialReference<ProgramDto>> getSelectedProgram() {
        return ImmutableList.copyOf(getSelectedDataByProgram().keySet());
    }

    // Ne pas supprimer utiliser dans les templates
    @SuppressWarnings("unused")
    public ImmutableList<DataReference<?>> getSelectedTripsByProgram(ReferentialReference<ProgramDto> program) {
        ProgramSelectionTreeNode programNode = (ProgramSelectionTreeNode) getRoot().findById(program.getId());
        return programNode.getSelected();
    }

    private NavigationRequest toNavigationRequest() {
        NavigationRequest navigationRequest = new NavigationRequest();
        navigationRequest.setLoadLongline(isLoadLongline());
        navigationRequest.setLoadSeine(isLoadSeine());
        navigationRequest.setLoadEmptyProgram(isShowEmptyTrips());
        return navigationRequest;
    }

}
