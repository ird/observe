package fr.ird.observe.application.swing.ui;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveRunner;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.configuration.ObserveSwingApplicationConfig;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.beans.AbstractBean;
import org.nuiton.jaxx.runtime.JAXXObject;

import javax.swing.Icon;
import javax.swing.JComponent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Locale;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 05/12/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class ObserveMainUIModel extends AbstractBean {

    /** Logger */
    private static final Log log = LogFactory.getLog(ObserveMainUIModel.class);
    private final MouseAdapter focusOnBobyMouseListener = new MouseAdapter() {
        @Override
        public void mouseEntered(MouseEvent e) {
            if (log.isDebugEnabled()) {
                log.debug("Enter in formula zone: " + e);
            }
            setFocusOnNavigation(false);
        }
    };

    private static final Icon DB_LOCAL_ICON = UIHelper.getUIManagerActionIcon("db-local");
    private static final Icon DB_REMOTE_ICON = UIHelper.getUIManagerActionIcon("db-remote");
    private static final Icon DB_SERVER_ICON = UIHelper.getUIManagerActionIcon("db-server");
    private static final Icon DB_NONE_ICON = UIHelper.getUIManagerActionIcon("db-none");
    public static final String PROPERTY_FOCUS_ON_NAVIGATION = "focusOnNavigation";
    public static final String PROPERTY_MODE = "mode";

    private ObserveUIMode mode = ObserveUIMode.NO_DB;
    private boolean h2WebServer;
    private boolean busy;
    private boolean focusOnNavigation;

    private boolean storageStatusEnabled;
    private String storageStatusText;
    private String storageStatusTip;
    private Icon storageStatusIcon;
    private boolean storageCloseEnabled;
    private boolean storageReloadEnabled;
    private boolean storageImportEnabled;
    private boolean storageSaveEnabled;
    private boolean startServerVisible;
    private boolean navigationEnabled;
    private boolean actionsEnabled;
    private boolean storageEnabled;
    private boolean configEnabled;
    private boolean fileEnabled;
    private boolean changeApplicationLanguageToFrenchEnabled;
    private boolean changeApplicationLanguageToEnglishEnabled;
    private boolean changeApplicationLanguageToSpanishEnabled;
    private boolean changeDbLanguageToFrenchEnabled;
    private boolean changeDbLanguageToEnglishEnabled;
    private boolean changeDbLanguageToSpanishEnabled;
    private boolean devMode;
    private boolean fullScreen;
    private boolean blockFocus;

    private boolean adjusting;

    public void init(ObserveSwingApplicationConfig config) {
        config.addPropertyChangeListener(evt -> {
            if (adjusting) {
                return;
            }
            if (log.isInfoEnabled()) {
                log.info("Config property " + evt.getPropertyName() + " changed, reload main ui model.");
            }
            reload(config);
        });

        addPropertyChangeListener(evt -> {
            if (adjusting || PROPERTY_FOCUS_ON_NAVIGATION.equals(evt.getPropertyName())) {
                return;
            }
            if (log.isInfoEnabled()) {
                log.info("Main ui model property " + evt.getPropertyName() + " changed, reload main ui model.");
            }
            reload(config);
        });

        addPropertyChangeListener(PROPERTY_MODE, evt -> ObserveRunner.cleanMemory());
    }

    public void reload(ObserveSwingApplicationConfig config) {

        if (log.isDebugEnabled()) {
            log.debug("Reload ui model");
        }
        adjusting = true;

        try {

            boolean notBusy = !isBusy();
            boolean mainStorageOpened = config.isMainStorageOpened();

            setStorageStatusEnabled(acceptMode(getMode(), notBusy, ObserveUIMode.DB, ObserveUIMode.NO_DB));
            setStorageStatusText(updateStorageSatutText(config));
            setStorageStatusTip(updateStorageStatutToolTipText(config));
            setStorageStatusIcon(updateStorageSatutIcon(config));
            setStorageCloseEnabled(acceptMode(getMode(), mainStorageOpened, ObserveUIMode.DB));
            setStorageReloadEnabled(acceptMode(getMode(), mainStorageOpened, ObserveUIMode.DB));
            setNavigationEnabled(acceptMode(getMode(), notBusy, ObserveUIMode.DB));
            setActionsEnabled(acceptMode(getMode(), notBusy, ObserveUIMode.DB, ObserveUIMode.NO_DB));
            setFileEnabled(acceptMode(getMode(), notBusy, ObserveUIMode.NO_DB, ObserveUIMode.DB));
            setConfigEnabled(acceptMode(getMode(), notBusy, ObserveUIMode.DB, ObserveUIMode.NO_DB));
            setStorageEnabled(acceptMode(getMode(), notBusy, ObserveUIMode.DB, ObserveUIMode.NO_DB));
            setStartServerVisible(acceptMode(getMode(), config.isLocalStorageExist() && notBusy, ObserveUIMode.NO_DB));
            setStorageImportEnabled(acceptMode(getMode(), ObserveUIMode.DB, ObserveUIMode.NO_DB));
            setStorageSaveEnabled(acceptMode(getMode(), mainStorageOpened, ObserveUIMode.DB));
            Locale applicationLocale = config.getLocale();
            setChangeApplicationLanguageToEnglishEnabled(!acceptLocale(applicationLocale, "en_GB"));
            setChangeApplicationLanguageToFrenchEnabled(!acceptLocale(applicationLocale, "fr_FR"));
            setChangeApplicationLanguageToSpanishEnabled(!acceptLocale(applicationLocale, "es_ES"));
            Locale dbLocale = config.getDbLocale();
            setChangeDbLanguageToEnglishEnabled(!acceptLocale(dbLocale, "en_GB"));
            setChangeDbLanguageToFrenchEnabled(!acceptLocale(dbLocale, "fr_FR"));
            setChangeDbLanguageToSpanishEnabled(!acceptLocale(dbLocale, "es_ES"));
            setFullScreen(config.isFullScreen());
            setDevMode(config.isDevMode());
            setBusy(isBusy());
        } finally {
            adjusting = false;
        }


    }

    public ObserveUIMode getMode() {
        return mode;
    }

    public void setMode(ObserveUIMode mode) {
        this.mode = mode;
        firePropertyChange("mode", null, mode);
    }

    public boolean isH2WebServer() {
        return h2WebServer;
    }

    public void setH2WebServer(boolean h2WebServer) {
        this.h2WebServer = h2WebServer;
        firePropertyChange("h2WebServer", null, h2WebServer);
    }

    public boolean isBusy() {
        return busy;
    }

    public void setBusy(boolean busy) {
        this.busy = busy;
        firePropertyChange("busy", null, busy);
    }

    public boolean canAcquireFocus() {
        return !isFocusOnNavigation() && !isBlockFocus();
    }

    public boolean isFocusOnNavigation() {
        return focusOnNavigation;
    }

    public void setFocusOnNavigation(boolean focusOnNavigation) {
        this.focusOnNavigation = focusOnNavigation;
        firePropertyChange(PROPERTY_FOCUS_ON_NAVIGATION, null, focusOnNavigation);
    }

    public boolean isStorageStatusEnabled() {
        return storageStatusEnabled;
    }

    public void setStorageStatusEnabled(boolean storageStatusEnabled) {
        this.storageStatusEnabled = storageStatusEnabled;
        firePropertyChange("storageStatusEnabled", null, storageStatusEnabled);
    }

    public String getStorageStatusText() {
        return storageStatusText;
    }

    public void setStorageStatusText(String storageStatusText) {
        this.storageStatusText = storageStatusText;
        firePropertyChange("storageStatusText", null, storageStatusText);
    }

    public String getStorageStatusTip() {
        return storageStatusTip;
    }

    public void setStorageStatusTip(String storageStatusTip) {
        this.storageStatusTip = storageStatusTip;
        firePropertyChange("storageStatusTip", null, storageStatusTip);
    }

    public Icon getStorageStatusIcon() {
        return storageStatusIcon;
    }

    public void setStorageStatusIcon(Icon storageStatusIcon) {
        this.storageStatusIcon = storageStatusIcon;
        firePropertyChange("storageStatusIcon", null, storageStatusIcon);
    }

    public boolean isStorageCloseEnabled() {
        return storageCloseEnabled;
    }

    public void setStorageCloseEnabled(boolean storageCloseEnabled) {
        this.storageCloseEnabled = storageCloseEnabled;
        firePropertyChange("storageCloseEnabled", null, storageCloseEnabled);
    }

    public boolean isStorageReloadEnabled() {
        return storageReloadEnabled;
    }

    public void setStorageReloadEnabled(boolean storageReloadEnabled) {
        this.storageReloadEnabled = storageReloadEnabled;
        firePropertyChange("storageReloadEnabled", null, storageReloadEnabled);
    }

    public boolean isNavigationEnabled() {
        return navigationEnabled;
    }

    public void setNavigationEnabled(boolean navigationEnabled) {
        this.navigationEnabled = navigationEnabled;
        firePropertyChange("navigationEnabled", null, navigationEnabled);
    }

    public boolean isActionsEnabled() {
        return actionsEnabled;
    }

    public void setActionsEnabled(boolean actionsEnabled) {
        this.actionsEnabled = actionsEnabled;
        firePropertyChange("actionsEnabled", null, actionsEnabled);
    }

    public boolean isStorageImportEnabled() {
        return storageImportEnabled;
    }

    public void setStorageImportEnabled(boolean storageImportEnabled) {
        this.storageImportEnabled = storageImportEnabled;
        firePropertyChange("storageImportEnabled", null, storageImportEnabled);
    }

    public boolean isStorageSaveEnabled() {
        return storageSaveEnabled;
    }

    public void setStorageSaveEnabled(boolean storageSaveEnabled) {
        this.storageSaveEnabled = storageSaveEnabled;
        firePropertyChange("storageSaveEnabled", null, storageSaveEnabled);
    }

    public boolean isStartServerVisible() {
        return startServerVisible;
    }

    public void setStartServerVisible(boolean startServerVisible) {
        this.startServerVisible = startServerVisible;
        firePropertyChange("startServerVisible", null, startServerVisible);
    }

    public boolean isStorageEnabled() {
        return storageEnabled;
    }

    public void setStorageEnabled(boolean storageEnabled) {
        this.storageEnabled = storageEnabled;
        firePropertyChange("storageEnabled", null, storageEnabled);
    }

    public boolean isConfigEnabled() {
        return configEnabled;
    }

    public void setConfigEnabled(boolean configEnabled) {
        this.configEnabled = configEnabled;
        firePropertyChange("configEnabled", null, configEnabled);
    }

    public boolean isFileEnabled() {
        return fileEnabled;
    }

    public void setFileEnabled(boolean fileEnabled) {
        this.fileEnabled = fileEnabled;
        firePropertyChange("fileEnabled", null, fileEnabled);
    }

    public boolean isChangeApplicationLanguageToFrenchEnabled() {
        return changeApplicationLanguageToFrenchEnabled;
    }

    public void setChangeApplicationLanguageToFrenchEnabled(boolean changeApplicationLanguageToFrenchEnabled) {
        this.changeApplicationLanguageToFrenchEnabled = changeApplicationLanguageToFrenchEnabled;
        firePropertyChange("changeApplicationLanguageToFrenchEnabled", null, changeApplicationLanguageToFrenchEnabled);
    }

    public boolean isChangeApplicationLanguageToEnglishEnabled() {
        return changeApplicationLanguageToEnglishEnabled;
    }

    public void setChangeApplicationLanguageToEnglishEnabled(boolean changeApplicationLanguageToEnglishEnabled) {
        this.changeApplicationLanguageToEnglishEnabled = changeApplicationLanguageToEnglishEnabled;
        firePropertyChange("changeApplicationLanguageToEnglishEnabled", null, changeApplicationLanguageToEnglishEnabled);
    }

    public boolean isChangeApplicationLanguageToSpanishEnabled() {
        return changeApplicationLanguageToSpanishEnabled;
    }

    public void setChangeApplicationLanguageToSpanishEnabled(boolean changeApplicationLanguageToSpanishEnabled) {
        this.changeApplicationLanguageToSpanishEnabled = changeApplicationLanguageToSpanishEnabled;
        firePropertyChange("changeApplicationLanguageToSpanishEnabled", null, changeApplicationLanguageToSpanishEnabled);
    }

    public boolean isChangeDbLanguageToFrenchEnabled() {
        return changeDbLanguageToFrenchEnabled;
    }

    public void setChangeDbLanguageToFrenchEnabled(boolean changeDbLanguageToFrenchEnabled) {
        this.changeDbLanguageToFrenchEnabled = changeDbLanguageToFrenchEnabled;
        firePropertyChange("changeDbLanguageToFrenchEnabled", null, changeDbLanguageToFrenchEnabled);
    }

    public boolean isChangeDbLanguageToEnglishEnabled() {
        return changeDbLanguageToEnglishEnabled;
    }

    public void setChangeDbLanguageToEnglishEnabled(boolean changeDbLanguageToEnglishEnabled) {
        this.changeDbLanguageToEnglishEnabled = changeDbLanguageToEnglishEnabled;
        firePropertyChange("changeDbLanguageToEnglishEnabled", null, changeDbLanguageToEnglishEnabled);
    }

    public boolean isChangeDbLanguageToSpanishEnabled() {
        return changeDbLanguageToSpanishEnabled;
    }

    public void setChangeDbLanguageToSpanishEnabled(boolean changeDbLanguageToSpanishEnabled) {
        this.changeDbLanguageToSpanishEnabled = changeDbLanguageToSpanishEnabled;
        firePropertyChange("changeDbLanguageToSpanishEnabled", null, changeDbLanguageToSpanishEnabled);
    }

    public boolean isDevMode() {
        return devMode;
    }

    public void setDevMode(boolean devMode) {
        this.devMode = devMode;
        firePropertyChange("devMode", null, devMode);
    }

    public boolean isFullScreen() {
        return fullScreen;
    }

    public void setFullScreen(boolean fullScreen) {
        this.fullScreen = fullScreen;
        firePropertyChange("fullScreen", null, fullScreen);
    }

    protected Icon updateStorageSatutIcon(ObserveSwingApplicationConfig config) {
        Icon icon;

        if (config.isMainStorageOpened()) {

            ObserveSwingDataSource service = ObserveSwingApplicationContext.get().getDataSourcesManager().getMainDataSource();

            if (service.isLocal()) {
                icon = DB_LOCAL_ICON;
            } else if (service.isRemote()) {
                icon = DB_REMOTE_ICON;
            } else {
                icon = DB_SERVER_ICON;
            }
        } else {
            icon = DB_NONE_ICON;
        }
        return icon;
    }

    protected String updateStorageSatutText(ObserveSwingApplicationConfig config) {
        String text;
        if (config.isMainStorageOpened()) {
            ObserveSwingDataSource source = ObserveSwingApplicationContext.get().getDataSourcesManager().getMainDataSource();
            text = source.getLabel();
        } else {
            text = t("observe.message.db.none.loaded");
        }
        return text;
    }

    protected String updateStorageStatutToolTipText(ObserveSwingApplicationConfig config) {
        String text;
        if (config.isMainStorageOpened()) {
            ObserveSwingDataSource source = ObserveSwingApplicationContext.get().getDataSourcesManager().getMainDataSource();
            text = t("observe.message.loaded.tip", source.getLabel());
        } else {
            text = t("observe.message.db.none.loaded.tip");
        }
        return text;
    }

    public boolean acceptMode(ObserveUIMode mode, ObserveUIMode... modes) {
        return acceptMode(mode, true, modes);
    }

    public boolean acceptMode(ObserveUIMode mode, boolean condition, ObserveUIMode... modes) {
        if (condition) {
            for (ObserveUIMode m : modes) {
                if (m.equals(mode)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean acceptLocale(Locale l, String expected) {
        return l != null && l.toString().equals(expected);
    }


    public boolean isBlockFocus() {
        return blockFocus;
    }

    public void blockFocus() {
        this.blockFocus = true;
    }

    public void unblockFocus() {
        this.blockFocus = false;
    }

    public void attachFocusOnBody(JComponent c) {
        c.addMouseListener(focusOnBobyMouseListener);
    }

    public void attachFocusOnBodyDeep(JAXXObject result) {
        if (log.isDebugEnabled()) {
            log.debug("Adding to " + result);
        }
        for (Object o : result.get$objectMap().values()) {
            if (o instanceof JComponent) {
                attachFocusOnBody((JComponent) o);
            }
            if (o instanceof JAXXObject && o != result) {
                attachFocusOnBodyDeep((JAXXObject) o);
            }
        }

    }
}
