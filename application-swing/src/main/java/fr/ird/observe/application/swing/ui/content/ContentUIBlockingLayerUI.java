package fr.ird.observe.application.swing.ui.content;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.ui.actions.AbstractUIAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.jxlayer.JXLayer;
import org.nuiton.jaxx.runtime.swing.BlockingLayerUI;

import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.Set;

/**
 * Created on 09/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class ContentUIBlockingLayerUI extends BlockingLayerUI {

    /** Logger */
    private static final Log log = LogFactory.getLog(ContentUIBlockingLayerUI.class);

    private static final Set<Integer> GLOBAL_KEY_CODES = ImmutableSet.of(
            KeyEvent.VK_F1,
            KeyEvent.VK_F2,
            KeyEvent.VK_F3,
            KeyEvent.VK_F4,
            KeyEvent.VK_F5,
            KeyEvent.VK_F6,
            KeyEvent.VK_F7,
            KeyEvent.VK_F8,
            KeyEvent.VK_F9,
            KeyEvent.VK_F10,
            KeyEvent.VK_F11,
            KeyEvent.VK_F12
    );

    private final ContentUI ui;

    public ContentUIBlockingLayerUI(ContentUI ui) {
        this.ui = ui;
    }

    @Override
    protected void processMouseEvent(MouseEvent e, JXLayer<? extends JComponent> l) {

        switch (e.getID()) {
            case MouseEvent.MOUSE_ENTERED:
                if (log.isDebugEnabled()) {
                    log.debug("Enter in formula zone: " + e);
                }
                ObserveSwingApplicationContext.get().getMainUI().getModel().setFocusOnNavigation(false);
        }
        super.processMouseEvent(e, l);
    }

    @Override
    protected void processKeyEvent(KeyEvent e, JXLayer<? extends JComponent> l) {

        if (!block) {
            return;
        }

        InputMap inputMap = ui.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        ActionMap actionMap = ui.getActionMap();

        boolean consumed = e.isConsumed();

        if (!consumed && e.isControlDown() && e.getKeyChar() != '\uFFFF') {

            KeyStroke keyStroke = KeyStroke.getKeyStroke("ctrl pressed " + (char) e.getKeyCode());

            if (keyStroke == null) {
                super.processKeyEvent(e, l);
                return;
            }
            consumed = doAction(keyStroke, inputMap, actionMap);
        }

        if (!consumed && e.getID() == KeyEvent.KEY_RELEASED && !e.isAltDown() && !e.isAltGraphDown()
                && !e.isMetaDown() && GLOBAL_KEY_CODES.contains(e.getKeyCode())) {

            if (!e.isShiftDown()) {

                KeyStroke keyStroke = KeyStroke.getKeyStroke(e.getKeyCode(), e.isControlDown() ? KeyEvent.CTRL_DOWN_MASK : 0);
                if (keyStroke == null) {
                    super.processKeyEvent(e, l);
                    return;
                }

                consumed = doAction(keyStroke, inputMap, actionMap);

            } else if (e.isControlDown()) {

                KeyStroke keyStroke = KeyStroke.getKeyStroke(e.getKeyCode(), KeyEvent.CTRL_DOWN_MASK + KeyEvent.SHIFT_DOWN_MASK);
                if (keyStroke == null) {
                    super.processKeyEvent(e, l);
                    return;
                }

                consumed = doAction(keyStroke, inputMap, actionMap);
            }

        }

        if (consumed) {
            e.consume();
        } else {
            super.processKeyEvent(e, l);
        }

    }

    protected boolean doAction(KeyStroke keyStroke, InputMap inputMap, ActionMap actionMap) {

        String actionName = (String) inputMap.get(keyStroke);
        if (actionName != null) {

            Action action = actionMap.get(actionName);

            JComponent editor = (JComponent) action.getValue(AbstractUIAction.EDITOR);
            if (editor == null || (editor.isVisible() && editor.isEnabled())) {

                if (log.isInfoEnabled()) {
                    log.info("Found action: " + action.getValue(Action.NAME) + " for keyStroke: " + keyStroke);
                }
                SwingUtilities.invokeLater(() -> action.actionPerformed(new ActionEvent(ui, 0, (String) action.getValue(Action.NAME))));
                return true;
            } else {
                if (log.isInfoEnabled()) {
                    log.info("Found disabled action: " + action.getValue(Action.NAME) + " for keyStroke: " + keyStroke);
                }
            }
        }
        return false;
    }
}
