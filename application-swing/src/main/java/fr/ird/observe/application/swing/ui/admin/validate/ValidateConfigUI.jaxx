<!--
  #%L
  ObServe :: Application Swing
  %%
  Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->
<!-- ************************************************************* -->
<!-- La configuration de l'import GPS                              -->
<!-- ************************************************************* -->

<JPanel id="validateConfig">

  <import>
    fr.ird.observe.application.swing.I18nEnumHelper
    fr.ird.observe.application.swing.ui.admin.AdminUIModel
    fr.ird.observe.application.swing.validation.ValidationModelMode

    org.nuiton.validator.NuitonValidatorScope

    java.io.File

    javax.swing.JComboBox

    static fr.ird.observe.application.swing.ui.UIHelper.getStringValue
  </import>

  <ValidateUIHandler id='handler'
                     initializer='getContextValue(ValidateUIHandler.class)'/>

  <AdminUIModel id='model' initializer='getContextValue(AdminUIModel.class)'/>

  <ValidateModel id='validateModel' initializer='model.getValidateModel()'/>

  <ButtonGroup id='validateContentModel'
               onStateChanged='validateModel.setModelMode((ValidationModelMode) validateContentModel.getSelectedValue())'/>

  <script><![CDATA[
public void destroy() {
    model = null;
    validateModel = null;
}

@Override
protected void finalize() throws Throwable {
    super.finalize();
    destroy();
}

]]>
  </script>

  <JPanel constraints='BorderLayout.NORTH' layout='{new BorderLayout()}'>
    <Table id="validateDataConfig" constraints='BorderLayout.NORTH'>

      <!-- validation mode  -->
      <row>
        <cell anchor="west">
          <JLabel id='validateModeLabel'/>
        </cell>
        <cell weightx='1' fill="horizontal">
          <Table fill='both' weightx='1'>
            <row>
              <cell>
                <JRadioButton id="DATA"/>
              </cell>
            </row>
            <row>
              <cell>
                <JRadioButton id="REFERENTIEL"/>
              </cell>
            </row>
            <!--row>
                <cell>
                    <JRadioButton id="ALL"/>
                </cell>
            </row-->
          </Table>
        </cell>
      </row>
      <row>
        <cell anchor="west" columns="2">
          <JLabel id='credentialsInfo'/>
        </cell>
      </row>

      <!-- validation scopes -->
      <row>
        <cell anchor="west">
          <JLabel id='validateScopesLabel'/>
        </cell>
        <cell weightx='1' fill="horizontal">
          <Table id='validateScopes' fill='both' weightx='1'>
            <row>
              <cell>
                <JCheckBox id='ERROR'
                           onItemStateChanged='getHandler().updateValidationScopes((JCheckBox)event.getSource())'/>
              </cell>
            </row>
            <row>
              <cell>
                <JCheckBox id='WARNING'
                           onItemStateChanged='getHandler().updateValidationScopes((JCheckBox)event.getSource())'/>
              </cell>
            </row>
            <row>
              <cell>
                <JCheckBox id='INFO'
                           onItemStateChanged='getHandler().updateValidationScopes((JCheckBox)event.getSource())'/>
              </cell>
            </row>
          </Table>
        </cell>
      </row>
      <!-- validation resume -->
      <row>
        <cell anchor="west" columns="2">
          <JLabel id='validateResume'/>
        </cell>
      </row>
      <row>
        <cell anchor="west" columns="2">
          <JLabel actionIcon='information'
                  text='observe.info.selected.validators'/>
        </cell>
      </row>
    </Table>

    <Table id='validationReportConfig' fill='both'
           constraints='BorderLayout.CENTER'>
      <row>
        <cell columns="2">
          <JCheckBox id='generateValidationReport'
                     onItemStateChanged='validateModel.setGenerateReport(((JCheckBox)event.getSource()).isSelected())'/>
        </cell>
      </row>
      <row>
        <cell>
          <JLabel id="validationReportDirectoryLabel"/>
        </cell>
        <cell weightx='1' fill="horizontal">
          <JTextField id='validationReportDirectoryText'
                      onKeyReleased='getHandler().changeValidationReportDirectory(this, new File(((JTextField)event.getSource()).getText()))'/>
        </cell>
        <cell anchor="east">
          <JButton id="chooseValidationReportFileAction"
                   onActionPerformed="getHandler().chooseValidationReportFile(this)"/>
        </cell>
      </row>
      <row>
        <cell>
          <JLabel id="validationReportFileLabel"/>
        </cell>
        <cell weightx='1' fill="horizontal" columns="2">
          <JTextField
                  id='validationReportFilenameText'
                  onKeyReleased='getHandler().changeValidationReportFilename(this, ((JTextField)event.getSource()).getText())'/>
        </cell>
      </row>
    </Table>

  </JPanel>
  <Table constraints='BorderLayout.SOUTH' weightx='1' weighty='1'>
    <row>
      <cell>
        <JLabel/>
      </cell>
    </row>
  </Table>

</JPanel>
