package fr.ird.observe.application.swing.ui.actions.content;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ui.ObserveKeyStrokes;
import fr.ird.observe.application.swing.ui.ObserveMainUI;

/**
 * Created on 31/12/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class GoToTab6UIAction extends GoToTabUIActionSupport {

    public static final String ACTION_NAME = GoToTab6UIAction.class.getName();

    public GoToTab6UIAction(ObserveMainUI mainUI) {
        super(mainUI, ACTION_NAME, ObserveKeyStrokes.KEY_STROKE_GO_TAB_6, 5, MAIN_TABBED_PANE);
    }

}
