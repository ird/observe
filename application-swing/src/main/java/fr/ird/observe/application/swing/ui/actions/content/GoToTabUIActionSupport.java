package fr.ird.observe.application.swing.ui.actions.content;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.content.ContentUI;

import javax.swing.JTabbedPane;
import javax.swing.KeyStroke;

/**
 * Created on 31/12/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public abstract class GoToTabUIActionSupport extends AbstractContentUIAction {

    public static final String MAIN_TABBED_PANE = "mainTabbedPane";
    public static final String SUB_TABBED_PANE = "subTabbedPane";

    private final int tabIndex;
    private final String tabbedPaneName;

    public GoToTabUIActionSupport(ObserveMainUI mainUI, String actionName, KeyStroke keyStroke, int tabIndex, String tabbedPaneName) {
        super(mainUI, actionName, null, null, null, keyStroke);
        this.tabIndex = tabIndex;
        this.tabbedPaneName = tabbedPaneName;
    }

    @Override
    protected void actionPerformed(ContentUI<?, ?> contentUI) {
        JTabbedPane mainTabbedPane = (JTabbedPane) contentUI.getObjectById(tabbedPaneName);
        if (mainTabbedPane == null) {

            // rien à faire
            return;
        }
        mainTabbedPane.setSelectedIndex(tabIndex);
    }
}
