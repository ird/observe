package fr.ird.observe.application.swing.ui.content.ref.impl;

/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ui.content.ref.ContentReferenceUIHandler;
import fr.ird.observe.services.dto.reference.ReferentialReference;
import fr.ird.observe.services.dto.referential.GearDto;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.validator.swing.SwingValidatorMessage;
import org.nuiton.jaxx.validator.swing.SwingValidatorMessageTableModel;
import org.nuiton.validator.NuitonValidatorScope;

import javax.swing.event.TableModelListener;
import java.util.HashSet;
import java.util.Set;

/**
 * Created on 3/23/15.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.16
 */
public class GearUIHandler extends ContentReferenceUIHandler<GearDto, GearUI> implements UIHandler<GearUI> {

    // Compute valid state of tab from the error table
    protected final TableModelListener computeTabValidStateListener;

    public GearUIHandler() {
        super();
        computeTabValidStateListener = e -> {

            SwingValidatorMessageTableModel source = (SwingValidatorMessageTableModel) e.getSource();
            computeTabValidState(source);

        };
    }

    @Override
    public void afterInit(GearUI ui) {
        super.afterInit(ui);

        // To be sure always remove listener (could prevent some leaks)
        ui.getErrorTableModel().removeTableModelListener(computeTabValidStateListener);
        // listen messages to see if required to add
        ui.getErrorTableModel().addTableModelListener(computeTabValidStateListener);
    }

    @Override
    public void selectBean(ReferentialReference<GearDto> selectedBean) {

        super.selectBean(selectedBean);

        SwingValidatorMessageTableModel errorTableModel = getUi().getErrorTableModel();
        computeTabValidState(errorTableModel);

        getUi().getMainTabbedPane().setSelectedIndex(0);

    }

    protected void computeTabValidState(SwingValidatorMessageTableModel errorTableModel) {

        Set<String> errorProperties = new HashSet<>();
        int rowCount = errorTableModel.getRowCount();
        for (int i = 0; i < rowCount; i++) {

            SwingValidatorMessage row = errorTableModel.getRow(i);
            if (NuitonValidatorScope.ERROR.equals(row.getScope())) {
                errorProperties.add(row.getField());
            }
        }

        boolean generalTabValid = !errorProperties.removeAll(GearUIModel.GENERAL_TAB_PROPERTIES);
        boolean gearCaracteristicTabValid = !errorProperties.removeAll(GearUIModel.GEAR_CARACTERISTIC_TAB_PROPERTIES);

        GearUIModel model = (GearUIModel) getModel();
        model.setGeneralTabValid(generalTabValid);
        model.setGearCaracteristicTabValid(gearCaracteristicTabValid);

    }
}
