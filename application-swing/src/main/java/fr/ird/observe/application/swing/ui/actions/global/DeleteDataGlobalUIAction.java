package fr.ird.observe.application.swing.ui.actions.global;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.ui.ObserveKeyStrokes;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.content.ContentUI;
import fr.ird.observe.application.swing.ui.content.ref.ContentReferenceUI;
import fr.ird.observe.application.swing.ui.content.table.ContentTableUI;

import javax.swing.JButton;
import java.awt.event.ActionEvent;
import java.util.Objects;

/**
 * Created on 11/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class DeleteDataGlobalUIAction extends AbstractGlobalUIAction {

    public static final String ACTION_NAME = "deleteDataGlobal";

    public DeleteDataGlobalUIAction(ObserveMainUI mainUI) {
        super(mainUI, ACTION_NAME, ObserveKeyStrokes.KEY_STROKE_DELETE_DATA_GLOBAL);
    }


    @Override
    public void actionPerformed(ActionEvent e) {

        ContentUI<?, ?> contentUI = ObserveSwingApplicationContext.get().getContentUIManager().getSelectedContentUI();

        JButton button;
        if (contentUI instanceof ContentTableUI) {
            ContentTableUI ui = (ContentTableUI) contentUI;
            button = ui.getDeleteEntry();
        } else if (contentUI instanceof ContentReferenceUI) {
            ContentReferenceUI ui = (ContentReferenceUI) contentUI;
            if (ui.getModel().isEditing()) {
                button = ui.getDeleteFromDetail();
            } else {
                button = ui.getDeleteFromList();
            }
        } else {
            button = (JButton) contentUI.getObjectById("delete");
        }
        Objects.requireNonNull(button);

        button.getAction().actionPerformed(e);

    }

}
