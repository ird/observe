/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.actions.content;

import fr.ird.observe.application.swing.ui.ObserveKeyStrokes;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.content.ContentUI;
import fr.ird.observe.application.swing.ui.tree.navigation.NavigationTree;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.NavigationTreeNodeSupport;
import org.nuiton.jaxx.runtime.JAXXObject;

import javax.swing.AbstractButton;
import javax.swing.JComponent;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import javax.swing.tree.TreeNode;
import java.awt.GridLayout;

import static org.nuiton.i18n.I18n.n;

/**
 * Action pour sélectionner un noeud (attaché à l'éditeur) dans l'arbre de
 * navigation.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public class GoUpUIAction extends AbstractContentUIAction {

    private static final long serialVersionUID = 1L;

    public static final String ACTION_NAME = "goUp";

    public GoUpUIAction(ObserveMainUI mainUI) {
        super(mainUI,
              ACTION_NAME,
              n("observe.action.goUp"),
              n("observe.action.goUp.tip"),
              "go-up",
              ObserveKeyStrokes.KEY_STROKE_GO_UP
        );
    }

    @Override
    protected void actionPerformed(ContentUI<?, ?> contentUI) {

        SwingUtilities.invokeLater(() -> {
            JComponent c = contentUI.getActionUp();
            JPopupMenu p = getMainUI().getScopeUpPopup();
            p.show(c, 2, c.getHeight());
        });
    }

    @Override
    public void updateAction(JAXXObject ui, AbstractButton editor) {

        NavigationTree treeHelper = getMainUI().getNavigation();

        NavigationTreeNodeSupport<?> node;

        JPopupMenu scopePopup = getMainUI().getScopeUpPopup();

        SelectNodeUIAction action = (SelectNodeUIAction) getMainUI().getObserveActionMap().get(SelectNodeUIAction.ACTION_NAME);

        node = treeHelper.getSelectedNode();
        TreeNode root = node.getRoot();
        scopePopup.removeAll();
        scopePopup.setLayout(new GridLayout(0, 1));
        while (!root.equals(node.getParent())) {
            node = node.getParent();
            scopePopup.add(node.toMenuItem(action, ui));
        }
        scopePopup.revalidate();

        int nbNodes = scopePopup.getComponentCount();

        editor.setEnabled(nbNodes > 0);
    }
}
