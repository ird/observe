package fr.ird.observe.application.swing.ui.admin.synchronize.data;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.decoration.DecoratorService;
import fr.ird.observe.application.swing.decoration.decorators.ReferentialReferenceDecorator;
import fr.ird.observe.application.swing.ui.ObserveKeyStrokes;
import fr.ird.observe.application.swing.ui.admin.AdminStep;
import fr.ird.observe.application.swing.ui.admin.AdminTabUIHandler;
import fr.ird.observe.application.swing.ui.admin.config.ConfigModel;
import fr.ird.observe.application.swing.ui.admin.config.ConfigUI;
import fr.ird.observe.application.swing.ui.admin.synchronize.data.task.CopyToLeftDataSynchronizeTask;
import fr.ird.observe.application.swing.ui.admin.synchronize.data.task.CopyToRightDataSynchronizeTask;
import fr.ird.observe.application.swing.ui.admin.synchronize.data.task.DataSynchronizeTaskSupport;
import fr.ird.observe.application.swing.ui.admin.synchronize.data.task.DeleteFromLeftDataSynchronizeTask;
import fr.ird.observe.application.swing.ui.admin.synchronize.data.task.DeleteFromRightDataSynchronizeTask;
import fr.ird.observe.application.swing.ui.tree.selection.SelectionTree;
import fr.ird.observe.application.swing.ui.tree.selection.SelectionTreeModel;
import fr.ird.observe.application.swing.ui.util.ProgressModel;
import fr.ird.observe.services.configuration.topia.ObserveDataSourceConfigurationTopiaPG;
import fr.ird.observe.services.dto.reference.DataReference;
import fr.ird.observe.services.dto.reference.ReferentialReference;
import fr.ird.observe.services.dto.referential.ProgramDto;
import fr.ird.observe.services.service.data.DeleteTripRequest;
import fr.ird.observe.services.service.data.DeleteTripResult;
import fr.ird.observe.services.service.data.ExportTripRequest;
import fr.ird.observe.services.service.data.ExportTripResult;
import fr.ird.observe.services.service.data.ImportTripRequest;
import fr.ird.observe.services.service.data.ImportTripResult;
import fr.ird.observe.services.service.data.TripManagementService;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardState;

import javax.swing.DefaultListModel;
import javax.swing.border.TitledBorder;
import java.awt.Color;
import java.util.List;
import java.util.Map;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Created on 02/08/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0
 */
public class DataSynchroUIHandler extends AdminTabUIHandler<DataSynchroUI> implements UIHandler<DataSynchroUI> {

    @Override
    public void afterInit(DataSynchroUI ui) {
        super.afterInit(this.ui);
        DataSynchroModel stepModel = getStepModel();

        SelectionTree leftTree = ui.getLeftTree();
        SelectionTreeModel leftSelectionDataModel = leftTree.getTreeModel();
        stepModel.setLeftSelectionDataModel(leftSelectionDataModel);
        leftSelectionDataModel.setLoadLongline(true);
        leftSelectionDataModel.setLoadSeine(true);
        leftSelectionDataModel.setLoadReferential(false);
        leftSelectionDataModel.setShowEmptyTrips(false);


        SelectionTree rightTree = ui.getRightTree();
        SelectionTreeModel rightSelectionDataModel = rightTree.getTreeModel();
        stepModel.setRightSelectionDataModel(rightSelectionDataModel);
        rightSelectionDataModel.setLoadLongline(true);
        rightSelectionDataModel.setLoadSeine(true);
        rightSelectionDataModel.setLoadReferential(false);
        rightSelectionDataModel.setShowEmptyTrips(false);

        // on n'installe qu'une seule fois l'ui
        leftTree.installUI2(rightTree);

        stepModel.getLeftSelectionDataModel().addPropertyChangeListener(evt -> {

            SelectionTreeModel model = (SelectionTreeModel) evt.getSource();
            boolean withDataSelected = !model.isSelectionEmpty();

            ui.getCopyToRight().setEnabled(withDataSelected);
            ui.getDeleteFromLeft().setEnabled(withDataSelected);

        });
        stepModel.getRightSelectionDataModel().addPropertyChangeListener(evt -> {

            SelectionTreeModel model = (SelectionTreeModel) evt.getSource();
            boolean withDataSelected = !model.isSelectionEmpty();

            ui.getCopyToLeft().setEnabled(withDataSelected);
            ui.getDeleteFromRight().setEnabled(withDataSelected);

        });

        ObserveKeyStrokes.addKeyStrokeFromMnemonic(ui.getStartAction());
        ObserveKeyStrokes.addKeyStrokeFromMnemonic(ui.getApplyAction());

        stepModel.getProgressModel().installUI(ui.getProgressBar());
    }

    public DataSynchroModel getStepModel() {
        return getModel().getDataSynchroModel();
    }

    public void doStartAction() {

        addAdminWorker(getUi().getStartAction().getToolTipText(), this::doStartAction0);

    }

    public void addCopyToLeftTasks() {

        SelectionTreeModel rightSelectionDataModel = getStepModel().getRightSelectionDataModel();
        SelectionTreeModel leftSelectionDataModel = getStepModel().getLeftSelectionDataModel();
        Map<ReferentialReference<ProgramDto>, List<DataReference<?>>> selectedDataByProgram = rightSelectionDataModel.getSelectedDataByProgram();
        rightSelectionDataModel.removeAllSelectedData();
        for (Map.Entry<ReferentialReference<ProgramDto>, List<DataReference<?>>> entry : selectedDataByProgram.entrySet()) {
            ReferentialReference<ProgramDto> program = entry.getKey();
            for (DataReference trip : entry.getValue()) {
                boolean tripExistOnLeft = leftSelectionDataModel.containsData(program, trip);
                getStepModel().addTask(new CopyToLeftDataSynchronizeTask(program, trip, tripExistOnLeft));

                if (tripExistOnLeft) {
                    leftSelectionDataModel.removeTrip(program, trip);
                }
            }
        }

    }

    public void addCopyToRightTasks() {

        SelectionTreeModel leftSelectionDataModel = getStepModel().getLeftSelectionDataModel();
        SelectionTreeModel rightSelectionDataModel = getStepModel().getRightSelectionDataModel();
        Map<ReferentialReference<ProgramDto>, List<DataReference<?>>> selectedDataByProgram = leftSelectionDataModel.getSelectedDataByProgram();
        leftSelectionDataModel.removeAllSelectedData();
        for (Map.Entry<ReferentialReference<ProgramDto>, List<DataReference<?>>> entry : selectedDataByProgram.entrySet()) {
            ReferentialReference<ProgramDto> program = entry.getKey();
            for (DataReference trip : entry.getValue()) {

                boolean tripExistOnRight = rightSelectionDataModel.containsData(program, trip);
                getStepModel().addTask(new CopyToRightDataSynchronizeTask(program, trip, tripExistOnRight));

                if (tripExistOnRight) {
                    rightSelectionDataModel.removeTrip(program, trip);
                }
            }
        }

    }

    public void addDeleteFromLeftTasks() {

        SelectionTreeModel selectionDataModel = getStepModel().getLeftSelectionDataModel();
        Map<ReferentialReference<ProgramDto>, List<DataReference<?>>> selectedDataByProgram = selectionDataModel.getSelectedDataByProgram();
        selectionDataModel.removeAllSelectedData();
        for (Map.Entry<ReferentialReference<ProgramDto>, List<DataReference<?>>> entry : selectedDataByProgram.entrySet()) {
            ReferentialReference<ProgramDto> program = entry.getKey();
            for (DataReference trip : entry.getValue()) {
                getStepModel().addTask(new DeleteFromLeftDataSynchronizeTask(program, trip));
            }
        }
    }

    public void addDeleteFromRightTasks() {

        SelectionTreeModel selectionDataModel = getStepModel().getRightSelectionDataModel();
        Map<ReferentialReference<ProgramDto>, List<DataReference<?>>> selectedDataByProgram = selectionDataModel.getSelectedDataByProgram();
        selectionDataModel.removeAllSelectedData();
        for (Map.Entry<ReferentialReference<ProgramDto>, List<DataReference<?>>> entry : selectedDataByProgram.entrySet()) {
            ReferentialReference<ProgramDto> program = entry.getKey();
            for (DataReference trip : entry.getValue()) {
                getStepModel().addTask(new DeleteFromRightDataSynchronizeTask(program, trip));
            }
        }
    }

    public void doExecuteAction() {

        addAdminWorker(getUi().getStartAction().getToolTipText(), this::doExecuteAction0);

    }

    private WizardState doStartAction0() {

        DataSynchroUI tabUI = getUi();
        DataSynchroModel stepModel = getStepModel();
        ConfigUI configUI = (ConfigUI) parentUI.getStepUI(AdminStep.CONFIG);

        ConfigModel configModel = getModel().getConfigModel();

        ObserveSwingDataSource leftSource = configModel.getSafeLocalSource(true);
        stepModel.setLeftSource(leftSource);

        ObserveSwingDataSource rightSource = configModel.getSafeCentralSource(true);
        stepModel.setRightSource(rightSource);

//        tabUI.getLeftTreePane().setBorder(new TitledBorder(configModel.getLocalSourceModel().getLabel()));
        stepModel.populateLeftSelectionModel();
        sendMessage(t("observe.actions.synchro.referential.message.data.leftData.loaded"));

        TitledBorder leftBorder = new TitledBorder(configModel.getLocalSourceModel().getLabelWithUrl());
        boolean leftCanWriteData = configModel.getLocalSourceModel().getDataSourceInformation().canWriteData();
        leftBorder.setTitleColor(leftCanWriteData ? Color.GREEN : Color.RED);
        tabUI.getLeftTreePane().setBorder(leftBorder);
        if (!leftCanWriteData) {
            tabUI.getCopyToLeft().setVisible(false);
            tabUI.getDeleteFromRight().setVisible(false);
        }
        configUI.getLocalSourceConfig().setBorder(leftBorder);

//        tabUI.getRightTreePane().setBorder(new TitledBorder(configModel.getCentralSourceModel().getLabel()));
        stepModel.populateRightSelectionModel();
        sendMessage(t("observe.actions.synchro.referential.message.data.rightData.loaded"));

        TitledBorder rightBorder = new TitledBorder(configModel.getCentralSourceModel().getLabelWithUrl());
        boolean rightCanWriteData = configModel.getCentralSourceModel().getDataSourceInformation().canWriteData();
        rightBorder.setTitleColor(rightCanWriteData ? Color.GREEN : Color.RED);
        tabUI.getRightTreePane().setBorder(rightBorder);
        if (!rightCanWriteData) {
            tabUI.getCopyToRight().setVisible(false);
            tabUI.getDeleteFromLeft().setVisible(false);
        }
        configUI.getCentralSourceConfig().setBorder(rightBorder);

        return WizardState.NEED_FIX;

    }

    private WizardState doExecuteAction0() {

        ObserveSwingDataSource leftSource = getStepModel().getLeftSource();
        boolean leftSourceIsPG = leftSource.getConfiguration() instanceof ObserveDataSourceConfigurationTopiaPG;

        ObserveSwingDataSource rightSource = getStepModel().getRightSource();
        boolean rightSourceIsPG = rightSource.getConfiguration() instanceof ObserveDataSourceConfigurationTopiaPG;

        DefaultListModel<DataSynchronizeTaskSupport> tasks = getStepModel().getTasks();
        int size = tasks.size();

        int stepsCount = 0;
        for (int i = 0; i < size; i++) {
            DataSynchronizeTaskSupport task = tasks.getElementAt(i);
            stepsCount++;
            if (task instanceof CopyToLeftDataSynchronizeTask || task instanceof CopyToRightDataSynchronizeTask) {
                stepsCount++;
            }
        }
        ProgressModel progressModel = getStepModel().getProgressModel();
        progressModel.setStepsCount(stepsCount);

        DecoratorService decoratorService = getDecoratorService();
        ReferentialReferenceDecorator<ProgramDto> programDecorator = decoratorService.getReferentialReferenceDecorator(ProgramDto.class);

        TripManagementService leftTripManagementService = leftSource.newTripManagementService();
        TripManagementService rightTripManagementService = rightSource.newTripManagementService();

        for (int i = 0; i < size; i++) {

            DataSynchronizeTaskSupport task = tasks.getElementAt(i);

            ReferentialReference<ProgramDto> program = task.getProgram();
            String programId = program.getId();
            String programStr = programDecorator.toString(program);

            DataReference trip = task.getTrip();
            String tripId = trip.getId();
            String tripStr = decoratorService.getTripReferenceDecorator(trip).toString(trip);

            progressModel.incrementsCurrentStep();

            if (task instanceof DeleteFromLeftDataSynchronizeTask) {

                sendMessage(t("observe.actions.synchro.data.prepare.deleteFromLeftTask", programStr, tripStr));

                DeleteTripRequest deleteTripRequest = new DeleteTripRequest(programId, tripId);
                DeleteTripResult deleteTripResult = leftTripManagementService.deleteTrip(deleteTripRequest);
                logDeleteResult(n("observe.actions.synchro.data.result.delete.left.trip"),
                                deleteTripResult,
                                programDecorator,
                                program,
                                trip);
                continue;

            }

            if (task instanceof DeleteFromRightDataSynchronizeTask) {

                sendMessage(t("observe.actions.synchro.data.prepare.deleteFromRightTask", programStr, tripStr));

                DeleteTripRequest deleteTripRequest = new DeleteTripRequest(programId, tripId);
                DeleteTripResult deleteTripResult = rightTripManagementService.deleteTrip(deleteTripRequest);
                logDeleteResult(n("observe.actions.synchro.data.result.delete.right.trip"),
                                deleteTripResult,
                                programDecorator,
                                program,
                                trip);
                continue;

            }

            if (task instanceof CopyToLeftDataSynchronizeTask) {

                sendMessage(t("observe.actions.synchro.data.prepare.copyToLeftTask", programStr, tripStr));

                ExportTripRequest exportTripRequest = new ExportTripRequest(rightSourceIsPG, programId, tripId);
                ExportTripResult exportTripResult = rightTripManagementService.exportTrip(exportTripRequest);
                logExportResult(n("observe.actions.synchro.data.result.export.right.trip"),
                                exportTripResult,
                                programDecorator,
                                program,
                                trip);

                progressModel.incrementsCurrentStep();
                ImportTripRequest importTripRequest = new ImportTripRequest(exportTripResult);
                ImportTripResult importTripResult = leftTripManagementService.importTrip(importTripRequest);
                logImportResult(n("observe.actions.synchro.data.result.import.left.trip"),
                                n("observe.actions.synchro.data.result.delete.left.trip"),
                                importTripResult,
                                programDecorator,
                                program,
                                trip);
                continue;

            }

            if (task instanceof CopyToRightDataSynchronizeTask) {

                sendMessage(t("observe.actions.synchro.data.prepare.copyToRightTask", programStr, tripStr));

                ExportTripRequest exportTripRequest = new ExportTripRequest(leftSourceIsPG, programId, tripId);
                ExportTripResult exportTripResult = leftTripManagementService.exportTrip(exportTripRequest);
                logExportResult(n("observe.actions.synchro.data.result.export.left.trip"),
                                exportTripResult,
                                programDecorator,
                                program,
                                trip);

                progressModel.incrementsCurrentStep();
                ImportTripRequest importTripRequest = new ImportTripRequest(exportTripResult);
                ImportTripResult importTripResult = rightTripManagementService.importTrip(importTripRequest);
                logImportResult(n("observe.actions.synchro.data.result.import.right.trip"),
                                n("observe.actions.synchro.data.result.delete.right.trip"),
                                importTripResult,
                                programDecorator,
                                program,
                                trip);

            }

        }

        return WizardState.SUCCESSED;

    }
}
