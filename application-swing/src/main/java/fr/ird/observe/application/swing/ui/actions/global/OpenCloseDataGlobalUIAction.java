package fr.ird.observe.application.swing.ui.actions.global;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveOpenDataManager;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.ui.ObserveKeyStrokes;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.actions.content.CloseOpenUIAction;
import fr.ird.observe.application.swing.ui.actions.content.ReOpenUIAction;
import fr.ird.observe.application.swing.ui.content.ContentUI;
import fr.ird.observe.application.swing.ui.content.list.ContentListUI;
import fr.ird.observe.application.swing.ui.content.open.ContentOpenableUI;

import java.awt.event.ActionEvent;
import java.util.Objects;

/**
 * Created on 11/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class OpenCloseDataGlobalUIAction extends AbstractGlobalUIAction implements Runnable {

    public static final String ACTION_NAME = "openCloseDataGlobal";

    private final ReOpenUIAction openAction;
    private final CloseOpenUIAction closeAction;

    public OpenCloseDataGlobalUIAction(ObserveMainUI mainUI) {
        super(mainUI, ACTION_NAME, ObserveKeyStrokes.KEY_STROKE_OPEN_CLOSE_DATA);
        openAction = new ReOpenUIAction(mainUI);
        closeAction = new CloseOpenUIAction(mainUI);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        actionPerformed(this);
    }

    @Override
    public void run() {

        ContentUI<?, ?> contentUI = ObserveSwingApplicationContext.get().getContentUIManager().getSelectedContentUI();

        if (contentUI instanceof ContentListUI) {
            ContentListUI ui = (ContentListUI) contentUI;

            if (ui.getReopen().isEnabled()) {

                // on réouvre
                openAction.actionPerformed(ui);
            } else if (ui.getClose().isEnabled()) {

                // on ferme
                closeAction.actionPerformed(ui);

            }

            return;
        }

        if (contentUI.getModel().isCreatingMode() || !(contentUI instanceof ContentOpenableUI)) {
            return;
        }

        // le seul cas où on peut intervenir (sur un ecran open et pas en mode création)

        String currentId = contentUI.getModel().getBean().getId();
        Objects.requireNonNull(currentId);

        ObserveOpenDataManager openDataManager = ObserveSwingApplicationContext.get().getOpenDataManager();

        boolean isOpened = openDataManager.isOpen(currentId);

        if (isOpened) {
            closeAction.actionPerformed(contentUI);
        } else {
            openAction.actionPerformed(contentUI);
        }

    }

}
