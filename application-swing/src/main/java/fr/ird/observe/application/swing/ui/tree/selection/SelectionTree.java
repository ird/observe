package fr.ird.observe.application.swing.ui.tree.selection;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ui.ObserveKeyStrokes;
import fr.ird.observe.application.swing.ui.tree.selection.nodes.SelectionTreeNodeSupport;
import org.jdesktop.swingx.JXTree;

import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.ListSelectionModel;
import java.awt.event.ActionEvent;

/**
 * Created on 14/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class SelectionTree extends JXTree {

    public SelectionTree() {
        super(new SelectionTreeModel());
        setLargeModel(true);
        setCellRenderer(new SelectionTreeCellRenderer());
        setRootVisible(false);
        setRowHeight(30);
        getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

    }

    public void installUI() {

        InputMap inputMap = getInputMap(WHEN_IN_FOCUSED_WINDOW);
        ActionMap actionMap = getActionMap();
        inputMap.put(ObserveKeyStrokes.KEY_STROKE_ENTER, "select/unselect");
        actionMap.put("select/unselect", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object selectedRow = getLastSelectedPathComponent();
                if (selectedRow == null) {
                    return;
                }

                SelectionTreeNodeSupport node = (SelectionTreeNodeSupport) selectedRow;
                getTreeModel().setValueAt(node, !node.isSelected());
            }
        });
    }

    public void installUI2(SelectionTree oppositeTree) {

        InputMap inputMap = getInputMap(WHEN_IN_FOCUSED_WINDOW);
        ActionMap actionMap = getActionMap();

        inputMap.put(ObserveKeyStrokes.KEY_STROKE_ENTER, "select/unselect");
        actionMap.put("select/unselect", new AbstractAction() {

            protected void forTree(SelectionTree tree) {
                Object selectedRow = tree.getLastSelectedPathComponent();
                if (selectedRow == null) {
                    return;
                }

                SelectionTreeNodeSupport node = (SelectionTreeNodeSupport) selectedRow;
                tree.getTreeModel().setValueAt(node, !node.isSelected());
            }

            @Override
            public void actionPerformed(ActionEvent e) {
                if (isFocusOwner()) {
                    forTree(SelectionTree.this);
                } else {
                    forTree(oppositeTree);
                }
            }
        });
    }

    public SelectionTreeModel getTreeModel() {
        return (SelectionTreeModel) getModel();
    }

}
