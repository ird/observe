package fr.ird.observe.application.swing.ui.admin.config;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.admin.AdminTabUIHandler;
import fr.ird.observe.application.swing.ui.tree.selection.SelectionTree;
import org.nuiton.jaxx.runtime.spi.UIHandler;

import javax.swing.SwingUtilities;

/**
 * Created on 28/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class SelectDataUIHandler extends AdminTabUIHandler<SelectDataUI> implements UIHandler<SelectDataUI> {

    @Override
    public void afterInit(SelectDataUI ui) {
        super.afterInit(this.ui);

        UIHelper.setLayerUI(ui.getContent(), null);
        UIHelper.setLayerUI(ui.getSelectTreePane(), parentUI.getConfigBlockLayerUI());

        SelectionTree tree = ui.getSelectTree();
        getModel().getSelectDataModel().setSelectionDataModel(tree.getTreeModel());

        tree.installUI();
        //FIXME Init tree ?
//        UIHelper.initUI(tabUI.getSelectTreePane(), tree);
    }


    public void onSelectDataOpen() {

        SelectionTree selectTree = ui.getSelectTree();
        selectTree.clearSelection();

        SwingUtilities.invokeLater(selectTree::grabFocus);

        if (selectTree.getTreeModel().isNotEmpty()) {

            selectTree.setSelectionRow(0);
        }
        if (ui.getModel().getConfigModel().getSafeLocalSource(true).isLocal()) {
            SwingUtilities.invokeLater(selectTree::expandAll);
        }

    }
}
