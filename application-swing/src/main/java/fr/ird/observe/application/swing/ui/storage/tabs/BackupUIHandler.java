package fr.ird.observe.application.swing.ui.storage.tabs;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ui.ObserveKeyStrokes;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.storage.StorageStep;
import org.nuiton.jaxx.runtime.spi.UIHandler;

import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import java.io.File;

import static javax.swing.JComponent.WHEN_IN_FOCUSED_WINDOW;
import static org.nuiton.i18n.I18n.t;

/**
 * Created on 27/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since X
 */
public class BackupUIHandler extends StorageTabUIHandler<BackupUI> implements UIHandler<BackupUI> {

    public static final String ACTION_DO_BACKUP = "doBackup";
    public static final String ACTION_DO_CHOOSE_DIRECTORY = "doChooseDirectory";

    @Override
    public void afterInit(BackupUI ui) {
        if (ui.getStep() != null) {
            ui.setDescriptionText(t(ui.getStep().getDescription()));

            ui.getActionMap().put(ACTION_DO_BACKUP, new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (StorageStep.BACKUP == ui.getModel().getStep()) {
                        ui.getDoBackup().doClick();
                    }
                }
            });
            ui.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(ObserveKeyStrokes.KEY_STROKE_STORAGE_DO_BACKUP, ACTION_DO_BACKUP);
            ObserveKeyStrokes.addKeyStroke(ui.getDoBackup(), ObserveKeyStrokes.KEY_STROKE_STORAGE_DO_BACKUP);

            ui.getActionMap().put(ACTION_DO_CHOOSE_DIRECTORY, new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (StorageStep.BACKUP == ui.getModel().getStep()) {
                        ui.getChooseFileAction().doClick();
                    }
                }
            });
            ui.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(ObserveKeyStrokes.KEY_STROKE_STORAGE_DO_CHOOSE_DIRECTORY, ACTION_DO_CHOOSE_DIRECTORY);
            ObserveKeyStrokes.addKeyStroke(ui.getChooseFileAction(), ObserveKeyStrokes.KEY_STROKE_STORAGE_DO_CHOOSE_DIRECTORY);
        }
    }


    public void chooseBackupFile() {
        File f = UIHelper.chooseDirectory(ui,
                                          t("observe.title.choose.db.dump.directory"),
                                          t("observe.action.choose.db.dump.directory"),
                                          new File(ui.getDirectoryText().getText()));
        if (f != null) {
            changeDirectory(f);
        }
    }

    public void changeDirectory(File f) {
        ui.getModel().setBackupFile(new File(f, ui.getFilenameText().getText()));
    }

    public void changeFilename(String filename) {
        ui.getModel().setBackupFile(new File(ui.getDirectoryText().getText(), filename));
    }

}
