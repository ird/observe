/*
 * #%L
 * ObServe :: Swing
 * %%
 * Copyright (C) 2008 - 2010 IRD, Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

NumberEditor {
  bean:{tableEditBean};
  autoPopup:{config.isAutoPopupNumberEditor()};
  showPopupButton:{config.isShowNumberEditorButton()};
  showReset:true;
}

JToolBar {
  borderPainted:false;
  floatable:false;
  opaque:false;
}

#speciesLabel {
  labelFor:{species};
}

#species {
  _entityClass:{SpeciesDto.class};
  property:species;
  bean:{tableEditBean};
  showReset:true;
  selectedItem:{tableEditBean.getSpecies()};
}

#sexLabel {
  text:"observe.common.sex";
  toolTipText:"observe.content.targetSample.table.sex.tip";
  labelFor:{sex};
}

#sex {
  _entityClass:{SexDto.class};
  property:sex;
  bean:{tableEditBean};
  showReset:true;
  selectedItem:{tableEditBean.getSex()};
}

#modeAndCodePanel {
  layout:{new GridLayout(1,0)};
}

#acquisitionModeGroup {
  selectedValue:{ModeSaisieEchantillonEnum.valueOf(tableEditBean.getAcquisitionMode())};
}

#acquisitionModePanel {
  border:{new TitledBorder(t("observe.common.acquisitionMode"))};
  layout:{new GridLayout(1,0)};
}

#acquisitionModeEffectif {
  buttonGroup:"acquisitionModeGroup";
  value:{ModeSaisieEchantillonEnum.byEffectif};
  text:{ModeSaisieEchantillonEnum.byEffectif.getI18nKey()};
  selected:{tableEditBean.getAcquisitionMode() == 0};
  enabled:{!tableModel.isEditable() || tableModel.isCreate()};
}

#acquisitionModeIndividu {
  buttonGroup:"acquisitionModeGroup";
  value:{ModeSaisieEchantillonEnum.byIndividu};
  text:{ModeSaisieEchantillonEnum.byIndividu.getI18nKey()};
  selected:{tableEditBean.getAcquisitionMode() == 1};
  enabled:{!tableModel.isEditable() || tableModel.isCreate()};
}

#sizeMeasureTypePanel {
  border:{new TitledBorder(t("observe.common.sizeMeasureType"))};
}

#sizeMeasureTypeLabel {
  labelFor:{sizeMeasureType};
}

#sizeMeasureType {
  _entityClass:{SizeMeasureTypeDto.class};
  property:sizeMeasureType;
  bean:{tableEditBean};
  _tablePropertyName:{"sizeMeasureType"};
  selectedItem:{tableEditBean.getSizeMeasureType()};
}

#defaultSizeMeasureType {
  actionIcon:combobox-reset2;
  toolTipText:"observe.content.sample.resetDefaultSizeMeasureType";
}

#lengthLabel {
  labelFor:{length};
}

#length {
  property:length;
  numberValue:{tableEditBean.getLength()};
}

#lengthSourceInformation {
  disabledIcon: {iconDataObserve};
  icon: {iconDataCalcule};
  enabled: {tableEditBean.isIsLengthComputed()};
  toolTipText:{getLengthDataTip(tableEditBean.isIsLengthComputed())};
}

#weightLabel {
  text:"observe.common.weight.ind";
  labelFor:{weight};
}

#weight {
  property:weight;
  numberValue:{tableEditBean.getWeight()};
  _validatorLabel:{t("observe.common.weight.ind")};
}

#weightSourceInformation {
  disabledIcon: {iconDataObserve};
  icon: {iconDataCalcule};
  enabled: {tableEditBean.isIsWeightComputed()};
  toolTipText:{getWeightDataTip(tableEditBean.isIsWeightComputed())};
}

#comment {
  minimumSize:{new Dimension(10,80)};
}

#comment2 {
  text:{getStringValue(bean.getComment())};
}

#tagNumberLabel {
  text:"observe.content.common.tagNumber";
  labelFor:{tagNumber};
}

#resetTagNumber {
  toolTipText:"observe.content.common.action.reset.tagNumber.tip";
  _resetTablePropertyName:{"tagNumber"};
  enabled:{tableEditBean.getAcquisitionMode() == 1};
}

#tagNumber {
  text:{getStringValue(tableEditBean.getTagNumber())};
  _tablePropertyName:{"tagNumber"};
  enabled:{tableEditBean.getAcquisitionMode() == 1};
}
