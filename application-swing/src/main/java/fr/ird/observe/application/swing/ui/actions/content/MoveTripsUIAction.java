/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.actions.content;

import fr.ird.observe.application.swing.ObserveOpenDataManager;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.content.ContentUI;
import fr.ird.observe.application.swing.ui.content.list.ContentListUIModel;
import fr.ird.observe.application.swing.ui.tree.menu.MoveTripLonglineNodeMenuPopulator;
import fr.ird.observe.application.swing.ui.tree.menu.MoveTripSeineNodeMenuPopulator;
import fr.ird.observe.application.swing.ui.tree.navigation.NavigationTree;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.NavigationTreeNodeSupport;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.RootNavigationTreeNode;
import fr.ird.observe.application.swing.ui.util.DecoratedNodeEntity;
import fr.ird.observe.common.constants.GearType;
import fr.ird.observe.services.dto.DataDto;
import fr.ird.observe.services.dto.reference.DataReference;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JOptionPane;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Action pour changer le programme d'une ou plusieurs marée dans la liste.
 *
 * @author Kevin Morin (Code Lutin)
 * @since 5.0
 */
public abstract class MoveTripsUIAction<T extends DataDto> extends AbstractContentUIAction {

    private static final long serialVersionUID = 1L;

    /**
     * Logger.
     */
    private static final Log log = LogFactory.getLog(MoveTripsUIAction.class);

    public static final String ACTION_NAME = "moveTrips";

    public MoveTripsUIAction(ObserveMainUI mainUI, String actionName) {
        super(mainUI,
              actionName,
              n("observe.content.action.move.trips"),
              n("observe.content.action.move.trips.tip"),
              "move-trips",
              null
        );
    }

    @Override
    protected void actionPerformed(ContentUI<?, ?> ui) {

        checkUIClass(ui);

        // get current program id
        NavigationTree tree = getMainUI().getNavigation();
        NavigationTreeNodeSupport oldProgramNode = tree.getSelectedNode();
        String oldProgramId = oldProgramNode.getId();

        // choose the new program
        String programId = chooseNewProgram(ui, oldProgramId);

        if (programId != null) {

            // change the program of the selected trips
            List<DataReference<T>> selectedDatas = ((ContentListUIModel) ui.getModel()).getSelectedDatas();
            List<String> tripIds = selectedDatas.stream().map(DataReference.ID_FUNCTION).collect(Collectors.toList());
            List<Integer> positions = getPositions(tripIds, programId);

            // update the tree
            updateTree(ui, oldProgramNode, oldProgramId, programId, tripIds, positions);
        }

    }

    protected String chooseNewProgram(ContentUI<?, ?> ui, String oldProgramId) {

        NavigationTree tree = getMainUI().getNavigation();

        GearType geartype = getGearType(ui);

        // racine
        RootNavigationTreeNode rootNode = tree.getTreeModel().getRoot();

        //on crée un tableau avec un programme en moins car on ne propose pas le programme actuel
        List<DecoratedNodeEntity> decoratedProgramList = new ArrayList<>();

        if (geartype == GearType.seine) {
            MoveTripSeineNodeMenuPopulator.createPossibleParents(oldProgramId, decoratedProgramList, rootNode);
        } else if (geartype == GearType.longline) {
            MoveTripLonglineNodeMenuPopulator.createPossibleParents(oldProgramId, decoratedProgramList, rootNode);
        }
        DecoratedNodeEntity[] decoratedPrograms =
                decoratedProgramList.toArray(new DecoratedNodeEntity[decoratedProgramList.size()]);

        DecoratedNodeEntity decoratedProgram = (DecoratedNodeEntity) JOptionPane.showInputDialog(ui,
                                                                                                 t("observe.action.choose.program.message"),
                                                                                                 t("observe.action.choose.program.title"),
                                                                                                 JOptionPane.QUESTION_MESSAGE,
                                                                                                 null,
                                                                                                 decoratedPrograms,
                                                                                                 null);

        return decoratedProgram == null ? null : decoratedProgram.getId();
    }

    protected void updateTree(ContentUI<?, ?> ui,
                              NavigationTreeNodeSupport oldProgramNode,
                              String oldProgramId,
                              String programId,
                              List<String> tripIds,
                              List<Integer> positions) {

        ObserveSwingApplicationContext applicationContext = ObserveSwingApplicationContext.get();
        ObserveOpenDataManager openDataManager = applicationContext.getOpenDataManager();
        NavigationTree tree = getMainUI().getNavigation();

        RootNavigationTreeNode rootNode = tree.getTreeModel().getRoot();
        NavigationTreeNodeSupport newProgramNode = tree.getChild(rootNode, programId);

        for (int i = 0, s = positions.size(); i < s; i++) {

            String tripId = tripIds.get(i);
            NavigationTreeNodeSupport tripNode = tree.getChild(oldProgramNode, tripId);
            boolean wasOpen = tripNode.isOpen();
            tree.removeNode(tripNode);

            if (wasOpen) {
                openDataManager.closeProgram(oldProgramId);
                openDataManager.openProgram(programId);
            }

            NavigationTreeNodeSupport newTripNode = tree.getChild(newProgramNode, tripId);

            if (newTripNode == null) {

                // create it
                if (log.isInfoEnabled()) {
                    log.info("Insert trip node: ");
                }
                tree.insertNode(newProgramNode, tripNode, positions.get(i));
            }
        }

        updateModelData(ui);

        tree.reloadNode(oldProgramNode, true);
        tree.reloadNode(newProgramNode, true);
        tree.selectNode(newProgramNode);
    }

    protected abstract void checkUIClass(ContentUI<?, ?> ui) throws IllegalStateException;

    protected abstract GearType getGearType(ContentUI<?, ?> ui);

    protected abstract List<Integer> getPositions(List<String> tripIds, String programId);

    protected abstract void updateModelData(ContentUI<?, ?> ui);

}
