/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.actions.content;

import fr.ird.observe.application.swing.ObserveOpenDataManager;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.decoration.ObserveI18nDecoratorHelper;
import fr.ird.observe.application.swing.decoration.decorators.DataReferenceDecorator;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.content.ContentUI;
import fr.ird.observe.application.swing.ui.content.list.impl.seine.ActivitySeinesUI;
import fr.ird.observe.application.swing.ui.content.list.impl.seine.ActivitySeinesUIModel;
import fr.ird.observe.application.swing.ui.tree.navigation.NavigationTree;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.NavigationTreeNodeSupport;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.seine.RouteSeineNavigationTreeNode;
import fr.ird.observe.application.swing.ui.util.DecoratedNodeEntity;
import fr.ird.observe.services.dto.reference.DataReference;
import fr.ird.observe.services.dto.seine.ActivitySeineDto;
import fr.ird.observe.services.dto.seine.RouteDto;
import fr.ird.observe.services.service.data.seine.ActivitySeineService;

import javax.swing.JOptionPane;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Action pour changer le programme d'une ou plusieurs marée dans la liste.
 *
 * @author Kevin Morin (Code Lutin)
 * @since 5.0
 */
public class MoveActivitySeinesUIAction extends AbstractContentUIAction {

    private static final long serialVersionUID = 1L;

    public static final String ACTION_NAME = "moveActivitySeines";

    public MoveActivitySeinesUIAction(ObserveMainUI mainUI) {
        super(mainUI,
              ACTION_NAME,
              n("observe.content.action.move.activities.seine"),
              n("observe.content.action.move.activities.seine.tip"),
              "move-activities",
              null
        );
    }

    @Override
    protected void actionPerformed(ContentUI<?, ?> ui) {


        if (!(ui instanceof ActivitySeinesUI)) {
            throw new IllegalStateException("Can not come here!");
        }

        // get current route id
        NavigationTree tree = getMainUI().getNavigation();
        NavigationTreeNodeSupport oldActivitiesNode = tree.getSelectedNode();
        NavigationTreeNodeSupport oldRouteNode = oldActivitiesNode.getParent();

        // choose the new route
        String routeId = chooseNewRoute(ui, oldRouteNode);

        if (routeId != null) {
            // change the route of the selected activities
            List<DataReference<ActivitySeineDto>> selectedDatas = ((ActivitySeinesUIModel) ui.getModel()).getSelectedDatas();
            List<String> activityIds = selectedDatas.stream()
                                                    .map(DataReference.ID_FUNCTION)
                                                    .collect(Collectors.toList());
            ActivitySeineService service = ObserveSwingApplicationContext.get().getMainDataSourceServicesProvider().newActivitySeineService();
            List<Integer> positions = service.moveActivitySeinesToRoute(activityIds, routeId);

            // update the tree
            updateTree(oldActivitiesNode, routeId, activityIds);
        }

    }

    protected String chooseNewRoute(ContentUI<?, ?> ui, NavigationTreeNodeSupport oldRouteNode) {
        NavigationTreeNodeSupport routesNode = oldRouteNode.getParent();
        String oldRouteId = oldRouteNode.getId();
        int routeNb = routesNode.getChildCount();

        ObserveSwingApplicationContext applicationContext = ObserveSwingApplicationContext.get();
        DataReferenceDecorator<RouteDto> decorator = applicationContext.getDecoratorService().getDataReferenceDecorator(RouteDto.class);

        //on crée un tableau avec une route en moins car on ne propose pas la route actuel
        DecoratedNodeEntity[] decoratedRoutes = new DecoratedNodeEntity[routeNb - 1];

        int j = 0;
        for (int i = 0; i < routeNb; i++) {

            RouteSeineNavigationTreeNode routeNode = (RouteSeineNavigationTreeNode) routesNode.getChildAt(i);

            String routeId = routeNode.getId();

            if (!oldRouteId.equals(routeId)) {
                decoratedRoutes[j++] = DecoratedNodeEntity.newDecoratedNodeEntity(routeNode, decorator);
            }
        }

        Object decoratedRoute = JOptionPane.showInputDialog(ui,
                                                            t("observe.action.choose.route.message"),
                                                            t("observe.action.choose.route.title"),
                                                            JOptionPane.QUESTION_MESSAGE,
                                                            null,
                                                            decoratedRoutes,
                                                            null);

        return decoratedRoute != null ? ((DecoratedNodeEntity) decoratedRoute).getId() : null;
    }

    protected void updateTree(NavigationTreeNodeSupport oldActivitiesNode,
                              String routeId,
                              List<String> activityIds) {

        ObserveSwingApplicationContext applicationContext = ObserveSwingApplicationContext.get();
        ObserveOpenDataManager openDataManager = applicationContext.getOpenDataManager();
        NavigationTree tree = getMainUI().getNavigation();

        NavigationTreeNodeSupport oldRouteNode = oldActivitiesNode.getParent();
        NavigationTreeNodeSupport routesNode = oldRouteNode.getParent();
//        ObserveNode tripNode = routesNode.getParent();
        NavigationTreeNodeSupport newRouteNode = tree.getChild(routesNode, routeId);
        String activitiesNodeId = ObserveI18nDecoratorHelper.getTypePluralI18nKey(ActivitySeineDto.class);
        NavigationTreeNodeSupport newActivitiesNode = tree.getChild(newRouteNode, activitiesNodeId);

        // Let's check if we're moving an open activity
        Optional<String> openActivity = activityIds
                .stream()
                .filter(openDataManager::isOpenActivitySeine)
                .findFirst();

        // If so, we close it to avoid ending up with an open activity into a closed route.
        openActivity.ifPresent(openDataManager::closeActivitySeine);

        // Let's reload the sub tree of each activities node.
        // As the change have already be done in database, we just call the child loaders to regenerate the activities nodes sub trees
        tree.reloadNodeSubTree(oldActivitiesNode);
        tree.reloadNodeSubTree(newActivitiesNode);

        // Let's put the focus on the activities node which received the activities
        tree.selectNode(newActivitiesNode);
    }

}
