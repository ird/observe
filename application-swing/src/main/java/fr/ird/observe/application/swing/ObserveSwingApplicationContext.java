/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import fr.ird.observe.application.swing.configuration.ObserveSwingApplicationConfig;
import fr.ird.observe.application.swing.db.DataContext;
import fr.ird.observe.application.swing.decoration.DecoratorService;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.content.ContentUIManager;
import fr.ird.observe.application.swing.ui.content.ObserveActionMap;
import fr.ird.observe.application.swing.ui.content.ObserveFocusManager;
import fr.ird.observe.application.swing.validation.ValidationContext;
import fr.ird.observe.services.ObserveServicesProvider;
import fr.ird.observe.services.binder.ReferenceBinderEngine;
import fr.ird.observe.services.binder.ReferenceBinderEngineSupplier;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import fr.ird.observe.services.runner.ObserveDataSourceConfigurationMainFactory;
import fr.ird.observe.services.runner.ObserveServiceMainFactory;
import fr.ird.observe.services.service.actions.validate.ValidateServiceUtils;
import fr.ird.observe.services.service.actions.validate.ValidatorDto;
import org.nuiton.jaxx.runtime.context.DefaultApplicationContext;
import org.nuiton.jaxx.runtime.context.JAXXContextEntryDef;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.h2.tools.Server;

import java.io.Closeable;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import static fr.ird.observe.application.swing.ObserveSwingApplicationContext.Entries.ACTIONS;
import static fr.ird.observe.application.swing.ObserveSwingApplicationContext.Entries.ACTION_MAP;
import static fr.ird.observe.application.swing.ObserveSwingApplicationContext.Entries.CONFIG;
import static fr.ird.observe.application.swing.ObserveSwingApplicationContext.Entries.CONTENT_UI_MANAGER;
import static fr.ird.observe.application.swing.ObserveSwingApplicationContext.Entries.DATA_CONTEXT;
import static fr.ird.observe.application.swing.ObserveSwingApplicationContext.Entries.DATA_SOURCES_MANAGER;
import static fr.ird.observe.application.swing.ObserveSwingApplicationContext.Entries.DATA_SOURCE_CONFIGURATION_FACTORY;
import static fr.ird.observe.application.swing.ObserveSwingApplicationContext.Entries.DECORATOR_SERVICE;
import static fr.ird.observe.application.swing.ObserveSwingApplicationContext.Entries.FOCUS_MANAGER;
import static fr.ird.observe.application.swing.ObserveSwingApplicationContext.Entries.H2_SERVER;
import static fr.ird.observe.application.swing.ObserveSwingApplicationContext.Entries.H2_SERVER_MODE;
import static fr.ird.observe.application.swing.ObserveSwingApplicationContext.Entries.H2_WEBSERVER;
import static fr.ird.observe.application.swing.ObserveSwingApplicationContext.Entries.LOCAL_DATABASE_BACKUP_TIMER;
import static fr.ird.observe.application.swing.ObserveSwingApplicationContext.Entries.MAIN_UI;
import static fr.ird.observe.application.swing.ObserveSwingApplicationContext.Entries.NODE_TO_RESELECT;
import static fr.ird.observe.application.swing.ObserveSwingApplicationContext.Entries.OBSERVE_SWING_SESSION_HELPER;
import static fr.ird.observe.application.swing.ObserveSwingApplicationContext.Entries.OPEN_DATA_MANAGER;
import static fr.ird.observe.application.swing.ObserveSwingApplicationContext.Entries.SERVICE_MAIN_FACTORY;
import static fr.ird.observe.application.swing.ObserveSwingApplicationContext.Entries.TEXT_GENERATOR;
import static fr.ird.observe.application.swing.ObserveSwingApplicationContext.Entries.VALIDATION_CONTEXT;
import static fr.ird.observe.application.swing.ObserveSwingApplicationContext.Entries.VALIDATORS;
import static fr.ird.observe.application.swing.ui.UIHelper.newContextEntryDef;
import static fr.ird.observe.application.swing.ui.UIHelper.newListContextEntryDef;

/**
 * Le contexte de l'application.
 *
 * On définit ici toutes les entrées du contexte.
 *
 * Note : cette classe possède une instance partagée accéssible via la méthode
 * {@link #get()}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class ObserveSwingApplicationContext extends DefaultApplicationContext implements Closeable {

    /** Logger */
    private static final Log log = LogFactory.getLog(ObserveSwingApplicationContext.class);
    protected final ScheduledFuture<?> scheduledFuture;

    enum Entries {

        CONFIG("Config", ObserveSwingApplicationConfig.class),
        DECORATOR_SERVICE("Decorator service", DecoratorService.class),
        TEXT_GENERATOR("Text generator", ObserveTextGenerator.class),
        SERVICE_MAIN_FACTORY("Service main factory", ObserveServiceMainFactory.class),
        DATA_SOURCE_CONFIGURATION_FACTORY("Data source configuration main factory", ObserveDataSourceConfigurationMainFactory.class),
        OPEN_DATA_MANAGER("Open data manager", ObserveOpenDataManager.class),
        DATA_CONTEXT("Data context", DataContext.class),
        CONTENT_UI_MANAGER("Content UI manager", ContentUIManager.class),
        DATA_SOURCES_MANAGER("Data sources manager", ObserveSwingApplicationDataSourcesManager.class),
        OBSERVE_SWING_SESSION_HELPER("Swing session Helper", ObserveSwingSessionHelper.class),
        VALIDATION_CONTEXT("Validation context", ValidationContext.class),
        MAIN_UI("Main UI", ObserveMainUI.class),
        ACTIONS("Command line Actions", ObserveCLAction.class),
        ACTION_MAP("UI Actions", ObserveActionMap.class),
        H2_SERVER_MODE("H2 Server mode", Boolean.class),
        H2_SERVER("H2 Server", Server.class),
        H2_WEBSERVER("H2 Web server", Server.class),
        FOCUS_MANAGER("Focus manager", ObserveFocusManager.class),
        VALIDATORS("Validators"),
        LOCAL_DATABASE_BACKUP_TIMER("Local database backup task", ScheduledThreadPoolExecutor.class),
        NODE_TO_RESELECT("Node to reselect");

        private final String objectName;
        private final JAXXContextEntryDef entryDef;

        <O> Entries(String objectName, Class<O> entryType) {
            this.objectName = objectName;
            this.entryDef = newContextEntryDef(objectName, entryType);
        }

        <O> Entries(String objectName) {
            this.objectName = objectName;
            this.entryDef = newListContextEntryDef(objectName);
        }

        public <O> O get() {
            return (O) entryDef.getContextValue(ObserveSwingApplicationContext.get());
        }

        private <O> void set(O instance) {
            entryDef.setContextValue(ObserveSwingApplicationContext.get(), instance);
            if (log.isInfoEnabled()) {
                log.info("Add to application context " + objectName + ": " + (instance instanceof Collection ? ((Collection) instance).size() + " element(s)" : instance));
            }
        }

        private <O> void remove() {
            O instance = get();
            entryDef.removeContextValue(ObserveSwingApplicationContext.get());
            if (log.isInfoEnabled()) {
                log.info("Remove from application context " + objectName + ": " + (instance instanceof Collection ? ((Collection) instance).size() + " element(s)" : instance));
            }
        }
    }

    private static ObserveSwingApplicationContext INSTANCE;

    /** Un objet pour bloquer le context */
    private final Object lock;

    private final ReferenceBinderEngine referenceBinderEngine;

    /** Un drapeau pour savoir quand l'application est en cours de fermeture. */
    private boolean closed;

    /**
     * Récupération du contexte applicatif.
     *
     * @return l'instance partagé du contexte.
     * @throws IllegalStateException si le contexte n'a pas été initialisé
     */
    public static ObserveSwingApplicationContext get() throws IllegalStateException {
        Objects.requireNonNull(INSTANCE, "no application context initialized.");
        return INSTANCE;
    }

    /** @return {@code true} si le context a été initialisé */
    public static boolean isInit() {
        return INSTANCE != null;
    }

    public ObserveSwingApplicationContext(ObserveSwingApplicationConfig config) {

        Preconditions.checkState(INSTANCE == null, "application context already registred.");

        INSTANCE = this;

        CONFIG.set(config);
        ACTIONS.set(new ObserveCLAction());
        CONTENT_UI_MANAGER.set(new ContentUIManager());
        DATA_SOURCES_MANAGER.set(new ObserveSwingApplicationDataSourcesManager());
        DATA_SOURCE_CONFIGURATION_FACTORY.set(new ObserveDataSourceConfigurationMainFactory());
        DECORATOR_SERVICE.set(new DecoratorService(ReferentialLocale.valueOf(config.getDbLocale())));
        TEXT_GENERATOR.set(new ObserveTextGenerator(config));
        DATA_CONTEXT.set(new DataContext());
        OPEN_DATA_MANAGER.set(new ObserveOpenDataManager(getDataContext()));
        VALIDATION_CONTEXT.set(new ValidationContext(config, getDataContext()));
        VALIDATORS.set(Lists.newArrayList(ValidateServiceUtils.getValidators()));
        SERVICE_MAIN_FACTORY.set(ObserveServiceMainFactory.get());
        LOCAL_DATABASE_BACKUP_TIMER.set(new ScheduledThreadPoolExecutor(1));
        OBSERVE_SWING_SESSION_HELPER.set(new ObserveSwingSessionHelper(config.getSwingSessionFile()));
        FOCUS_MANAGER.set(new ObserveFocusManager());
        referenceBinderEngine = new ReferenceBinderEngineSupplier().get();

        scheduledFuture = getLocalDatabaseBackupTimer().scheduleAtFixedRate(new LocalDatabaseBackupTask(), 0, config.getBackupDelay(), TimeUnit.MINUTES);

        lock = new Object();
    }

    //-------------------------------------------
    // - Read
    //-------------------------------------------

    /**
     * @return {@code true} si le context applicatif a été fermé (et est donc
     * passé dans la méthode {@link #close()}, {@code false} autrement.
     */
    public boolean isClosed() {
        return closed;
    }

    public ObserveSwingApplicationConfig getConfig() {
        return CONFIG.get();
    }

    public ContentUIManager getContentUIManager() {
        return CONTENT_UI_MANAGER.get();
    }

    public ObserveSwingApplicationDataSourcesManager getDataSourcesManager() {
        return DATA_SOURCES_MANAGER.get();
    }

    public ReferenceBinderEngine getReferenceBinderEngine() {
        return referenceBinderEngine;
    }

    public DecoratorService getDecoratorService() {
        return DECORATOR_SERVICE.get();
    }

    public ObserveTextGenerator getTextGenerator() {
        return TEXT_GENERATOR.get();
    }

    public ObserveOpenDataManager getOpenDataManager() {
        return OPEN_DATA_MANAGER.get();
    }

    public ObserveDataSourceConfigurationMainFactory getObserveDataSourceConfigurationMainFactory() {
        return DATA_SOURCE_CONFIGURATION_FACTORY.get();
    }

    public ObserveServicesProvider getMainDataSourceServicesProvider() {
        return getDataSourcesManager().getMainDataSource();
    }

    public DataContext getDataContext() {
        return DATA_CONTEXT.get();
    }

    public ValidationContext getValidationContext() {
        return VALIDATION_CONTEXT.get();
    }

    public final ImmutableSet<ValidatorDto> getValidators() {
        return ImmutableSet.copyOf((List) VALIDATORS.get());
    }

    public ObserveMainUI getMainUI() {
        return MAIN_UI.get();
    }

    public List<Object> getNodesToReselect() {
        return NODE_TO_RESELECT.get();
    }

    public ObserveActionMap getActionMap() {
        return ACTION_MAP.get();
    }

    public Server getH2Server() {
        return H2_SERVER.get();
    }

    public Server getH2WebServer() {
        return H2_WEBSERVER.get();
    }

    public ObserveServiceMainFactory getMainServiceFactory() {
        return SERVICE_MAIN_FACTORY.get();
    }

    public ScheduledThreadPoolExecutor getLocalDatabaseBackupTimer() {
        return LOCAL_DATABASE_BACKUP_TIMER.get();
    }

    public ObserveSwingSessionHelper getSwingSessionHelper() {
        return OBSERVE_SWING_SESSION_HELPER.get();
    }

    public ObserveFocusManager getFocusManager() {
        return FOCUS_MANAGER.get();
    }

    //-------------------------------------------
    // - Write
    //-------------------------------------------

    public void setNodesToReselect(Object[] paths) {
        if (paths == null) {
            NODE_TO_RESELECT.remove();

        } else {
            NODE_TO_RESELECT.set(Arrays.asList(paths));
        }
    }

    public void setH2Server(Server server) {
        H2_SERVER.set(server);
    }

    public void setH2WebServer(Server server) {
        H2_WEBSERVER.set(server);
    }

    public void setMainUI(ObserveMainUI ui) {
        MAIN_UI.set(ui);

        ObserveActionMap actionMap = ui.getObserveActionMap();
        ui.getRootPane().setActionMap(actionMap);

        ACTION_MAP.set(actionMap);

    }

    public void removeMainUI() {
        MAIN_UI.remove();
    }

    public void removeH2ServerMode() {
        H2_SERVER_MODE.remove();
    }

    //-------------------------------------------
    // - Locks
    //-------------------------------------------

    public void lock() throws InterruptedException {
        synchronized (lock) {
            lock.wait();
        }
    }

    public void releaseLock() {
        synchronized (lock) {
            lock.notifyAll();
        }
    }

    @Override
    public void close() throws IOException {
        if (log.isInfoEnabled()) {
            log.info("Closing swing application context " + this);
        }

        getSwingSessionHelper().close();

        if (getConfig().isBackupAtClose()) {

            try {
                getLocalDatabaseBackupTimer().submit(new LocalDatabaseBackupTask()).get();
            } catch (InterruptedException | ExecutionException e) {
                if (log.isErrorEnabled()) {
                    log.error(e);
                }
            }

        }

        try {
            getLocalDatabaseBackupTimer().awaitTermination(0, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            if (log.isErrorEnabled()) {
                log.error(e);
            }
        }

        // fermeture de touts les context de donnée ouvert
        getDataSourcesManager().close();

        ObserveServiceMainFactory serviceMainFactory = getMainServiceFactory();
        if (log.isInfoEnabled()) {
            log.info("Closing main service factory: " + serviceMainFactory);
        }
        serviceMainFactory.close();

        // fermeture du context principal
        clear();

        INSTANCE = null;

        closed = true;

    }

    @Override
    protected void finalize() throws Throwable {
        if (!closed) {
            close();
        }
        super.finalize();
    }

//    private void registerMainAction(ActionMap actionMap, AbstractUIAction action) {
//        String actionId = action.getActionId();
//        if (log.isInfoEnabled()) {
//            log.info("Register UI action " + actionId);
//        }
//        actionMap.put(actionId, action);
//    }

}
