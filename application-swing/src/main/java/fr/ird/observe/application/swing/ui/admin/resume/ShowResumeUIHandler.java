/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.admin.resume;

import fr.ird.observe.application.swing.I18nEnumHelper;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.admin.AdminStep;
import fr.ird.observe.application.swing.ui.admin.AdminTabUIHandler;
import fr.ird.observe.application.swing.ui.admin.AdminUIModel;
import org.nuiton.jaxx.runtime.spi.UIHandler;
import org.nuiton.jaxx.runtime.swing.wizard.ext.WizardState;

import static org.nuiton.i18n.I18n.t;

/**
 * Le controleur des onglets.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public class ShowResumeUIHandler extends AdminTabUIHandler<ShowResumeUI> implements UIHandler<ShowResumeUI> {

    @Override
    public void afterInit(ShowResumeUI ui) {
        super.afterInit(ui);
        UIHelper.setLayerUI(ui.getContent(), null);
    }

    public void updateText() {
        AdminUIModel model = ui.getModel();
        StringBuilder buffer = new StringBuilder();
        if (!model.isWasStarted()) {
            buffer.append(t("observe.admin.resume.no.operation.done"));
        } else {
            for (AdminStep s : model.getOperations()) {
                WizardState state = model.getStepState(s);
                String stateStr = "";
                switch (state) {
                    case CANCELED:
                        stateStr = t("observe.admin.resume.operation.canceled");
                        break;
                    case FAILED:
                        stateStr = t("observe.admin.resume.operation.failed");
                        break;
                    case NEED_FIX:
                        stateStr = t("observe.admin.resume.operation.need.fix");
                        break;
                    case PENDING:
                        stateStr = t("observe.admin.resume.operation.not.started");
                        break;
                    case RUNNING:
                        stateStr = t("observe.admin.resume.operation.running");
                        break;
                    case SUCCESSED:
                        stateStr = t("observe.admin.resume.operation.done");
                        break;
                }
                buffer.append("\n\n");
                buffer.append(I18nEnumHelper.getLabel(s));
                buffer.append(" : ");
                buffer.append(stateStr);
                buffer.append(".");
            }
            buffer.append("\n\n");
        }
        getUi().getResume().setText(buffer.toString());
    }

    public void addMessage(AdminStep step, String text) {
        getUi().getGlobalProgression().append(I18nEnumHelper.getLabel(step) + " - " + text + "\n");
    }
}
