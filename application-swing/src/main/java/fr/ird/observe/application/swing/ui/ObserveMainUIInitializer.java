/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui;

import fr.ird.observe.application.swing.ObserveRunner;
import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.configuration.ObserveSwingApplicationConfig;
import fr.ird.observe.application.swing.db.DataContext;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.decoration.DecoratorService;
import fr.ird.observe.application.swing.decoration.ObserveI18nDecoratorHelper;
import fr.ird.observe.application.swing.ui.actions.AbstractUIAction;
import fr.ird.observe.application.swing.ui.content.ContentUI;
import fr.ird.observe.application.swing.ui.content.ContentUIManager;
import fr.ird.observe.application.swing.ui.content.ObserveActionMap;
import fr.ird.observe.application.swing.ui.tree.ObserveNavigationTreeShowPopupAction;
import fr.ird.observe.application.swing.ui.tree.navigation.NavigationTree;
import fr.ird.observe.application.swing.ui.tree.navigation.NavigationTreeModel;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.NavigationTreeNodeSupport;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.ReferenceNavigationTreeNodeSupport;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.StringNavigationTreeNodeSupport;
import fr.ird.observe.application.swing.ui.util.ObserveSwingValidatorMessageTableModel;
import fr.ird.observe.application.swing.ui.util.ObserveValidatorMessageTableRenderer;
import fr.ird.observe.services.dto.constants.ReferentialLocale;
import org.nuiton.jaxx.runtime.context.JAXXInitialContext;
import org.nuiton.jaxx.widgets.error.ErrorDialogUI;
import org.nuiton.jaxx.validator.swing.SwingValidatorMessageTableModel;
import org.nuiton.jaxx.validator.swing.SwingValidatorUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.i18n.I18n;

import javax.swing.AbstractButton;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.tree.TreePath;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import static fr.ird.observe.application.swing.ui.content.ContentUIInitializer.OBSERVE_ACTION;
import static org.nuiton.i18n.I18n.t;

/**
 * Le handler de l'ui principale.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @see ObserveMainUI
 * @since 1.0
 */
public class ObserveMainUIInitializer {

    /** Logger */
    private static final Log log = LogFactory.getLog(ObserveMainUIInitializer.class);

    private static final String EMPTY_SELECTION = "emptySelection";

    private final ObserveMainUI ui;

    public ObserveMainUIInitializer(ObserveMainUI ui) {
        this.ui = ui;
    }

    public void changeNavigationNode(TreeSelectionEvent event) {

        ObserveSwingDataSource source = ObserveSwingApplicationContext.get().getDataSourcesManager().getMainDataSource();
        if (source == null || !source.isOpen()) {

            // no open data source
            if (log.isDebugEnabled()) {
                log.debug("No open Data source.");
            }
            return;
        }
        if (ui.getNavigation().isSelectionEmpty()) {
            if (log.isDebugEnabled()) {
                log.debug("No selection, show empty panel...");
            }

            boolean foundEmptySelection = false;
            for (Component component : ui.getContent().getComponents()) {
                if (component.getName().equals(EMPTY_SELECTION)) {
                    foundEmptySelection = true;
                }
            }
            if (!foundEmptySelection) {
                ui.getContent().add(ui.getEmptySelection(), EMPTY_SELECTION);
            }
            ui.getContentLayout().show(ui.getContent(), EMPTY_SELECTION);
            return;
        }

        TreePath path = event.getPath();
        NavigationTreeNodeSupport node = (NavigationTreeNodeSupport) path.getLastPathComponent();
        ContentUIManager manager = ui.getContentUIManager();

        // obtain the ui type to show
        Class<? extends ContentUI<?, ?>> uiClass = manager.convertNodeToContentUI(node);
        if (log.isDebugEnabled()) {
            log.debug("new selected path = " + node + ", ui = " + uiClass);
        }

        if (uiClass == null) {

            // pas d'ui trouvé, on ne fait donc rien
            return;
        }

        ui.getModel().setBusy(true);

        try {

            doOpencontent(ui, path, uiClass);

        } catch (Exception e) {
            UIHelper.handlingError(e);
        } finally {
            ui.getModel().setBusy(false);
            String params;
            if (node instanceof StringNavigationTreeNodeSupport) {
                params = t(((StringNavigationTreeNodeSupport) node).getData());
            } else {
                params = t(ObserveI18nDecoratorHelper.getTypeI18nKey(node.getDataType()));
            }
            ui.getStatus().setStatus(t("observe.action.open.screen", params));
        }
    }

    public void doOpencontent(ObserveMainUI ui, TreePath path, Class<? extends ContentUI<?, ?>> uiClass) {

        ContentUIManager manager = ui.getContentUIManager();

        // compute the selected ids to put in data context
        Object[] nodes = path.getPath();
        List<String> ids = new ArrayList<>();
        for (Object o : nodes) {
            NavigationTreeNodeSupport n = (NavigationTreeNodeSupport) o;

            if (n instanceof ReferenceNavigationTreeNodeSupport) {
                ids.add(n.getId());
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("new selected ids from tree = " + ids);
        }
        String[] selectedIds = ids.toArray(new String[ids.size()]);

        // update selected ids in data context
        DataContext context = ui.getDataContext();
        context.populateSelectedIds(selectedIds);

        boolean focusOnNavigation = false;
        JComponent focusOwner;
        if (ui.getFocusOwner() == ui) {
            focusOnNavigation = true;
            focusOwner = ui.getNavigation();
        } else {
            focusOwner = (JComponent) ui.getFocusOwner();

            if (focusOwner != null) {
                if (ui.getNavigation().equals(focusOwner)) {
                    focusOnNavigation = true;
                }
                if (ui.getNavigationTreeHeader().equals(focusOwner)) {
                    focusOnNavigation = true;
                }
                if (ui.getNavigationView().equals(focusOwner)) {
                    focusOnNavigation = true;
                }
                if (!focusOnNavigation) {
                    Container focusOwnerParent = focusOwner.getParent();
                    while (focusOwnerParent != null) {
                        if (ui.getNavigation().equals(focusOwnerParent)) {
                            focusOnNavigation = true;
                            break;
                        }
                        if (ui.getNavigationTreeHeader().equals(focusOwnerParent)) {
                            focusOnNavigation = true;
                            break;
                        }
                        if (ui.getNavigationView().equals(focusOwnerParent)) {
                            focusOnNavigation = true;
                            break;
                        }
                        focusOwnerParent = focusOwnerParent.getParent();
                    }
                }
            }
        }
        if (focusOnNavigation) {
            if (log.isInfoEnabled()) {
                log.info("Focus on navigation: " + focusOwner);
            }
        }

        ContentUI<?, ?> previousSelectedContent = manager.getSelectedContentUI();
        if (previousSelectedContent != null) {
            manager.removeSelectedContentUI();
        }

        // on recherche l'ui (voir si elle existe déjà)
        ContentUI<?, ?> content = manager.createContent(uiClass);

        // on ouvre l'écran
        manager.openContent(content);

        ObserveRunner.cleanMemory();

        if (focusOnNavigation) SwingUtilities.invokeLater(focusOwner::requestFocusInWindow);
    }

    /**
     * Methode pour initialiser l'ui principale sans l'afficher.
     *
     * @param context le context applicatif
     * @param config  la configuration a utiliser
     * @return l'ui instancie et initialisee mais non visible encore
     */
    public static ObserveMainUI initUI(ObserveSwingApplicationContext context, ObserveSwingApplicationConfig config) {

        SwingValidatorMessageTableModel errorModel = new ObserveSwingValidatorMessageTableModel();

        DecoratorService decoratorService = context.getDecoratorService();

        boolean reloadDecorators = false;
        Locale currentLocale = I18n.getDefaultLocale();
        Locale configurationLocale = config.getLocale();
        if (!configurationLocale.equals(currentLocale)) {
            if (log.isInfoEnabled()) {
                log.info("re-init I18n with locale " + configurationLocale);
            }
            I18n.setDefaultLocale(configurationLocale);
            reloadDecorators = true;
        }
        if (!config.getDbLocale().equals(decoratorService.getReferentialLocale().getLocale())) {
            if (log.isInfoEnabled()) {
                log.info("re-init db with locale " + config.getDbLocale());
            }
            decoratorService.setReferentialLocale(ReferentialLocale.valueOf(config.getDbLocale()));

            reloadDecorators = true;
        }

        if (reloadDecorators) {
            if (log.isInfoEnabled()) {
                log.info("reload decorators");
            }
            decoratorService.reload();
        }

        JAXXInitialContext tx = new JAXXInitialContext();
        tx.add(context).add(errorModel);

        // show main ui
        ObserveMainUI ui = new ObserveMainUI(tx);
        ui.setUndecorated(config.isFullScreen());

        String title = t("observe.title.welcome.admin") + (" v " + config.getVersion());
        ui.setTitle(title);

        context.setMainUI(ui);

        ErrorDialogUI.init(ui);

        // set fullscreen propery on main ui
        ui.getGraphicsConfiguration().getDevice().setFullScreenWindow(config.isFullScreen() ? ui : null);

        new ObserveNavigationTreeShowPopupAction(ui.getNavigation(), ui.getNavigationScrollPane(), ui.getNavigationPopup());

        return ui;
    }

    /**
     * Permet de recharger l'ui principale et de changer de le mode
     * d'affichage.
     *
     * @param rootContext le contexte applicatif
     * @param config      la configuration a utiliser
     */
    public void reloadUI(ObserveSwingApplicationContext rootContext, ObserveSwingApplicationConfig config) {

        // must remove all properties listener on config
        config.removeJaxxPropertyChangeListener();

        // scan main ui
        ObserveMainUI ui = rootContext.getMainUI();

        ObserveSwingDataSource mainStorage = rootContext.getDataSourcesManager().getMainDataSource();

        ObserveUIMode oldMode = null;
        Object[] ids = null;
        if (ui != null) {

            oldMode = ui.getModel().getMode();

            ids = ui.getNavigation().getSelectedIds();

            ErrorDialogUI.init(null);

            rootContext.removeMainUI();

            ui.dispose();

            ui.setVisible(false);

            System.runFinalization();
        }

        focusBorder = null;
        noFocusBorder = null;
        ui = initUI(rootContext, config);

        if (oldMode == null) {
            if (mainStorage == null) {
                oldMode = ObserveUIMode.NO_DB;

            } else {
                oldMode = ObserveUIMode.DB;
            }
        }

        ui.getModel().setMode(oldMode);

        if (oldMode == ObserveUIMode.DB) {

            // on met à jour le modèle de navigation
            NavigationTreeModel treeModel = ui.getNavigation().getTreeModel();
            treeModel.setLoadSeine(ui.getNavigationTreeHeader().getShowSeine().isSelected());
            treeModel.setLoadLongline(ui.getNavigationTreeHeader().getShowLongline().isSelected());
            treeModel.setLoadReferential(ui.getNavigationTreeHeader().getShowReferential().isSelected());
            treeModel.setLoadEmptyProgram(ui.getNavigationTreeHeader().getShowEmptyProgram().isSelected());

            treeModel.populate();

            // on conserve les noeuds a reselectionner
            rootContext.setNodesToReselect(ids);

            // selection du noeud d'ouverture (le noeud precedemment selectionne,
            // ou le noeud le plus ouvert
            // ou le premier program si aucune donnée ouverte)
            ui.getNavigation().selectInitialNode();
        }

        // show ui
        UIHelper.setMainUIVisible(ui);
    }

    protected void initialize() {

        ObserveActionMap actionMap = ui.getObserveActionMap();

        for (Object o : ui.get$objectMap().values()) {

            if (o instanceof AbstractButton) {
                init(actionMap, (AbstractButton) o);
            }

        }

        ui.getModel().reload(ui.getConfig());

        ObserveKeyStrokes.addKeyStrokeFromMnemonic(ui);

        // bad binding, force value
        ui.getStopH2WebServer().setEnabled(false);

        ui.getStatus().init();

        // ajout d'un ecouteur sur la navigation pour toujours mettre la scrollbar
        // tout à droite a chaque selection
        ui.getNavigation().addTreeSelectionListener(this::changeNavigationNode);

        SwingValidatorUtil.installUI(ui.getErrorTable(), new ObserveValidatorMessageTableRenderer());

        // installation layer de blocage en mode busy
        UIHelper.setLayerUI(ui.getBody(), ui.getBusyBlockLayerUI());

        // ecoute des changements de l'état busy
        ui.getModel().addPropertyChangeListener("busy", evt -> {
            boolean newvalue = (boolean) evt.getNewValue();
            updateBusyState(newvalue);
        });

        ui.getModel().addPropertyChangeListener("focusOnNavigation", evt -> {
            boolean newvalue = (boolean) evt.getNewValue();
            updateFocusOnNavigation(newvalue);
        });

        MouseAdapter mouseAdapter = new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                if (log.isDebugEnabled()) {
                    log.debug("Enter in navigation zone: " + e);
                }
                ui.getModel().setFocusOnNavigation(true);
            }
        };
        ui.getNavigationView().addMouseListener(mouseAdapter);
        ui.getNavigation().addMouseListener(mouseAdapter);
        ui.getNavigationTreeHeader().addMouseListener(mouseAdapter);
    }

    protected void init(ObserveActionMap actionMap, AbstractButton editor) {
        String actionId = (String) editor.getClientProperty(OBSERVE_ACTION);
        if (actionId == null) {

            return;
        }

        // on a trouve une action commune
        AbstractUIAction action = (AbstractUIAction) actionMap.get(actionId);
        Objects.requireNonNull(action, "action [" + actionId + "] not found for ui " + ui.getClass().getName());

        if (log.isDebugEnabled()) {
            log.debug("init common action " + actionId);
        }

        action.initForMainUi(editor);

    }

    private Border focusBorder;
    private Border noFocusBorder;

    public Border getFocusBorder() {
        if (focusBorder == null) {
            focusBorder = new LineBorder(ObserveSwingApplicationContext.get().getConfig().getFocusBorderColor(), 3, true);
        }
        return focusBorder;
    }

    public Border getNoFocusBorder() {
        if (noFocusBorder == null) {
            noFocusBorder = new LineBorder(ObserveSwingApplicationContext.get().getConfig().getNoFocusBorderColor(), 3, true);
        }
        return noFocusBorder;
    }

    private void updateFocusOnNavigation(boolean newvalue) {
        if (newvalue) {
            if (log.isDebugEnabled()) {
                log.debug("Focus on navigation");
            }
            ui.getNavigationView().setBorder(getFocusBorder());
            ui.getSplitpane2().setBorder(getNoFocusBorder());
        } else {
            if (log.isDebugEnabled()) {
                log.debug("Focus on content");
            }
            ui.getNavigationView().setBorder(getNoFocusBorder());
            ui.getSplitpane2().setBorder(getFocusBorder());
        }
    }

    protected void updateBusyState(boolean busy) {
        if (busy) {
            // ui bloquee
            if (log.isDebugEnabled()) {
                log.debug("block ui in busy mode");
            }
            ui.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        } else {
            // ui debloquee
            if (log.isDebugEnabled()) {
                log.debug("unblock ui in none busy mode");
            }
            ui.setCursor(Cursor.getDefaultCursor());
        }
    }

    /**
     * Nettoye des ui tout ce qui concerne un modèle de navigation.
     *
     * <b>Note:</b> cette méthode doit être appelée avant tout rechargement de modèle de naivgation.
     */
    public void cleanNavigationUI() {

        // reset content uis
        ui.getContentLayout().reset(ui.getContent());

        // clean messages
        ui.getContextValue(SwingValidatorMessageTableModel.class).clear();

        // clean tree model
        NavigationTree tree = ui.getNavigation();

        tree.clearModel();

        // no tree navigation view
        tree.setVisible(false);

    }

    /**
     * Charge dans l'ui un nouveau modèle de navigation.
     *
     * <b>Note:</b> cette méthode doit être appelée après tout rechargement de modèle de naivgation.
     **/
    public void loadNavigationUI() {

        NavigationTree tree = ui.getNavigation();

        NavigationTreeModel treeModel = tree.getTreeModel();
        treeModel.populate();

        // select initial node
        tree.selectInitialNode();

        tree.setVisible(true);

        ui.getModel().setFocusOnNavigation(true);

        SwingUtilities.invokeLater(tree::requestFocusInWindow);
    }
}
