/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.content.list.impl.seine;

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.DataContext;
import fr.ird.observe.application.swing.db.constants.DataContextType;
import fr.ird.observe.application.swing.ui.content.ContentMode;
import fr.ird.observe.application.swing.ui.content.list.ContentListUIHandler;
import fr.ird.observe.application.swing.ui.tree.navigation.NavigationTree;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.NavigationTreeNodeSupport;
import fr.ird.observe.services.dto.reference.DataReference;
import fr.ird.observe.services.dto.seine.RouteDto;
import fr.ird.observe.services.dto.seine.TripSeineDto;
import org.nuiton.jaxx.runtime.spi.UIHandler;

import static org.nuiton.i18n.I18n.n;

/**
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0
 */
public class RoutesUIHandler extends ContentListUIHandler<TripSeineDto, RouteDto, RoutesUI> implements UIHandler<RoutesUI> {

    public RoutesUIHandler() {
        super(DataContextType.TripSeine, DataContextType.Route);
    }

    @Override
    protected boolean computeCanGotoOpen() {
        // il faut que la route ouverte soit dans cette marée
        boolean result = ObserveSwingApplicationContext.get().getDataContext().isOpenRoute();
        if (result) {

            String id = ObserveSwingApplicationContext.get().getDataContext().getOpenRouteId();
            result = false;
            for (DataReference<RouteDto> reference : ui.getModel().getData()) {
                if (id.equals(reference.getId())) {
                    result = true;
                    break;
                }
            }
        }
        return result;
    }

    protected boolean computeCanReopen() {
        return ui.getModel().isOneSelectedData() && !ObserveSwingApplicationContext.get().getDataContext().isOpenRoute();
    }

    @Override
    protected boolean computeCanClose() {
        // il faut que la route ouverte soit dans cette marée et qu'il n'y a pas d'activité ouverte
        return computeCanGotoOpen() && !ObserveSwingApplicationContext.get().getDataContext().isOpenActivitySeine();
    }

    @Override
    public NavigationTreeNodeSupport<?> getOpenNode() {
        DataContext dataContext = ObserveSwingApplicationContext.get().getDataContext();
        NavigationTree treeHelper = getTreeHelper(ui);
        return treeHelper.getRouteNode(dataContext.getOpenProgramId(), dataContext.getOpenTripSeineId(), dataContext.getOpenRouteId());
    }

    @Override
    protected ContentMode getContentMode(DataContext dataContext) {

        // autorisé à écrire

        String openTripId = dataContext.getOpenTripId();

        if (openTripId == null) {

            // pas de marée d'ouverte, donc on ne peut pas ouvrir une route
            addInfoMessage(n("observe.content.tripSeine.message.no.active.found"));
            return ContentMode.READ;
        }

        //
        // il existe une marée ouverte
        //

        boolean openRoute = dataContext.isOpenRoute();

        if (dataContext.isSelectedOpen(TripSeineDto.class)) {

            // la marée courante est ouverte

            if (openRoute) {

                // il existe une route ouverte dans la marée courante
                addInfoMessage(n("observe.content.route.message.active.found"));
                return ContentMode.UPDATE;
            }

            // pas de route ouverte, on peut en ouvrir une
            addInfoMessage(n("observe.content.route.message.no.active.found"));
            return ContentMode.CREATE;
        }

        //
        // la marée ouverte est dans un autre program
        //

        if (openRoute) {

            //il existe  une route existe dans la maree ouverte
            addInfoMessage(n("observe.content.route.message.active.found.for.other.trip"));
        } else {

            // pas de route ouverte dans la maree ouverte
            addInfoMessage(n("observe.content.route.message.no.active.found.for.other.trip"));
        }

        return ContentMode.READ;
    }

}
