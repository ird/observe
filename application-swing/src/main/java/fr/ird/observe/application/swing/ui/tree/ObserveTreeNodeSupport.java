package fr.ird.observe.application.swing.ui.tree;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.swing.Icon;
import javax.swing.UIManager;
import javax.swing.tree.DefaultMutableTreeNode;
import java.awt.Color;

/**
 * Created on 18/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public abstract class ObserveTreeNodeSupport<O> extends DefaultMutableTreeNode {

    public abstract boolean isOpen();

    public abstract String getId();

    public abstract String getText();

    public abstract String getIconPath();

    public abstract Color getColor();

    public O getData() {
        return (O) getUserObject();
    }

    public ObserveTreeNodeSupport(O userObject, boolean allowsChildren) {
        super(userObject, allowsChildren);
    }

    public Icon getIcon(String suffix) {
        String iconPath = getIconPath();
        return iconPath == null ? null : UIManager.getIcon(iconPath + suffix);
    }

}
