/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.content.table;

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.db.DataContext;
import fr.ird.observe.application.swing.db.ObserveSwingDataSource;
import fr.ird.observe.application.swing.db.constants.DataContextType;
import fr.ird.observe.application.swing.ui.ObserveKeyStrokes;
import fr.ird.observe.application.swing.ui.UIHelper;
import fr.ird.observe.application.swing.ui.content.ContentMode;
import fr.ird.observe.application.swing.ui.content.ContentUIHandler;
import fr.ird.observe.application.swing.ui.content.ContentUIModel;
import fr.ird.observe.application.swing.ui.content.table.impl.longline.WeightMeasuresTableModel;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.NavigationTreeNodeSupport;
import fr.ird.observe.application.swing.ui.util.table.EditableTableModelSupport;
import fr.ird.observe.application.swing.ui.util.table.InlineTableAutotSelectRowAndShowPopupAction;
import fr.ird.observe.services.dto.IdDto;
import fr.ird.observe.services.dto.longline.ActivityLonglineDto;
import fr.ird.observe.services.dto.reference.ReferentialReference;
import fr.ird.observe.services.dto.referential.SpeciesDto;
import fr.ird.observe.services.dto.referential.seine.WeightCategoryDto;
import fr.ird.observe.services.dto.seine.ActivitySeineDto;
import fr.ird.observe.services.dto.seine.TargetCatchDto;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.validator.NuitonValidatorScope;

import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TableModelEvent;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.Rectangle;
import java.beans.PropertyChangeListener;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Le controleur d'un écran d'édition avec tableau.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public abstract class ContentTableUIHandler<E extends IdDto, D extends IdDto, U extends ContentTableUI<E, D, U>> extends ContentUIHandler<E, U> {

    /** Logger */
    private static final Log log = LogFactory.getLog(ContentTableUIHandler.class);

    protected ContentTableUIHandler(DataContextType dataContextType) {
        super(dataContextType, null);
    }

    /**
     * Hook lors du changement d'entrée éditée.
     *
     * @param editingRow la nouvelle entrée à editer
     * @param bean       le bean d'édition d'une entrée
     * @param create     un drapeau pour savoir si l'entrée est en création
     */
    protected abstract void onSelectedRowChanged(int editingRow, D bean, boolean create);

    @Override
    protected boolean computeCanWrite(ObserveSwingDataSource source) {
        return source.canWriteData();
    }

    @Override
    protected ContentMode getContentMode(DataContext dataContext) {

        // par defaut, on suppose qu'on peut afficher les données
        getModel().setShowData(true);

        String activityId = dataContext.getSelectedActivityId();

        if (getOpenDataManager().isOpenActivity(activityId)) {

            // mode mise a jour
            return ContentMode.UPDATE;
        }

        // mode lecture

        if (dataContext.isSelectedActivityLongline()) {

            // l'activité n'est pas ouverte, mode lecture
            addMessage(getUi(),
                       NuitonValidatorScope.INFO,
                       getTypeI18nKey(ActivityLonglineDto.class),
                       t("observe.storage.activityLongline.message.not.open"));

        } else {

            addMessage(getUi(),
                       NuitonValidatorScope.INFO,
                       getTypeI18nKey(ActivitySeineDto.class),
                       t("observe.storage.activitySeine.message.not.open"));

        }

        return ContentMode.READ;
    }

    @Override
    public ContentTableUIModel<E, D> getModel() {
        return (ContentTableUIModel<E, D>) super.getModel();
    }

    public final D getTableEditBean() {
        return getModel().getTableEditBean();
    }

    public final void updateEditor(ListSelectionEvent event) {

        U ui = getUi();

        if (ui.getValidatorTable() == null) {
            log.debug("skip validator is null!");
            return;
        }
        if (getBean() == null) {
            log.debug("skip bean is null!");
            return;
        }
        if (log.isDebugEnabled()) {
            log.debug(event);
        }
        if (getTableModel().isEmpty()) {
            // le modele est vide, on ne propage rien
            //FIXME le modèle de selection ne devrait plus declancher des evenement
            //FIXME des que le modele est vide ?
            log.debug("empty model, do nothing");
            return;
        }

        ListSelectionModel sModel = (ListSelectionModel) event.getSource();
        int newIndex = sModel.getAnchorSelectionIndex();

        if (newIndex == -1 || sModel.isSelectionEmpty()) {
            //FIXME le modèle de selection ne devrait plus declancher des evenement
            //FIXME des que le modele est vide ?
            log.debug("skip with row == -1, or empty selection, do nothing");
            return;
        }

        SwingUtilities.invokeLater(() -> {
            if (!ui.getSelectionModel().isSelectionEmpty()) {
                // on veut toujours que la ligne sélectionnée soit visible
                Rectangle rect = ui.getTable().getCellRect(ui.getSelectionModel().getAnchorSelectionIndex(), 0, false);
                ui.getTable().scrollRectToVisible(rect);
            }
        });

        int selectedRow = getTableModel().getSelectedRow();
        if (newIndex == selectedRow) {
            // on bloque du code re-entrant
            log.debug("new index already set in model " + newIndex + ", do nothing");
            return;
        }
        // on doit changer de ligne selectionne dans le modele
        getTableModel().changeSelectedRow(newIndex);
    }

    protected abstract void initTableUI(DefaultTableCellRenderer renderer);

    protected abstract String getEditBeanIdToLoad();

    protected E loadEditBean(ContentMode mode) {

        String id = getEditBeanIdToLoad();

        if (id == null) {
            throw new IllegalStateException("Could not find id form " + this);
        }

        E editBean = getBean();

        // preparation du bean d'édition
        loadEditBean(id);

        // initialisation du modèle du tableau
        getUi().getTableModel().attachModel();

        return editBean;
    }

    protected abstract void loadEditBean(String beanId);

    @Override
    public void afterInit(U ui) {

        ContentTableUIInitializer<E, D, U> uiInitializer = new ContentTableUIInitializer<>(ui);
        uiInitializer.initUI();

        ui.setTitle(t(updateTitle(ui.getContentTitle())));
        updateTitle(getModel().getMode());

        getModel().addPropertyChangeListener(ContentUIModel.PROPERTY_FORM, evt -> updateUiWithReferenceSetsFromModel());

        installFocusTraversalPolicy();
    }

    @Override
    public void openUI() {

        super.openUI();

        // récupération du mode de l'écran
        ContentMode mode = computeContentMode();

        U ui = getUi();

        int oldIndex = ui.getTableModel().getSelectedRow();

        ui.getTableModel().dettachModel();

        getModel().setMode(mode);

        // chargement du bean d'édition
        loadEditBean(mode);

        boolean canEdit = mode == ContentMode.UPDATE;

        if (canEdit) {
            // on lance le mode edition
            ui.startEdit(null);
        }

        if (!ui.getTableModel().isEmpty()) {
            if (ui.getTableModel().getRowCount() <= oldIndex) {
                oldIndex = 0;
            }
            if (oldIndex == -1) {
                oldIndex = 0;
            }

            // le tableau contient au moins une entrée
            // on la sélectionne la première entrée
            ui.getTableModel().changeSelectedRow(oldIndex);
        }

        if (canEdit) {

            // on force l'ecran en non modification
            //FIXME normalement, on ne devrait pas a avoir a faire ca ?
            //FIXME mais il est possible que les validateurs modifient l'état
            //FIXME modified sur l'ecran lors des bindings d'initilisation...
            getModel().setModified(false);
        }

        updateSaveAction(false);
    }

    @Override
    public void startEditUI(String... binding) {

        addUpdateInfoMessage();
        super.startEditUI(binding);
    }

    protected void addUpdateInfoMessage() {
        String message = n("observe.entity.message.updating");
        NavigationTreeNodeSupport node = getTreeHelper(getUi()).getSelectedNode();
        String entityLabel = getTypeI18nKey(node.getParent().getDataType());
        message = t(message, t(entityLabel));
        addMessage(getUi(), NuitonValidatorScope.INFO, entityLabel, message);
    }

    public void removeSelectedRow(int selectedRow) {
        try {
            getTableModel().doRemoveRow(selectedRow, false);
        } finally {

            // always reset busy model to false
            ObserveSwingApplicationContext.get().getMainUI().getModel().setBusy(false);
        }
    }

    @Override
    protected boolean doSave(E bean) {

        U ui = getUi();

        List<D> objets = ui.getTableModel().getData();

        boolean canContinue;
        try {
            canContinue = prepareSave(bean, objets);
        } catch (Exception e) {
            UIHelper.handlingError(e);
            canContinue = false;
        }
        if (!canContinue) {

            // l'utilisateur a choisi de ne pas sauvegarder
            return false;
        }
        doPersist(bean);

        return true;
    }

    protected abstract void doPersist(E bean);

    @Override
    protected void afterSave(boolean refresh) {

        // on recharge l'écran
        resetEditUI();
    }

    protected boolean prepareSave(E editBean, List<D> objets) throws Exception {

        // par defaut, rien de specifique a faire avant de faire la sauvegarde
        return true;
    }

    protected ContentTableModel<E, D> getTableModel() {
        return getUi().getTableModel();
    }

    protected void resetEditBean() {
        getTableModel().resetEditBean();
    }

    /**
     * @param weightCategory la catégorie de poids
     * @return la référence sur l'espèce de la catégorie de poids
     */
    protected Optional<ReferentialReference<SpeciesDto>> getWeightCategorySpecies(ReferentialReference<WeightCategoryDto> weightCategory) {

        String speciesId = (String) weightCategory.getPropertyValue(WeightCategoryDto.PROPERTY_SPECIES);

        return getModel().tryGetReferentialReferenceById(TargetCatchDto.PROPERTY_SPECIES, speciesId);
    }

    protected <EE extends Serializable, MM extends EditableTableModelSupport<EE>> void initInlineTable(JScrollPane pane,
                                                                                                       JTable table,
                                                                                                       MM tableModel,
                                                                                                       PropertyChangeListener modifiedListener,
                                                                                                       JPopupMenu popup,
                                                                                                       JMenuItem addMeasure,
                                                                                                       JMenuItem deleteMeasure) {

        tableModel.addPropertyChangeListener(WeightMeasuresTableModel.MODIFIED_PROPERTY, modifiedListener);

        ListSelectionModel selectionModel = table.getSelectionModel();
        selectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tableModel.installSelectionListener(table);
        tableModel.installTableKeyListener(table);

        tableModel.addTableModelListener(e -> {
            if (e.getType() == TableModelEvent.DELETE) {
                tableModel.validate();
            }
        });

        new InlineTableAutotSelectRowAndShowPopupAction<>(
                pane,
                table,
                tableModel,
                popup,
                addMeasure,
                deleteMeasure);

    }

    public void updateSaveAction(boolean create) {
        JButton button = ui.getSaveEntry();
        if (create) {
            button.putClientProperty("text", t(ui.getSaveNewEntryText()));
            button.putClientProperty("toolTipText", t(ui.getSaveNewEntryTip()));
            button.setIcon(UIHelper.getUIManagerActionIcon("add"));
        } else {
            button.putClientProperty("text", t(ui.getSaveEntryText()));
            button.putClientProperty("toolTipText", t(ui.getSaveEntryTip()));
            button.setIcon(UIHelper.getUIManagerActionIcon("save"));
        }
        ObserveKeyStrokes.addKeyStroke2(button, ObserveKeyStrokes.KEY_STROKE_SAVE_TABLE_ENTRY);
    }
}
