package fr.ird.observe.application.swing.ui.tree.menu;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ird.observe.application.swing.ObserveSwingApplicationContext;
import fr.ird.observe.application.swing.decoration.decorators.ReferentialReferenceDecorator;
import fr.ird.observe.application.swing.ui.tree.actions.ChangeTripProgramActionListener;
import fr.ird.observe.application.swing.ui.tree.actions.NodeChangeActionListener;
import fr.ird.observe.application.swing.ui.tree.navigation.NavigationTree;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.NavigationTreeNodeSupport;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.ReferenceNavigationTreeNodeSupport;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.RootNavigationTreeNode;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.seine.ProgramSeineNavigationTreeNode;
import fr.ird.observe.application.swing.ui.util.DecoratedNodeEntity;
import fr.ird.observe.services.dto.reference.ReferentialReference;
import fr.ird.observe.services.dto.referential.ProgramDto;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 5.0
 */
public class MoveTripSeineNodeMenuPopulator extends MoveNodeMenuPopulator {

    @Override
    public NodeChangeActionListener createChangeActionListener(NavigationTree tree,
                                                               String id,
                                                               String parentId) {
        return new ChangeTripProgramActionListener(tree, id, parentId);
    }

    @Override
    public List<DecoratedNodeEntity> getPossibleParentNodes(NavigationTreeNodeSupport tripNode) {

        // noeud du programme parent
        NavigationTreeNodeSupport parentNode = tripNode.getParent();

        // programmes du même type que le noeud de marée, sans le parent actuel
        List<DecoratedNodeEntity> possibleParents = new ArrayList<>();

        // racine
        RootNavigationTreeNode rootNode = tripNode.getRoot();

        createPossibleParents(parentNode.getId(), possibleParents, rootNode);

        return possibleParents;
    }

    public static void createPossibleParents(String oldProgramId, List<DecoratedNodeEntity> possibleParents, RootNavigationTreeNode rootNode) {

        ReferentialReferenceDecorator<ProgramDto> programDecorator = ObserveSwingApplicationContext.get().getDecoratorService().getReferentialReferenceDecorator(ProgramDto.class);

        for (ReferenceNavigationTreeNodeSupport<ProgramDto, ReferentialReference<ProgramDto>> programNode : rootNode) {

            String programId = programNode.getId();

            // si le noeud programme n'est pas le même que le parent actuel
            if (!oldProgramId.equals(programId)) {

                if (programNode instanceof ProgramSeineNavigationTreeNode) {
                    possibleParents.add(DecoratedNodeEntity.newDecoratedNodeEntity(programNode, programDecorator));
                }

            }
        }
    }

}
