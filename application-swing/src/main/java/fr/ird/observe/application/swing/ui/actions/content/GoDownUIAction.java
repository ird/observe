/*
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ird.observe.application.swing.ui.actions.content;

import fr.ird.observe.application.swing.ui.ObserveKeyStrokes;
import fr.ird.observe.application.swing.ui.ObserveMainUI;
import fr.ird.observe.application.swing.ui.content.ContentUI;
import fr.ird.observe.application.swing.ui.tree.navigation.NavigationTree;
import fr.ird.observe.application.swing.ui.tree.navigation.nodes.NavigationTreeNodeSupport;
import org.nuiton.jaxx.runtime.JAXXObject;

import javax.swing.AbstractButton;
import javax.swing.JComponent;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import java.util.Enumeration;

import static org.nuiton.i18n.I18n.n;

/**
 * Action pour sélectionner un noeud (attaché à l'éditeur) dans l'arbre de
 * navigation.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.4
 */
public class GoDownUIAction extends AbstractContentUIAction {

    private static final long serialVersionUID = 1L;

    public static final String ACTION_NAME = "goDown";

    public GoDownUIAction(ObserveMainUI mainUI) {
        super(mainUI,
              ACTION_NAME,
              n("observe.action.goDown"),
              n("observe.action.goDown.tip"),
              "go-down",
              ObserveKeyStrokes.KEY_STROKE_GO_DOWN);
    }

    @Override
    protected void actionPerformed(ContentUI<?, ?> contentUI) {
        SwingUtilities.invokeLater(() -> {
            JComponent c = contentUI.getActionDown();
            JPopupMenu p = getMainUI().getScopeDownPopup();
            p.show(c, 2, c.getHeight());
        });
    }

    @Override
    public void updateAction(JAXXObject ui, AbstractButton editor) {

        ObserveMainUI mainUI = getMainUI();
        NavigationTree tree = getMainUI().getNavigation();
        JPopupMenu scopePopup = mainUI.getScopeDownPopup();
        SelectNodeUIAction action = (SelectNodeUIAction) mainUI.getObserveActionMap().get(SelectNodeUIAction.ACTION_NAME);

        NavigationTreeNodeSupport<?> node = tree.getSelectedNode();
        NavigationTreeNodeSupport nodeOriginal = node;
        scopePopup.removeAll();
        Enumeration<?> e = node.children();
        while (e.hasMoreElements()) {
            node = (NavigationTreeNodeSupport) e.nextElement();
            String text = node.getText();
            if (text == null) {
                continue;
            }
            scopePopup.add(node.toMenuItem(action, ui));
        }

        if (nodeOriginal.getChildCount() == 1 && !nodeOriginal.getChildAt(0).isLeaf()) {
            e = nodeOriginal.getChildAt(0).children();
            while (e.hasMoreElements()) {
                node = (NavigationTreeNodeSupport) e.nextElement();
                scopePopup.add(node.toMenuItem(action, ui));
            }
        }

        int nbNodes = scopePopup.getComponentCount();

        editor.setEnabled(nbNodes > 0);
    }

}
