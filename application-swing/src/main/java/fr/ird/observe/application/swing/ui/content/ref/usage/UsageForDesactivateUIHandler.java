package fr.ird.observe.application.swing.ui.content.ref.usage;

/*-
 * #%L
 * ObServe :: Application Swing
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.jaxx.widgets.select.BeanComboBox;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * Created on 16/12/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.1
 */
public class UsageForDesactivateUIHandler extends UsageUIHandlerSupport<UsageForDesactivateUI> {

    @Override
    protected JLabel getMessage() {
        return ui.getMessage();
    }

    @Override
    protected JPanel getUsages() {
        return ui.getUsages();
    }

    @Override
    protected BeanComboBox<?> getReplace() {
        return ui.getReplace();
    }

    @Override
    public void afterInit(UsageForDesactivateUI ui) {
        super.afterInit(ui);
        getReplace().addPropertyChangeListener(BeanComboBox.PROPERTY_SELECTED_ITEM, evt -> updateCanApply());
        ui.getShouldReplace().addItemListener(evt -> updateCanApply());
    }

    @Override
    public void attachToOptionPane(JOptionPane pane, String message) {
        super.attachToOptionPane(pane, message);
        updateCanApply();
    }

    private void updateCanApply() {
        boolean canApply = !ui.getShouldReplace().isSelected() || getReplace().getSelectedItem() != null;
        ui.setCanApply(canApply);
    }
}
