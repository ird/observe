###
# #%L
# ObServe :: Admin Client
# 
# $Id$
# $HeadURL$
# %%
# Copyright (C) 2008 - 2010 IRD, Codelutin, Tony Chemit
# %%
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the 
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public 
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/gpl-3.0.html>.
# #L%
###
#!/bin/sh

#
# Pour créer la base obstuna de l'ird
#
# Note : Ce script doit etre lance par l'utilisateur postgres sur la machine
# hebergeant la base.
#
# Usage :
# ./create-ird_obstuna.sh

./create-empty-obstuna.sh obstuna admin "adamiano lfloch pcauquil utilisateur referentiel"
