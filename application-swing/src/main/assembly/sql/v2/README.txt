Ce répertoire contient les scripts pour mettre à jour un base obstuna (postgres)

Pour créer une nouvelle base obstuna :

./create-ird_obstuna.sh

ou

./create-empty-obstuna.sh

Ce script va créer les rôles (si nécessaire) et la base vide.

Créer les scripts de sauvegarde d'une base obstuna :

./backup-obstuna.sh host filename_prefix

Ce script est utilisé pour créer les scripts embarquées dans l'application
lors de toute nouvelle version de base.

ajout du support postgis :

./create-postgis.sh

