###
# #%L
# ObServe :: Admin Client
# 
# $Id$
# $HeadURL$
# %%
# Copyright (C) 2008 - 2010 IRD, Codelutin, Tony Chemit
# %%
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the 
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public 
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/gpl-3.0.html>.
# #L%
###
#!/bin/sh

# creation du script sql de la securité sur la base obstuna :

if [ ! $# -eq 4 ] ; then
    echo "le nombre de paramètres n'est pas correct."
    echo ""
    echo "usage $0 outputfile owner \"technicien1 technicien2 ...\" \"reader1 reader2 ...\""
    exit 1
fi

# parameter 1 : output file
OUTPUT=$1

# parameter 2 : owner
OBSTUNA_OWNER=$2

# parameter 3 : techniciens
OBSTUNA_TECHNICIENS="$3"

# parameter 4 : readers
OBSTUNA_READERS="$4"

TABLES=`cat tables.txt`

echo "creation du script '$OUTPUT'"

echo "-- default security " > $OUTPUT
./create-default-security-script.sh "$TABLES" >> $OUTPUT

echo "" >> $OUTPUT
echo "-- security for owner '$OBSTUNA_OWNER'" >> $OUTPUT
echo "" >> $OUTPUT
./create-admin-security-script.sh "$TABLES" $OBSTUNA_OWNER >> $OUTPUT

for role in $OBSTUNA_TECHNICIENS ;
do
    echo "" >> $OUTPUT
    echo "-- security for technicien '$role'" >> $OUTPUT
    echo "" >> $OUTPUT
    ./create-technicien-security-script.sh "$TABLES" $role >> $OUTPUT
done

for role in $OBSTUNA_READERS ;
do
    echo "" >> $OUTPUT
    echo "-- security for reader '$role'" >> $OUTPUT
    echo "" >> $OUTPUT
    ./create-reader-security-script.sh "$TABLES" $role >> $OUTPUT
done


