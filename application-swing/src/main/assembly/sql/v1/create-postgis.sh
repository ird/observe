###
# #%L
# ObServe :: Admin Client
# 
# $Id$
# $HeadURL$
# %%
# Copyright (C) 2008 - 2010 IRD, Codelutin, Tony Chemit
# %%
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the 
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public 
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/gpl-3.0.html>.
# #L%
###
(
cat << EOF 
SELECT AddGeometryColumn('activite', 'the_geom', 4326, 'GEOMETRY',2 );

CREATE OR REPLACE function syncgg () returns trigger as '
DECLARE
        latitude  numeric(15,2);
        longitude numeric(15,2);
BEGIN

  -- TODO : differencier si on est en creation ou update 
  --  IF (TG_OP = ''UPDATE'') THEN ...

  IF (NEW.latitude IS NULL OR NEW.longitude IS NULL OR NEW.quadrant < 0 ) THEN
    -- on ne calcule pas le point postgis si au moins une des
    -- coordonnees n est pas renseignee

    RAISE NOTICE ''No latitude or longitude, can not compute postgis field for activite % '', NEW.topiaId;
    return NEW;
  END IF;

  -- calcul de la latitude et longitude signes selon le quadrant
  latitude  = NEW.latitude;
  longitude = NEW.longitude;
 
  IF ( NEW.quadrant = 2 ) THEN
    latitude  = - latitude;
  
  ELSEIF ( NEW.quadrant = 3 ) THEN
    latitude  = - latitude;  
    longitude = - longitude;
  ELSEIF (NEW.quadrant = 4 ) THEN
    longitude = - longitude;
  END IF;

  -- affectation du point 
  NEW.the_geom := GeomFromText(''POINT(''||longitude||'' ''||latitude||'')'', 4326); 

  RAISE NOTICE ''Will compute the_geom for activite % with quadrant % - latitude % and longitude %'', NEW.topiaId, NEW.quadrant, NEW.latitude, NEW.longitude;
  RAISE NOTICE ''Computed for activite % latitude % and longitude %, the_geom %'', NEW.topiaId, latitude, longitude, NEW.the_geom;
    
  RETURN NEW;
END
'
LANGUAGE 'plpgsql';

DROP TRIGGER gg on activite; 
CREATE TRIGGER gg BEFORE insert or update ON activite FOR EACH ROW EXECUTE PROCEDURE syncgg();

-- procedure pour mettre a jour les colonnes postgis sur les activites
-- existante dans la base
CREATE OR REPLACE FUNCTION update_activite_the_geom() RETURNS void AS $$

DECLARE
  line record;
  latitude  numeric(15,2);
  longitude numeric(15,2);

BEGIN

-- ------------------------------------------------------
  FOR line IN select * from activite order by topiaid LOOP

    -- calcul de la latitude et longitude signes selon le quadrant
    latitude  = line.latitude;
    longitude = line.longitude;

    IF ( line.quadrant = 2 ) THEN
      latitude  = - latitude;

    ELSEIF ( line.quadrant = 3 ) THEN
      latitude  = - latitude;
      longitude = - longitude;
    ELSEIF (line.quadrant = 4 ) THEN
      longitude = - longitude;
    END IF;

    -- affectation du point
    EXECUTE 'UPDATE activite SET the_geom=GeomFromText(''POINT(''||longitude||'' ''||latitude||'')'', 4326) WHERE topiaid=''' || line.topiaid || '''';

  END LOOP;



  RETURN;

END;
EOF
) | psql -d obstuna
