Ce répertoire contient les scripts pour créer un base obstuna (postgres)

Le répertoire sql contient le script sql de création de la base et le référentiel.

Le répertoire security contient les scripts sh pour construire le script sql de
la sécurité à appliquer à la base (voir security/create-security-script.sh).

Le script create-script.sh est le script principal à utiliser, il prend 4 paramètres :

- le nom de la base
- le nom du propriétaire de la base
- le nom des techniciens
- le nom des lecteurs

exemple :

./createdb-script.sh obstuna admin "tech1 tech2 ..." "reader1 reader2 ..."


