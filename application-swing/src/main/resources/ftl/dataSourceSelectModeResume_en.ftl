<#--
 #%L
 ObServe :: Application Swing
 %%
 Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<html>
<body>

  <h3>Selected data source type</h3>

  <#if dbMode.name() == "USE_LOCAL">
    Use a H2 local database
  <#elseif dbMode.name() == "CREATE_LOCAL">
    Create a H2 local database
  <#elseif dbMode.name() == "USE_REMOTE">
    Use a PostgreSQL remote database
  <#elseif dbMode.name() == "USE_SERVER">
    Use a remote web service
  <#else>
    No datasource type has been selected
  </#if>

  <h3>Selected creation mode</h3>

  <#if dbMode.name() == "CREATE_LOCAL">
    <#if !creationMode??>
      No creation mode has been selected
    <#elseif creationMode.name() == "EMPTY">
      Generate a new empty local database. This database won't contain reference data and you will have to import them into it after that.
    <#elseif creationMode.name() == "IMPORT_INTERNAL_DUMP">
      Generate a new local database from the last version of the embedded database.
    <#elseif creationMode.name() == "IMPORT_EXTERNAL_DUMP">
      Create a new local database from a previous backup.
    <#elseif creationMode.name() == "IMPORT_LOCAL_STORAGE">
      Generate a new local database and import the reference data from another local database.
    <#elseif creationMode.name() == "IMPORT_REMOTE_STORAGE">
      Generate a new local database and import the reference data from a remote database.
    <#elseif creationMode.name() == "IMPORT_SERVER_STORAGE">
      Generate a new local database and import the reference data from a remote web service.
    </#if>
  <#else>
    Not required.
  </#if>
</body>
</html>
