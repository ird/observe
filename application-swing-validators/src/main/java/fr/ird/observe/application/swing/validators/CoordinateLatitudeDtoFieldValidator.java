package fr.ird.observe.application.swing.validators;

/*-
 * #%L
 * ObServe :: Application Swing Validators
 * %%
 * Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.FieldValidatorSupport;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.jaxx.widgets.gis.CoordinateFormat;
import org.nuiton.jaxx.widgets.gis.absolute.AbsoluteDdCoordinateEditor;
import org.nuiton.jaxx.widgets.gis.absolute.AbsoluteDdCoordinateEditorModel;
import org.nuiton.jaxx.widgets.gis.absolute.AbsoluteDmdCoordinateEditor;
import org.nuiton.jaxx.widgets.gis.absolute.AbsoluteDmdCoordinateEditorModel;
import org.nuiton.jaxx.widgets.gis.absolute.AbsoluteDmsCoordinateEditor;
import org.nuiton.jaxx.widgets.gis.absolute.AbsoluteDmsCoordinateEditorModel;
import org.nuiton.jaxx.widgets.gis.absolute.CoordinatesEditor;

import java.util.Map;
import java.util.Objects;

import static org.nuiton.i18n.I18n.n;

/**
 * Created on 03/11/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0
 */
public class CoordinateLatitudeDtoFieldValidator extends FieldValidatorSupport {

    private String editorName;

    public void setEditorName(String editorName) {
        this.editorName = editorName;
    }

    @Override
    public void validate(Object object) throws ValidationException {

        if (StringUtils.isEmpty(editorName)) {
            throw new ValidationException("editorName is not defined");
        }

        Map<String, CoordinatesEditor> map = (Map<String, CoordinatesEditor>) getFieldValue("coordinatesEditors", object);
        CoordinatesEditor coordinatesEditor = map.get(editorName);
        //FIXME une validation est lancé dans l'ui alors que l'on ne devrait pas et on a pas ce composant alors
        if (coordinatesEditor == null) {
            return;
        }
        Objects.requireNonNull(coordinatesEditor, "can't find editor named " + editorName);

        CoordinateFormat format = coordinatesEditor.getModel().getFormat();
        switch (format) {

            case dd:
                validate(coordinatesEditor.getLatitudeDd());
                break;
            case dms:
                validate(coordinatesEditor.getLatitudeDms());
                break;
            case dmd:
                validate(coordinatesEditor.getLatitudeDmd());
                break;
        }

    }

    @Override
    public String getValidatorType() {
        return "coordinate_latitude";
    }

    private void validate(AbsoluteDdCoordinateEditor editor) {
        AbsoluteDdCoordinateEditorModel model = editor.getModel();
        Integer degree = model.getDegree();
        if (degree != null) {
            if (degree > 90) {
                setMessageKey(n("validator.ui.coordinate.degree.latitude.outOfBound"));
                addFieldError(getFieldName(), degree);
            }
        }
    }

    private void validate(AbsoluteDmsCoordinateEditor editor) {
        AbsoluteDmsCoordinateEditorModel model = editor.getModel();
        Integer degree = model.getDegree();
        if (degree != null) {
            if (degree > 90) {
                setMessageKey(n("validator.ui.coordinate.degree.latitude.outOfBound"));
                addFieldError(getFieldName(), degree);
            }
        }
        Integer minute = model.getMinute();
        if (minute != null) {

            if (minute > 59) {
                setMessageKey(n("validator.ui.coordinate.minute.latitude.outOfBound"));
                addFieldError(getFieldName(), minute);
            }
        }
        Integer second = model.getSecond();
        if (second != null) {
            if (second > 59) {
                setMessageKey(n("validator.ui.coordinate.second.latitude.outOfBound"));
                addFieldError(getFieldName(), second);
            }
        }
    }

    private void validate(AbsoluteDmdCoordinateEditor editor) {
        AbsoluteDmdCoordinateEditorModel model = editor.getModel();
        Integer degree = model.getDegree();
        if (degree != null) {
            if (degree > 90) {
                setMessageKey(n("validator.ui.coordinate.degree.latitude.outOfBound"));
                addFieldError(getFieldName(), degree);
            }
        }
        Integer minute = model.getMinute();
        if (minute != null) {

            if (minute > 59) {
                setMessageKey(n("validator.ui.coordinate.minute.latitude.outOfBound"));
                addFieldError(getFieldName(), minute);
            }
        }
    }
}
