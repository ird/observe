<?xml version="1.0" encoding="UTF-8"?>
<!--
  #%L
  ObServe :: Application Swing Validation
  %%
  Copyright (C) 2008 - 2017 IRD, Code Lutin, Tony Chemit
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

<!DOCTYPE validators PUBLIC
  "-//Apache Struts//XWork Validator 1.0.3//EN"
  "http://struts.apache.org/dtds/xwork-validator-1.0.3.dtd">
<validators>

  <field name="tripType">

    <!-- pas de tripType selectionne -->
    <field-validator type="required" short-circuit="true">
      <message>validator.ui.trip.required.tripType</message>
    </field-validator>

    <!-- tripType desactive  -->
    <field-validator type="fieldexpression" short-circuit="true">
      <param name="expression">
        <![CDATA[ tripType.enabled ]]>
      </param>
      <message>validator.ui.trip.desactivated.tripType</message>
    </field-validator>

  </field>
  
  <field name="observer">

    <!-- pas de observer selectionne -->
    <field-validator type="required" short-circuit="true">
      <message>validator.ui.trip.required.observer</message>
    </field-validator>

    <!-- observer desactive  -->
    <field-validator type="fieldexpression" short-circuit="true">
      <param name="expression">
        <![CDATA[ observer.enabled ]]>
      </param>
      <message>validator.ui.trip.desactivated.observer</message>
    </field-validator>

  </field>

  <field name="captain">

    <!-- captain desactive  -->
    <field-validator type="fieldexpression" short-circuit="true">
      <param name="expression">
        <![CDATA[ captain == null || captain.enabled ]]>
      </param>
      <message>validator.ui.trip.desactivated.captain</message>
    </field-validator>

  </field>

  <field name="dataEntryOperator">

    <!-- dataEntryOperator desactive  -->
    <field-validator type="fieldexpression" short-circuit="true">
      <param name="expression">
        <![CDATA[ dataEntryOperator == null || dataEntryOperator.enabled ]]>
      </param>
      <message>validator.ui.trip.desactivated.dataEntryOperator</message>
    </field-validator>

  </field>

  <field name="vessel">

    <!-- pas de vessel sélectionné -->
    <field-validator type="required" short-circuit="true">
      <message>validator.ui.trip.required.vessel</message>
    </field-validator>

    <!-- vessel desactive  -->
    <field-validator type="fieldexpression" short-circuit="true">
      <param name="expression">
        <![CDATA[ vessel.enabled ]]>
      </param>
      <message>validator.ui.trip.desactivated.vessel</message>
    </field-validator>

  </field>

  <field name="ocean">

    <!-- pas d'ocean selectionne -->
    <field-validator type="required" short-circuit="true">
      <message>validator.ui.trip.required.ocean</message>
    </field-validator>

    <!-- ocean desactive  -->
    <field-validator type="fieldexpression" short-circuit="true">
      <param name="expression">
        <![CDATA[ ocean.enabled ]]>
      </param>
      <message>validator.ui.trip.desactivated.ocean</message>
    </field-validator>

  </field>

  <field name="departureHarbour">

    <!-- pas de departureHarbour selectionne -->
    <field-validator type="required" short-circuit="true">
      <message>validator.ui.trip.required.departureHarbour</message>
    </field-validator>

    <!-- departureHarbour desactive  -->
    <field-validator type="fieldexpression" short-circuit="true">
      <param name="expression">
        <![CDATA[ departureHarbour.enabled ]]>
      </param>
      <message>validator.ui.trip.desactivated.departureHarbour</message>
    </field-validator>

  </field>

  <field name="landingHarbour">

    <!-- landingHarbour desactive  -->
    <field-validator type="fieldexpression" short-circuit="true">
      <param name="expression">
        <![CDATA[ landingHarbour == null || landingHarbour.enabled ]]>
      </param>
      <message>validator.ui.trip.desactivated.landingHarbour</message>
    </field-validator>

  </field>

  <field name="startDate">

    <!-- pas de date de debut selectionne -->
    <field-validator type="required" short-circuit="true">
      <message>validator.ui.trip.required.startDate</message>
    </field-validator>

  </field>

  <field name="endDate">

    <!-- pas de date de fin selectionnee -->
    <field-validator type="required" short-circuit="true">
      <message>validator.ui.trip.required.endDate</message>
    </field-validator>

    <!-- date de fin avant date de debut -->
    <field-validator type="fieldexpression" short-circuit="true">
      <param name="expression">
        <![CDATA[ endDate == null || endDate.time >= startDate.time ]]>
      </param>
      <message>validator.ui.trip.endDate.after.startDate</message>
    </field-validator>

  </field>

  <field name="comment">

    <!-- comentaire de moins de 1024 caractères -->
    <field-validator type="stringlength">
      <param name="maxLength">1024</param>
      <message>validator.ui.trip.comment.tobig</message>
    </field-validator>

    <!-- comment requis selon le type de tripType choisi -->
    <field-validator type="fieldexpression">
      <param name="expression">
        <![CDATA[ tripType == null || !tripType.needComment || (comment != null && !comment.empty)  ]]>
      </param>
      <message>validator.ui.trip.required.comment.for.tripType</message>
    </field-validator>

    <!-- comment requis selon le type de departureHarbour choisi -->
    <field-validator type="fieldexpression">
      <param name="expression">
        <![CDATA[ departureHarbour == null || !departureHarbour.needComment || (comment != null && !comment.empty)  ]]>
      </param>
      <message>validator.ui.trip.required.comment.for.departureHarbour</message>
    </field-validator>

    <!-- comment requis selon le type de landingHarbour choisi -->
    <field-validator type="fieldexpression">
      <param name="expression">
        <![CDATA[ landingHarbour == null || !landingHarbour.needComment || (comment != null && !comment.empty)  ]]>
      </param>
      <message>validator.ui.trip.required.comment.for.landingHarbour</message>
    </field-validator>

    <!-- comment requis selon le type de vessel choisi -->
    <field-validator type="fieldexpression">
      <param name="expression">
        <![CDATA[ vessel == null || !vessel.needComment || (comment != null && !comment.empty)  ]]>
      </param>
      <message>validator.ui.trip.required.comment.for.vessel</message>
    </field-validator>

    <!-- comment requis selon le type de ocean choisi -->
    <field-validator type="fieldexpression">
      <param name="expression">
        <![CDATA[ ocean == null || !ocean.needComment || (comment != null && !comment.empty)  ]]>
      </param>
      <message>validator.ui.trip.required.comment.for.ocean</message>
    </field-validator>

    <!-- comment requis selon le type de dataEntryOperator choisi -->
    <field-validator type="fieldexpression">
      <param name="expression">
        <![CDATA[ dataEntryOperator == null || !dataEntryOperator.needComment || (comment != null && !comment.empty)  ]]>
      </param>
      <message>validator.ui.trip.required.comment.for.dataEntryOperator</message>
    </field-validator>

    <!-- comment requis selon le type de observer choisi -->
    <field-validator type="fieldexpression">
      <param name="expression">
        <![CDATA[ observer == null || !observer.needComment || (comment != null && !comment.empty)  ]]>
      </param>
      <message>validator.ui.trip.required.comment.for.observer</message>
    </field-validator>

    <!-- comment requis selon le type de captain choisi -->
    <field-validator type="fieldexpression">
      <param name="expression">
        <![CDATA[ captain == null || !captain.needComment || (comment != null && !comment.empty)  ]]>
      </param>
      <message>validator.ui.trip.required.comment.for.captain</message>
    </field-validator>

  </field>
</validators>
